/**
 * 
 */
(function($){

		  $.fn.simplyCountable = function(options){
		    
		    options = $.extend({
		      counter:            '#counter',
		      countType:          'characters',
		      maxCount:           140,
		      strictMax:          false,
		      countDirection:     'down',
		      safeClass:          'safe',
		      overClass:          'over',
		      thousandSeparator:  ',',
		      onOverCount:        function(){},
		      onSafeCount:        function(){},
		      onMaxCount:         function(){}
		    }, options);

		    var navKeys = [33,34,35,36,37,38,39,40];

		    return $(this).each(function(){

		      var countable = $(this);
		      var counter = $(options.counter);
		      if (!counter.length) { return false; }
		      
		      var countCheck = function(){
		             
		        var count;
		        var revCount;
		        
		        var reverseCount = function(ct){
		          return ct - (ct*2) + options.maxCount;
		        }
		        
		        var countInt = function(){
		          return (options.countDirection === 'up') ? revCount : count;
		        }
		        
		        var numberFormat = function(ct){
		          var prefix = '';
		          if (options.thousandSeparator){
		            ct = ct.toString();          
		            // Handle large negative numbers
		            if (ct.match(/^-/)) { 
		              ct = ct.substr(1);
		              prefix = '-';
		            }
		            for (var i = ct.length-3; i > 0; i -= 3){
		              ct = ct.substr(0,i) + options.thousandSeparator + ct.substr(i);
		            }
		          }
		          return prefix + ct;
		        }

		        var changeCountableValue = function(val){
		          countable.val(val).trigger('change');
		        }
		        
		        /* Calculates count for either words or characters */
		        if (options.countType === 'words'){
		          count = options.maxCount - $.trim(countable.val()).split(/\s+/).length;
		          if (countable.val() === ''){ count += 1; }
		        }
		        else { count = options.maxCount - countable.val().length; }
		        revCount = reverseCount(count);
		        
		        /* If strictMax set restrict further characters */
		        if (options.strictMax && count <= 0){
		          var content = countable.val();
		          if (count < 0) {
		            options.onMaxCount(countInt(), countable, counter);
		          }
		          if (options.countType === 'words'){
		            var allowedText = content.match( new RegExp('\\s?(\\S+\\s+){'+ options.maxCount +'}') );
		            if (allowedText) {
		              changeCountableValue(allowedText[0]);
		            }
		          }
		          else { changeCountableValue(content.substring(0, options.maxCount)); }
		          count = 0, revCount = options.maxCount;
		        }
		        
		        counter.text(numberFormat(countInt()));
		        
		        /* Set CSS class rules and API callbacks */
		        if (!counter.hasClass(options.safeClass) && !counter.hasClass(options.overClass)){
		          if (count < 0){ counter.addClass(options.overClass); }
		          else { counter.addClass(options.safeClass); }
		        }
		        else if (count < 0 && counter.hasClass(options.safeClass)){
		          counter.removeClass(options.safeClass).addClass(options.overClass);
		          options.onOverCount(countInt(), countable, counter);
		        }
		        else if (count >= 0 && counter.hasClass(options.overClass)){
		          counter.removeClass(options.overClass).addClass(options.safeClass);
		          options.onSafeCount(countInt(), countable, counter);
		        }
		        
		      };
		      
		      countCheck();

		      countable.on('keyup blur paste', function(e) {
		        switch(e.type) {
		          case 'keyup':
		            // Skip navigational key presses
		            if ($.inArray(e.which, navKeys) < 0) { countCheck(); }
		            break;
		          case 'paste':
		            // Wait a few miliseconds if a paste event
		            setTimeout(countCheck, (e.type === 'paste' ? 5 : 0));
		            break;
		          default:
		            countCheck();
		            break;
		        }
		      });

		    });
		    
		  };

		})(jQuery);
	
	function setCounter(obj, idCounter, length) {

		$(obj).simplyCountable({
			counter: idCounter,
			countType: 'characters',
			maxCount: parseInt($(obj).attr("maxlength")),
			strictMax: false,
			countDirection: 'down',
			safeClass: 'safe',
			overClass: 'over',
			thousandSeparator: ',',
			onOverCount: function (count, countable, counter) { },
			onSafeCount: function (count, countable, counter) { },
			onMaxCount: function (count, countable, counter) { }
		});
	}
	
	function checkInputFile(idInput) {
		if($(idInput + " input").val().length != 0){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			console.log($(idInput + " label").text($(idInput + " input").val()));
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(idInput + " label").text("Selecciona archivo");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					" Revisa que estes subiendo el archivo de cupones correctamente" +
					"</div>");
			return false;
		}
	}
	
	function checkTextarea(idText) {
		if($(idText + " textarea").val().length > 5 && $(idText + " textarea").val().length < 200)
		{
			$(idText).removeClass("has-danger");
			$(idText).addClass("has-success");
			$(idText + " textarea").removeClass("is-invalid");
			$(idText + " textarea").addClass("is-valid");
			return true;
		}else{
			$(idText).removeClass("has-success");
			$(idText).addClass("has-danger");
			$(idText + " textarea").addClass("is-invalid");
			$(idText + " textarea").removeClass("is-valid");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					" Debes especificar una condicion para los cupones" +
					"</div>");
			return false;
		}
	}
	
	function checkSelectOne(idItem){
		if($(idItem + " select").val() != 0)
		{
			$(idItem).removeClass("has-danger");
			$(idItem).addClass("has-success");
			$(idItem + " select").removeClass("is-invalid");
			$(idItem + " select").addClass("is-valid");
			return true;
		}else{
			$(idItem).removeClass("has-success");
			$(idItem).addClass("has-danger");
			$(idItem + " select").addClass("is-invalid");
			$(idItem + " select").removeClass("is-valid");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					" Debes indicar la campaña para los cupones" +
					"</div>");
			return false;
		}
	}
	
	function checkInput(idInput, pattern, errMsg) {
		
		if($(idInput + " input").val().match(pattern)){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(msgPanel).html(errMsg);
			return false;
		}
	}
	
	
	$(function(){
		
		var namePattern = "^[a-z A-Z]{4,30}$";
		var emailPattern = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$";
		var datePattern = "^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.][0-9]{4})$";
		var urlPattern = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/";
		var contrasenaPattern = "(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";
		var telefonoPattern = "^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$";
		var tcreditoPattern = "^((67\d{2})|(4\d{3})|(5[1-5]\d{2})|(6011))(-?\s?\d{4}){3}|(3[4,7])\ d{2}-?\s?\d{6}-?\s?\d{5}$ ";
		var codigoPostalPattern ="^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$";
		var percentPattern = "^[0-9]+([,][0-9]+)?$";
		
		
		var percentERR = "<div class='alert alert-dismissible alert-danger'>" +
		"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
		" Debes ingresar un porcentaje valido" +
		"</div>";
		
		setCounter($("#cargarCupones\\:condiciones"), "#contadorCondiciones", "200");
	
		
		
		
		

		
	 
		
		$(".alert .alert-dismissible .alert-success button").click(function(){
			$(".alert .alert-dismissible .alert-success").toggle();
		})
	
		
	})
	
	
	
	
	