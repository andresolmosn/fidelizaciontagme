function on() {
    document.getElementById("overlay").style.display = "block";
}

function off() {
	topFunction();
    document.getElementById("overlay").style.display = "none";
}

function topFunction() {
 document.body.scrollTop = 0;
 document.documentElement.scrollTop = 0;
}
