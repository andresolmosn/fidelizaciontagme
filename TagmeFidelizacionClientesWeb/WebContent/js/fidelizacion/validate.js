/**
 * 
 */




function checkInputFile(idInput, msgPanel, errMsg) {
		if($(idInput + " input").val().length != 0){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			console.log($(idInput + " label").text($(idInput + " input").val()));
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(idInput + " label").text("Selecciona archivo");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					" Revisa que estes subiendo el archivo de cupones correctamente" +
					"</div>");
			return false;
		}
	}
	
	function checkTextarea(idText) {
		if($(idText + " textarea").val().length > 5 && $(idText + " textarea").val().length < 200)
		{
			$(idText).removeClass("has-danger");
			$(idText).addClass("has-success");
			$(idText + " textarea").removeClass("is-invalid");
			$(idText + " textarea").addClass("is-valid");
			return true;
		}else{
			$(idText).removeClass("has-success");
			$(idText).addClass("has-danger");
			$(idText + " textarea").addClass("is-invalid");
			$(idText + " textarea").removeClass("is-valid");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					" Debes especificar una condicion para los cupones" +
					"</div>");
			return false;
		}
	}
	
	function checkSelectOne(idItem, msgPanel, err){
		if($(idItem + " select").val() != 0)
		{
			$(idItem).removeClass("has-danger");
			$(idItem).addClass("has-success");
			$(idItem + " select").removeClass("is-invalid");
			$(idItem + " select").addClass("is-valid");
			return true;
		}else{
			$(idItem).removeClass("has-success");
			$(idItem).addClass("has-danger");
			$(idItem + " select").addClass("is-invalid");
			$(idItem + " select").removeClass("is-valid");
			$(msgPanel).html(
					"<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! hay un problema!</strong>"+
					+ err +
					"</div>");
			return false;
		}
	}
	
	function checkInput(idInput, pattern, msgPanel, errMsg) {
		
		if($(idInput + " input").val().match(pattern)){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(msgPanel).html("<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! encontramos un problema! </strong>"+
					errMsg +
					"</div>");
			return false;
		}
	}
	
	function checkURL(idInput, msgPanel, errMsg) {
		regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		if(regexp.test($(idInput + " input").val())){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(msgPanel).html("<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button> <strong>Oh! encontramos un problema! </strong>"+
					errMsg +
					"</div>");
			return false;
		}
	}
	
	function checkNotNull(idInput, msgPanel, errMsg) {
		
		if($(idInput + " input").val() != null && $(idInput + " input").val() != ""){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(msgPanel).html("<div class='alert alert-dismissible alert-danger'>" +
					"  <button type='button' class='close' data-dismiss='alert'>&times;</button><h4 class='alert-heading'>Validacion</h4> <spam>"+
					errMsg +
					"</spam></div>");
			return false;
		}
	}


