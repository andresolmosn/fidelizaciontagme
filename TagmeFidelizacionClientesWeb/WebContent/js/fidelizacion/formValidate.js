/**
 * 
 */


function checkInputFile(idInput) {
		if($(idInput + " input").val().length != 0){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			$(idInput + " label").text("Selecciona archivo");
			
			return false;
		}
	}
	
	function checkTextarea(idText) {
		if($(idText + " textarea").val().length > 5 && $(idText + " textarea").val().length < 500)
		{
			$(idText).removeClass("has-danger");
			$(idText).addClass("has-success");
			$(idText + " textarea").removeClass("is-invalid");
			$(idText + " textarea").addClass("is-valid");
			return true;
		}else{
			$(idText).removeClass("has-success");
			$(idText).addClass("has-danger");
			$(idText + " textarea").addClass("is-invalid");
			$(idText + " textarea").removeClass("is-valid");
			return false;
		}
	}
	
	function checkSelectOne(idItem){
		if($(idItem + " select").val() != 0)
		{
			$(idItem).removeClass("has-danger");
			$(idItem).addClass("has-success");
			$(idItem + " select").removeClass("is-invalid");
			$(idItem + " select").addClass("is-valid");
			return true;
		}else{
			$(idItem).removeClass("has-success");
			$(idItem).addClass("has-danger");
			$(idItem + " select").addClass("is-invalid");
			$(idItem + " select").removeClass("is-valid");
			
			
			return false;
		}
	}
	
	function checkInput(idInput, pattern) {
		
		if($(idInput + " input").val().match(pattern)){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			return false;
		}
	}
	
	function checkInputRut(idInput) {
		
		if(validarRut($(idInput + " input").val()))
		{
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			return false;
		}
	}
	
	function validarRut(rutCliente){
		if(rutCliente.length < 9 || rutCliente.length > 10)
		{
			return false;
		}else{

			rutCompleto = rutCliente;
		      if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test( rutCompleto )){
		      	$("#rut").removeClass("has-success");
				$("#rut").addClass("has-error");
				$("#dv").removeClass("has-success");
				$("#dv").addClass("has-error");
		    	  return false;
		      }
		        
		        $("#rut").addClass("has-success");
				$("#rut").removeClass("has-error");
				$("#dv").addClass("has-success");
				$("#dv").removeClass("has-error");
		        var tmp     = rutCompleto.split('-');
		        var digv    = tmp[1]; 
		        var rut     = tmp[0];
		        if ( digv == 'K' ) digv = 'k' ;
		        
		        return (dv(rut) == digv);
		}
		
	}
	
	function dv(T){
        var M=0,S=1;
        for(;T;T=Math.floor(T/10))
            S=(S+T%10*(9-M++%6))%11;
        return S?S-1:'k';
    }
	
	
	function checkURL(idInput) {
		regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
		if(regexp.test($(idInput + " input").val())){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			
			return false;
		}
	}
	
	function checkNotNull(idInput) {
		
		if($(idInput + " input").val() != null && $(idInput + " input").val() != ""){
			$(idInput).removeClass("has-danger");
			$(idInput).addClass("has-success");
			$(idInput + " input").removeClass("is-invalid");
			$(idInput + " input").addClass("is-valid");
			return true;
		}else{
			$(idInput).removeClass("has-success");
			$(idInput).addClass("has-danger");
			$(idInput + " input").addClass("is-invalid");
			$(idInput + " input").removeClass("is-valid");
			return false;
		}
	}
	
	function notValid(idInput){
		$(idInput).removeClass("has-success");
		$(idInput).addClass("has-danger");
		$(idInput + " input").addClass("is-invalid");
		$(idInput + " input").removeClass("is-valid");
	}
	
	function valid(idInput){
		$(idInput).removeClass("has-danger");
		$(idInput).addClass("has-success");
		$(idInput + " input").removeClass("is-invalid");
		$(idInput + " input").addClass("is-valid");
	}


