package com.puma.fidelizacion.bean;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.to.TipoCampaniaTO;
import com.puma.fidelizacion.to.UsuarioTO;
import com.puma.fidelizacion.util.GeneradorHash;
import com.puma.fidelizacion.util.GeneradorHash.Algoritmo;


@ManagedBean
@ViewScoped
public class FidelizarClienteBean extends BaseBean{
	
	private static final Long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(FidelizarClienteBean.class);
	
	
	private List<CampaniaTO> campanias;
	private List<TiendaTO> tiendas;
	private List<UsuarioTO> vendedores;
	private int campaniaSeleccionadaId;
	private int tiendaSeleccionadaId;
	private int vendedorSeleccionadoId;
	private UsuarioTO vendedorSeleccionado;
	private TiendaTO tiendaSeleccionada;
	private ClienteTO nuevoCliente;
	private CampaniaTO campaniaSeleccionada;
	

	public int getCampaniaSeleccionadaId() {
		return campaniaSeleccionadaId;
	}

	public void setCampaniaSeleccionadaId(int campaniaSeleccionadaId) {
		this.campaniaSeleccionadaId = campaniaSeleccionadaId;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	
	public int getTiendaSeleccionadaId() {
		return tiendaSeleccionadaId;
	}

	public void setTiendaSeleccionadaId(int tiendaSeleccionadaId) {
		this.tiendaSeleccionadaId = tiendaSeleccionadaId;
	}
	

	public List<TiendaTO> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<TiendaTO> tiendas) {
		this.tiendas = tiendas;
	}

	public ClienteTO getNuevoCliente() {
		return nuevoCliente;
	}

	public void setNuevoCliente(ClienteTO nuevoCliente) {
		this.nuevoCliente = nuevoCliente;
	}

	public List<UsuarioTO> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<UsuarioTO> vendedores) {
		this.vendedores = vendedores;
	}

	public int getVendedorSeleccionadoId() {
		return vendedorSeleccionadoId;
	}

	public void setVendedorSeleccionadoId(int vendedorSeleccionadoId) {
		this.vendedorSeleccionadoId = vendedorSeleccionadoId;
	}

	public String init()
	{
		deleteAllMensajes();
		Controller c = Controller.getInstance();
		isSecure();
		int empresaId = this.usuario.getIdEmpresa();
		this.nuevoCliente = new ClienteTO();
		List<TipoCampaniaTO> tipoCampanias = c.findAllTipoCampaniasByEmpresa(empresaId);
		String[] parametroTipoCampania = null;
		if(tipoCampanias != null && tipoCampanias.size() > 0)
		{
			parametroTipoCampania = new String[tipoCampanias.size()];
			for(int i=0; i < tipoCampanias.size(); i++) {
				parametroTipoCampania[i] = tipoCampanias.get(i).getNombre();
				logger.info("Tipos de campania agregados ==> ["+ parametroTipoCampania[i] +"]");
			}
			
			for(int i=0; i < parametroTipoCampania.length; i++) {
				addList(c.findCampaniaActiva(parametroTipoCampania[i], empresaId));
			}
			tiendas = c.getTiendaUsuarioActivaByEmpresa(empresaId, this.usuario.getId());
			vendedores = null;
			if(this.campanias == null || this.campanias.size() <= 0 )
			{
				addMensaje(new MensajeTO("Sin campa�as", "Tu empresa no tiene ninguna campa�a disponible. Comunicate con el administrador de campa�as de tu empresa para que cree o habilite una campa�a", MensajeTO.INFO));
			}else {
				logger.info("Cantidad de campa�as para la empresa " + empresaId + " : " + campanias.size());
			}
		}else {
			addMensaje(new MensajeTO("Sin tipos de campania", "Extra�amente tu empresa no tiene tipos de campa�a, contacta al soporte de TagmeFidelizacion", MensajeTO.INFO));
		}
		return "";
	}
	
	private void addList(List<CampaniaTO> listaHijo )
	{
		if(listaHijo !=null && listaHijo.size() > 0)
		{
			if(campanias == null)
			{
				campanias = new LinkedList<CampaniaTO>();
			}
			Iterator<CampaniaTO> i = listaHijo.iterator();
			while(i.hasNext())
			{
				campanias.add(i.next());
			}
		}else {
			deleteAllMensajes();
			addMensaje(new MensajeTO("Informaci�n Carga p�gina", "Al buscar las campa�as activas de la empresa, no se encontr� campa�a disponible para uno(s) de los tipos de campa�a indicados en el parametro 'fidelizarCliente.tiposdeCampa�a'.<br />Favor de revisar si tienes campa�as activas para todos los tipos de campa�a", MensajeTO.INFO));
		}
	}
	
	public void selectCampania()
	{
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		campaniaSeleccionada = c.findCampaniasById(this.campaniaSeleccionadaId, this.usuario.getIdEmpresa());
		addMensaje(new MensajeTO("Informaci�n Carga p�gina", "Id de la campania seleccionada: " + getCampaniaSeleccionadaId(), MensajeTO.INFO));
		setTiendaSeleccionadaId(this.usuario.getTiendas().get(0).getId());
		vendedores = c.findAllVendedoresByTienda(tiendaSeleccionadaId, this.usuario.getId());
		setVendedorSeleccionadoId(this.usuario.getId());
	}
	
	public void selectTienda()
	{
		Controller c = Controller.getInstance();
		this.tiendaSeleccionada = c.getTiendaById(this.tiendaSeleccionadaId);
		deleteAllMensajes();
		addMensaje(new MensajeTO("Informaci�n Carga p�gina", "Id de la tienda seleccionada: " + getTiendaSeleccionadaId(), MensajeTO.INFO));
		vendedores = c.findAllVendedoresByTienda(tiendaSeleccionadaId, this.usuario.getId());
		setVendedorSeleccionadoId(this.usuario.getId());
	}
	
	public void selectVendedor()
	{
		Controller c = Controller.getInstance();
		this.vendedorSeleccionado = c.getUsuarioById(this.vendedorSeleccionadoId);
		deleteAllMensajes();
		addMensaje(new MensajeTO("Informaci�n Carga p�gina", "Id del vendedor seleccionada: " + getVendedorSeleccionadoId(), MensajeTO.INFO));
	}
	
	public void fidelizarCliente()
	{
		this.nuevoCliente.setTienda(tiendaSeleccionada);
		this.nuevoCliente.setTienda_id(this.tiendaSeleccionadaId);
		this.nuevoCliente.setVendedor(this.vendedorSeleccionado);
		this.nuevoCliente.setEmpresa_id(this.usuario.getIdEmpresa());
		this.nuevoCliente.setTipoCampania(this.campaniaSeleccionada.getTipoCampania());
		GeneradorHash gh = new GeneradorHash();
		String texto=nuevoCliente.getMail().trim()+(new Date()).getTime()+ ((Math.random() * 9) + 1);
		String hash = gh.generateHash(texto, Algoritmo.SHA256);
		this.nuevoCliente.setHash(hash);
		Controller c = Controller.getInstance();
		c.nuevoCliente(nuevoCliente);
		logger.info("Nuevo Cliente : " + nuevoCliente);
		
	}
}
