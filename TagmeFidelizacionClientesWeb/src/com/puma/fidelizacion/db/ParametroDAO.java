package com.puma.fidelizacion.db;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.Parametro;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.util.Lista;

public class ParametroDAO extends BaseDAO{
	
	public List<Parametro> getAllParaemtroByEmpresa(int empresaId)
	{
		try {
			Root<Parametro> root = cq.from(Parametro.class);
			List<Predicate> criteria = new LinkedList<Predicate>();

			if (empresaId > 0) {
				criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));
			}
			List<Object> lista = this.getList(root, Parametro.class, criteria, null, null);
			return Lista.filter(Parametro.class, lista);
		} catch(Exception e) {
			return null;
		}
	}
	
	public List<Parametro> getParaemtroByTypeAndEmpresa(int empresaId, String parametro)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Parametro> criteria = cb.createQuery(Parametro.class);
			Root<Parametro> root = cq.from(Parametro.class);

			Expression<String> tipo = cb.parameter(String.class, "nombre");
			Expression<String> nombreParametro = root.get("nombre");
			
			criteria.select(root)
				.where(
						cb.and(
								cb.equal(root.get("empresa").get("id"), empresaId),
								cb.like(nombreParametro, tipo)
								)
						);
			
			List<Parametro> parametros = em.createQuery(criteria)
										.setParameter("nombre", parametro)
										.getResultList();
			return parametros;
		} catch(Exception e) {
			return null;
		}
	}
	
	public Parametro getSingleParaemtroByTypeAndEmpresa(int empresaId, String parametro)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Parametro> criteria = cb.createQuery(Parametro.class);
			Root<Parametro> root = cq.from(Parametro.class);
			criteria.select(root)
				.where(
						cb.and(
								cb.equal(root.get("empresa").get("id"), empresaId),
								cb.equal(root.get("nombre"), parametro)
								)
						);
			
			Parametro parametros = em.createQuery(criteria).getSingleResult();
			return parametros;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public List<Parametro> getParaemtroByGrupoAndEmpresa(int empresaId, String parametro)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Parametro> criteria = cb.createQuery(Parametro.class);
			Root<Parametro> root = cq.from(Parametro.class);

			Expression<String> tipo = cb.parameter(String.class, "nombre");
			Expression<String> nombreParametro = root.get("grupo");
			
			criteria.select(root)
				.where(
						cb.and(
								cb.equal(root.get("empresa").get("id"), empresaId),
								cb.equal(nombreParametro, tipo)
								)
						);
			
			List<Parametro> parametros = em.createQuery(criteria)
										.setParameter("nombre", parametro)
										.getResultList();
			return parametros;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public MensajeTO updateParametro(ParametroTO to)
	{
		try {
			this.beginTransaction();
			Parametro p = em.find(Parametro.class, to.getId());
			if(!p.getValor().equals(to.getValor()))
			{
				p.setValor(to.getValor());
				this.commitTransaction();
				this.cerrarConexion();
				return new MensajeTO("Actualizar Parametro","Se actualizo el parametro: " +to.getNombre(), MensajeTO.SUCCESS);
			}
			this.commitTransaction();
			this.cerrarConexion();		
			return new MensajeTO("Actualizar Parametro","No se modifico: " +to.getNombre(), MensajeTO.WARNING);
		}catch(Exception e)	{
			
			return new MensajeTO("Actualizar Parametro","No se pudo actualizar: " +to.getNombre(), MensajeTO.ERROR);
		}
	}
	
	public void addParametro(Parametro p)
	{
		try {
			em.persist(p);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public Parametro getParametroByNombre(String nombre)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Parametro> criteria = cb.createQuery(Parametro.class);
			Root<Parametro> root = criteria.from(Parametro.class);

			Expression<String> tipo = cb.parameter(String.class, "valor");
			Expression<String> nombreParametro = root.get("nombre");
			
			criteria.select(root)
				.where(
						cb.and(
								cb.equal(nombreParametro, tipo)
								)
						);
			Parametro parametros = em.createQuery(criteria)
										.setParameter("valor", nombre)
										.getSingleResult();
			return parametros;
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
