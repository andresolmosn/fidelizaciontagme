package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the cupon database table.
 * 
 */
@Entity
@NamedQuery(name="Cupon.findAll", query="SELECT c FROM Cupon c")
public class Cupon implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private int asignado;

	@Column(name="campania_id")
	private int campaniaId;

	@Column(length=13)
	private String codigo;

	@Temporal(TemporalType.DATE)
	private Date desde;

	@Temporal(TemporalType.DATE)
	private Date hasta;
	
	private int porcentaje;
	
	@Column(name="empresa_id")
	private int empresaId;
	
	@Column(name="condicion_id")
	private int condicionId;
	
	@Column(name="fecha_asignacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_asignacion;
	
	@Column(name="fecha_envio")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_envio;
	
	@Column(name="fecha_utilizacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_utilizacion;
	
	private int enviado;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carga_masiva_id")
	private CargaMasiva cargaMasiva;
	
	@ManyToOne (cascade=CascadeType.ALL)
	@JoinColumn(name="cliente_id")
	private Cliente cliente;

//	//bi-directional many-to-one association to Condicion
//	@ManyToOne
//	@JoinColumn(name="condicion_id", referencedColumnName="id")
//	private Condicion condicion;

	public Cupon() {
	}

	public int getAsignado() {
		return this.asignado;
	}

	public void setAsignado(int asignado) {
		this.asignado = asignado;
	}

	public int getCampaniaId() {
		return this.campaniaId;
	}

	public void setCampaniaId(int campaniaId) {
		this.campaniaId = campaniaId;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Date getDesde() {
		return this.desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return this.hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}

	public int getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}

	public int getCondicionId() {
		return condicionId;
	}

	public void setCondicionId(int condicionId) {
		this.condicionId = condicionId;
	}




	public CargaMasiva getCargaMasiva() {
		return cargaMasiva;
	}

	public void setCargaMasiva(CargaMasiva cargaMasiva) {
		this.cargaMasiva = cargaMasiva;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	

	public Date getFecha_asignacion() {
		return fecha_asignacion;
	}

	public void setFecha_asignacion(Date fecha_asignacion) {
		this.fecha_asignacion = fecha_asignacion;
	}

	public Date getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	public int getEnviado() {
		return enviado;
	}

	public void setEnviado(int enviado) {
		this.enviado = enviado;
	}
	
	public Date getFecha_utilizacion() {
		return fecha_utilizacion;
	}

	public void setFecha_utilizacion(Date fecha_utilizacion) {
		this.fecha_utilizacion = fecha_utilizacion;
	}

	@Override
	public String toString() {
		return "Cupon [id=" + id + ", asignado=" + asignado + ", campaniaId=" + campaniaId + ", codigo=" + codigo
				+ ", desde=" + desde + ", hasta=" + hasta + ", porcentaje=" + porcentaje + ", empresaId=" + empresaId
				+ ", condicionId=" + condicionId + ", cargaMasiva=" + cargaMasiva + ", cliente=" + cliente + "]";
	}


	
//	public Condicion getCondicion() {
//		return this.condicion;
//	}
//
//	public void setCondicion(Condicion condicion) {
//		this.condicion = condicion;
//	}
	
	

}