package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="formato_mail")
public class FormatoMail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String texto;

	@Column(name="texto_boton")
	private String textoBoton;

	private String url;

	@Column(name="url_foto")
	private String urlFoto;

	//bi-directional one-to-one association to Campania
//	@OneToOne(mappedBy="formatoMail")
//	private Campania campania;
//	

	
	@ManyToOne
	@JoinColumn(name="campania_id")
	private Campania campania;

	public FormatoMail() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTextoBoton() {
		return this.textoBoton;
	}

	public void setTextoBoton(String textoBoton) {
		this.textoBoton = textoBoton;
	}

	public String getUrl() {
		return this.url;
	}
	
	

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlFoto() {
		return this.urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	public Campania getCampania() {
		return this.campania;
	}

	public void setCampania(Campania campania) {
		this.campania = campania;
	}

}