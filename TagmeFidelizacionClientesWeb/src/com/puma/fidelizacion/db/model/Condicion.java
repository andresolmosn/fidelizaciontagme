package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the condicion database table.
 * 
 */
@Entity
@NamedQuery(name="Condicion.findAll", query="SELECT c FROM Condicion c")
public class Condicion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String texto;
	
	@Column(name="empresa_id")
	private int empresa;

//	//bi-directional many-to-one association to Cupon
//	@OneToMany(mappedBy="condicion")
//	private List<Cupon> cupons;

	public Condicion() {
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

//	public List<Cupon> getCupons() {
//		return this.cupons;
//	}
//
//	public void setCupons(List<Cupon> cupons) {
//		this.cupons = cupons;
//	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	
	

//	public Cupon addCupon(Cupon cupon) {
//		getCupons().add(cupon);
//		cupon.setCondicion(this);
//
//		return cupon;
//	}
//
//	public Cupon removeCupon(Cupon cupon) {
//		getCupons().remove(cupon);
//		cupon.setCondicion(null);
//
//		return cupon;
//	}

}