package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.CategoriaTO;

import java.util.List;


/**
 * The persistent class for the categoria database table.
 * 
 */
@Entity
@NamedQuery(name="Categoria.findAll", query="SELECT c FROM Categoria c")
public class Categoria implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private int prioridad;

	private String texto;
	
	@Column(name="is_vigente")
	private int vigente;

	@ManyToMany(mappedBy = "categorias")
	private List<Cliente> clientes;

	//bi-directional many-to-one association to Empresa
	@ManyToOne
	@JoinColumn(name="empresa_id", referencedColumnName="id")
	private Empresa empresa;

	public Categoria() {
	}
	
	
	
	public Categoria(CategoriaTO categoria) {
		super();
		this.id = categoria.getId();
		this.prioridad = categoria.getPrioridad();
		this.texto = categoria.getTexto();
		this.vigente = categoria.isVigente();
		this.empresa = new Empresa();
		empresa.setId(categoria.getEmpresa());
	}

	public Categoria(int id, int prioridad, String texto, int vigente, Empresa empresa) {
		super();
		this.id = id;
		this.prioridad = prioridad;
		this.texto = texto;
		this.vigente = vigente;
		this.empresa = empresa;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPrioridad() {
		return this.prioridad;
	}

	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public List<Cliente> getClientes() {
		return this.clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}
	
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	
	public int isVigente() {
		return vigente;
	}

	public void setVigente(int vigente) {
		this.vigente = vigente;
	}



	@Override
	public String toString() {
		return "Categoria [id=" + id + ", prioridad=" + prioridad + ", texto=" + texto + ", vigente=" + vigente
				+ ", clientes=" + clientes + ", empresa=" + empresa + "]";
	}

	
	
}