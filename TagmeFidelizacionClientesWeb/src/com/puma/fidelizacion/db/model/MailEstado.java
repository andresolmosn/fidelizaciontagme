package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.MailEstadoTO;

import java.util.Date;
import java.util.List;


@Entity(name="mail_estado")

public class MailEstado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String mail;
	private String estado;
	private int campania;
	private int procesado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	public MailEstado() {}
	public MailEstado(MailEstadoTO to) {
		estado=to.getEstado();
		mail=to.getMail();
		campania=to.getCampania();
		fecha = to.getFecha();
		procesado=to.getProcesado();
	}
	
	
	public int getProcesado() {
		return procesado;
	}
	public void setProcesado(int procesado) {
		this.procesado = procesado;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getCampania() {
		return campania;
	}
	public void setCampania(int campania) {
		this.campania = campania;
	}
}