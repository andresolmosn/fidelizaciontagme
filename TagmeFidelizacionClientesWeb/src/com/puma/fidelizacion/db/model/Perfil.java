package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.puma.fidelizacion.to.PerfilTO;

@Entity
public class Perfil implements Serializable{
	private static final Long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	private String def_url;
	
//	bi-directional many-to-many association to Categoria
	@ManyToMany(cascade = { 
	        CascadeType.PERSIST, 
	        CascadeType.MERGE
	    })
	@JoinTable(name = "perfil_menu",
	        joinColumns = @JoinColumn(name = "perfil_id"),
	        inverseJoinColumns = @JoinColumn(name = "menu_intranet_id")
	    )
	@OrderBy("orden ASC")
	private List<MenuIntranet> menuIntranet;
	
	@OneToMany(mappedBy="perfil")
	private List<Usuario> usuarios;
	
	public Perfil() {
	
	}

	public Perfil(PerfilTO perfil) {
		this.id = perfil.getId();
		this.nombre = perfil.getNombre();
	}

	public List<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<MenuIntranet> getMenuIntranet() {
		return menuIntranet;
	}

	public void setMenuIntranet(List<MenuIntranet> menuIntranet) {
		this.menuIntranet = menuIntranet;
	}
	
	public void addMenuIntranet(MenuIntranet menuIntranet) {
		this.menuIntranet.add(menuIntranet);
		menuIntranet.getPerfil().add(this);
    }
 
    public void removeMenuIntranet(MenuIntranet menuIntranet) {
        this.menuIntranet.remove(menuIntranet);
        menuIntranet.getPerfil().remove(this);
    }
    
	public String getDef_url() {
		return def_url;
	}

	public void setDef_url(String def_url) {
		this.def_url = def_url;
	}

	@Override
	public String toString() {
		return "Perfil [id=" + id + ", nombre=" + nombre + ", def_url=" + def_url + ", menuIntranet=" + menuIntranet
				+ ", usuarios=" + usuarios + "]";
	}

	
}
