package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name = "resumen_experiencia_tienda")
@NamedQuery(name = "ResumenExperiencia.getAll", query = "SELECT c FROM resumen_experiencia_tienda c")
public class ResumenExperiencia implements Serializable {

	@Id
	private int id;
	private int experiencia;
	private String estadoRegistro;
	private String vendedor;
	private String tienda;
	@Temporal(TemporalType.DATE)
	private Date f_registroTienda;
	@Temporal(TemporalType.DATE)
	private Date f_envio;
	@Temporal(TemporalType.DATE)
	private Date f_actualizacion;
	private int vendedor_id;
	private int tienda_id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

	public int getVendedor_id() {
		return vendedor_id;
	}

	public void setVendedor_id(int vendedor_id) {
		this.vendedor_id = vendedor_id;
	}

	public int getTienda_id() {
		return tienda_id;
	}

	public void setTienda_id(int tienda_id) {
		this.tienda_id = tienda_id;
	}

}
