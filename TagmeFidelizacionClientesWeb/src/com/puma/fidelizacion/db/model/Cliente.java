package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.util.Utilidad;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@NamedQuery(name="Cliente.findAll", query="SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="apellido_m")
	private String apellidoM;

	@Column(name="apellido_p")
	private String apellidoP;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;

	private String fono;

	private String hash;

	private String mail;

	private String movil;

	private String nombre;

	private String rut;

	private String sexo;
	
	private int registro;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_actualizacion;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_envio;
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_registro;
	
	@Column(name="experiencia")
	private int experiencia;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date enviado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date aceptado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fallado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date falso;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date abierto;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date clickeado;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date reclamo;
	
//	@Column(name="tienda_id")
//	private int idTienda;

//	bi-directional many-to-many association to Categoria
	@ManyToMany(cascade = { 
	        CascadeType.PERSIST, 
	        CascadeType.MERGE
	    })
	@JoinTable(name = "preferencia_cliente",
	        joinColumns = @JoinColumn(name = "cliente_id"),
	        inverseJoinColumns = @JoinColumn(name = "categoria_id")
	    )
	private List<Categoria> categorias;

	//bi-directional one-to-one association to Empresa
	@OneToOne
	@JoinColumn(name="empresa_id", referencedColumnName="id")
	private Empresa empresa;
	
	private int tienda_id;
	
	private int usuario_id;
	
	private int unsuscribe;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha_unsuscribe;

	//bi-directional many-to-many association to Cupon
	@OneToMany(mappedBy="cliente",cascade=CascadeType.ALL)
	private List<Cupon> cupones;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tipo_campania_id", nullable=false)
	private TipoCampania tipoCampania;

	public Cliente() {
	}
	
	public Cliente(ClienteTO cliente) {
		this.id = cliente.getId();
		this.apellidoM = cliente.getApellidoM();
		this.apellidoP = cliente.getApellidoP();
		this.fechaNacimiento = cliente.getFechaNacimiento();
		this.fono = cliente.getFono();
		this.hash = cliente.getHash();
		this.mail = cliente.getMail();
		this.movil = cliente.getMovil();
		this.nombre = cliente.getNombre();
		this.rut = cliente.getRut();
		this.sexo = cliente.getSexo();
		this.experiencia = cliente.getExperiencia();
		Empresa e = new Empresa();
		e.setId(cliente.getEmpresa_id());
		this.empresa = e;
		this.registro = cliente.getRegistro();
		this.fecha_actualizacion = Utilidad.getFechaDatetime(cliente.getFecha_actualizacion());
		this.fecha_envio =  Utilidad.getFechaDatetime(cliente.getFecha_envio());
		this.fecha_registro = Utilidad.getFechaDatetime(cliente.getFecha_registro());
		this.fecha_unsuscribe = cliente.getFechaUnsubscribeDate();
		this.enviado = cliente.getEnviadoDate();
		this.aceptado = cliente.getAceptadoDate();
		this.fallado = cliente.getFalladoDate();
		this.falso = cliente.getFalsoDate();
		this.abierto = cliente.getAbiertoDate();
		this.clickeado = cliente.getClickeadoDate();
		this.reclamo = cliente.getReclamoDate();
		this.tipoCampania = new TipoCampania(cliente.getTipoCampania());
		if(cliente.getTienda() != null)
		{
			this.setTienda_id(cliente.getTienda().getId());
		}
		if(cliente.getVendedor() != null) {
			this.setUsuario_id(cliente.getVendedor().getId());
		}
		//idTienda=cliente.getTienda_id();
	}


	public int getUnsuscribe() {
		return unsuscribe;
	}

	public void setUnsuscribe(int unsuscribe) {
		this.unsuscribe = unsuscribe;
	}

	public Date getFecha_unsuscribe() {
		return fecha_unsuscribe;
	}

	public void setFecha_unsuscribe(Date fecha_unsuscribe) {
		this.fecha_unsuscribe = fecha_unsuscribe;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellidoM() {
		return this.apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}

	public String getApellidoP() {
		return this.apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFono() {
		return this.fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public String getHash() {
		return this.hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMovil() {
		return this.movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public List<Categoria> getCategorias() {
		return this.categorias;
	}

	public void setCategorias(List<Categoria> categorias) {
		this.categorias = categorias;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	public void addCategoria(Categoria categoria) {
        categorias.add(categoria);
        categoria.getClientes().add(this);
    }
 
    public void removeCategoria(Categoria categoria) {
        categorias.remove(categoria);
        categoria.getClientes().remove(this);
    }

    public Date getEnviado() {
		return enviado;
	}

	public void setEnviado(Date enviado) {
		this.enviado = enviado;
	}

	public Date getAceptado() {
		return aceptado;
	}

	public void setAceptado(Date aceptado) {
		this.aceptado = aceptado;
	}

	public Date getFallado() {
		return fallado;
	}

	public void setFallado(Date fallado) {
		this.fallado = fallado;
	}

	public Date getFalso() {
		return falso;
	}

	public void setFalso(Date falso) {
		this.falso = falso;
	}

	public Date getAbierto() {
		return abierto;
	}

	public void setAbierto(Date abierto) {
		this.abierto = abierto;
	}

	public Date getClickeado() {
		return clickeado;
	}

	public void setClickeado(Date clickeado) {
		this.clickeado = clickeado;
	}

	public Date getReclamo() {
		return reclamo;
	}

	public void setReclamo(Date reclamo) {
		this.reclamo = reclamo;
	}

	public int getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}

	public int getRegistro() {
		return registro;
	}

	public void setRegistro(int registro) {
		this.registro = registro;
	}
	
	
	public int getTienda_id() {
		return tienda_id;
	}

	public void setTienda_id(int tienda_id) {
		this.tienda_id = tienda_id;
	}

	public int getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(int usuario_id) {
		this.usuario_id = usuario_id;
	}
	
	

	public Date getFecha_actualizacion() {
		return fecha_actualizacion;
	}

	public void setFecha_actualizacion(Date fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}

	public List<Cupon> getCupones() {
		return cupones;
	}

	public void setCupones(List<Cupon> cupones) {
		this.cupones = cupones;
	}
	
	

	public Date getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}
	
	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	
	

	public TipoCampania getTipoCampania() {
		return tipoCampania;
	}

	public void setTipoCampania(TipoCampania tipoCampania) {
		this.tipoCampania = tipoCampania;
	}

	@Override
	public String toString() {
		return "\nCliente [id=" + id + ", apellidoM=" + apellidoM + ", apellidoP=" + apellidoP + ", fechaNacimiento="
				+ fechaNacimiento + ", fono=" + fono + ", hash=" + hash + ", mail=" + mail + ", movil=" + movil
				+ ", nombre=" + nombre + ", rut=" + rut + ", sexo=" + sexo + ", registro=" + registro
				+ ", fecha_actualizacion=" + fecha_actualizacion + ", fecha_envio=" + fecha_envio + ", fecha_registro="
				+ fecha_registro + ", experiencia=" + experiencia + ", categorias=" + categorias + ", empresa="
				+ empresa + ", tienda_id=" + tienda_id + ", usuario_id=" + usuario_id + ", cupones=" + cupones + "]";
	}


	
}