package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="campos_adicionales")
public class CamposAdicionales implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String label;
	private String tipo;
	private int id_empresa;
	
	
	
//	@OneToOne
//	@JoinColumn(name="id_campo")
//	private Campania campania;

//	@OneToOne
//	@JoinColumn(name="defCampo")
//	private CamposAdicionalesCampania campoSeleccionado;
	
public int getId_empresa() {
		return id_empresa;
	}
	public void setId_empresa(int id_empresa) {
		this.id_empresa = id_empresa;
	}
	//	public Campania getCampania() {
//		return campania;
//	}
//	public void setCampania(Campania campania) {
//		this.campania = campania;
//	}
//	public CamposAdicionalesCampania getCampoSeleccionado() {
//		return campoSeleccionado;
//	}
//	public void setCampoSeleccionado(CamposAdicionalesCampania campoSeleccionado) {
//		this.campoSeleccionado = campoSeleccionado;
//	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
}