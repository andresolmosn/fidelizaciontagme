package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="campos_adicionales_cliente")
public class CamposAdicionalesCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne
	@JoinColumn(name="id_campo_campania")
	private CamposAdicionalesCampania campoCampania;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	private String valor;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CamposAdicionalesCampania getCampoCampania() {
		return campoCampania;
	}

	public void setCampoCampania(CamposAdicionalesCampania campoCampania) {
		this.campoCampania = campoCampania;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}