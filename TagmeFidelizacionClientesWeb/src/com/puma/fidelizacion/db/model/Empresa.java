package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.EmpresaTO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * The persistent class for the empresa database table.
 * 
 */
@Entity(name="empresa")
@NamedQuery(name="Empresa.findAll", query="SELECT e FROM empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nombre;

	@Column(name="razon_social")
	private String razonSocial;

	private String rut;
	
	@Column(name="hashcode")
	private String hash;
	
	private String estado;

//	//bi-directional many-to-one association to Campania
//	@OneToMany(mappedBy="empresa")
//	private List<Campania> campanias;
//
//	//bi-directional many-to-one association to Categoria
//	@OneToMany(mappedBy="empresa")
//	private List<Categoria> categorias;
//
//	//bi-directional one-to-one association to Cliente
//	@OneToOne(mappedBy="empresa")
//	private Cliente cliente;
//
//	//bi-directional many-to-one association to Cupon
//	@OneToMany(mappedBy="empresa")
//	private List<Cupon> cupones;
//
//	//bi-directional one-to-one association to Menu
//	@OneToOne
//	@JoinColumn(name="id", referencedColumnName="empresa_id")
//	private Menu menu;
//
	//bi-directional many-to-one association to Tienda
	@OneToMany(mappedBy="empresa")
	private List<Tienda> tiendas;
	
	@OneToMany(mappedBy = "empresa",
			cascade = CascadeType.PERSIST,
			orphanRemoval = true
			)
	private List<Parametro> parametros = new LinkedList<Parametro>();

	public Empresa() {
		parametros = new LinkedList<Parametro>();
	}
	
	public Empresa(EmpresaTO empresa) {
		super();
		this.id = empresa.getId();
		this.nombre = empresa.getNombre();
		this.razonSocial = empresa.getRazonSocial();
		this.rut = empresa.getRut();
		this.hash = empresa.getHash();
		this.estado = empresa.getEstado();
	}
	
	public Empresa(int empresaId) {
		super();
		this.id = empresaId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRazonSocial() {
		return this.razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRut() {
		return this.rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public List<Tienda> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<Tienda> tiendas) {
		this.tiendas = tiendas;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public List<Parametro> getParametros() {
		return parametros;
	}

	public void setParametros(List<Parametro> parametros) {
		this.parametros = parametros;
	}
	
	public void addParametro(Parametro parametro) {
		parametros.add(parametro);
		parametro.setEmpresa(this);
	}
	 
	public void removeParametro(Parametro parametro) {
		parametros.remove(parametro);
		parametro.setEmpresa(null);
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
}