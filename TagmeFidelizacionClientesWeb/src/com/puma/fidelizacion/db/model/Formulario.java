package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.FormularioTO;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="formato_formulario")
public class Formulario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String boton;
	private String seccion1;
	private String seccion2;
	private String seccion3;
	private int activoS2;
	private int activoS3;
	private String condiciones;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="campania_id")
	private Campania campania;

	
	public int getActivoS2() {
		return activoS2;
	}
	public void setActivoS2(int activoS2) {
		this.activoS2 = activoS2;
	}
	public int getActivoS3() {
		return activoS3;
	}
	public void setActivoS3(int activoS3) {
		this.activoS3 = activoS3;
	}
	public Formulario() {
	}
	public Formulario(FormularioTO to) {
		boton = to.getBoton();
		seccion1=to.getSeccion1();
		seccion2=to.getSeccion2();
		seccion3=to.getSeccion3();
		condiciones=to.getCondiciones();
		activoS2=to.getActivoS2();
		activoS3=to.getActivoS3();
		id= to.getId();
				
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBoton() {
		return boton;
	}

	public void setBoton(String boton) {
		this.boton = boton;
	}

	public String getSeccion1() {
		return seccion1;
	}

	public void setSeccion1(String seccion1) {
		this.seccion1 = seccion1;
	}

	public String getSeccion2() {
		return seccion2;
	}

	public void setSeccion2(String seccion2) {
		this.seccion2 = seccion2;
	}

	public String getSeccion3() {
		return seccion3;
	}

	public void setSeccion3(String seccion3) {
		this.seccion3 = seccion3;
	}

	public String getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}

	public Campania getCampania() {
		return campania;
	}

	public void setCampania(Campania campania) {
		this.campania = campania;
	}

	

}