package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;
import javax.persistence.criteria.Expression;

import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.util.Utilidad;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the campania database table.
 * 
 */
@Entity
public class Campania implements Serializable {
	private static final long serialVersionUID = 1L;

//	public static final String FIDELIZACION = "FIDELIZACION";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	

	
	private String nombre;

	@Temporal(TemporalType.DATE)
	private Date desde;

	private String estado;

	@Temporal(TemporalType.DATE)
	private Date hasta;

	@ManyToOne
	@JoinColumn(name="empresa_id", referencedColumnName="id")
	private Empresa empresa;

	@OneToMany(mappedBy="campania", cascade = CascadeType.ALL)
	private List<FormatoMail> formatoMails;
	
	@OneToMany(mappedBy="campania")
	private List<CamposAdicionalesCampania> campos;
	
	@OneToMany(mappedBy="campania",cascade = CascadeType.ALL)
	@OrderBy(value="posicion asc")
	private List<Menu> menu;
	 
	@OneToOne(mappedBy="campania", cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY, optional = false)
	private Formulario formulario;

	private String asunto;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="tipo_campania_id", nullable=false)
	private TipoCampania tipoCampania;
	
	@OneToOne(mappedBy="campania", cascade = CascadeType.ALL, 
            fetch = FetchType.LAZY, optional = false)
	private FormatoCupon formatoCupon;

	public Campania() {
		super();
	}

	public List<CamposAdicionalesCampania> getCampos() {
		return campos;
	}

	public void setCampos(List<CamposAdicionalesCampania> campos) {
		this.campos = campos;
	}

	public Campania(CampaniaTO campania) {
		super();
		this.id = campania.getId();

		this.nombre = campania.getNombre();
		this.desde = Utilidad.getFechaDate(campania.getDesde());
		this.estado = campania.getEstado();
		this.hasta = Utilidad.getFechaDate(campania.getHasta());
		this.empresa = new Empresa(campania.getEmpresaId());
		this.asunto = campania.getAsunto();
		this.tipoCampania = new TipoCampania(campania.getTipoCampania());
	}
	
	public Campania(int idCampania) {
		super();
		this.id = idCampania;
	}
	

	public Formulario getFormulario() {
		return formulario;
	}

	public void setFormulario(Formulario formulario) {
		this.formulario = formulario;
	}

	public List<Menu> getMenu() {
		return menu;
	}

	public void setMenu(List<Menu> menu) {
		this.menu = menu;
	}
	
	

	public FormatoCupon getFormatoCupon() {
		return formatoCupon;
	}

	public void setFormatoCupon(FormatoCupon formatoCupon) {
		this.formatoCupon = formatoCupon;
	}

	public Date getDesde() {
		return this.desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}


	public String getEstado() {
		return this.estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getHasta() {
		return this.hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	 

	public List<FormatoMail> getFormatoMails() {
		return formatoMails;
	}

	public void setFormatoMails(List<FormatoMail> formatoMails) {
		this.formatoMails = formatoMails;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	
	public TipoCampania getTipoCampania() {
		return tipoCampania;
	}

	public void setTipoCampania(TipoCampania tipoCampania) {
		this.tipoCampania = tipoCampania;
	}


	
}