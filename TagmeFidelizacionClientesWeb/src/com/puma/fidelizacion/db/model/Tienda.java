package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.TiendaTO;

import java.util.LinkedList;
import java.util.List;


/**
 * The persistent class for the tienda database table.
 * 
 */
@Entity(name="tienda")
 
public class Tienda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nombre;

	private String tipo;

	private String ubicacion;
	
	private int estado;

	//bi-directional many-to-one association to Empresa
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="empresa_id", referencedColumnName="id")
	private Empresa empresa;

	//bi-directional many-to-one association to Usuario
	@ManyToMany(cascade = { 
	        CascadeType.PERSIST, 
	        CascadeType.MERGE
	    })
	@JoinTable(name = "usuario_tienda",
	        joinColumns = @JoinColumn(name = "tienda_id"),
	        inverseJoinColumns = @JoinColumn(name = "usuario_id")
	    )
	private List<Usuario> usuarios;

	public Tienda() {
	}

	public Tienda(TiendaTO tienda) {
		super();
		this.id = tienda.getId();
		this.nombre = tienda.getNombre();
		this.tipo = tienda.getTipo();
		this.ubicacion = tienda.getUbicacion();
		this.estado = tienda.getEstado();
		this.empresa = new Empresa(tienda.getEmpresaId());
	}




	public int getEstado() {
		return estado;
	}



	public void setEstado(int estado) {
		this.estado = estado;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUbicacion() {
		return this.ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public Empresa getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public List<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Usuario addUsuario(Usuario usuario) {
		getUsuarios().add(usuario);
		usuario.getTiendas().add(this);
	
		
		return usuario;
	}

	public Usuario removeUsuario(Usuario usuario) {
		getUsuarios().remove(usuario);
		usuario.getTiendas().remove(null);

		return usuario;
	}

}