package com.puma.fidelizacion.db.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity(name="cupon_disponible")
@NamedQuery(name="Cupones.disponible",query="SELECT c FROM cupon_disponible c")
public class Cupones implements Serializable {

	@Id
	private int id;
	private String nombre;
	private int asignado;
	private int empresa;
	private Long cantidad;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getAsignado() {
		return asignado;
	}
	public void setAsignado(int asignado) {
		this.asignado = asignado;
	}
	public int getEmpresa() {
		return empresa;
	}
	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}
	public Long getCantidad() {
		return cantidad;
	}
	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	@Override
	public String toString() {
		return "Cupones [id=" + id + ", nombre=" + nombre + ", asignado=" + asignado + ", empresa=" + empresa
				+ ", cantidad=" + cantidad + "]";
	}
	
}
