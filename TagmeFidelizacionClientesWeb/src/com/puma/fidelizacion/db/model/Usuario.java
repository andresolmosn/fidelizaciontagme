package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.UsuarioTO;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	private String apellido;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String nick;

	private String nombre;

	private String pass;
	
	private int estado;

	@ManyToMany(mappedBy = "usuarios")
	private List<Tienda> tiendas;
	
	@ManyToOne
	@JoinColumn(name="perfil_id", referencedColumnName="id")
	private Perfil perfil;
	
	

	public Usuario(UsuarioTO u) {
		super();
		this.apellido = u.getApellido();
		this.id = u.getId();
		this.nick = u.getNick();
		this.nombre = u.getNombre();
		this.pass = u.getApellido();
		this.estado = u.getEstado();
	}
	
	public Usuario(UsuarioTO u, PerfilTO perfil) {
		super();
		this.apellido = u.getApellido();
		this.id = u.getId();
		this.nick = u.getNick();
		this.nombre = u.getNombre();
		this.pass = u.getPass();
		this.estado = u.getEstado();
		this.perfil = new Perfil(perfil);
		
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Usuario() {
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return this.pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Tienda> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<Tienda> tiendas) {
		this.tiendas = tiendas;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}



}