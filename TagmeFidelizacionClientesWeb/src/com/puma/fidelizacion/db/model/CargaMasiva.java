package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.util.Utilidad;

@Entity(name="carga_masiva")
public class CargaMasiva implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String nombre;
	
	@Temporal(TemporalType.DATE)
	private Date fecha;
	
	@OneToMany(
	        mappedBy = "cargaMasiva", 
	        cascade = CascadeType.ALL, 
	        orphanRemoval = true
	    )
	private List<Cupon> cupones = new ArrayList();
	 
	//Constructors, getters and setters removed for brevity
	 
	public void addCupon(Cupon cupon) {
		cupones.add(cupon);
		cupon.setCargaMasiva(this);
	}
	 
	public void removeCupon(Cupon cupon) {
		cupones.remove(cupon);
		cupon.setCargaMasiva(null);
	}
	
	public CargaMasiva() {
		super();
	}

	public CargaMasiva(CargaMasivaTO c) {
		super();
		this.id = c.getId();
		this.nombre = c.getNombre();
		this.fecha = Utilidad.getFechaDate(c.getFecha());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<Cupon> getCupones() {
		return cupones;
	}

	public void setCupones(List<Cupon> cupones) {
		this.cupones = cupones;
	}

	@Override
	public String toString() {
		return "CargaMasiva [id=" + id + ", nombre=" + nombre + ", fecha=" + fecha + ", cupones=" + cupones + "]";
	}

	
}
