package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.MenuTO;


/**
 * The persistent class for the menu database table.
 * 
 */
@Entity(name="menu")
@NamedQuery(name="Menu.findAll", query="SELECT m FROM menu m")
public class Menu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="id_menu_padre")
	private int idMenuPadre;

	private int posicion;

	private String texto;

	private String url;
	
	private int alto;
	
	private int ancho;

//	@JoinColumn(name="empresa_id", referencedColumnName="id")
//	private Empresa empresa;
	
	@ManyToOne
	@JoinColumn(name="campania_id")
	private Campania campania;

	private String imagen;
	
	public Menu() {
	}
	
	public Menu(MenuTO menu) {
		super();
		this.id = menu.getId();
		this.idMenuPadre = menu.getIdMenuPadre();
		this.posicion = menu.getPosicion();
		this.texto = menu.getTexto();
		this.url = menu.getUrl();
		this.imagen=menu.getImagen();
		this.alto=menu.getAltoImagen();
		this.ancho=menu.getAnchoImagen();
//		Empresa e = new Empresa();
//		e.setId(menu.getEmpresa());
		Campania c = new Campania();
		c.setId(menu.getCampania().getId());
		campania=c;
//	÷this.empresa = e;
	}



	public Campania getCampania() {
		return campania;
	}

	public void setCampania(Campania campania) {
		this.campania = campania;
	}

	public int getAlto() {
		return alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdMenuPadre() {
		return this.idMenuPadre;
	}

	public void setIdMenuPadre(int idMenuPadre) {
		this.idMenuPadre = idMenuPadre;
	}

	public int getPosicion() {
		return this.posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

//	public Empresa getEmpresa() {
//		return this.empresa;
//	}
//
//	public void setEmpresa(Empresa empresa) {
//		this.empresa = empresa;
//	}

}