package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.FormularioTO;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="formato_cupon")
public class FormatoCupon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="url_foto")
	private String urlFoto;
	private String asunto;
	private String texto;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="campania_id")
	private Campania campania;

	
	public FormatoCupon() {
	}
	
	public FormatoCupon(FormatoCuponTO to) {
		id= to.getId();
		urlFoto=to.getUrlFoto();
		asunto=to.getAsunto();
		texto=to.getTexto();
		campania = new Campania(to.getCampania_id());
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUrlFoto() {
		return urlFoto;
	}
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public Campania getCampania() {
		return campania;
	}

	public void setCampania(Campania campania) {
		this.campania = campania;
	}

	@Override
	public String toString() {
		return "FormatoCupon [id=" + id + ", urlFoto=" + urlFoto + ", asunto=" + asunto + ", texto=" + texto
				+ ", campania=" + campania + "]";
	}

	

}