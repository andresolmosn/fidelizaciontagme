package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.puma.fidelizacion.to.TipoCampaniaTO;


@Entity(name="tipo_campania")
public class TipoCampania implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private int id;
	
	private String nombre;
	
	private String url;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "empresa_id")
	private Empresa empresa;
	
	@OneToMany(
			mappedBy = "tipoCampania",
			cascade = CascadeType.ALL,
			orphanRemoval = true
		)
	private List<Cliente> clientes;
	
	@OneToMany(
				mappedBy = "tipoCampania",
				cascade = CascadeType.ALL,
				orphanRemoval = true
			)
	private List<Campania> campanias;
	
	
	public TipoCampania() {
		// TODO Auto-generated constructor stub
	}

	

	public TipoCampania(TipoCampaniaTO to) {
		super();
		this.id = to.getId();
		this.nombre = to.getNombre();
		this.url = to.getUrl();
		this.empresa = new Empresa(to.getEmpresa());
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public Empresa getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


	public List<Cliente> getClientes() {
		return clientes;
	}


	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}


	public List<Campania> getCampanias() {
		return campanias;
	}


	public void setCampanias(List<Campania> campanias) {
		this.campanias = campanias;
	}
	
	

}
