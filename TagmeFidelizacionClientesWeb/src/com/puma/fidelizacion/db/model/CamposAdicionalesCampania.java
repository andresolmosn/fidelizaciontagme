package com.puma.fidelizacion.db.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the formato_mail database table.
 * 
 */
@Entity
@Table(name="campos_adicionales_campania")
public class CamposAdicionalesCampania implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToOne
	@JoinColumn(name="id_campo")
	private CamposAdicionales defCampo;
		
	@ManyToOne
	@JoinColumn(name="campania_id")
	private Campania campania;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CamposAdicionales getDefCampo() {
		return defCampo;
	}

	public void setDefCampo(CamposAdicionales defCampo) {
		this.defCampo = defCampo;
	}

	public Campania getCampania() {
		return campania;
	}

	public void setCampania(Campania campania) {
		this.campania = campania;
	}

	
	
}