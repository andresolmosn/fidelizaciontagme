package com.puma.fidelizacion.db;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.UsuarioTO;
import com.puma.fidelizacion.util.Lista;


public class UsuarioDAO extends BaseDAO {
	Logger logger = Logger.getLogger(UsuarioDAO.class);

	  public Usuario autenticar1(String usuario, String pass) {
		    TypedQuery<Usuario> q = em.createNamedQuery("Usuario.login", Usuario.class);
		    q.setParameter(1, usuario);
		    q.setParameter(2, pass);
		    try {
		      return (Usuario)q.getSingleResult();
		    } catch (Exception e) {}
		    return null;
		  }
	  public Usuario autenticar(String usuario, String pass, String compania) {
	  
		String passEncript = DigestUtils.sha512Hex(pass);
		
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Object> cq = criteriaBuilder.createQuery();
		Root<Usuario> from = cq.from(Usuario.class);
		Join<Usuario,Tienda> tienda = from.join("tiendas");
		Join<Tienda,Empresa> empresa = tienda.join("empresa");
		CriteriaQuery<Object> select = cq.select(from);
		select.where(criteriaBuilder.and(
				criteriaBuilder.equal(from.get("nick"), usuario),criteriaBuilder.equal(from.get("pass"),passEncript),
				criteriaBuilder.equal(from.get("estado"), 1), criteriaBuilder.equal(empresa.get("nombre"),compania)
						)
				);
		TypedQuery<Object> typedQuery = em.createQuery(select);
		try {
			Usuario s = (Usuario) typedQuery.getSingleResult();
			return s;
		} catch (Exception e) {
			return null;
		}
	  }
	
	  public MensajeTO desactivaVendedor(int usuarioId)
		{
		  	try {
		  		this.beginTransaction();
				Usuario usuario = em.find(Usuario.class, usuarioId);
				usuario.setEstado(0);
				this.commitTransaction();
				this.cerrarConexion();
				return new MensajeTO("Eliminar Vendedor", "Se elimino el vendedor '"+ usuario.getNombre() +"'.",MensajeTO.SUCCESS);
		  	}catch(Exception e) {
		  		return new MensajeTO("Eliminar Vendedor", "Problemas al eliminar vendedor.",MensajeTO.ERROR);
		  	}
			
		}

		public Usuario getUsuarioById(int id){
			try{
//				Root<Usuario> root = cq.from(Usuario.class);
//				List<Predicate> criteria = new LinkedList<Predicate>();
//				criteria.add(criteriaBuilder.equal(root.get("id"), id));
//				Usuario c = (Usuario) this.getOne(root, Usuario.class, criteria, null);
				Usuario u = em.find(Usuario.class, id);
				return u;
			}catch(Exception e)	{
				return null;
			}
		}
		

		public List<Usuario> findAllVendedoresByTienda(int idTienda, int idUsuario) {
			try{
				
				Usuario u = em.find(Usuario.class, idUsuario );
				Root<Usuario> root = cq.from(Usuario.class);
				List<Predicate> criteria = new LinkedList<Predicate>();
				if(!u.getPerfil().getNombre().equals("Administrador"))
				{
					criteria.add(criteriaBuilder.equal(root.get("perfil").get("nombre"), "Vendedor"));
				}
				
				criteria.add(criteriaBuilder.equal(root.get("tiendas").get("id"), idTienda));
				criteria.add(criteriaBuilder.equal(root.get("estado"), 1));
				List<Object> lista =  this.getList(root, Usuario.class, criteria, null,null);
				List<Usuario> users = Lista.filter(Usuario.class, lista);
				return users;
				 	
			}catch(Exception e)	{
				e.printStackTrace();
				return null;
			}
		}
		
		public MensajeTO addVendedor(Usuario usuario, int idTienda, int empresaId)
		{
		  	try {
		  		this.beginTransaction();
		  		Usuario u = getUsuarioByNickEmpresa(usuario.getNick(), empresaId);
		  		logger.info("Usuario con el mismo nick ===> : " + u);
		  		if(u == null) {
		  			usuario.setTiendas(new LinkedList<Tienda>());
			  		Tienda t = em.find(Tienda.class, idTienda);
			  		t.addUsuario(usuario);
			  		this.commitTransaction();
			  		this.cerrarConexion();
			  		return new MensajeTO("Agregar Vendedor", "Se agrego el vendedor '"+ usuario.getNombre() +"'.",MensajeTO.SUCCESS);
		  		}else {
		  			this.commitTransaction();
			  		this.cerrarConexion();
			  		return new MensajeTO("Agregar Vendedor", "El nick del usuario o Rut ya se encuentra en uso.",MensajeTO.WARNING);
		  		}
		  	}catch(Exception e) {
		  		e.printStackTrace();
		  		return new MensajeTO("Agregar Vendedor", "No se pudo agregar el vendedor.",MensajeTO.ERROR);
		  	}
			
		}
		
		public Usuario getUsuarioByNick(String nick) {
			try {
				Root<Usuario> root = cq.from(Usuario.class);
				List<Predicate> criteria = new LinkedList<Predicate>();
				criteria.add(criteriaBuilder.equal(root.get("nick"), nick));
				criteria.add(criteriaBuilder.equal(root.get("estado"), 1));
				Usuario u = (Usuario)this.getOne(root, Usuario.class, criteria, null);
				return u;
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public Usuario getUsuarioByNickEmpresa(String nick, int empresaId) {
			try {
				CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
				CriteriaQuery<Object> cq = criteriaBuilder.createQuery();
				Root<Usuario> from = cq.from(Usuario.class);
				Join<Usuario,Tienda> tienda = from.join("tiendas");
				Join<Tienda,Empresa> empresa = tienda.join("empresa");
				CriteriaQuery<Object> select = cq.select(from);
				select.where(criteriaBuilder.and(
						criteriaBuilder.equal(from.get("nick"), nick), criteriaBuilder.equal(from.get("estado"), 1),
						criteriaBuilder.equal(empresa.get("id"),empresaId)
								)
						);
				TypedQuery<Object> typedQuery = em.createQuery(select);
				try {
					Usuario s = (Usuario) typedQuery.getSingleResult();
					return s;
				} catch (Exception e) {
					return null;
				}
			}catch(Exception e) {
				e.printStackTrace();
				return null;
			}
		}
		
		public MensajeTO updateUsuario(UsuarioTO usuario)
		{
			try {
		  		this.beginTransaction();
		  		Usuario usr = em.find(Usuario.class, usuario.getId());
		  		usr.setNick(usuario.getNick());
		  		usr.setNombre(usuario.getNombre());
		  		usr.setApellido(usuario.getApellido());
		  		this.commitTransaction();
		  		this.cerrarConexion();
				return new MensajeTO("Modificar Vendedor", "Se modifico el vendedor.",MensajeTO.SUCCESS);
		  	}catch(Exception e) {
		  		return new MensajeTO("Modificar Vendedor", "No se pudo modificar el vendedor.",MensajeTO.ERROR);
		  	}
		}
		
}
