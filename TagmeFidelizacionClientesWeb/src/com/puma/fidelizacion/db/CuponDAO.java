package com.puma.fidelizacion.db;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Cupones;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.GenericInfoTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;

public class CuponDAO extends BaseDAO {
	
	Logger logger = Logger.getLogger(CuponDAO.class);
	private static final long serialVersionUID = 1L;
	
	public void addCupon(Cupon cupon)
	{
		this.beginTransaction();
		em.persist(cupon);
		this.commitTransaction();
		this.cerrarConexion();
	}
	
	public void findAllCuponByEmpresa(int empresaId)
	{
		
	}
	
	public MensajeTO asignarCupon(int empresaId, int clienteId)
	{
		try{
			
			CampaniaDAO campaniadao = new CampaniaDAO();
			int campaniaId = campaniadao.getCampaniaByCliente(clienteId);
			if(campaniaId != 0)
			{
				Root<Cupon> root = cq.from(Cupon.class);
				List<Predicate> criteria = new LinkedList<Predicate>();
				criteria.add(criteriaBuilder.equal(root.get("empresaId"), empresaId));
				//criteria.add(criteriaBuilder.equal(root.get("cliente").get("id"), 0));
				criteria.add(criteriaBuilder.isNull(root.get("fecha_asignacion")));
				criteria.add(criteriaBuilder.equal(root.get("asignado"), 0));
				criteria.add(criteriaBuilder.equal(root.get("campaniaId"), campaniaId ));
				Object o = this.getFirst(root, Cupon.class, criteria, null);
				if(o != null){
					Cupon cupon = (Cupon)o;
					Cliente cliente = em.find(Cliente.class, clienteId);
					this.beginTransaction();
					cupon.setCliente(cliente);
					cupon.setAsignado(1);
					cupon.setFecha_asignacion(new java.util.Date());
					this.commitTransaction();
					return new MensajeTO("Se asigno el cupon " + cupon.getId() + " al cliente " + cliente.getId() + ".", MensajeTO.SUCCESS);
				}else{
					return new MensajeTO("No se pudo asignar cupon. No hay cupones disponibles para la campania con id: '" + campaniaId + "'.", MensajeTO.ERROR);
				}
			}else {
				return new MensajeTO("No se pudo asignar cupon. No se encontro campania para el cliente: " + clienteId , MensajeTO.ERROR);
			}
		}catch(Exception e)	{
			e.printStackTrace();
			return new MensajeTO("No se pudo asignar cupon. Algo salio mal", MensajeTO.ERROR);
		}
	}

	public CuponTO getCuponByIdCliente(int idClient) {
		CuponTO cto=null;CuponTO cuponTo =null;
		try{
			Root<Cupon> root = cq.from(Cupon.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("cliente").get("id"), idClient));
			Cupon c = (Cupon) this.getFirst(root, Cupon.class, criteria, null);
			if(c!=null) {
				cuponTo = new CuponTO(c);
				
				logger.info(cto);
			} 
	
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return cuponTo;
	}
	
	public List<Cupon> findCuponNoEnviado(int idEmpresa, int maxResults){
		try{
			Root<Cupon> root = cq.from(Cupon.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresaId"), idEmpresa));
			criteria.add(criteriaBuilder.equal(root.get("asignado"),1));
//			criteria.add(criteriaBuilder.isNull(root.get("fecha_envio")));
			criteria.add(criteriaBuilder.equal(root.get("enviado"),0));
			List<Object> lista =  this.getList(root, Cupon.class, criteria, null,null, maxResults);
			List<Cupon> cupones = Lista.filter(Cupon.class, lista);
			logger.info(cupones);
			return cupones;
			 	
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	public void cuponEnviado(Cupon c)
	{
		this.beginTransaction();
		Cupon cupon = em.find(Cupon.class, c.getId());
		cupon.setEnviado(1);
		cupon.setFecha_envio(new java.util.Date());
		this.commitTransaction();
		this.cerrarConexion();
	}
	
	public List<Cupones> checkCuponesDisponiblesCampania()
	{
		try {
//			CriteriaBuilder cb = em.getCriteriaBuilder();
//			CriteriaQuery<Object[]> criteria = cb.createQuery(Object[].class);
//			Root<Cupon> root = criteria.from( Cupon.class );
////			Join<Cupon,Campania> campania = root.join("campaniaId");
//			criteria.multiselect(root.get("campaniaId"), cb.count(root.get("id")))
//				.where(cb.and(
//						cb.equal(root.get( "empresaId" ), empresaId ),
//						cb.equal(root.get( "asignado" ), 0 )
//				)).groupBy(root.get("campaniaId"));
//			return em.createQuery(criteria).getResultList();
			List<Cupones> cupones = em.createNamedQuery("Cupones.disponible").getResultList();
			return cupones;
//			for (Object[] obj: result )
//			{
//				System.out.println(obj[0]+" : " +obj[1]);
//			}
			//result.forEach(System.out::println);
//			List<CargaMasivaTO> listaCarga = new LinkedList<CargaMasivaTO>();
//			if(result != null && result.size() > 0)
//			{
//				for(Object[] object : result)
//				{
//					CargaMasivaTO to = new CargaMasivaTO();
//					to.setId((Integer)object[0]);
//					to.setNombre((String)object[1]);
//					to.setFecha(Utilidad.getFechaDateStr((java.util.Date)object[2]));
//					to.setCupones((Long)object[3]);
//					listaCarga.add(to);
//				}
//			}
//			return listaCarga;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
//	public void findCuponCondicion(int clienteId)
//	{
//		try {
//			CriteriaBuilder cb = em.getCriteriaBuilder();
//			CriteriaQuery<Object[]> criteria = cb.createQuery(Object[].class);
//			Root<Cupon> root = criteria.from( Cupon.class );
//			Join<Cupon,Condicion> condicion = root.join("condicionId");
//			criteria.multiselect(root.get("id"),root.get("codigo"), condicion.get("texto")).where(cb.equal(root.get( "clienteId" ), clienteId ));
//			List<Object[]> result = em.createQuery(criteria).getResultList();
//			for(Object[] object : result) {
//				System.out.println(object);
//			}
//			//result.forEach(System.out::println);
////			List<CargaMasivaTO> listaCarga = new LinkedList<CargaMasivaTO>();
////			if(result != null && result.size() > 0)
////			{
////				for(Object[] object : result)
////				{
////					CargaMasivaTO to = new CargaMasivaTO();
////					to.setId((Integer)object[0]);
////					to.setNombre((String)object[1]);
////					to.setFecha(Utilidad.getFechaDateStr((java.util.Date)object[2]));
////					to.setCupones((Long)object[3]);
////					listaCarga.add(to);
////				}
////			}
////			return listaCarga;
//		} catch (Exception e) {
//			e.printStackTrace();
////			return null;
//		}
//	}
	
	public void count() {
		List<Cupones> cupones = em.createNamedQuery("Cupones.disponible").getResultList();
		logger.info(cupones);
	}
	
	public List<Cupon> cuponesUtilizadosByCarga(int idEmpresa, int carga)
	{
		try {
			Root<Cupon> root = cq.from(Cupon.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresaId"), idEmpresa));
			criteria.add(criteriaBuilder.equal(root.get("asignado"),1));
			criteria.add(criteriaBuilder.equal(root.get("cargaMasiva").get("id"),carga));
			List<Object> lista =  this.getList(root, Cupon.class, criteria, null,null);
			List<Cupon> cupones = Lista.filter(Cupon.class, lista);
			logger.info(cupones);
			return cupones;
		}catch(Exception e){
			return null;
		}
		
	}

	public int getCuponesDisponible(int empresaId, int idCamp) {
		// TODO Auto-generated method stub
		int disp=0;
			Query query = em.createNativeQuery("{call getCuponesDisponibles (?,?)}" );
			query.setParameter(1,empresaId);
			query.setParameter(2, idCamp);
			Object tmp = query.getSingleResult();
			if (tmp!=null)
				disp=Integer.parseInt(tmp.toString());
		return disp;
	}
	
	public Hashtable getCupon(int empresaId, String codigo) {
		Hashtable ht = new Hashtable();
		try {
			logger.info("Buscando el cupon: " + codigo);
			logger.info("de la empresa: " + empresaId);
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Cupon> criteria = cb.createQuery(Cupon.class);
			Root<Cupon> root = criteria.from(Cupon.class);	
			criteria.select(root)
			.where(
					cb.and(
							cb.equal(root.get("empresaId"), empresaId),
							cb.equal(root.get("codigo"), codigo)
					)
			);
			Cupon cupon = em.createQuery(criteria).getSingleResult();
			if(cupon != null) {
				logger.info(cupon);
				ht.put("DATA", cupon);
				ht.put("MENSAJE", new MensajeTO("Busqueda Exitosa","Se ha encontrado el cup�n n�mero: " + codigo, MensajeTO.INFO));
				return ht;
			}else {
				logger.info("No se encontro cup�n");
				ht.put("DATA", null);
				ht.put("MENSAJE", new MensajeTO("Cupon no encontrado","No se encontro el cup�n " + codigo + " en nuestros registros.", MensajeTO.ERROR));
				return ht;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			ht.put("MENSAJE", new MensajeTO("Cupon no encontrado","No se encontro el cup�n " + codigo + " en nuestros registros.", MensajeTO.ERROR));
			return ht;
		}
	}
	
	public MensajeTO setFechaUtilizacion(int idCupon) {
		try {
			this.beginTransaction();
			Cupon cupon = em.find(Cupon.class, idCupon);
			cupon.setFecha_utilizacion(new java.util.Date());
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Utilizar Cupon","Cupon utilizado.", MensajeTO.SUCCESS);
		}catch(Exception e) {
			e.printStackTrace();
			return new MensajeTO("Utilizar Cupon","Ocurrio un problema al intentar utilizar el cupon", MensajeTO.ERROR);
		}
	}

}
