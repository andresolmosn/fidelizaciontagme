package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.Parametro;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;

public class EmpresaDAO extends BaseDAO{
	
	private static final long serialVersionUID = 1L;

	public Empresa addEmpresa(Empresa empresa)
	{
		this.beginTransaction();
		em.persist(empresa);
		this.commitTransaction();
		this.cerrarConexion();
		return empresa;
	}
	
	public void addTienda(Tienda tienda, int empresaId) {
		this.beginTransaction();
		Empresa e = em.find(Empresa.class, empresaId);
		e.getTiendas().add(tienda);
		tienda.setEmpresa(e);
		this.commitTransaction();
		this.cerrarConexion();
	}
	
	public List<Empresa> findAllEmpresas()
	{
		try {
			Root<Empresa> root = cq.from(Empresa.class);
			List<Object> lista = this.getList(root, Empresa.class, null, null, null);
			return  Lista.filter(Empresa.class, lista);
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
		
	public Empresa findEmpresaById(int id)
	{
		return em.find(Empresa.class,id);
	}
	
	public void updateEmpresa (Empresa empresa, Empresa newEmpresa)
	{
		
	}
	
	public void deleteEmpresa(Empresa empresa)
	{
		
	}
	
	public void addParametros(Parametro p,  Empresa e)
	{
		
		e.addParametro(p);
	}
	
	public List<Empresa> getAllEmpresaActiva()
	{
		try {
			Root<Empresa> root = cq.from(Empresa.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("estado"), "ACTIVA"));
			List<Object> result = this.getList(root, Empresa.class, criteria, null, null);
			List<Empresa> empresas = new LinkedList<Empresa>();
			UtilArray.transformarListas(result, empresas, Empresa.class);
			return empresas;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
