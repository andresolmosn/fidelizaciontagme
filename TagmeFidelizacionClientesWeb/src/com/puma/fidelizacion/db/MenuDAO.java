package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.Menu;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Lista;

public class MenuDAO extends BaseDAO{
	
	private static final long serialVersionUID = 1L;
	
	private List<Menu> toMenu(List<Object> lista)
	{
		List<Menu> listaSolicitud = new LinkedList<Menu>();
		if(lista != null && lista.size() > 0){
			Iterator<Object> i = lista.iterator();
			while(i.hasNext()){
				Menu s = (Menu) i.next();
				listaSolicitud.add(s);
			}
		}
		return listaSolicitud;
	}
		
	public MensajeTO addMenu(Menu menu)
	{
		try {
			this.beginTransaction();
			em.persist(menu);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Agregar Menu","Se agrego el menu correctamente",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Agregar Menu","No se pudo agregar el menu.",MensajeTO.ERROR);
		}
		
	}
	
	public void findAllMenu()
	{
		
	}
	
	public List<Menu> findAllMenuByCampania(int campId)
	{
		try{
			Root<Menu> root = cq.from(Menu.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			if(campId > 0)
			{
				criteria.add(criteriaBuilder.equal(root.get("campania").get("id"), campId));
			}
			
			List<Order> orderBy = new ArrayList<Order>();
			orderBy.add(criteriaBuilder.asc(root.get("posicion")));
			
			List<Object> lista = this.getList(root, Menu.class, criteria, null, orderBy);
			return toMenu(lista);
		}catch(Exception e)	{
			return null;
		}
	}
	
	public List<Menu> findAllMenuPadreByEmpresa(int empresaId, int campania)
	{
		try{
			
			Root<Menu> root = cq.from(Menu.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("campania").get("empresa").get("id"), empresaId));
			criteria.add(criteriaBuilder.equal(root.get("campania").get("id"), campania));
			criteria.add(criteriaBuilder.equal(root.get("idMenuPadre"), 1));
			
			List<Order> orderBy = new ArrayList<Order>();
			orderBy.add(criteriaBuilder.asc(root.get("posicion")));
			
			List<Object> lista = this.getList(root, Menu.class, criteria, null, orderBy);
			return  Lista.filter(Menu.class, lista);
		}catch(Exception e)	{
			return null;
		}
	}
	
	
	
	public MensajeTO updatePosicionMenu(Menu menu)
	{
		try {
			Menu m = em.find(Menu.class, menu.getId());
			this.beginTransaction();
			m.setPosicion(menu.getPosicion());
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Actualizar Menu","Se ha cambiado correctamente el orden del menu.",MensajeTO.SUCCESS);
		}catch(Exception e)	{
			return new MensajeTO("Actualizar Menu","Ocurrio un error mientras se realizaba la actualización del menu.",MensajeTO.ERROR);
		}
	}
	
	public MensajeTO deleteMenu(int id)
	{
		try {
			this.beginTransaction();
			Menu m = em.find(Menu.class, id);
			em.remove(m);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Eliminar Menu","Se ha eliminado el menu '"+ m.getTexto() +"'",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Eliminar Menu","Ocurrio un error al intentar eliminar el menu.",MensajeTO.ERROR);
		}
		
	}
}
