package com.puma.fidelizacion.db;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.CountCuota;
import com.puma.fidelizacion.to.MensajeTO;

public class CountCuotaDAO extends BaseDAO{
	private static final Long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(CountCuotaDAO.class);

	public CountCuota getCountCuotaByEmpresa(int empresaId)
	{
		try {
			Root<CountCuota> root = this.cq.from(CountCuota.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(this.criteriaBuilder.equal(root.get("empresaId"), empresaId));
			
			CountCuota cc = (CountCuota)this.getOne(root, CountCuota.class, criteria, null);
			return cc;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public MensajeTO addCountCuota(CountCuota cc)
	{
		try {
			beginTransaction();
			em.persist(cc);
			commitTransaction();
			cerrarConexion();
			return new MensajeTO("Parametro CountCuota","Parametro ContCuota Inicializado para la empresa con id: " + cc.getEmpresaId(), MensajeTO.INFO);
		}catch(Exception e) {
			e.printStackTrace();
			return new MensajeTO("Parametro CountCuota","Error al inicializar los parametros de CountCuota [id empresa: "+ cc.getEmpresaId() +"]", MensajeTO.ERROR);

		}
	}
}
