package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.Perfil;
import com.puma.fidelizacion.util.Lista;

public class PerfilDAO extends BaseDAO{
	
	private static final Long serialVersionUID = 1L;
	
	public Perfil getPerfilByUsuario(int idUsuario) {
		try {
			Root<Perfil> root = cq.from(Perfil.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			
//			criteria.add(criteriaBuilder.greaterThanOrEqualTo(root.get("hasta").as(java.util.Date.class),
//			new java.util.Date()));

			criteria.add(criteriaBuilder.equal(root.get("usuario").get("id"), idUsuario));

			Perfil perfil = (Perfil)this.getFirst(root, Perfil.class, criteria, null);
			return perfil;
			// return toCategoria(lista);
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
