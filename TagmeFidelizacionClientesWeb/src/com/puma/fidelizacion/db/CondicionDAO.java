package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.util.Lista;

public class CondicionDAO extends BaseDAO{
	
	Logger logger = Logger.getLogger(CondicionDAO.class);
	private static final long serialVersionUID = 1L;
	
	public int addCondicion(String condicion, int empresaId)
	{
		Condicion c = new Condicion();
		c.setTexto(condicion);
		c.setEmpresa(empresaId);
		this.beginTransaction();
		em.persist(c);
		em.flush();
		this.commitTransaction();
		this.cerrarConexion();
		return c.getId();
	}
	public CondicionTO getCondicionById(int idCondicion) {
		CondicionTO cto=null;CondicionTO condicionTo =null;
		try{
			Root<Condicion> root = cq.from(Condicion.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("id"), idCondicion));
			Condicion c = (Condicion) this.getFirst(root, Condicion.class, criteria, null);
			condicionTo = new CondicionTO(c);
			logger.info(cto);
		}catch(Exception e)	{
			e.printStackTrace();
		}
		return condicionTo;
	}
	
	
	public List<Condicion> findAllCondicionByEmpresa(int empresaId){
		try{
			Root<Condicion> root = cq.from(Condicion.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			
			if(empresaId > 0)
			{
				criteria.add(criteriaBuilder.equal(root.get("empresa"), empresaId));
			}
			
			List<Object> lista = this.getList(root, Condicion.class, criteria, null, null);
			return  Lista.filter(Condicion.class, lista);
			
		}catch(Exception e)	{
			e.printStackTrace();
			
			return null;
		}
	}
}
