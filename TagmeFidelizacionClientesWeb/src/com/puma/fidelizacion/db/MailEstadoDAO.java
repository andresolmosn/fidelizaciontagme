package com.puma.fidelizacion.db;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Cupones;
import com.puma.fidelizacion.db.model.MailEstado;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.GenericInfoTO;
import com.puma.fidelizacion.to.MailEstadoTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;

public class MailEstadoDAO extends BaseDAO {
	
	Logger logger = Logger.getLogger(MailEstadoDAO.class);
	private static final long serialVersionUID = 1L;
	
	
	public void guardarEstados(List<MailEstadoTO> mails)
	{
		try{
			
		Iterator<MailEstadoTO> it= mails.iterator();
		int i= 0;
		int count=20;
		int indice=0;
		while(it.hasNext()) {
			if(i%count==0 && i == 0 ) {
				beginTransaction();
			}
			
			MailEstado m = new MailEstado((MailEstadoTO)it.next());
			em.persist(m);
			i++;
			indice ++;
			
			if((i%count==0 && i==count) || indice ==mails.size() ) {
				commitTransaction();
				i=0;
			}
			
		}
		}catch(Exception e)	{
			e.printStackTrace();
		}
	} 
	
	public 	void actualizarEstados( ) {
		beginTransaction();
		Query query = em.createNativeQuery("{call actualizarEstadoMail ()}" );
		
		int tmp = query.executeUpdate();
		commitTransaction();
		
		
	}
}
