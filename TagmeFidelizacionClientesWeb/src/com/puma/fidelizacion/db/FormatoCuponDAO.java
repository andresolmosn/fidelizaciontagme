package com.puma.fidelizacion.db;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.FormatoCupon;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Utilidad;

public class FormatoCuponDAO extends BaseDAO {

	public MensajeTO addFormatoCupon(FormatoCupon nuevoFormatoCupon)
	{
		try {
			this.beginTransaction();
			em.persist(nuevoFormatoCupon);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Ingresar formato cupon","Se ingreso el formato correctamente",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Ingresar formato cupon","Error al ingresar el formato cupon",MensajeTO.ERROR);
		}
		
	}
	
	public MensajeTO updateFormatoCupon(FormatoCuponTO to)
	{
		try {
			this.beginTransaction();
			FormatoCupon fc = em.find(FormatoCupon.class, to.getId());
			fc.setAsunto(to.getAsunto());
			fc.setTexto(to.getTexto());
			fc.setUrlFoto(to.getUrlFoto());
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Actualizar formato cupon","Se actualizo el formato correctamente",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Actualizar formato cupon","Error al actualizar el formato cupon",MensajeTO.ERROR);
		}
	}
	
	public String getAsuntoByCampania(int campaniaId)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<FormatoCupon> criteria = cb.createQuery(FormatoCupon.class);
			Root<FormatoCupon> root = criteria.from(FormatoCupon.class);	
//			List<String> in = Arrays.asList(new String[]{"ACTIVA", "ESPERA"});
//			Expression<String> exp = root.get("estado");
//			ParameterExpression<List> inParameter = criteriaBuilder.parameter(List.class, "IN");
			criteria.select(root.get("asunto")).where(cb.and(
					cb.equal(root.get("campania").get("id"), campaniaId)
//					cb.equal(root.get("tipo"), "FIDELIZACION"),
//					cb.lessThanOrEqualTo(root.get("campania").get("hasta"), fechaRegistro),
//					cb.greaterThanOrEqualTo(root.get("campania").get("desde"), fechaRegistro)
//					exp.in(in)
					));
			Object obj = em.createQuery(criteria).getSingleResult();
			
			return  (String) obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String getAsuntoByFechaRegistro(java.util.Date fechaRegistro)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<FormatoCupon> criteria = cb.createQuery(FormatoCupon.class);
			Root<FormatoCupon> root = criteria.from(FormatoCupon.class);	
//			List<String> in = Arrays.asList(new String[]{"ACTIVA", "ESPERA"});
//			Expression<String> exp = root.get("estado");
//			ParameterExpression<List> inParameter = criteriaBuilder.parameter(List.class, "IN");
			criteria.select(root.get("asunto")).where(cb.and(
//					cb.equal(root.get("campania").get("id"), campaniaId)
//					cb.equal(root.get("tipo"), "FIDELIZACION"),
					cb.lessThanOrEqualTo(root.get("campania").get("hasta"), fechaRegistro),
					cb.greaterThanOrEqualTo(root.get("campania").get("desde"), fechaRegistro)
//					exp.in(in)
					));
			Object obj = em.createQuery(criteria).getSingleResult();
			
			return  (String) obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
