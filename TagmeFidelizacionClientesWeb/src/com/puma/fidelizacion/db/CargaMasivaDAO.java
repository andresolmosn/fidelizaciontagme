package com.puma.fidelizacion.db;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Utilidad;

public class CargaMasivaDAO extends BaseDAO {

	private static final long serialVersionUID = 1L;
	
	 

	public int addCarga_Masiva(CargaMasiva cargaMasiva)
	{
		this.beginTransaction();
		em.persist(cargaMasiva);
		em.flush();
		this.commitTransaction();
		this.cerrarConexion();
		return cargaMasiva.getId();
	}
	
	public MensajeTO addCarga_MasivaWithCupones(CargaMasivaTO cargaMasivaTO, List<String> cupones, int dcto, int campaniaId, int condicionId, int empresaId)
	{
		try {
			this.beginTransaction();
			CargaMasiva cargaMasiva = new CargaMasiva(cargaMasivaTO);
			Iterator<String> i = cupones.iterator();
			int insertados = 0;
			int total = 0;
			while (i.hasNext()) {
				
				String ean = i.next();
				Cupon duplicado = this.getCuponByEANAndEmpresa(ean, empresaId);
				if(duplicado == null) {
					
					Cupon c = new Cupon();
					c.setCodigo(ean);
					c.setPorcentaje(dcto);
					c.setCampaniaId(campaniaId);
					c.setCondicionId(condicionId);
					c.setEmpresaId(empresaId);
					insertados++;
					logger.info("cantidad ingresada : " + insertados);
					cargaMasiva.addCupon(c);
				}
				total++;
			}

			if(insertados == 0)
			{
				this.cerrarConexion();
				return new MensajeTO("Carga de cupones", "No puedes cargar los mismos cupones en el sistema" ,MensajeTO.ERROR);
			}else {
				em.persist(cargaMasiva);
				this.commitTransaction();
				this.cerrarConexion();
				return new MensajeTO("Carga de cupones", "Se han cargado correctamente " + insertados + " cupones de " + total,MensajeTO.SUCCESS);
			}
			
		}catch(Exception e) {
			return new MensajeTO("Carga de cupones", "Ha ocurrido un problema al realizar la carga de cupones.",MensajeTO.ERROR);
		}
		
	}
	
	public MensajeTO addCarga_MasivaCupon(CargaMasivaTO cargaMasivaTO, List<String> cupones, int dcto, int campaniaId, int condicionId, int empresaId)
	{
		try {
			this.beginTransaction();
			CargaMasiva cargaMasiva = new CargaMasiva(cargaMasivaTO);
			Iterator<String> i = cupones.iterator();
			int insertados = 0;
			int total = 0;
			while (i.hasNext()) {
				String ean = i.next();
				//Cupon duplicado = this.getCuponByEANAndEmpresa(ean, empresaId);
//				if(duplicado == null) {
					Cupon c = new Cupon();
					c.setCodigo(ean);
					c.setPorcentaje(dcto);
					c.setCampaniaId(campaniaId);
					c.setCondicionId(condicionId);
					c.setEmpresaId(empresaId);
					insertados++;
					logger.info("cantidad ingresada : " + insertados);
					cargaMasiva.addCupon(c);
//				}
				total++;
			}

			if(insertados == 0)
			{
				this.cerrarConexion();
				return new MensajeTO("Carga de cupones", "No puedes cargar los mismos cupones en el sistema" ,MensajeTO.ERROR);
			}else {
				em.persist(cargaMasiva);
				this.commitTransaction();
				this.cerrarConexion();
				return new MensajeTO("Carga de cupones", "Se han cargado correctamente " + insertados + " cupones de " + total,MensajeTO.SUCCESS);
			}
			
		}catch(Exception e) {
			return new MensajeTO("Carga de cupones", "Ha ocurrido un problema al realizar la carga de cupones.",MensajeTO.ERROR);
		}
		
	}
	
	public Cupon getCuponByEANAndEmpresa(String ean, int idEmpresa)
	{
		try {
			Root<Cupon> root = cq.from(Cupon.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
				criteria.add(criteriaBuilder.equal(root.get("empresaId"), idEmpresa));
				criteria.add(criteriaBuilder.equal(root.get("codigo"), ean));
			Object o = this.getFirst(root, Campania.class, criteria, null);
			Cupon cupon = (Cupon) o;
			return cupon;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
		
	public List<CargaMasivaTO> findAllCargaMasivaByEmpresa(int empresaId)
	{
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Object[]> criteria = cb.createQuery(Object[].class);
			Root<CargaMasiva> root = criteria.from( CargaMasiva.class );
			Join<CargaMasiva,Cupon> cupones = root.join("cupones");
			criteria.multiselect(root.get("id"),root.get("nombre"), root.get("fecha"), cb.count(cupones)).where(cb.equal(cupones.get( "empresaId" ), empresaId )).groupBy(root.get("id"));
			List<Object[]> result = em.createQuery(criteria).getResultList();
			//result.forEach(System.out::println);
			List<CargaMasivaTO> listaCarga = new LinkedList<CargaMasivaTO>();
			if(result != null && result.size() > 0)
			{
				for(Object[] object : result)
				{
					CargaMasivaTO to = new CargaMasivaTO();
					to.setId((Integer)object[0]);
					to.setNombre((String)object[1]);
					to.setFecha(Utilidad.getFechaDateStr((java.util.Date)object[2]));
					to.setCupones((Long)object[3]);
					listaCarga.add(to);
				}
			}
			return listaCarga;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
public List<CargaMasivaTO> findAllCargaMasivaByCampania(int idCampania)
{
	try {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object[]> criteria = cb.createQuery(Object[].class);
		Root<CargaMasiva> root = criteria.from( CargaMasiva.class );
		Join<CargaMasiva,Cupon> cupones = root.join("cupones");
		criteria.multiselect(root.get("id"),root.get("nombre"), root.get("fecha"), cb.count(cupones)).where(cb.equal(cupones.get( "campaniaId" ), idCampania )).groupBy(root.get("id"));
		List<Object[]> result = em.createQuery(criteria).getResultList();
		
		List<CargaMasivaTO> listaCarga = new LinkedList<CargaMasivaTO>();
		if(result != null && result.size() > 0)
		{
			for(Object[] object : result)
			{
				CargaMasivaTO to = new CargaMasivaTO();
				to.setId((Integer)object[0]);
				to.setNombre((String)object[1]);
				to.setFecha(Utilidad.getFechaDateStr((java.util.Date)object[2]));
				to.setCupones((Long)object[3]);
				listaCarga.add(to);
			}
		}
		return listaCarga;
	} catch (Exception e) {
		e.printStackTrace();
		return null;
	}
}

	public MensajeTO eliminarCargaMasiva(int idCarga)
	{
		try {
			this.beginTransaction();
			CargaMasiva c = em.find(CargaMasiva.class, idCarga);
//			logger.info("Carga Masiva" + c);
			em.remove(c);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Eliminar carga de cupones","Se ha eliminado la carga de cupones", MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Eliminar carga de cupones","Ocurrio un error al intentar eliminar los cupones", MensajeTO.ALERT);
		}
	}
	
}
