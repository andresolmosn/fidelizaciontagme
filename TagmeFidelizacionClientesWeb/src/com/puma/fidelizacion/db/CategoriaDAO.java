package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Lista;

public class CategoriaDAO extends BaseDAO {
	
	private static final long serialVersionUID = 1L;
	
	Logger logger = Logger.getLogger(CategoriaDAO.class);

	private List<Categoria> toCategoria(List<Object> lista)
	{
		try {
			List<Categoria> listaSolicitud = new LinkedList<Categoria>();
			if(lista != null && lista.size() > 0){
				Iterator<Object> i = lista.iterator();
				while(i.hasNext()){
					Categoria s = (Categoria) i.next();
					listaSolicitud.add(s);
				}
			}
			return listaSolicitud;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
		
	public MensajeTO addCategoria(Categoria categoria)
	{
		try {
			this.beginTransaction();
			em.persist(categoria);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Registro categor�a","La categor�a '" + categoria.getTexto() + "' se ingres� correctamente.", MensajeTO.SUCCESS);
		}catch(Exception e){
			return new MensajeTO("Registro categor�a","Ocurrio un problema al intentar ingresar la categoria", MensajeTO.ERROR);
		}
		
	}
	
	public void findAllCategorias()
	{
		
	}
	
	
	public List<Categoria> findAllCategoriasByEmpresa(int empresaId, int isVisible)
	{

		try{
			Root<Categoria> root = cq.from(Categoria.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			
			 
				criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));
			 
			
			if(isVisible == 0) {
				criteria.add(criteriaBuilder.equal(root.get("vigente"), 0));
			}else if(isVisible == 1){
				criteria.add(criteriaBuilder.equal(root.get("vigente"), 1));
			}
			
			List<Order> orderBy = new ArrayList<Order>();
			
			orderBy.add(criteriaBuilder.asc(root.get("prioridad")));
			
			List<Object> lista = this.getList(root, Categoria.class, criteria, null, orderBy);
			return  Lista.filter(Categoria.class, lista);
//			return toCategoria(lista);
		}catch(Exception e)	{
			e.printStackTrace();
			
			return null;
		}
	}
	
	
	
	
	public void findCategoriaById(int id)
	{
		
	}
	
	public Categoria findCategoriaByTexto(String texto, int empresaId)
	{
		try{
			Root<Categoria> root = cq.from(Categoria.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			if(texto != null)
			{
				criteria.add(criteriaBuilder.equal(root.get("texto"), texto));
				criteria.add(criteriaBuilder.notEqual(root.get("vigente"), 2));
				
			}
			if(empresaId != 0)
			{
				criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));	
			}
			Categoria categoria =(Categoria) this.getOne(root, Categoria.class, criteria, null);
			
			return categoria;
		}catch(Exception e)	{
			return null;
		}
	}
	
	public void updatePrioridadCategoria(Categoria categoria)
	{
		Categoria c = em.find(Categoria.class, categoria.getId());
		this.beginTransaction();
		c.setPrioridad(categoria.getPrioridad());
		c.setVigente(categoria.isVigente());
		this.commitTransaction();
		this.cerrarConexion();
	}
	
	public MensajeTO deleteCategoria(int id)
	{
		try {
			this.beginTransaction();
			Categoria c = em.find(Categoria.class, id);
			c.setVigente(2);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Eliminar Categoria", "Se elimino la categoria '"+ c.getTexto()+"'.", MensajeTO.SUCCESS); 
		}catch(Exception e) {
			return new MensajeTO("Eliminar Categoria", "Problema al eliminar la categoria.", MensajeTO.ERROR); 
		}
		
	}
	
	public void agregasPreferencia(Categoria c, Cliente cl)
	{
		this.beginTransaction();
		c.getClientes().add(cl);
		
		this.commitTransaction();
		this.cerrarConexion();
		
	}
	

}
