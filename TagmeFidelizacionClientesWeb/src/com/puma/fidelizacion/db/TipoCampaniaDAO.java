package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.TipoCampania;
import com.puma.fidelizacion.util.Lista;

public class TipoCampaniaDAO extends BaseDAO{
	
	private static final long serialVersionUID = 1L;
	
	public List<TipoCampania> findAllTipoCampaniasByEmpresa(int empresaId) {

		try {
			Root<TipoCampania> root = cq.from(TipoCampania.class);
			List<Predicate> criteria = new LinkedList<Predicate>();

			if (empresaId > 0) {
				criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));
			}

//			List<Order> orderBy = new ArrayList<Order>();
//
//			orderBy.add(criteriaBuilder.asc(root.get("id")));

			List<Object> lista = this.getList(root, TipoCampania.class, criteria, null, null);
			return Lista.filter(TipoCampania.class, lista);
			// return toCategoria(lista);
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}
	
	public void addTipoCampaniaByEmpresa(String nombre, int empresaId) {
		
		TipoCampania t = new TipoCampania();
		t.setNombre(nombre);
		t.setEmpresa(new Empresa(empresaId));
		t.setUrl("http://");
		this.beginTransaction();
		em.persist(t);
		this.commitTransaction();
		this.cerrarConexion();
	}
	
}
