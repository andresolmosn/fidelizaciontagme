package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CamposAdicionales;
import com.puma.fidelizacion.db.model.CamposAdicionalesCampania;
import com.puma.fidelizacion.db.model.CamposAdicionalesCliente;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Cupones;
import com.puma.fidelizacion.db.model.FormatoCupon;
import com.puma.fidelizacion.to.CamposAdicionalesClienteTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;

public class CamposAdicionalesDAO extends BaseDAO {
	
	Logger logger = Logger.getLogger(CamposAdicionalesDAO.class);
	private static final long serialVersionUID = 1L;
	
	public List<CamposAdicionales> getAllCamposAdicionales() {
		try {
			Root<CamposAdicionales> root = cq.from(CamposAdicionales.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			List<Object> lista =  this.getList(root, CamposAdicionales.class, criteria, null,null);
			List<CamposAdicionales> cupones = Lista.filter(CamposAdicionales.class, lista);
			logger.info(cupones);
			return cupones;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<CamposAdicionalesCampania> findCamposAdicionalesCampania(int idCampania) {
		try {
			Root<CamposAdicionalesCampania> root = cq.from(CamposAdicionalesCampania.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			if(idCampania!=0)
				criteria.add(criteriaBuilder.equal(root.get("campania").get("id"), idCampania));
			
			List<Order> orderBy = new ArrayList<Order>();
			orderBy.add(criteriaBuilder.asc(root.get("defCampo").get("label")));
			
			List<Object> lista =  this.getList(root, CamposAdicionalesCampania.class, criteria, null,orderBy);
			List<CamposAdicionalesCampania> c = Lista.filter(CamposAdicionalesCampania.class, lista);
			return c;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	public List<CamposAdicionalesClienteTO> findCamposAdicionalesCliente(int idCliente) {
		try {
			Root<CamposAdicionalesCliente> root = cq.from(CamposAdicionalesCliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("cliente").get("id"), idCliente));
			List<Object> lista =  this.getList2(root, CamposAdicionalesCampania.class, criteria, null,null);
			Iterator it=lista.iterator();
			List<CamposAdicionalesClienteTO> reps=new LinkedList();
			while(it.hasNext()) {
				CamposAdicionalesCliente tmp =(CamposAdicionalesCliente) it.next();
				CamposAdicionalesClienteTO nuevo = new CamposAdicionalesClienteTO();
				nuevo.setId(tmp.getId());
				nuevo.setValor(tmp.getValor());
				nuevo.setIdCampoCampania(tmp.getCampoCampania().getDefCampo().getId());
				reps.add(nuevo);
			}
			return reps;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public CamposAdicionalesCampania getCamposAdicionalesCampaniaById(int id) {
		try {
			Root<CamposAdicionalesCampania> root = cq.from(CamposAdicionalesCampania.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("id"), id));
			Object o = this.getOne(root, CamposAdicionalesCampania.class, criteria, null);
			CamposAdicionalesCampania re = (CamposAdicionalesCampania) o;
			return re;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	

	
	public CamposAdicionales getCamposAdicionalesById(int id) {
		try {
			Root<CamposAdicionales> root = cq.from(CamposAdicionales.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("id"), id));
			Object o = this.getOne(root, CamposAdicionales.class, criteria, null);
			CamposAdicionales re = (CamposAdicionales) o;
			return re;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
