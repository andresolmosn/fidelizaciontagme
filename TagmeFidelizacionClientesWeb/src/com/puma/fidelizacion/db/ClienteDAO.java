package com.puma.fidelizacion.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

//import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Menu;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;

public class ClienteDAO extends BaseDAO{

	Logger logger = Logger.getLogger(ClienteDAO.class);
	private static final long serialVersionUID = 1L;
	
	public ClienteTO findClienteByHash(String hash)
	{
		try{
			Root<Cliente> root = cq.from(Cliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			if(hash != null && hash.length()>0)
			{
				criteria.add(criteriaBuilder.equal(root.get("hash"), hash));
			}
			Cliente c = (Cliente) this.getFirst(root, Cliente.class, criteria, null);
			
			ClienteTO to = new ClienteTO(c);
			logger.info("Cliente :" + to);
			return to;
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	public Cliente findClienteById(int ClienteId)
	{
		try{
			Root<Cliente> root = cq.from(Cliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			
			criteria.add(criteriaBuilder.equal(root.get("id"), ClienteId));
			
			
			Cliente c = (Cliente) this.getOne(root, Cliente.class, criteria, null);
			return c;
		}catch(Exception e)	{
			return null;
		}
	}
	
	public void updateCliente(Cliente cliente)
	{
		Cliente c = em.find(Cliente.class, cliente.getId());
		c.setApellidoM(cliente.getApellidoM());
		c.setApellidoP(cliente.getApellidoP());
		c.setFechaNacimiento(cliente.getFechaNacimiento());
		c.setFono(cliente.getFono());
		c.setMovil(cliente.getMovil());
		c.setNombre(cliente.getNombre());
		c.setRut(cliente.getRut());
		c.setRegistro(cliente.getRegistro());
		c.setFecha_actualizacion(cliente.getFecha_actualizacion());
		if (cliente.getSexo()!= null && cliente.getSexo().length()>0)
		{
			c.setSexo(cliente.getSexo());
		}
		logger.info("Experiencia del cliente:" + cliente.getExperiencia());
		c.setExperiencia(cliente.getExperiencia());
		
	}
	
	public void addCategoria(int clienteId, int categoriaId)
	{
		Cliente cliente = em.find(Cliente.class, clienteId);
		Categoria categoria = em.find(Categoria.class, categoriaId);
		this.beginTransaction();
		cliente.addCategoria(categoria);
		this.commitTransaction();
		this.cerrarConexion();
	}

	public int getIdCliente(String id) {
		Root<Cliente> root = cq.from(Cliente.class);
		List<Predicate> criteria = new LinkedList<Predicate>();
		if(id != null && id.length()>0)
			criteria.add(criteriaBuilder.equal(root.get("hash"), id));
		Cliente c = (Cliente) this.getOne(root, Cliente.class, criteria, null);
		if (c!=null)
			return c.getId();
		return 0;
		
	}
	
	
	public List<Cliente> findClientesRegistradoSinCupon(int idEmpresa, int maxResults){
		try{
			Root<Cliente> root = cq.from(Cliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			criteria.add(criteriaBuilder.isNull(root.get("fecha_envio")));
			List<Object> lista =  this.getList(root, Cliente.class, criteria, null,null, maxResults);
			 
			 	return Lista.filter(Cliente.class, lista);
			 	
		}catch(Exception e)	{
			return null;
		}
	}
	
	public List<ClienteTO> findAllCliente()
	{
		try{
			Root<Cliente> root = cq.from(Cliente.class);		
			List<Cliente> clientes = Lista.filter(Cliente.class, this.getList(root, Cliente.class, null, null, null));
			List<ClienteTO> clienteTO = new LinkedList<ClienteTO>();
			clienteTO = UtilArray.transformarListas(clientes, clienteTO, ClienteTO.class);
			return clienteTO;
		}catch(Exception e)	{
			return null;
		}
	}
	
	public List<Cliente> findClienteReporte(int idUsuario, int idTienda,int idCampania,String desde,String hasta)
	{
		try{
			Usuario u = em.find(Usuario.class, idUsuario);
			
			Root<Cliente> root = cq.from(Cliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			List<ClienteTO> clienteTO = new LinkedList();
			
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), u.getTiendas().get(0).getEmpresa().getId()));
			if(idTienda!=0) {
				criteria.add(criteriaBuilder.equal(root.get("tienda_id"), idTienda));
			}else {
				if("JefeTienda".equals(u.getPerfil().getNombre())) {
					List<Tienda> tiendas = u.getTiendas();
					List<Integer> in = new LinkedList<Integer>();
					tiendas.forEach((temp) -> { in.add(temp.getId()); });
					Expression<Integer> exp = root.get("tienda_id");
					criteria.add(exp.in(in));
				}
			}
				
			if(desde!=null && desde.length()>0 && hasta!=null && hasta.length()>0) {
				criteria.add(criteriaBuilder.greaterThanOrEqualTo(root.get("fecha_envio"), Utilidad.getFechaDatetime(desde+" 00:01" )));
				criteria.add(criteriaBuilder.lessThanOrEqualTo(root.get("fecha_envio"),  Utilidad.getFechaDatetime( hasta+" 23:59")));
				
			}
			
			List<Object> lista =  this.getList(root, Cliente.class, criteria, null,null);
			
			return Lista.filter(Cliente.class, lista);
		}catch(Exception e)	{
			return null;
		}
	}
	public List<Cliente> findClienteReporteV2(int idEmpresa, int idTienda,int idCampania,String desde,String hasta,List<TiendaTO> tiendasTO)
	{
		try{
			 
			
			Root<Cliente> root = cq.from(Cliente.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			List<ClienteTO> clienteTO = new LinkedList();
			
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			if(idTienda!=0) {
				criteria.add(criteriaBuilder.equal(root.get("tienda_id"), idTienda));
			}else {
					if(tiendasTO!=null) {
						List<Integer> in = new LinkedList<Integer>();
						tiendasTO.forEach((temp) -> { in.add(temp.getId()); });
						Expression<Integer> exp = root.get("tienda_id");
						criteria.add(exp.in(in));
					}
			}
				
			if(desde!=null && desde.length()>0 && hasta!=null && hasta.length()>0) {
				criteria.add(criteriaBuilder.greaterThanOrEqualTo(root.get("fecha_envio"), Utilidad.getFechaDatetime(desde+" 00:01" )));
				criteria.add(criteriaBuilder.lessThanOrEqualTo(root.get("fecha_envio"),  Utilidad.getFechaDatetime( hasta+" 23:59")));
				
			}
			
			List<Object> lista =  this.getList(root, Cliente.class, criteria, null,null);
			
			return Lista.filter(Cliente.class, lista);
		}catch(Exception e)	{
			return null;
		}
	}
	
	public void mailEnviado(Cliente cliente)
	{
		try {
			this.beginTransaction();
			Cliente c = em.find(Cliente.class, cliente.getId());
			c.setFecha_envio(new java.util.Date());
			this.commitTransaction();
			this.cerrarConexion();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Integer> getClientesSinCuponAsignado(int empresaId, int maxResults)
	{
		try{
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Object[]> criteria = cb.createQuery(Object[].class);
			Root<Cliente> clientes = criteria.from( Cliente.class );
			Join<Cliente,Cupon> cupones = clientes.join("cupones", JoinType.LEFT );
//			cupones.on(
//					cb.equal(cupones.get("cliente_id"), clientes.get("id")),
//					cb.equal(clientes.get("cupones").get("id"), cupones.get("id") )
//					);
			
			criteria.multiselect(clientes.get("id"), cb.count(cupones)).where(
					cb.and(
							cb.isNotNull(clientes.get("fecha_actualizacion")),
							cb.equal(clientes.get("registro"), 1),
							cb.equal(clientes.get("empresa").get("id"), empresaId)
					)).groupBy(clientes.get("id"))
					.having(cb.equal(cb.count(cupones), 0));
			List<Object[]> result = em.createQuery(criteria).setMaxResults(maxResults).getResultList();
			List<Integer> sinCupon = new LinkedList<Integer>();
			if(result != null && result.size() > 0)
			{
				logger.info("Clientes sin cupon: " + result.size());
				for(Object[] object : result)
				{
					sinCupon.add((Integer)object[0]);
				}
				return sinCupon;
			}
			logger.info("Todos los clientes tienen cupon");
			return null;
		}catch(Exception e)	{
			logger.info("Ocurrio un error");
			return null;
		}
	}
	
	public MensajeTO existeRUT(String rut, int empresaId) {
		try {
			MensajeTO mensaje = new MensajeTO();
			Root<Cliente> root = cq.from(Cliente.class);	
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));
			criteria.add(criteriaBuilder.equal(root.get("rut"), rut));
			Object OBJcliente = this.getFirst(root, Cliente.class, criteria, null);
			if(OBJcliente != null)
			{
				mensaje.setMensaje("El rut ya est� registrado.");
				mensaje.setTipo(MensajeTO.ERROR);
				return mensaje;
			}
			mensaje.setMensaje("El rut est� disponible.");
			mensaje.setTipo(MensajeTO.INFO);
			return mensaje;
		}catch(Exception e) {
			e.printStackTrace();
			MensajeTO error = new MensajeTO();
			error.setMensaje("Algo salio mal al momento de validar el rut.");
			error.setTipo(MensajeTO.ERROR);
			return error;
		}
	}
	
	public MensajeTO existeCorreo(String correo, int empresaId)
	{
		try {
			MensajeTO mensaje = new MensajeTO();
			mensaje.setTitulo("Ingresar nuevo Cliente");
			Root<Cliente> root = cq.from(Cliente.class);	
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), empresaId));
			criteria.add(criteriaBuilder.equal(root.get("mail"), correo));
			Object OBJcliente = this.getFirst(root, Cliente.class, criteria, null);
			if(OBJcliente != null)
			{
				mensaje.setMensaje("El correo ya existe dentro de nuestros registros");
				mensaje.setTipo(MensajeTO.ERROR);
				return mensaje;
			}
			mensaje.setMensaje("El correo est� disponible.");
			mensaje.setTipo(MensajeTO.INFO);
			return mensaje;
		}catch(Exception e) {
			e.printStackTrace();
			MensajeTO error = new MensajeTO();
			error.setMensaje("Algo salio mal al momento de validar el correo.");
			error.setTipo(MensajeTO.ERROR);
			return error;
		}
	}
	
	public MensajeTO nuevoCliente(Cliente cliente)
	{
		try {
			this.beginTransaction();
			em.persist(cliente);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Nuevo cliente","Cliente registrado correctamente.",MensajeTO.SUCCESS);
		}catch(Exception e) {
			e.printStackTrace();
			return new MensajeTO("Nuevo cliente","Error al crear nuevo cliente.",MensajeTO.ERROR);
		}
	}
	
//	public void addCupon(int clienteId, int cuponId)
//	{
//		Cliente cliente = em.find(Cliente.class, clienteId);
//		Cupon cupon = em.find(Cupon.class, cuponId);
//		this.beginTransaction();
//		cliente.addCupon(cupon);
//		this.commitTransaction();
//		this.cerrarConexion();
//	}

}
