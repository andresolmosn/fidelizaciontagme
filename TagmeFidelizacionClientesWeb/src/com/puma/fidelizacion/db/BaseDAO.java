package com.puma.fidelizacion.db;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

public class BaseDAO implements Serializable {

	private static final long serialVersionUID = 1L;

	EntityManagerFactory emf;
	EntityManager em;
	CriteriaBuilder criteriaBuilder;
	CriteriaQuery<Object> cq;
	Logger logger = Logger.getLogger(BaseDAO.class);

	public BaseDAO() {
//		System.out.println("BASEO DAO CONSTRUCTOR");
		emf = Persistence.createEntityManagerFactory("FidelizacionClienteWeb");
		em = emf.createEntityManager();
		em.getEntityManagerFactory().getCache().evictAll();
		criteriaBuilder = em.getCriteriaBuilder();
		cq = criteriaBuilder.createQuery(Object.class);
	}

	public void save(Object o) {
		em.persist(o);
	}

	public void remove(Object o) {
		em.remove(o);
	}

	public void cerrarConexion() {
		em.close();
		emf.close();
	}

	public void flushTransaction() {
		em.flush();
	}

	public void commitTransaction() {
		em.getTransaction().commit();
	}

	public void beginTransaction() {
		em.getTransaction().begin();
	}

	// public List<Object> getAll()
	// {
	//
	// try{
	// CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
	// CriteriaQuery<Object> criteriaQuery =
	// criteriaBuilder.createQuery(Object.class);
	// Root<Object> root = criteriaQuery.from(Object.class);
	// criteriaQuery.select(root);
	// em.getTransaction().begin();
	// List<Object> result = em.createQuery(criteriaQuery).getResultList();
	// em.getTransaction().commit();
	// return result;
	//
	// } catch(Exception e) {
	// return null;
	// }
	// }

	protected List<Object> getList(Root<?> root, Class<?> clase) {
		try {
			CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
			CriteriaQuery<Object> cq = criteriaBuilder.createQuery(Object.class);
			root = cq.from(clase);
			cq.select(root);
			// em.getTransaction().begin();
			List<Object> result = em.createQuery(cq).getResultList();
			// em.getTransaction().commit();
			// logger.info("CategoriaDAO: " + result);
			return result;

		} catch (Exception e) {
			return null;
		}

	}

	protected List<Object> getList(Root<?> root, Class<?> clase, List<Predicate> andPredicate,
			List<Predicate> orPredicate, List<Order> orderBy) {
		try {
			addAnd(root, andPredicate);
			addOr(root, orPredicate);
			addOrderBy(root, orderBy);
			List<Object> result = em.createQuery(cq).getResultList();

			em.close();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	protected List<Object> getList2(Root<?> root, Class<?> clase, List<Predicate> andPredicate,
			List<Predicate> orPredicate, List<Order> orderBy) {
		try {
			addAnd(root, andPredicate);
			addOr(root, orPredicate);
			addOrderBy(root, orderBy);
			List<Object> result = em.createQuery(cq).getResultList();

			 
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	protected List<Object> getList(Root<?> root, Class<?> clase, List<Predicate> andPredicate,
			List<Predicate> orPredicate, List<Order> orderBy, int cantidadElementos) {
		try {
			addAnd(root, andPredicate);
			addOr(root, orPredicate);
			addOrderBy(root, orderBy);
		
			List<Object> result = em.createQuery(cq).setMaxResults(cantidadElementos).getResultList();

			em.close();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	protected Object getOne(Root<?> root, Class<?> clase, List<Predicate> andPredicate, List<Predicate> orPredicate) {
		try {
			addAnd(root, andPredicate);
			addOr(root, orPredicate);
			// em.getTransaction().begin();
			Object result = em.createQuery(cq).getSingleResult();
			// em.getTransaction().commit();
			return result;
		} catch (Exception e) {
			return null;
		}
	}

	protected Object getFirst(Root<?> root, Class<?> clase, List<Predicate> andPredicate, List<Predicate> orPredicate) {
		try {
			addAnd(root, andPredicate);
			addOr(root, orPredicate);
			return em.createQuery(cq).setFirstResult(0).setMaxResults(1).getResultList().isEmpty() ? null
					: em.createQuery(cq).setFirstResult(0).setMaxResults(1).getResultList().get(0);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void addAnd(Root<?> root, List<Predicate> andPredicate) {
		if (andPredicate != null && andPredicate.size() > 0) {
			cq.select(root).where(criteriaBuilder.and(andPredicate.toArray(new Predicate[] {})));
		}
	}

	private void addOr(Root<?> root, List<Predicate> orPredicate) {
		if (orPredicate != null && orPredicate.size() > 0) {
			cq.select(root).where(criteriaBuilder.or(orPredicate.toArray(new Predicate[] {})));
		}
	}

	private void addOrderBy(Root<?> root, List<Order> orderBy) {
		if (orderBy != null && orderBy.size() > 0) {
			cq.select(root).orderBy(orderBy);
		}
	}

}
