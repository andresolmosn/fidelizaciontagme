package com.puma.fidelizacion.db;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Cupones;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;

public class TiendaDAO extends BaseDAO {
	
	Logger logger = Logger.getLogger(TiendaDAO.class);
	private static final long serialVersionUID = 1L;
	
	public Hashtable addTiendaEmpresa(Tienda tienda)
	{
		Hashtable ht = new Hashtable();
		try {
			this.beginTransaction();
			em.persist(tienda);
			this.commitTransaction();
			this.cerrarConexion();
			ht.put("MENSAJE", new MensajeTO("Nueva Tienda", "Se ingreso la tienda correctamente.",MensajeTO.SUCCESS));
			ht.put("TIENDA", tienda);
			return ht;
		
		}catch(Exception e) {
			e.printStackTrace();
			ht.put("MENSAJE",new MensajeTO("Nueva Tienda", "Ocurrio un error al ingresar la tienda.",MensajeTO.ERROR));
			ht.put("TIENDA", null);
			return ht;
		}

	}
	
	public Hashtable addTienda(Tienda tienda)
	{
		Hashtable ht = new Hashtable();
		try {
			this.beginTransaction();
			Empresa e = em.find(Empresa.class, tienda.getEmpresa().getId());
			tienda.setEmpresa(e);
			em.persist(tienda);
			this.commitTransaction();
			this.cerrarConexion();
			ht.put("MENSAJE", new MensajeTO("Nueva Tienda", "Se ingreso la tienda correctamente.",MensajeTO.SUCCESS));
			ht.put("TIENDA", tienda);
			return ht;
		
		}catch(Exception e) {
			e.printStackTrace();
			ht.put("MENSAJE",new MensajeTO("Nueva Tienda", "Ocurrio un error al ingresar la tienda.",MensajeTO.ERROR));
			ht.put("TIENDA", null);
			return ht;
		}

	}
	
//	public void findAllCuponByEmpresa(int empresaId)
//	{
//		
//	}
//	
	public List<Tienda> getTiendaActivaByEmpresa(int idEmpresa){
		try{
			Root<Tienda> root = cq.from(Tienda.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			criteria.add(criteriaBuilder.equal(root.get("estado"), 1));
			List<Object> lista =  this.getList(root, Tienda.class, criteria, null,null);
			List<Tienda> tiendas = Lista.filter(Tienda.class, lista);
			return tiendas;
			 	
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Tienda> getTiendaActivaToAdministradorEmpresa(int idEmpresa){
		try{
			Root<Tienda> root = cq.from(Tienda.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			criteria.add(criteriaBuilder.equal(root.get("estado"), 1));
			criteria.add(criteriaBuilder.notEqual(root.get("tipo"), "virtual"));
			List<Object> lista =  this.getList(root, Tienda.class, criteria, null,null);
			List<Tienda> tiendas = Lista.filter(Tienda.class, lista);
			return tiendas;
			 	
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Tienda> getTiendaUsuarioActivaByEmpresa(int idEmpresa, int idUsuario){
		try{
			Root<Tienda> root = cq.from(Tienda.class);
			Join<Tienda,Usuario> usuario = root.join("usuarios");
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			criteria.add(criteriaBuilder.equal(root.get("estado"), 1));
			criteria.add(criteriaBuilder.equal(usuario.get("id"), idUsuario));
			List<Object> lista =  this.getList(root, Tienda.class, criteria, null,null);
			List<Tienda> tiendas = Lista.filter(Tienda.class, lista);
			return tiendas;
			 	
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	public Tienda getTiendaById(int id){
		try{
			Root<Tienda> root = cq.from(Tienda.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("id"), id));
			Tienda c = (Tienda) this.getOne(root, Tienda.class, criteria, null);
			return c;
		}catch(Exception e)	{
			return null;
		}
	}
	public List<Tienda> getTiendaByEmpresa(int idEmpresa){
		try{
			Root<Tienda> root = cq.from(Tienda.class);
			List<Predicate> criteria = new LinkedList<Predicate>();
			criteria.add(criteriaBuilder.equal(root.get("empresa").get("id"), idEmpresa));
			List<Object> lista =  this.getList(root, Tienda.class, criteria, null,null);
			List<Tienda> tiendas = Lista.filter(Tienda.class, lista);
			return tiendas;
			 	
		}catch(Exception e)	{
			e.printStackTrace();
			return null;
		}
	}
	
	public MensajeTO desactivaTienda(int tiendaId)
	{
		try {
			this.beginTransaction();
			Tienda cupon = em.find(Tienda.class, tiendaId);
			cupon.setEstado(0);
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Desactivar Tienda", "Se desactivo la tienda.",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Desactivar Tienda", "Ocurrio un error al desactivar la tienda.",MensajeTO.ERROR);
		}

	}
	
	public MensajeTO updateTienda(TiendaTO tienda)
	{
		try {
			
			this.beginTransaction();
			Tienda t = em.find(Tienda.class, tienda.getId());
			t.setNombre(tienda.getNombre());
			t.setTipo(tienda.getTipo());
			t.setUbicacion(tienda.getUbicacion());
			this.commitTransaction();
			this.cerrarConexion();
			return new MensajeTO("Modificar Tienda", "Se modifico la tienda '"+ tienda.getNombre() +"' correctamente.",MensajeTO.SUCCESS);
		}catch(Exception e) {
			e.printStackTrace();
			return new MensajeTO("Modificar Tienda", "Ocurrio un error al modificar la tienda.",MensajeTO.ERROR);
		}

	}

	
 

}
