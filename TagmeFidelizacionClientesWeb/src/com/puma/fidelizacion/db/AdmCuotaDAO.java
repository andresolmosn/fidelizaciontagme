package com.puma.fidelizacion.db;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.model.AdmCuota;
import com.puma.fidelizacion.to.MensajeTO;

public class AdmCuotaDAO extends BaseDAO{
	
	private static final Long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(AdmCuotaDAO.class);
	
	public MensajeTO addAdmCuota(AdmCuota admCuota)
	{
		try {
			beginTransaction();
			em.persist(admCuota);
			commitTransaction();
			cerrarConexion();
			return new MensajeTO("Parametro AdmCuota","Parametro AdmCuota Inicializado para la empresa con id: " + admCuota.getIdEmpresa(), MensajeTO.INFO);
		}catch(Exception e) {
			e.printStackTrace();
			return new MensajeTO("Parametro AdmCuota","Error al inicializar los parametros de AdmCuota [id empresa: "+ admCuota.getIdEmpresa() +"]", MensajeTO.ERROR);
		}
	}
}
