package com.puma.fidelizacion.bean;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.to.UsuarioTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name="admEmpBean")
@ViewScoped
public class AdministradorEmpresa implements Serializable{
	
	private static final Long serialVersionUID =1L;
	private static final Logger logger = Logger.getLogger(AdministradorEmpresa.class);
	
	private List<EmpresaTO> empresas;
	private EmpresaTO nuevaEmpresa;
	private UsuarioTO nuevoUsuario;
	private String contrasena;
	private String confirmContrasena;
	private String nombreEmpresa;
	private String rutEmpresa;
	private String razonSocialEmpresa;
	private String usuarioAdminsitrador;
	private String tipoCampania;
	private int cantidad;
	
	public List<EmpresaTO> getEmpresas() {
		return empresas;
	}
	public void setEmpresas(List<EmpresaTO> empresas) {
		this.empresas = empresas;
	}
	public EmpresaTO getNuevaEmpresa() {
		return nuevaEmpresa;
	}
	public void setNuevaEmpresa(EmpresaTO nuevaEmpresa) {
		this.nuevaEmpresa = nuevaEmpresa;
	}
	public UsuarioTO getNuevoUsuario() {
		return nuevoUsuario;
	}
	public void setNuevoUsuario(UsuarioTO nuevoUsuario) {
		this.nuevoUsuario = nuevoUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getConfirmContrasena() {
		return confirmContrasena;
	}
	public void setConfirmContrasena(String confirmContrasena) {
		this.confirmContrasena = confirmContrasena;
	}
	
	public String getNombreEmpresa() {
		return nombreEmpresa;
	}
	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	public String getRutEmpresa() {
		return rutEmpresa;
	}
	public void setRutEmpresa(String rutEmpresa) {
		this.rutEmpresa = rutEmpresa;
	}
	public String getRazonSocialEmpresa() {
		return razonSocialEmpresa;
	}
	public void setRazonSocialEmpresa(String razonSocialEmpresa) {
		this.razonSocialEmpresa = razonSocialEmpresa;
	}
	public String getUsuarioAdminsitrador() {
		return usuarioAdminsitrador;
	}
	public void setUsuarioAdminsitrador(String usuarioAdminsitrador) {
		this.usuarioAdminsitrador = usuarioAdminsitrador;
	}
	public String getTipoCampania() {
		return tipoCampania;
	}
	public void setTipoCampania(String tipoCampania) {
		this.tipoCampania = tipoCampania;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String init()
	{
		Controller c = Controller.getInstance();
		this.empresas = c.findAllEmpresas();
		this.nuevaEmpresa = new EmpresaTO();
		this.nuevoUsuario = new UsuarioTO();
		this.contrasena = "";
		this.confirmContrasena = "";
		logger.info("Fin init");
		return "";
	}
	
	public String crearEmpresa()
	{
		this.nuevaEmpresa = new EmpresaTO();
		this.nuevaEmpresa.setNombre(this.getNombreEmpresa());
		this.nuevaEmpresa.setRazonSocial(this.getRazonSocialEmpresa());
		this.nuevaEmpresa.setRut(this.getRutEmpresa());
		this.nuevaEmpresa.setEstado("ACTIVA");
		this.nuevoUsuario = new UsuarioTO();
		this.nuevoUsuario.setNick(getUsuarioAdminsitrador());
		Controller c = Controller.getInstance();
		String hash = getHash(Utilidad.getFechaDateStr(new java.util.Date()));
		this.nuevaEmpresa.setHash(hash);
//		this.nuevaEmpresa = c.addEmpresa(nuevaEmpresa);
		
		TiendaTO tienda = new TiendaTO();
		tienda.setEstado(1);
		tienda.setNombre(this.nuevaEmpresa.getNombre() + "Administrador");
		tienda.setTipo("virtual");
		tienda.setUbicacion(this.nuevaEmpresa.getNombre());
		tienda.setEmpresaId(this.nuevaEmpresa.getId());
		Hashtable ht = c.addTiendaHashtable(tienda, nuevaEmpresa);
		MensajeTO msg = (MensajeTO)ht.get("MENSAJE");
		Tienda t = (Tienda)ht.get("TIENDA");
		int idEmpresa = t.getEmpresa().getId();
		this.nuevoUsuario.setNombre("Administrador");
		this.nuevoUsuario.setApellido(nuevaEmpresa.getNombre());
		this.nuevoUsuario.setEstado(1);
		this.nuevoUsuario.setIdEmpresa(idEmpresa);
		this.nuevoUsuario.setPass(passEncript(contrasena));
		PerfilTO perfil = new PerfilTO();
		//TODO: Id del perfil administrador de empresa
		perfil.setId(Integer.parseInt(c.getParametroByNombre("tagme.app.idPerfilAdministrador").getValor()));
		this.nuevoUsuario.setPerfil(perfil);
		
		c.addBaseCountCuota(idEmpresa);
		c.addBaseAdmCuota(idEmpresa, cantidad);
		c.addVendedor(nuevoUsuario, t.getId(), idEmpresa);
		addParametros(idEmpresa);
		addTipoCampania(this.tipoCampania, idEmpresa);
		return "";
	}
	
	private String getHash(String str)
	{
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(str.getBytes());
			byte[] mb = md.digest();
			return Hex.encodeHexString(mb);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	private String passEncript(String pass) {
		return DigestUtils.sha512Hex(pass);
	}
	
	private void addParametros(int idEmpresa) {
		EmpresaTO empresa = new EmpresaTO(idEmpresa);
//		ParametroTO hostA = new ParametroTO("smtp.host", "smtp.mailgun.org", empresa, "MAIL", "Host del servidor de correo");
//		ParametroTO puertoA = new ParametroTO( "smtp.port", "2525", empresa, "MAIL", "Puerto para la conexi�n con el servidor de correo.");
//		ParametroTO mailgunUserA = new ParametroTO("smtp.user", "postmaster@tagmeservices.com", empresa, "MAIL", "Usuario del Servidor de correo");
//		ParametroTO mailgunPassA = new ParametroTO("smtp.passwd", "93b904c048f5e30ba02fa27ef24f29d6-a5d1a068-7a4b26cd", empresa, "MAIL", "Contrase�a del usuario para el servidor de correo");
//		ParametroTO mailFromA = new ParametroTO("smtp.from", "fiedlizacion@tagmeservices.com", empresa, "MAIL", "");
//		ParametroTO userNameAlertas = new ParametroTO("alert.usuario", "nombre", empresa, "", "Nombre del usuario que recibir� las alertas");
//		ParametroTO mailAlertas = new ParametroTO("alert.mail", "ingresar Correo", empresa, "ALERT", "Correo al cual se dirigir�n las alertas");
//		ParametroTO mailFromAlerts = new ParametroTO("alert.from", "Fidelizaci�n Report <fidelizacion@tagmeservices.com>", empresa, "", "Remitente que enviar� las alertas");
//		ParametroTO mailAsunto = new ParametroTO("alert.asunto", "Fidelizaci�n (errores)", empresa, "ALERT", "Asunto del correo de alerta");
//		ParametroTO hostB = new ParametroTO("smtp.alert.host", "smtp.mailgun.org", empresa, "MAIL", "Host del servidor de correo");
//		ParametroTO puertoB = new ParametroTO("smtp.alert.port", "2525", empresa, "MAIL", "Puerto para la conexi�n con el servidor de correo.");
//		ParametroTO mailgunUserB = new ParametroTO("smtp.alert.user", "postmaster@alert.tagmeservices.com", empresa, "MAIL", "Usuario del Servidor de correo");
//		ParametroTO mailgunPassB = new ParametroTO("smtp.alert.passwd", "bf5a3e76e17d90fd9fdfb9c8eeafaabc-a5d1a068-9b255b63", empresa, "MAIL", "Contrase�a del usuario para el servidor de correo");
//		ParametroTO mailFromB = new ParametroTO("smtp.alert.from", "fiedlizacion@tagmeservices.com", empresa, "MAIL", "");
//		ParametroTO footerA = new ParametroTO("footer.text", "Usted se suscribio con el mail", empresa, "FOOTER", "Texto que tendr� el footer del sitio 1");
//		ParametroTO footerIMG = new ParametroTO("footer.imagen", "img.png", empresa, "FOOTER", "URL de la imagen del sitio");
//		ParametroTO footerB = new ParametroTO("footer.text2", "Por favor no respondas a este correo electr�nico. De click en el link si ya no deseas recibir correos ", empresa, "FOOTER", "Texto que tendr� el footer del sitio 1");
//		ParametroTO footerMailContacto = new ParametroTO("footer.contacto", "", empresa, "FOOTER", "Correo de contacto");
//		ParametroTO redirectLink = new ParametroTO("registro.urlRedirect", "http://", empresa, "REGISTRO", "URL a la cual se redirigira al registrarce correctamente");
//		ParametroTO appState = new ParametroTO("app.sending", "Stop", empresa, "APP", "indica si la tarea est� enviando correos");
//		ParametroTO appCantCiclo = new ParametroTO("app.enviosPorCiclo", "25", empresa, "APP", "cantidad de correos enviados");
//		ParametroTO mailFrom = new ParametroTO("app.fromSendMail", "Default <fidelizacion@tagmeservices.com>", empresa, "APP", "Remitente del correo de la aplicacion");
		List<ParametroTO> parametros = new LinkedList<>();
		parametros.add(new ParametroTO("smtp.host", "smtp.mailgun.org", empresa, "MAIL", "Host del servidor de correo"));
		parametros.add(new ParametroTO( "smtp.port", "2525", empresa, "MAIL", "Puerto para la conexi�n con el servidor de correo."));
		parametros.add(new ParametroTO("smtp.user", "postmaster@tagmeservices.com", empresa, "MAIL", "Usuario del Servidor de correo"));
		parametros.add(new ParametroTO("smtp.passwd", "93b904c048f5e30ba02fa27ef24f29d6-a5d1a068-7a4b26cd", empresa, "MAIL", "Contrase�a del usuario para el servidor de correo"));
		parametros.add(new ParametroTO("smtp.from", "fiedlizacion@tagmeservices.com", empresa, "MAIL", ""));
		parametros.add(new ParametroTO("alert.usuario", "nombre", empresa, "", "Nombre del usuario que recibir� las alertas"));
		parametros.add(new ParametroTO("alert.mail", "ingresar Correo", empresa, "ALERT", "Correo al cual se dirigir�n las alertas"));
		parametros.add(new ParametroTO("alert.from", "Fidelizaci�n Report <fidelizacion@tagmeservices.com>", empresa, "", "Remitente que enviar� las alertas"));
		parametros.add(new ParametroTO("alert.asunto", "Fidelizaci�n (errores)", empresa, "ALERT", "Asunto del correo de alerta"));
		parametros.add(new ParametroTO("smtp.alert.host", "smtp.mailgun.org", empresa, "MAIL", "Host del servidor de correo"));
		parametros.add(new ParametroTO("smtp.alert.port", "2525", empresa, "MAIL", "Puerto para la conexi�n con el servidor de correo."));
		parametros.add(new ParametroTO("smtp.alert.user", "postmaster@alert.tagmeservices.com", empresa, "MAIL", "Usuario del Servidor de correo"));
		parametros.add(new ParametroTO("smtp.alert.passwd", "bf5a3e76e17d90fd9fdfb9c8eeafaabc-a5d1a068-9b255b63", empresa, "MAIL", "Contrase�a del usuario para el servidor de correo"));
		parametros.add(new ParametroTO("smtp.alert.from", "fiedlizacion@tagmeservices.com", empresa, "MAIL", ""));
		parametros.add(new ParametroTO("footer.text", "Usted se suscribio con el mail", empresa, "FOOTER", "Texto que tendr� el footer del sitio 1"));
		parametros.add(new ParametroTO("footer.imagen", "img.png", empresa, "FOOTER", "URL de la imagen del sitio"));
		parametros.add(new ParametroTO("footer.text2", "Por favor no respondas a este correo electr�nico. De click en el link si ya no deseas recibir correos ", empresa, "FOOTER", "Texto que tendr� el footer del sitio 1"));
		parametros.add(new ParametroTO("footer.contacto", "", empresa, "FOOTER", "Correo de contacto"));
		parametros.add(new ParametroTO("registro.urlRedirect", "http://", empresa, "REGISTRO", "URL a la cual se redirigira al registrarce correctamente"));
		parametros.add(new ParametroTO("app.sending", "Stop", empresa, "APP", "indica si la tarea est� enviando correos"));
		parametros.add(new ParametroTO("app.enviosPorCiclo", "25", empresa, "APP", "cantidad de correos enviados"));
		parametros.add(new ParametroTO("app.fromSendMail", "Default <fidelizacion@tagmeservices.com>", empresa, "APP", "Remitente del correo de la aplicacion"));
		parametros.add(new ParametroTO("app.facebook", "facebookpage", empresa, "APP", "Remitente del correo de la aplicacion"));
		parametros.add(new ParametroTO("app.twitter", "twittwepage", empresa, "APP", "Remitente del correo de la aplicacion"));
		parametros.add(new ParametroTO("app.instagram", "instagrampage", empresa, "APP", "Remitente del correo de la aplicacion"));
		parametros.add(new ParametroTO("app.youtube", "Default <fidelizacion@tagmeservices.com>", empresa, "APP", "Remitente del correo de la aplicacion"));
		Controller c = Controller.getInstance();
		c.addParametros(parametros, idEmpresa);
	}
	
	private void addTipoCampania(String tipoCampania, int empresaId)
	{
		Controller c = Controller.getInstance();
		c.addTipoCampania(tipoCampania, empresaId);
	}
	
	
}
