package com.puma.fidelizacion.bean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.util.Propiedades;
import com.puma.fidelizacion.util.Utilidad;


@ManagedBean
@ViewScoped
public class GenerarCuponBean extends BaseBean{
	private static final Long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(GenerarCuponBean.class);
	
	private List<CampaniaTO> campanias;
	private int cantidadDisponible;
	private List<CargaMasivaTO> cargaCupones;
	private CampaniaTO campaniaActual;
	private int idCampaniaSeleccionada;
	private List<CondicionTO> condiciones;
	private int cantidadCupones;
	private String textArea;

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	
	public int getCantidadDiponible() {
		return cantidadDisponible;
	}

	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}

	public List<CargaMasivaTO> getCargaCupones() {
		return cargaCupones;
	}

	public void setCargaCupones(List<CargaMasivaTO> cargaCupones) {
		this.cargaCupones = cargaCupones;
	}

	public CampaniaTO getCampaniaActual() {
		return campaniaActual;
	}

	public void setCampaniaActual(CampaniaTO campaniaActual) {
		this.campaniaActual = campaniaActual;
	}

	public int getIdCampaniaSeleccionada() {
		return idCampaniaSeleccionada;
	}

	public void setIdCampaniaSeleccionada(int idCampaniaSeleccionada) {
		this.idCampaniaSeleccionada = idCampaniaSeleccionada;
	}

	public int getCantidadDisponible() {
		return cantidadDisponible;
	}

	public List<CondicionTO> getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(List<CondicionTO> condiciones) {
		this.condiciones = condiciones;
	}

	public int getCantidadCupones() {
		return cantidadCupones;
	}

	public void setCantidadCupones(int cantidadCupones) {
		this.cantidadCupones = cantidadCupones;
	}
	
	public String getTextArea() {
		return textArea;
	}

	public void setTextArea(String textArea) {
		this.textArea = textArea;
	}

	public String init()
	{
		isSecure();
		Controller c = Controller.getInstance();
		campanias = c.findAllCampaniasVigentesByEmpresa(this.usuario.getIdEmpresa());
		condiciones = c.findAllCondicionByEmpresa(this.usuario.getIdEmpresa());
		return "";
	}
	
	public void setCampaniaActual()
	{
		java.util.Iterator<CampaniaTO> i = campanias.iterator();
		while (i.hasNext()) {
			CampaniaTO campaniaIterator = i.next();
			logger.info("setCampaniaActual ==> idCampaniaSeleccionada:" + this.idCampaniaSeleccionada + " == " + campaniaIterator.getId() + " ?");
			if(idCampaniaSeleccionada == campaniaIterator.getId())
			{
				Controller c = Controller.getInstance();
				this.campaniaActual = campaniaIterator;
				cargaCupones = c.findAllCargaMasivaByCampania(this.campaniaActual.getId());
				cantidadDisponible = c.getCantCuponesLibres(this.campaniaActual.getEmpresaId(),this.campaniaActual.getId());
				logger.info("Campania activa =====> " + this.campaniaActual);
			}
		}
	}
	
	public void generarCupones() {
		try{
			int secuencia = cargaCupones!=null?cargaCupones.size()+1:1;
			int empresaId= usuario.getIdEmpresa();
			int campaniaId = this.campaniaActual.getId();
			int cantidad = this.cantidadCupones;
			int condicionId = ingresarCondicion(this.textArea, empresaId);
			
			String formatoSecuencia = String.format("%02d", secuencia);
	//		String formatoEmpresa = String.format("%03d", this.usuario.getIdEmpresa());
			String formatoCampania = String.format("%04d", campaniaId);
			String formatoCupon;
			String dv;
			String ean;
			
			List<String> cupones = new LinkedList<String>();
			String c = null;
			for(int i= 1; i <= cantidad; i++)
			{
				formatoCupon = String.format("%06d", 999999 - i);
				ean =  formatoCampania + formatoSecuencia + formatoCupon;
				dv= String.valueOf(verificarEan13(ean));
				c = ean+dv;
				cupones.add(c);
			}
			Controller controller = Controller.getInstance();
			CargaMasivaTO carga = ingresarCargaMasiva(this.campaniaActual.getNombre() + new Date() + ".xlsx");
			controller.addCarga_MasivaCupon(cupones, 0, campaniaId, condicionId, empresaId, carga);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int verificarEan13(String codigoBarra) {
		int[] numeros = codigoBarra.chars().map(Character::getNumericValue).toArray();
        int somaPares = numeros[1] + numeros[3] + numeros[5] + numeros[7] + numeros[9] + numeros[11];
        int somaImpares = numeros[0] + numeros[2] + numeros[4] + numeros[6] + numeros[8] + numeros[10] ;
        int resultado = somaImpares + somaPares * 3;
        int digitoVerificador = 10 - resultado % 10;
        if(digitoVerificador == 10)
        {
        	return 0;
        }
        return digitoVerificador;
	}
	
	private int ingresarCondicion(String text, int empresaId) 
	{
		Controller c = Controller.getInstance();
		int condicionId = 0;
		if(this.condiciones != null && this.condiciones.size() > 0)
		{
			Iterator<CondicionTO> i = this.condiciones.iterator();
			while(i.hasNext()) {
				CondicionTO to = i.next();
				if(to.getTexto().trim().replaceAll("(\r\n|\n)", "<br />").equalsIgnoreCase(textArea.trim().replaceAll("(\r\n|\n)", "<br />"))) {
					return to.getId();
				}
			}
		}
		
		String nuevaCondicion = textArea.replaceAll("(\r\n|\n)", "<br />");
		return c.addCondicion(nuevaCondicion, empresaId);
	}
	
	private CargaMasivaTO ingresarCargaMasiva(String fileName)
	{
		Propiedades p = new Propiedades();
		CargaMasivaTO cargaMasivaTO = new CargaMasivaTO();
		cargaMasivaTO.setNombre(fileName.trim());
		cargaMasivaTO.setFecha(Utilidad.getFechaDateStr(new java.util.Date()));
		return cargaMasivaTO;
	}
}
