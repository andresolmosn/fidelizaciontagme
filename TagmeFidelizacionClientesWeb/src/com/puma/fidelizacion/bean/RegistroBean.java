package com.puma.fidelizacion.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.glassfish.api.container.RequestDispatcher;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormularioTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.to.ValidacionTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name = "registroBean")
@ViewScoped
public class RegistroBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(RegistroBean.class);

	private String[] rut = new String[2];
	private String fechaNacimiento;
	private List<CategoriaTO> listaCategoria;
	private String preferencias;
	private ClienteTO cliente;
	private boolean condiciones = false;
	public boolean[] dato;
	public int campania;
	public int empresa;
	private boolean validado = false;
	public List<MensajeTO> mensajes;
	private List<MenuTO> menu;
	private boolean existeMenu;
	public boolean registrado;
	private FormularioTO nuevo;
	private List<CamposAdicionalesCampaniaTO> camposCampania;
	private List<ParametroTO> parametros;
	private String urlRedirect;
	private boolean extranjero;
	private String idExtranjero;
	private boolean eCommerce;
	private boolean unsubscribe;

	public boolean isExisteMenu() {
		existeMenu = (menu != null && menu.size() > 0) ? true : false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}

	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getCampania() {
		return campania;
	}

	public void setCampania(int campania) {
		this.campania = campania;
	}

	public boolean[] getDato() {
		return dato;
	}

	public void setDato(boolean[] dato) {
		this.dato = dato;
	}

	public List<CategoriaTO> getListaCategoria() {
		return listaCategoria;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}

	public void setListaCategoria(List<CategoriaTO> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}

	public ClienteTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteTO cliente) {
		this.cliente = cliente;
	}

	public String[] getRut() {
		return rut;
	}

	public void setRut(String[] rut) {
		this.rut = rut;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public boolean isCondiciones() {
		return condiciones;
	}

	public void setCondiciones(boolean condiciones) {
		this.condiciones = condiciones;
	}

	public List<CamposAdicionalesCampaniaTO> getCamposCampania() {
		return camposCampania;
	}

	public void setCamposCampania(List<CamposAdicionalesCampaniaTO> camposCampania) {
		this.camposCampania = camposCampania;
	}

	public List<MensajeTO> getMensajes() {
		return mensajes;
	}

	public void setMensajes(List<MensajeTO> mensajes) {
		this.mensajes = mensajes;
	}

	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}
	
	public List<ParametroTO> getParametros() {
		return parametros;
	}

	public void setParametros(List<ParametroTO> parametros) {
		this.parametros = parametros;
	}

	public String getUrlRedirect() {
		return urlRedirect;
	}

	public void setUrlRedirect(String urlRedirect) {
		this.urlRedirect = urlRedirect;
	}
	
	public boolean isExtranjero() {
		return extranjero;
	}

	public void setExtranjero(boolean extranjero) {
		this.extranjero = extranjero;
	}
	
	public String getIdExtranjero() {
		return idExtranjero;
	}

	public void setIdExtranjero(String idExtranjero) {
		this.idExtranjero = idExtranjero;
	}
	
	

	public boolean iseCommerce() {
		return eCommerce;
	}

	public void seteCommerce(boolean eCommerce) {
		this.eCommerce = eCommerce;
	}
	
	public boolean isUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public String init()  {
		logger.info("----------------------init----------------------------");
		registrado = false;
		this.extranjero = false;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		Map params = externalContext.getRequestParameterMap();
		String clienteHash = (String) params.get("hash");
		Controller c = new Controller();
		
		if (clienteHash != null && clienteHash.length() > 0) {
			this.cliente = c.findClienteByHash(clienteHash);
			logger.info(cliente);
			
			if (cliente != null) {
				if(cliente.getUnsubscribe() == 0) {
					this.unsubscribe = false;
					this.parametros = c.getParaemtroByGrupoAndEmpresa(this.cliente.getEmpresa_id(), ParametroTO.REGISTRO);
					if(parametros != null && parametros.size() > 0)
					{
						this.urlRedirect = ParametroTO.getParametro(parametros, "registro.urlRedirect").getValor();
					}
					setListaCategoria(c.findAllCategoriasByEmpresa(cliente.getEmpresa_id(), 1));
					campania = c.getCampaniaCliente(cliente.getFecha_registro(), cliente.getEmpresa_id(), cliente.getTipoCampania().getId());
					
					
					empresa = cliente.getEmpresa_id();
					logger.debug("=========");
					logger.debug(campania);
					logger.debug(empresa);

					CampaniaTO campaniaSelecionada = c.findCampaniasFidelizacionById(campania, empresa);
					logger.info("Tipo de Campa�a: " + campaniaSelecionada.getTipoCampania().getNombre());
					if(campaniaSelecionada.getTipoCampania().getNombre().equals("E-COMMERCE"))
					{
						this.seteCommerce(true);
					}else {
						this.seteCommerce(false);
					}
					setMenu(campaniaSelecionada.getMenu());
					setNuevo(campaniaSelecionada.getFormulario());
					camposCampania = c.findCamposAdicionalesCampania(campania);
					logger.info("Oreden Menu: " + getMenu());
					if (cliente.getRegistro() == 1) {
//						String rutCliente[] = this.cliente.getRut().split("-");
//						this.rut[0] = rutCliente[0];
//						this.rut[1] = rutCliente[1];
//						this.fechaNacimiento = Utilidad.getFechaDateStr(this.cliente.getFechaNacimiento());
//						Iterator<CategoriaTO> i = this.cliente.getCategorias().iterator();
//						while (i.hasNext()) {
//							CategoriaTO cu = i.next();
//							for (CategoriaTO to : this.listaCategoria) {
//								logger.info("toId: " + to.getId() + " " + cu.getId());
//								if (to.getId() == cu.getId()) {
//									logger.info(true);
//									to.setSeleccionado(true);
//								}
//							}
//						}
						registrado = true;
					}
				}else {
					this.unsubscribe = true;
					campania = c.getCampaniaCliente(cliente.getFecha_registro(), cliente.getEmpresa_id(), cliente.getTipoCampania().getId());
					logger.info("Campania del cliente: " + campania);
					empresa = cliente.getEmpresa_id();
					CampaniaTO campaniaSelecionada = c.findCampaniasFidelizacionById(campania, empresa);
					setMenu(campaniaSelecionada.getMenu());
				}
				
			}
		} else {
			this.cliente = new ClienteTO();
		}
		logger.info("----------------------end----------------------------");
		return "";
	}

	public String ingresarCliente() {
		if(this.extranjero) {
			this.cliente.setRut(idExtranjero);
		}else {
			this.cliente.setRut(rut[0] + "-" + rut[1]);
		}
		this.cliente.setFechaNacimiento(Utilidad.getFechaDate(fechaNacimiento));
		this.cliente.setRegistro(1);
		this.cliente.setFecha_actualizacion(new java.util.Date());
		this.validarFormulario(cliente);
		if (validado) {
			logger.info("Ingresando Usuario");
			Controller c = Controller.getInstance();

			c.updateCliente(cliente);

			List<CategoriaTO> categoriasSeleccionadas = new LinkedList<CategoriaTO>();
			Iterator i = listaCategoria.iterator();
			while (i.hasNext()) {
				CategoriaTO categoria = (CategoriaTO) i.next();
				if (categoria.isSeleccionado()) {
					c.clienteAddCategoria(this.cliente.getId(), categoria.getId());
				}
			}

			if (camposCampania != null && camposCampania.size() > 0) {
				c.guardaCampoAdicionalesCliente(camposCampania, campania, cliente.getId());
			}

			registrado = true;

		}
		logger.info(validado);
		return "";
	}

	public void validarFormulario(ClienteTO cliente) {
		logger.info("Validando cliente:" + cliente);
		this.validado = true;
		mensajes = new LinkedList<MensajeTO>();
		Controller c = Controller.getInstance();
		List<ValidacionTO> validaciones = c.validaCliente(cliente);
		Iterator<ValidacionTO> i = validaciones.iterator();
		while (i.hasNext()) {
			ValidacionTO validacion = i.next();
			if (!validacion.isValido() && this.validado == true) {
				this.validado = false;
			}
			logger.info(validacion);
			this.mensajes.add(validacion.getMensaje());
		}
	}

	public boolean isRegistrado() {
		return registrado;
	}

	public void setRegistrado(boolean registrado) {
		this.registrado = registrado;
	}

	public FormularioTO getNuevo() {
		return nuevo;
	}

	public void setNuevo(FormularioTO nuevo) {
		this.nuevo = nuevo;
	}

}
