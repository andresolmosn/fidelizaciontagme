package com.puma.fidelizacion.bean;

import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.MensajeTO;

@ManagedBean
public class AlertMailBean extends BaseBean{

	private List<MensajeTO> listaMensajes;
	
	public List<MensajeTO> getListaMensajes() {
		return listaMensajes;
	}

	public void setListaMensajes(List<MensajeTO> listaMensajes) {
		this.listaMensajes = listaMensajes;
	}

	public String init()
	{
		Controller c = new Controller();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		Map params = externalContext.getRequestParameterMap();
		String id = (String) params.get("id");
		
		listaMensajes = c.validacion(Integer.parseInt(id));
		
		
		return "";
	}

}
