package com.puma.fidelizacion.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.MensajeTO;

@ManagedBean(name="validarCuponBean")
@ViewScoped
public class ValidarCuponBean extends BaseBean {
	
	private static final Long serialVersionUID = 1L;
	
	static final Logger logger = Logger.getLogger(ValidarCuponBean.class);
	
	private String codigo;
	private CuponTO cupon;
	private ClienteTO cliente;
	private int empresaId;
	private CampaniaTO campania;
	private boolean cuponDisponible;
	
	public void init() {
		isSecure();
		this.empresaId = this.usuario.getIdEmpresa();
	}
	
	public void findCupon()
	{
		Controller c = Controller.getInstance();
		Hashtable ht = c.getCupon(this.empresaId, this.codigo);
		
		
		MensajeTO mensaje = (MensajeTO)ht.get("MENSAJE");
		if(mensaje.getTipo() != MensajeTO.ERROR) {
			Object obj = (Object)ht.get("DATA");
			Cupon cup= (Cupon) obj;
			this.cliente = new ClienteTO(cup.getCliente());
			this.campania = c.findCampaniasFidelizacionById(cup.getCampaniaId(), cup.getEmpresaId());
			this.cupon = new CuponTO(cup);
			
			if(cupon.getFecha_utilizacion() == null) {
				logger.info("la fecha es nula");
				logger.info("fecha ===> " + cupon.getFecha_utilizacion() );
				this.cuponDisponible = true;
			}else {
				logger.info("la fecha no es nula");
				logger.info("fecha ===> " + cupon.getFecha_utilizacion() );
				this.cuponDisponible = false;
			}
			
			logger.info("cliente ===> " + cliente );
			logger.info("campania ===> " + campania );
			logger.info("cup�n ===> " + cupon );
		}
		deleteAllMensajes();
		addMensaje(mensaje);
	}
	
	public void setFechaUtilizacion()
	{
		Controller c = Controller.getInstance();
		MensajeTO mensaje = c.setFechaUtilizacion(this.cupon.getId());
		if (mensaje.getTipo().equals(MensajeTO.SUCCESS))
		{
			this.cuponDisponible = false;
			this.cupon.setFecha_utilizacion(new Date());
		}
		deleteAllMensajes();
		addMensaje(mensaje);
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public CuponTO getCupon() {
		return cupon;
	}

	public void setCupon(CuponTO cupon) {
		this.cupon = cupon;
	}

	public ClienteTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteTO cliente) {
		this.cliente = cliente;
	}

	public CampaniaTO getCampania() {
		return campania;
	}

	public void setCampania(CampaniaTO campania) {
		this.campania = campania;
	}

	public boolean isCuponDisponible() {
		return cuponDisponible;
	}

	public void setCuponDisponible(boolean cuponDisponible) {
		this.cuponDisponible = cuponDisponible;
	}
	
	
	

}
