package com.puma.fidelizacion.bean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.Document;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.util.Propiedades;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name="admCuponBean")
@ViewScoped
public class AdminsitracionCuponBean extends BaseBean {
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdminsitracionCuponBean.class);

	private Part file;
	private String textArea;
	private int idCampania;
	private int condicionSeleccionada;
	private List<CampaniaTO> campanias;
	private List<CondicionTO> condiciones;
	private List<CargaMasivaTO> cargaMasiva;
	private int descuento;
	private String filePath;
	private CampaniaTO campaniaActual;
	private int cantidadDisponible;
	
	
	public int getCantidadDisponible() {
		return cantidadDisponible;
	}

	public void setCantidadDisponible(int cantidadDisponible) {
		this.cantidadDisponible = cantidadDisponible;
	}

	public CampaniaTO getCampaniaActual() {
		return campaniaActual;
	}

	public void setCampaniaActual(CampaniaTO campaniaActual) {
		this.campaniaActual = campaniaActual;
	}

	public Part getFile() {
		return file;
	}
	
	public void setFile(Part file) {
		this.file = file;
	}

	public String getTextArea() {
		return textArea;
	}

	public void setTextArea(String textArea) {
		this.textArea = textArea;
	}

	public int getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	
	public int getDescuento() {
		return descuento;
	}

	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	
	

	public List<CondicionTO> getCondiciones() {
		return condiciones;
	}

	public void setCondiciones(List<CondicionTO> condiciones) {
		this.condiciones = condiciones;
	}
	
	public int getCondicionSeleccionada() {
		return condicionSeleccionada;
	}

	public void setCondicionSeleccionada(int condicionSeleccionada) {
		this.condicionSeleccionada = condicionSeleccionada;
	}
	
	public List<CargaMasivaTO> getCargaMasiva() {
		return cargaMasiva;
	}

	public void setCargaMasiva(List<CargaMasivaTO> cargaMasiva) {
		this.cargaMasiva = cargaMasiva;
	}
	
	

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void changeCampania(AjaxBehaviorEvent event) {
		Controller cont = Controller.getInstance();
		logger.info("changeCampania");
		Iterator<CampaniaTO> i = campanias.iterator();
		while(i.hasNext())
		{
			CampaniaTO c = i.next();
			if(c.getId() == idCampania) {
				setCampaniaActual(c); 
				cargaMasiva = cont.findAllCargaMasivaByCampania(c.getId());
				 cantidadDisponible = cont.getCantCuponesLibres(c.getEmpresaId(),c.getId());
				break;
			}
		}
	}
	public String init()
	{
		isSecure();
		Controller c = Controller.getInstance();
		logger.info("Usuario: " + this.usuario);
		this.campanias = c.findAllCampaniasVigentesByEmpresa(this.usuario.getIdEmpresa());
		this.condiciones = c.findAllCondicionByEmpresa(this.usuario.getIdEmpresa());
//		this.cargaMasiva = c.findAllCargaMasivaByEmpresa(this.usuario.getIdEmpresa());
		return "";
	}
	
	public void cargarCupones()
	{
		Controller c = Controller.getInstance();
		int condicionId = ingresarCondicion(this.textArea, this.usuario.getIdEmpresa());
		
		this.filePath = System.getProperty("CARPETA_ARCHIVOS")+ "cupones/";
		
		List<String> cupones = null;
		Document d = new Document();
		CargaMasivaTO cargaMasiva = null;
		try {
			String fileName = "Cupones" +(new Date()).getTime()+".xls";
			cargaMasiva = ingresarCargaMasiva(this.file.getInputStream(), fileName);
			cupones = d.readDoc(fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if(cupones != null && cupones.size() > 0)
		{
			logger.info("Creando condicion : " + this.textArea);
			this.deleteAllMensajes();
			this.addMensaje(c.addCarga_MasivaWithCupones(cupones, this.descuento, this.idCampania, condicionId, usuario.getIdEmpresa(), cargaMasiva));
		}
		this.cargaMasiva = c.findAllCargaMasivaByCampania(idCampania);
	}
	
	private int ingresarCondicion(String text, int empresaId) 
	{
		Controller c = Controller.getInstance();
		int condicionId = 0;
		if(this.condiciones != null && this.condiciones.size() > 0)
		{
			Iterator<CondicionTO> i = this.condiciones.iterator();
			while(i.hasNext()) {
				CondicionTO to = i.next();
				if(to.getTexto().trim().replaceAll("(\r\n|\n)", "<br />").equalsIgnoreCase(textArea.trim().replaceAll("(\r\n|\n)", "<br />"))) {
					return to.getId();
				}
			}
		}
		
		String nuevaCondicion = textArea.replaceAll("(\r\n|\n)", "<br />");
		return c.addCondicion(nuevaCondicion, empresaId);
	}
	
	private CargaMasivaTO ingresarCargaMasiva(InputStream file_input ,String fileName)
	{
		Propiedades p = new Propiedades();
//		String url = p.getValor("FILE_PATH") +fileName;
		String url = this.filePath + fileName;
		logger.info(url);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(url);
			try {
				IOUtils.copy(file_input, fos);
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		CargaMasivaTO cargaMasivaTO = new CargaMasivaTO();
		cargaMasivaTO.setNombre(fileName.trim());
		cargaMasivaTO.setFecha(Utilidad.getFechaDateStr(new java.util.Date()));
		return cargaMasivaTO;
	}
	
	
	 
	public void eliminarCargaMasiva()
	{
		this.deleteAllMensajes();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("id"));
		logger.info("Eliminar CargaMasiva :" + id);
		Controller c = Controller.getInstance();
		this.addMensaje(c.eliminarCargaMasiva(id, this.usuario.getIdEmpresa()));
		cargaMasiva = c.findAllCargaMasivaByCampania(this.campaniaActual.getId());
	}
	
}
