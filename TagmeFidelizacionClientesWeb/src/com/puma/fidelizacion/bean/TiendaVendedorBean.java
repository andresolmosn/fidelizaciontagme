package com.puma.fidelizacion.bean;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.to.UsuarioTO;

@ManagedBean(name = "tiendaVendedorBean")
@ViewScoped
public class TiendaVendedorBean extends BaseBean {
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(TiendaVendedorBean.class);

	private List<TiendaTO> tiendas;
	private TiendaTO nuevo;
	private UsuarioTO nuevoUsuario;
	private List<UsuarioTO> vendedores;
	private int idTienda;
	private boolean tiendaMod=false;
	private boolean vendedorMod=false;
	private String contrasena;
	private String contrasenaConfirmada;
	
	public List<UsuarioTO> getVendedores() {
		return vendedores;
	}

	public void setVendedores(List<UsuarioTO> vendedores) {
		this.vendedores = vendedores;
	}

	public int getIdTienda() {
		return idTienda;
	}

	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}

	public List<TiendaTO> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<TiendaTO> tiendas) {
		this.tiendas = tiendas;
	}

	public TiendaTO getNuevo() {
		return nuevo;
	}

	public void setNuevo(TiendaTO nuevo) {
		this.nuevo = nuevo;
	}
	

	public boolean isTiendaMod() {
		return tiendaMod;
	}

	public void setTiendaMod(boolean tiendaMod) {
		this.tiendaMod = tiendaMod;
	}

	public boolean isVendedorMod() {
		return vendedorMod;
	}

	public void setVendedorMod(boolean vendedorMod) {
		this.vendedorMod = vendedorMod;
	}
	
	
	public UsuarioTO getNuevoUsuario() {
		return nuevoUsuario;
	}

	public void setNuevoUsuario(UsuarioTO nuevoUsuario) {
		this.nuevoUsuario = nuevoUsuario;
	}
	
	
	public UsuarioTO getUsuario() {
		return this.usuario;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public String getContrasenaConfirmada() {
		return contrasenaConfirmada;
	}

	public void setContrasenaConfirmada(String contrasenaConfirmada) {
		this.contrasenaConfirmada = contrasenaConfirmada;
	}

	public void init() {
		logger.info(" INIT: ");
		isSecure();	
		this.nuevo = new TiendaTO();
		this.nuevoUsuario = new UsuarioTO();
		Controller c = Controller.getInstance();


		if(usuario.getPerfil().getNombre().equals("Administrador")) {
			tiendas = c.getTiendasActivaByEmpresa(usuario.getIdEmpresa());
		}else if(usuario.getPerfil().getNombre().equals("AdministradorEmpresa")){
			tiendas = c.getTiendasActivaByEmpresa(usuario.getIdEmpresa());
//			tiendas = usuario.getTiendas();
		}else {
			tiendas = usuario.getTiendas();
		}
		
		
		
	}
	
	public void addTienda()
	{
		this.nuevo.setEmpresaId(this.usuario.getIdEmpresa());
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		this.nuevo.setEstado(1);
		addMensaje(c.addTienda(nuevo));
	}
	
	public void selectModificarTienda()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("mod"));
		logger.info("Modificar campania :" + id);
		
		this.nuevo = getTienda(this.tiendas, id);
		this.tiendaMod = true;
		
	}
	
	public void selectModificarVendedor()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("modVendedor"));
		logger.info("Modificar vendedor :" + id);
		
		this.nuevoUsuario = getVendedor(this.vendedores, id);
		this.vendedorMod = true;
		
	}
	
	public void modificarTienda()
	{
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		logger.info("modificando" + nuevo);
		addMensaje(c.updateTienda(nuevo));
		reload();
		this.tiendaMod=false;
	}
	
	public void modificarVendedor()
	{
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		logger.info("modificando" + nuevoUsuario);
		addMensaje(c.updateUsuario(this.nuevoUsuario));
		findUsuariosByTienda(this.idTienda);
		this.vendedorMod=false;
		this.nuevoUsuario = new UsuarioTO();
	}
	
	public void salirModificacionTienda()
	{
		this.tiendaMod=false;
	
		this.nuevo = new TiendaTO();
		this.nuevoUsuario = new UsuarioTO();
	}
	
	public void salirModificacionVendedor()
	{
		this.vendedorMod=false;
		this.nuevoUsuario = new UsuarioTO();
		this.nuevo = new TiendaTO();
	}
	
	
	private TiendaTO getTienda(List<TiendaTO> tiendas, int tiendaId)
	{
		if(tiendas != null && tiendas.size()>0) {
			Iterator<TiendaTO> i = tiendas.iterator();
			while(i.hasNext())
			{
				TiendaTO t = i.next();
				if(t.getId() == tiendaId)
				{
					return t;
				}
			}
		}
		return null;
	}
	
	
	private UsuarioTO getVendedor(List<UsuarioTO> vendedores, int vendedorId)
	{
		if(vendedores != null && vendedores.size()>0) {
			Iterator<UsuarioTO> i = vendedores.iterator();
			while(i.hasNext())
			{
				UsuarioTO t = i.next();
				if(t.getId() == vendedorId)
				{
					return t;
				}
			}
		}
		return null;
	}
	
	
	private void reload()
	{
		this.nuevo = new TiendaTO();
		Controller c = Controller.getInstance();
		tiendas = c.getTiendasActivaByEmpresa(usuario.getIdEmpresa());
		
//		this.idTienda = 0;
	}
	
	public void eliminarTienda()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("id"));
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		addMensaje(c.desactivaTienda(id));
		reload();
		this.tiendaMod=false;
		
	}
	
	public void eliminarVendedor()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("idVendedor"));
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		addMensaje(c.desactivaVendedor(id));
		findUsuariosByTienda(this.idTienda);
		
		
	}
	
	public void findUsuariosByTienda(int tiendaId)
	{
		Controller c = Controller.getInstance();
		this.vendedores = c.findAllVendedoresByTienda(tiendaId, usuario.getId());
		logger.info("lista usuarios:" + this.vendedores);
	}
	
	public void selectTiendaVendedor()
	{
		logger.info("Buscando usuario con id:" + idTienda); 
		if(idTienda != 0)
		{
			findUsuariosByTienda(this.idTienda);
		}else {
			this.vendedores.clear();
		}
		
	}
	
	public void nuevoVendedor()
	{
		Controller c = Controller.getInstance();
		this.nuevoUsuario.setIdEmpresa(this.usuario.getIdEmpresa());
		this.nuevoUsuario.setEstado(1);
		PerfilTO to = new PerfilTO();
		to.setId(Integer.parseInt(c.getParametroByNombre("tagme.app.idPerfilVendedor").getValor()));
		this.nuevoUsuario.setPerfil(to);
		if(this.contrasena.equals(this.contrasenaConfirmada)) {
			this.nuevoUsuario.setPass(passEncript(this.contrasena));
			
			deleteAllMensajes();
			addMensaje(c.addVendedor(this.nuevoUsuario, this.idTienda, this.usuario.getIdEmpresa()));
			this.vendedores = c.findAllVendedoresByTienda(this.idTienda, usuario.getId());
		}else {
			deleteAllMensajes();
			addMensaje(new MensajeTO("Ingresar Vendedor", "Las contraseņas indicadas no coinciden", MensajeTO.ALERT));
		}
		
		
	}
	
	private String passEncript(String pass) {
		return DigestUtils.sha512Hex(pass);
	}
	
	
}
