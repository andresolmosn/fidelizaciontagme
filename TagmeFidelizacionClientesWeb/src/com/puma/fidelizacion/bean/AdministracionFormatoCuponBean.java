package com.puma.fidelizacion.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.glassfish.api.container.RequestDispatcher;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.FormularioTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ValidacionTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name="administracionFormatoCuponBean")
@ViewScoped
public class AdministracionFormatoCuponBean extends BaseBean{

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdministracionFormatoCuponBean.class);
	


	private Part url;
	private FormatoCuponTO formato;
	private int idCampania;
	private List<CampaniaTO> campanias;
	private String urlCupon;
	private String condicion;
	private List<MenuTO> menu;
	private boolean existeMenu;
	private String estadoCampania;
	

	
	public boolean isExisteMenu() {
		existeMenu = (menu!=null && menu.size()>0)?true:false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}
	public List<MenuTO> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getUrlCupon() {
		return urlCupon;
	}
	public void setUrlCupon(String urlCupon) {
		this.urlCupon = urlCupon;
	}
	public Part getUrl() {
		return url;
	}
	public void setUrl(Part url) {
		this.url = url;
	}
	public int getIdCampania() {
		return idCampania;
	}
	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}
	public List<CampaniaTO> getCampanias() {
		return campanias;
	}
	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	public FormatoCuponTO getFormato() {
		return formato;
	}
	public void setFormato(FormatoCuponTO formato) {
		this.formato = formato;
	}
	

	public String getEstadoCampania() {
		return estadoCampania;
	}

	public void setEstadoCampania(String estadoCampania) {
		this.estadoCampania = estadoCampania;
	}

	public void changeCampania(AjaxBehaviorEvent event) {
		Controller c = Controller.getInstance();
		formato = c.findFormatoCuponByCampania(idCampania);
		CampaniaTO campaniaSelecionada = c.findCampaniasFidelizacionById(idCampania,usuario.getIdEmpresa());
		menu=campaniaSelecionada.getMenu();
		estadoCampania = campaniaSelecionada.getEstado();
		this.condicion = "Condici�n de ejemplo.<br /> -Valido en outlets.<br /> -Valido en tiendas full price."; 
		if(formato==null) {
			formato = new FormatoCuponTO();
		}
		logger.info("Mensajes: " + getMensajes() );
	}
	
	public String init() throws IOException{
		this.isSecure();
		logger.info("Usuario :" + this.usuario );
		Controller c = Controller.getInstance();
		EmpresaTO emp= new EmpresaTO();
		emp.setId(usuario.getIdEmpresa());
		campanias = c.findAllCampaniasVigentesByEmpresa(usuario.getIdEmpresa());
		estadoCampania = "ESPERA";
		setMensajes(new LinkedList<MensajeTO>());
		return "";
	}
	public String actualizarCampos() {
		logger.debug("actualizar form cupon");
		deleteAllMensajes();
		if(cargarBanner())
		{
			this.formato.setCampania_id(this.idCampania);
			Controller c = new Controller();
			deleteAllMensajes();
			addMensaje(c.addFormatoCupon(this.formato));
		}
		return "";
	}
	
	public boolean cargarBanner()
	{
		
		if(formato.getUrlFoto() != null)
		{
			if(url != null)
			{
				eliminarBanner(formato.getUrlFoto());
				
				
				String filePath = System.getProperty("CARPETA_ARCHIVOS")+"cupon/";
				
				String extension = url.getSubmittedFileName().substring(url.getSubmittedFileName().length()-4,url.getSubmittedFileName().length());
				String fileName = "banner" +(new Date()).getTime()+extension;
				String url = filePath+fileName;
				int width = 0;
				int height = 0;
				logger.info(url);
				FileOutputStream fos;
					try {
						fos = new FileOutputStream(url);
						try {
							IOUtils.copy(this.url.getInputStream(), fos);
							BufferedImage bimg = ImageIO.read(new File(url));
							width          = bimg.getWidth();
							height         = bimg.getHeight();
							logger.info("imagen : " + width + "x" + height);
						} catch (IOException e) {
							e.printStackTrace();
						}
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}	
					logger.info("imagen : " + width + "x" + height);
					if(width <= 650)
					{
						this.formato.setUrlFoto(fileName);
						logger.info("nombre del archivo: " + this.formato.getUrlFoto());
						return true;
					} else { 
						deleteAllMensajes();
						addMensaje(new MensajeTO("Tama�o incorrecto","El tama�o de la imagen no puede superar los 650px, no se realizo la actualizaci�n para el mail del cupon.", MensajeTO.ERROR));
						eliminarBanner(fileName);
						return false;
					}
			}
		}
		
		return true;
	}
	
	public void eliminarBanner(String filename) {
		String url = System.getProperty("CARPETA_ARCHIVOS")+"cupon/" + filename;
		logger.info(url);
		File fichero = new File(url);
		fichero.delete();
	}
}
