package com.puma.fidelizacion.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.imageio.ImageIO;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.Document;
import com.puma.fidelizacion.db.CampaniaDAO;
import com.puma.fidelizacion.db.model.FormatoMail;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.UsuarioTO;


@ManagedBean(name="admMenuBean")
@ViewScoped
public class AdministracionMenuBean extends BaseBean {
	
	Logger logger = Logger.getLogger(AdministracionMenuBean.class);
	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private String err;
	private String menuOrden;
	private MenuTO nuevoMenu;
	private Part file;

	private List<MenuTO> menu;
	private List<MenuTO> menuPadre;
	private boolean padre;
	private int altoOptimo;
	private List<CampaniaTO> campanias;
	private int idCampania;
	private int anchoMenu;
	private CampaniaTO campaniaActual;
	
	public int getAnchoMenu() {
		return anchoMenu;
	}

	public void setAnchoMenu(int anchoMenu) {
		this.anchoMenu = anchoMenu;
	}

	public int getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}

	public int getAltoOptimo() {
		return altoOptimo;
	}

	public void setAltoOptimo(int altoOptimo) {
		this.altoOptimo = altoOptimo;
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}

	public MenuTO getNuevoMenu() {
		return nuevoMenu;
	}

	public void setNuevoMenu(MenuTO nuevoMenu) {
		this.nuevoMenu = nuevoMenu;
	}

	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}
	
	public boolean isPadre() {
		return padre;
	}

	public void setPadre(boolean padre) {
		this.padre = padre;
	}
	
	public List<MenuTO> getMenuPadre() {
		return menuPadre;
	}

	public void setMenuPadre(List<MenuTO> menuPadre) {
		this.menuPadre = menuPadre;
	}

	public String getMenuOrden() {
		return menuOrden;
	}

	public void setMenuOrden(String menuOrden) {
		this.menuOrden = menuOrden;
	}
	
	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	
	public CampaniaTO getCampaniaActual() {
		return campaniaActual;
	}

	public void setCampaniaActual(CampaniaTO campaniaActual) {
		this.campaniaActual = campaniaActual;
	}
	
	

	public String init()
	{
		
		this.isSecure();
		logger.info("Usuario :" + this.usuario );
		Controller c = Controller.getInstance();
		this.nuevoMenu = new MenuTO();
		EmpresaTO emp= new EmpresaTO();
		emp.setId(usuario.getIdEmpresa());
		//menuPadre = c.findAllMenuPadreByEmpresa(usuario.getIdEmpresa());
		campaniaActual = new CampaniaTO();
		campaniaActual.setEstado("ESPERA");
		campanias = c.findAllCampaniasVigentesByEmpresa(usuario.getIdEmpresa());
		return "";
	}
	public void actualizarPanel() {
		anchoMenu=0;
		Controller c = Controller.getInstance();
		menuPadre = c.findAllMenuByCampania(idCampania);
		if(menuPadre!=null&&menuPadre.size()>0) {
			Iterator<MenuTO> it = menuPadre.iterator();
			while(it.hasNext()) {
				MenuTO m = it.next();
				anchoMenu += m.getAnchoImagen();
				altoOptimo = m.getAltoImagen(); 
			}
		}
	}
	public void changeCampania(AjaxBehaviorEvent event) {
		logger.info("changeCampania");
		actualizarPanel();
		Iterator<CampaniaTO> i = campanias.iterator();
		while(i.hasNext())
		{
			CampaniaTO c = i.next();
			if(c.getId() == idCampania) {
				setCampaniaActual(c);
			}
		}
		logger.debug(menuPadre!=null ? menuPadre.size()+"" : "VACIO");
	}
	public void crearMenu()
	{
		this.err = null;
		this.mensaje = null;
		
		if(this.isPadre())
		{
			this.nuevoMenu.setUrl(null);
			this.nuevoMenu.setIdMenuPadre(1);
		}
		if(this.nuevoMenu.getIdMenuPadre() == 0)
		{
			this.nuevoMenu.setIdMenuPadre(1);
		}
		
		String filePath = System.getProperty("CARPETA_ARCHIVOS")+"/menu/";
		
		String extension = file.getSubmittedFileName().substring(file.getSubmittedFileName().length()-4,file.getSubmittedFileName().length());
			String fileName = "opcion" +(new Date()).getTime()+extension;
			String url = filePath+fileName;
			logger.info(url);
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(url);
				try {
					IOUtils.copy(file.getInputStream(), fos);
					BufferedImage bimg = ImageIO.read(new File(url));
					int width          = bimg.getWidth();
					int height         = bimg.getHeight();
					
					nuevoMenu.setAnchoImagen(width);
					nuevoMenu.setAltoImagen(height);
					CampaniaTO t = new CampaniaTO();
					t.setId(idCampania);
					nuevoMenu.setCampania(t);
				} catch (IOException e) {
					e.printStackTrace();
					
				}
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
			nuevoMenu.setImagen(fileName);
			
		Controller c = Controller.getInstance();
		this.nuevoMenu.setEmpresa(this.usuario.getIdEmpresa());
		setMensajes(new LinkedList<MensajeTO>());
		if(this.altoOptimo == 0 && nuevoMenu.getAltoImagen() > 80)
		{
			this.addMensaje(new MensajeTO("Error al ingresar Menu","La imagen no puede superar los 80px de alto.", MensajeTO.ALERT));
			eliminarImage(nuevoMenu);
		}else if(nuevoMenu.getAltoImagen() > this.altoOptimo && this.altoOptimo > 0)
		{
			this.addMensaje(new MensajeTO("Error al ingresar Menu","El alto de la imagen no coincide con la otras.", MensajeTO.ALERT));
			eliminarImage(nuevoMenu);
		}else if(this.anchoMenu + nuevoMenu.getAnchoImagen() > 661)
		{
			this.addMensaje(new MensajeTO("Error al ingresar Menu","La imagen que quieres agregar, excedes el m�ximo de 610px.", MensajeTO.ALERT));
			eliminarImage(nuevoMenu);
		}else {
			this.addMensaje(c.addMenu(this.nuevoMenu));
			
			//	menuPadre = c.findAllMenuByCampania(idCampania);
				actualizarPanel();
//				if(transaccion == 1)
//				{
//					this.mensaje="Se ha creado la nueva categoria: " + this.nuevaCategoria.getTexto();
//				} else {
//					this.err="Error al crear categoria. Revisa que el valor no sea nulo o que no esta duplicado";
//				}
//				logger.info(mensaje);
				
				this.nuevoMenu = new MenuTO();
		}
		
	}
	
	public void eliminarImage(MenuTO menu) {
		String url = System.getProperty("CARPETA_ARCHIVOS")+"/menu/" + menu.getImagen();
		logger.info(url);
		File fichero = new File(url);
		fichero.delete();
	}
	
	
	public String modificarPosicionMenu()
	{
		
		logger.info("Menu :" + menuOrden);
		Controller c=Controller.getInstance();
		String orden = getMenuOrden();
		StringTokenizer st = new StringTokenizer(orden,"/");
		List<MenuTO> raiz = new LinkedList<MenuTO>();
		int cantidadElementos = st.countTokens();
		while (st.hasMoreTokens()){
			int menuId = Integer.parseInt(st.nextToken());
 			for(int i = 0; i< this.menuPadre.size();i++){
 				MenuTO tmp2 = (MenuTO) this.menuPadre.get(i);
 				logger.info("POSISCION :" + i);
 				if(menuId == tmp2.getId()){
 					logger.info("POSISCION :" + (cantidadElementos - st.countTokens()));
 					tmp2.setPosicion(cantidadElementos - st.countTokens());
 					this.deleteAllMensajes();
 					addMensaje(c.updatePosicionMenu(tmp2));
 				}
 			}
		}
		actualizarPanel();
//		menuPadre = c.findAllMenuByCampania(idCampania);
		return "";
	}
	
	public void eliminarMenu(int x)
	{
		setMensajes(new LinkedList<MensajeTO>());
		logger.info(x);
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		Map params = facesContext.getExternalContext().getRequestParameterMap();
//		Integer id = new Integer((String) params.get("id"));
		Controller c = Controller.getInstance();
		addMensaje(c.deleteMenu(x));
		actualizarPanel();
//		menuPadre = c.findAllMenuByCampania(idCampania);
		
	}
	
	

}
