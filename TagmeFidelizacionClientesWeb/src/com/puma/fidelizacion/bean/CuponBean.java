
package com.puma.fidelizacion.bean;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.SendMail;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.UsuarioTO;
@ManagedBean(name = "cuponBean")
@SessionScoped
public class CuponBean extends BaseBean {
	final static Logger logger = Logger.getLogger(LoginBean.class);
	protected static final long serialVersionUID = 1L;
	private List<MenuTO> menu;
	private String to;
	private List<FormatoMailTO> fotos;
	private String urlCupon;
	private String condicion;
	private String urlPropia;
	private String parametro;
	private CuponTO cupon;
	private boolean existeMenu;
	private FormatoCuponTO formato;
	
	
	public FormatoCuponTO getFormato() {
		return formato;
	}

	public void setFormato(FormatoCuponTO formato) {
		this.formato = formato;
	}

	public boolean isExisteMenu() {
		existeMenu = (menu!=null && menu.size()>0)?true:false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}
	
	public CuponTO getCupon() {
		return cupon;
	}
	public void setCupon(CuponTO cupon) {
		this.cupon = cupon;
	}
	public String getParametro() {
		return parametro;
	}
	public void setParametro(String parametro) {
		this.parametro = parametro;
	}
	public String getUrlPropia() {
		return urlPropia;
	}
	public void setUrlPropia(String urlPropia) {
		this.urlPropia = urlPropia;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getUrlCupon() {
		return urlCupon;
	}
	public void setUrlCupon(String urlCupon) {
		this.urlCupon = urlCupon;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public List<MenuTO> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}
	public List<FormatoMailTO> getFotos() {
		return fotos;
	}
	public void setFotos(List<FormatoMailTO> fotos) {
		this.fotos = fotos;
	}
	public void agregarFoto( MimeMultipart multipart, String foto, String id) {
		try {
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(foto);
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", id);
			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	
	public String init() {
		logger.debug("init");
		parametro= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
		System.out.println("parametro: "+parametro);
		if(parametro!=null && parametro.length()>0) {
//			urlCupon = "https://"+getDireccionServer()+"/TagmeFidelizacion/cuponbarcode.xhtml?id="+parametro;
//			urlPropia = "https://"+direccionServer+"/TagmeFidelizacion/cupon.xhtml?id="+parametro;
			urlCupon = "https://"+getDireccionServer()+"/TagmeFidelizacion/cuponbarcode.xhtml?id="+parametro;
			urlPropia = "https://"+direccionServer+"/TagmeFidelizacion/cupon.xhtml?id="+parametro;
			Controller c = Controller.getInstance();
			 cupon = c.getCupon(parametro);
			if(cupon!=null) {
				CampaniaTO campaniaSelecionada = c.findCampaniasFidelizacionById(cupon.getCampaniaId(),cupon.getEmpresaId());
				logger.info("--------------------------------------------- enviando cupon para la campania -------------------------------------------------------");
				logger.info(campaniaSelecionada);
				logger.info("--------------------------------------------- ------------------------------- -------------------------------------------------------");
				menu=campaniaSelecionada.getMenu();
				  formato = c.findFormatoCuponByCampania(cupon.getCampaniaId());
				CondicionTO con = c.getCondicionById(cupon.getCondicionId());
				logger.info(con.getTexto());
				condicion = traducir(con.getTexto());
			}
		}else {
			logger.info("cliente desconocido");
		}
		return "";
	}
	
	private String traducir(String texto) {
		texto = texto.replaceAll("�","&aacute;" );
		texto = texto.replaceAll("�","&eacute;" );
		texto = texto.replaceAll("�","&iacute;" );
		texto = texto.replaceAll("�","&oacute;" );
		texto = texto.replaceAll("�","&uacute;" );
		texto = texto.replaceAll("�","&ntilde;" );
		return texto;
	}

	public void generar() {
		logger.info("generando EAN");
		
		if(cupon!=null) {
		    String strCode = cupon.getCodigo();
		    if(strCode!=null && strCode.length()>0) {
		    try {
		    	EAN13Bean bean = new EAN13Bean();
		             
		             final int dpi = 150;
		      
		             bean.setMsgPosition(HumanReadablePlacement.HRP_BOTTOM);
		             bean.setModuleWidth(0.5);
		             bean.setBarHeight(20.0);
		             bean.setFontSize(5.0);
		             bean.setQuietZone(5.0);
		             bean.doQuietZone(true);
		             String fileName=System.getProperty("FILE_PUBLICO")+"bc_"+(new Date()).getTime()+".png";
		             File outputFile = new File(fileName);
		             OutputStream out = new FileOutputStream(outputFile);
		      
		             try {
		      
		                 //Set up the canvas provider for monochrome PNG output
		                 BitmapCanvasProvider canvas = new BitmapCanvasProvider(
		                     out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
		      
		                 //Generate the barcode
		                 bean.generateBarcode(canvas, strCode);
		      
		                 //Signal end of generation
		                 canvas.finish();
		             } finally {
		                 out.close();
		             }
		             
			logger.info("abrir");
			File initialFile = new File(fileName);
		    InputStream targetStream = new FileInputStream(initialFile);
			BufferedInputStream in = new BufferedInputStream(targetStream);
			byte[] bytes = new byte[in.available()];
			in.read(bytes);
			in.close();
			 FacesContext facesContext = FacesContext.getCurrentInstance(); //Get the context ONCE
		     HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		    ServletOutputStream servletOutputStream = response.getOutputStream();
		    response.setContentType("image/png");
		    facesContext.responseComplete();
		    
		    servletOutputStream.write(bytes);
		    servletOutputStream.flush();
		    servletOutputStream.close();
		    logger.info("enviado");
		    initialFile.delete();
		    
		    
		} catch (Exception ex) {
			ex.printStackTrace();
		   }
		}
		}
	}
}