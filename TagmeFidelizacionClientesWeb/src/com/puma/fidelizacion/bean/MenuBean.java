package com.puma.fidelizacion.bean;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.persistence.PreUpdate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.UsuarioTO;
 

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
 

@ManagedBean (name="menuBean")
@SessionScoped
public class MenuBean extends BaseBean{
	
		private static final Logger logger = Logger.getLogger(MenuBean.class);
	 	
		private List<MenuTO> opciones;
		
		public String cerrarSesion(){
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
//			setOpciones(null);
//			setUsuario(null);
			return "login.xhtml?faces-redirect=true";
		}
		
		public String init()
		{
			isSecure();
			return "";
		}
		
		public void inicio(ComponentSystemEvent event){
//		public void init(){
			 System.out.println("INIT menu");
//			Controller c = Controller.getInstance();
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			UsuarioTO user = (UsuarioTO) sessionMap.get("usuario");
			setUsuario(user);
			if(user == null ){
				System.out.println("no user");
				FacesContext context = FacesContext.getCurrentInstance();
				HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
				try {
					response.sendRedirect("login.xhtml?faces-redirect=true");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
//				if(opciones==null || (opciones != null && opciones.size()==0)){
//					try {
//						opciones = c.getMenuByUser(usuario.getSeq());
//					} catch (SQLException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
			}
		}
	
		public String cerrar(){
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
//			setOpciones(null);
			return "login.xhtml?faces-redirect=true";
		}
//		public List<MenuTO> getOpciones() {
//			return opciones;
//		}
//
//		public void setOpciones(List<MenuTO> opciones) {
//			this.opciones = opciones;
//		}

		public UsuarioTO getUsuario() {
			return usuario;
		}

		public void setUsuario(UsuarioTO usuario) {
			this.usuario = usuario;
		}

		public List<MenuTO> getOpciones() {
			return opciones;
		}

		public void setOpciones(List<MenuTO> opciones) {
			this.opciones = opciones;
		}
		
		
		
}