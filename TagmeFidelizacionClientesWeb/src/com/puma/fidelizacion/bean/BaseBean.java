package com.puma.fidelizacion.bean;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.UsuarioTO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

 
public class BaseBean implements Serializable {
	final static Logger loggerb = Logger.getLogger(BaseBean.class);
		protected static final long serialVersionUID = 1L;
		protected String mensaje=null;
		protected String mensaje1;
		protected String error="";
		protected UsuarioTO usuario;
		protected String direccionServer;
		protected List<MensajeTO> mensajes= new LinkedList<MensajeTO>();
		
		private String ingresarAqui;
		private String clickAqui;
		

		
		public String getClickAqui() {
			clickAqui="haz click aqu&iacute;";
			return clickAqui;
		}

		public void setClickAqui(String clickAqui) {
			this.clickAqui = clickAqui;
		}

		public String getIngresarAqui() {
			ingresarAqui="ingrese aqu&iacute;";
			return ingresarAqui;
		}

		public void setIngresarAqui(String ingresarAqui) {
			this.ingresarAqui = ingresarAqui;
		}

		public String getDireccionServer() {
			 HttpServletRequest request =
			            (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
			        direccionServer  =  request.getHeader("host");
			return direccionServer;
		}

		public void setDireccionServer(String direccionServer) {
			this.direccionServer = direccionServer;
		}

		
		
	 	public  void  isSecure() {
	 		loggerb.debug("INIT BASE");
	 		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			UsuarioTO user = (UsuarioTO) sessionMap.get("usuario");
			 if(user!=null){
				 	usuario = user;
				    
			 }else {
				 try {
					 loggerb.debug("redirect");
				    	externalContext.redirect(externalContext.getRequestContextPath() + "/session.xhtml");
					} catch (IOException e) {
						e.printStackTrace();
					}
			 }
	 	}
	 		 	
	 	void emptyMessage(){
	 		mensaje="";
	 		mensaje1="";
	 		error="";
	 	}

		public String getError() {
			return error;
		}

		public void setError(String error) {
			this.error = error;
		}

		public String getMensaje() {
			return mensaje;
		}

		public void setMensaje(String mensaje) {
			this.mensaje = mensaje;
		}

		public List<MensajeTO> getMensajes() {
			return mensajes;
		}

		public void setMensajes(List<MensajeTO> mensajes) {
			this.mensajes = mensajes;
		}

		public void addMensaje(MensajeTO mensaje)
		{
			this.mensajes.add(mensaje);
		}
		
		public void deleteAllMensajes()
		{
			this.mensajes.clear();
		}
		
	 	
}