package com.puma.fidelizacion.bean;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.UsuarioTO;


@ManagedBean(name="administracionBean")
@ViewScoped
public class AdministracionBean extends BaseBean{
	
	private static final long serialVersionUID = 1L;
	
	
	static final Logger logger = Logger.getLogger(AdministracionBean.class);
	
	private CategoriaTO nuevaCategoria;
	private String mensaje;
	private String err;
	private String categoria;
	private String categoriasSinVigencia;
	private List<CategoriaTO> categoriasVisibles;
	private List<CategoriaTO> categoriasNoVisibles;
	private List<CategoriaTO> categorias;

	public CategoriaTO getNuevaCategoria() {
		return nuevaCategoria;
	}

	public void setNuevaCategoria(CategoriaTO nuevaCategoria) {
		this.nuevaCategoria = nuevaCategoria;
	}


	public String getErr() {
		return err;
	}

	public void setErr(String err) {
		this.err = err;
	}

	public String getCategoriasSinVigencia() {
		return categoriasSinVigencia;
	}

	public void setCategoriasSinVigencia(String categoriasSinVigencia) {
		this.categoriasSinVigencia = categoriasSinVigencia;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public List<CategoriaTO> getCategoriasVisibles() {
		return categoriasVisibles;
	}

	public void setCategoriasVisibles(List<CategoriaTO> categoriasVisibles) {
		this.categoriasVisibles = categoriasVisibles;
	}

	public List<CategoriaTO> getCategoriasNoVisibles() {
		return categoriasNoVisibles;
	}

	public void setCategoriasNoVisibles(List<CategoriaTO> categoriasNoVisibles) {
		this.categoriasNoVisibles = categoriasNoVisibles;
	}
	
	public List<CategoriaTO> getCategorias() {
		return categorias;
	}

	public void setCategorias(List<CategoriaTO> categorias) {
		this.categorias = categorias;
	}
	
	

	 

	public String init() {
		

		this.isSecure();
		logger.info("ID de empresa del usuario :" + usuario.getIdEmpresa());
		reload();
		
		logger.info(categorias);
		return "";
	}
	
	public void reload() {
		Controller c =  Controller.getInstance();
		this.nuevaCategoria = new CategoriaTO();
		this.categoriasVisibles = c.findAllCategoriasByEmpresa(this.usuario.getIdEmpresa(),1);
		this.categoriasNoVisibles = c.findAllCategoriasByEmpresa(this.usuario.getIdEmpresa(),0);
		this.categorias = c.findAllCategoriasByEmpresa(this.usuario.getIdEmpresa(),3);
	}
	
	public void crearCategoria()
	{
		Controller c =  Controller.getInstance();
		this.nuevaCategoria.setEmpresa(this.usuario.getIdEmpresa());
		setMensajes(new LinkedList<MensajeTO>());
		addMensaje(c.addCategoria(this.nuevaCategoria));
		this.nuevaCategoria = new CategoriaTO();
		reload();
	}
	
	public String agregar()
	{
		logger.info("CATEGORIAS :" + categoria);
		Controller c=Controller.getInstance();
		String categorias = getCategoria();
		StringTokenizer st = new StringTokenizer(categorias,"/");
		List<CategoriaTO> visibles = new LinkedList<CategoriaTO>();
		int cantidadEliementos = st.countTokens();
		while (st.hasMoreTokens()){
			int categoria= Integer.parseInt(st.nextToken());
 				for(int i = 0; i<getCategorias().size();i++){
 					CategoriaTO tmp2 = (CategoriaTO) getCategorias().get(i);
 					if(categoria == tmp2.getId()){
 						logger.info("POSISCION :" + (cantidadEliementos - st.countTokens()));
 						tmp2.setPrioridad(cantidadEliementos - st.countTokens());
 						tmp2.setVigente(1);
 						c.updatePrioridadCategoria(tmp2);
 						visibles.add(tmp2);
 					}
 				}
		}
		
		
		logger.info(" --------------------------- CATEGORIAS SIN VIGENCIA:" + getCategoriasSinVigencia());
		String categoriasSinVigencia = getCategoriasSinVigencia();
		StringTokenizer st2 = new StringTokenizer(categoriasSinVigencia,"/");
		List<CategoriaTO> noVisibles = new LinkedList<CategoriaTO>();
		int cantidad = st2.countTokens();
		while (st2.hasMoreTokens()){
			int categoria= Integer.parseInt(st2.nextToken());
 				for(int i = 0; i<getCategorias().size();i++){
 					CategoriaTO tmp2 = (CategoriaTO) getCategorias().get(i);
 					if(categoria == tmp2.getId()){
 						logger.info("POSISCION :" + (cantidad - st2.countTokens()));
 						tmp2.setPrioridad(cantidad - st2.countTokens());
 						tmp2.setVigente(0);
 						c.updatePrioridadCategoria(tmp2);
 						noVisibles.add(tmp2);
 					}
 				}
		}
		deleteAllMensajes();
		addMensaje(new MensajeTO("Categorias","Se actualizo correctamente el orden de las categorias",MensajeTO.SUCCESS));
		logger.debug("guardar: "+visibles);	
		reload();
		return "";
	}
	
	public void eliminar()
	{
		logger.info("Eliminar Categoria :");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("id"));
		Controller c =  Controller.getInstance();
		deleteAllMensajes();
		addMensaje(c.deleteCategoria(id));
		reload();
			
	}
	
	
	
	
	
	
}
