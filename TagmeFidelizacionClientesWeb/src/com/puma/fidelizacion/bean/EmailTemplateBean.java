
package com.puma.fidelizacion.bean;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.Document;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.util.Propiedades;


@ManagedBean(name="emailTemplateBean")
@ViewScoped
public class EmailTemplateBean extends BaseBean {
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(EmailTemplateBean.class);
	
	private Part file;
	private List<CampaniaTO> campanias;
	private int idCampania;
	private String urlForm;
	private String textoAlternativo;
	private int condicionSeleccionada;
	private List<FormatoMailTO> detalle;
	private boolean existeMenu;
	private List<MenuTO> menu;
	private List<FormatoMailTO> fotos;
	private CampaniaTO campaniaActual;
	private String logo;
	private String texto1;
	private String texto2; 
	private String contacto ; 
	
	
	
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTexto1() {
		return texto1;
	}

	public void setTexto1(String texto1) {
		this.texto1 = texto1;
	}

	public String getTexto2() {
		return texto2;
	}

	public void setTexto2(String texto2) {
		this.texto2 = texto2;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public boolean isExisteMenu() {
		existeMenu = (menu!=null && menu.size()>0)?true:false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}
	public List<MenuTO> getMenu() {
		return menu;
	}
	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}
	public void eliminarImagen(int id) {
		logger.info("eliminar: "+id);
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		addMensaje(c.eliminaFormatoMail(id));

		detalle=c.findAllFormatoMailByCampania(idCampania);
	}
	public String getUrlForm() {
		return urlForm;
	}

	public List<FormatoMailTO> getFotos() {
		return fotos;
	}

	public void setFotos(List<FormatoMailTO> fotos) {
		this.fotos = fotos;
	}

	public void setUrlForm(String urlForm) {
		this.urlForm = urlForm;
	}

	public Part getFile() {
		return file;
	}
	
	public void setFile(Part file) {
		this.file = file;
	}

	
	public String getTextoAlternativo() {
		return textoAlternativo;
	}

	public void setTextoAlternativo(String textoAlternativo) {
		this.textoAlternativo = textoAlternativo;
	}

	public int getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}
	
	public List<FormatoMailTO> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<FormatoMailTO> detalle) {
		this.detalle = detalle;
	}

	public int getCondicionSeleccionada() {
		return condicionSeleccionada;
	}

	public void setCondicionSeleccionada(int condicionSeleccionada) {
		this.condicionSeleccionada = condicionSeleccionada;
	}

	public CampaniaTO getCampaniaActual() {
		return campaniaActual;
	}

	public void setCampaniaActual(CampaniaTO campaniaActual) {
		this.campaniaActual = campaniaActual;
	}

	public String init()
	{
		isSecure();
		Controller co = Controller.getInstance();
		int empresaId = usuario.getIdEmpresa();
		this.campanias = co.findAllCampaniasVigentesByEmpresa(this.usuario.getIdEmpresa());
		ParametroTO p1 = co.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.text");
		ParametroTO p2 = co.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.text2");
		ParametroTO p3 = co.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.imagen");
		ParametroTO p4 = co.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.contacto");
		
		texto1= p1.getValor();
		logo= p3.getValor();
		contacto = p4.getValor();
		texto2= p2.getValor();
		return "";
	}
	
	public void changeCampania(AjaxBehaviorEvent event) {
		logger.debug("ChangeFilter " + idCampania);
		
		Controller c = Controller.getInstance();
		if(idCampania==0) {
			detalle=null;
			fotos = null;
			menu=null;
		}else {
			Iterator<CampaniaTO> i = campanias.iterator();
			while(i.hasNext())
			{
				CampaniaTO camp = i.next();
				if(camp.getId() == idCampania) {
					setCampaniaActual(camp);
				}
			}
			detalle=c.findAllFormatoMailByCampania(idCampania);
			CampaniaTO to = c.findCampaniasFidelizacionByEmpresa(usuario.getIdEmpresa(),idCampania);
			fotos = to.getFormato();
			setMenu(c.findAllMenuPadreByEmpresa(usuario.getIdEmpresa(),idCampania));
		}
	}
	
	
	public void cargarImagenes(){
		logger.debug("cargarImagenes");
		Controller c = Controller.getInstance();
		List<String> cupones = null;
		Document d = new Document();
		
		logger.debug(file.getContentType());
		String extension = file.getSubmittedFileName().substring(file.getSubmittedFileName().length()-4,file.getSubmittedFileName().length());
		logger.debug("formato:: "+extension);
		try {
			String fileName = "camp_" +(new Date()).getTime() ;
			String ruta = ingresarImagen(this.file.getInputStream(), fileName);
			logger.info(urlForm);
			FormatoMailTO fm = new FormatoMailTO();
			fm.setTexto(textoAlternativo);
			fm.setUrlFoto(fileName);
			fm.setIdCampania(idCampania);
			fm.setUrl(urlForm);
			deleteAllMensajes();
			addMensaje(c.guardarFormatoMail(fm));
			logger.debug("imagen copiada con exito");
			detalle=c.findAllFormatoMailByCampania(idCampania);
			limpiarCampos();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void limpiarCampos()
	{
		this.urlForm = "";
		this.textoAlternativo = "";
		this.file = null;
	}
	
	
	private String ingresarImagen(InputStream file_input ,String fileName)
	{
		Controller c = Controller.getInstance();
		String url =System.getProperty("FILE_PUBLICO")+fileName;
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(url);
			try {
				IOUtils.copy(file_input, fos);
			} catch (IOException e) {
				e.printStackTrace();
				
			}
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return url;
	}
	

	 
}
