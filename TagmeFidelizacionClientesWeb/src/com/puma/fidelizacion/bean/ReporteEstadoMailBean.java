package com.puma.fidelizacion.bean;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name="repEstadoMailBean")
@ViewScoped
public class ReporteEstadoMailBean extends BaseBean{
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(ReporteEstadoMailBean.class);
	
	private List<ClienteTO> clientes;
	private boolean nombre;
	private boolean apellido;
	private boolean sexo;
	private boolean email;
	private boolean experiencia;
	private boolean fecha_registro;
	private boolean fecha_envio;

	
	
	private String desde;
	private String hasta;
	private int tienda;
	private int campania;
	private List<CampaniaTO> campanias;
	private List<TiendaTO> tiendas;

	
	public List<TiendaTO> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<TiendaTO> tiendas) {
		this.tiendas = tiendas;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getDesde() {
		return desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

	public int getTienda() {
		return tienda;
	}

	public void setTienda(int tienda) {
		this.tienda = tienda;
	}

	public int getCampania() {
		return campania;
	}

	public void setCampania(int campania) {
		this.campania = campania;
	}

	public List<ClienteTO> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteTO> clientes) {
		this.clientes = clientes;
	}
	
	public boolean isNombre() {
		return nombre;
	}

	public void setNombre(boolean nombre) {
		this.nombre = nombre;
	}
	
	public boolean isApellido() {
		return apellido;
	}

	public void setApellido(boolean apellido) {
		this.apellido = apellido;
	}

	public boolean isSexo() {
		return sexo;
	}

	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isExperiencia() {
		return experiencia;
	}

	public void setExperiencia(boolean experiencia) {
		this.experiencia = experiencia;
	}
	

	public boolean isFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(boolean fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public boolean isFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(boolean fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	
	public void init()
	{
		isSecure();
		Controller c = Controller.getInstance();
//		campanias = c.findAllCampaniasVigentesByEmpresa(usuario.getIdEmpresa());
//		
		if(usuario.getPerfil().getNombre().equals("Administrador")) {
			tiendas = c.getTiendasActivaByEmpresa(usuario.getIdEmpresa());
		}else {
			tiendas = usuario.getTiendas();
		}
		
		
		hasta = Utilidad.getHoy();
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.get(Calendar.DAY_OF_WEEK);
		logger.info((Calendar.DAY_OF_WEEK));
		logger.info((cal.getFirstDayOfWeek()));
		cal.add(Calendar.DAY_OF_MONTH, -1);
		Date da= cal.getTime();
		hasta = Utilidad.getHoy();
		desde = Utilidad.getFechaDateStr(da);
//		clientes = c.findClienteReporteV2(usuario.getIdEmpresa(),tienda,campania,desde,hasta,tiendas);
		
//		Iterator it = clientes.iterator();
//		while (it.hasNext()) {
//			ClienteTO cl=(ClienteTO)it.next();
//			cl.setCategoriasReporte(allCategorias);	
//			cl.setAdicionalesReporte(camposAdicionales);
//		}
	}
	
	public void buscar() {
		Controller c = Controller.getInstance();
		List<TiendaTO> mistiendas=null;
		if(usuario.getPerfil().getNombre().equals("JefeTienda")) {
			mistiendas =usuario.getTiendas();
		}
		clientes = c.findClienteReporteV2(usuario.getIdEmpresa(),tienda,campania,desde,hasta,mistiendas);
		
//		Iterator it = clientes.iterator();
//		while (it.hasNext()) {
//			ClienteTO cl=(ClienteTO)it.next();
//			
//		}
	}
}
