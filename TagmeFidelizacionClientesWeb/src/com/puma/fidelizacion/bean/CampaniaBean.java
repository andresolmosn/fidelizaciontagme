package com.puma.fidelizacion.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.Document;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.FormularioTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.TipoCampaniaTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name = "campaniaBean")
@ViewScoped
public class CampaniaBean extends BaseBean{
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(CampaniaBean.class);
	
	private String desde;
	private String hasta;
	private CampaniaTO campania;
	private List<CampaniaTO> campanias;
//	private List<String> tipoCampanias;
	private List<TipoCampaniaTO> tipoCampanias;
	private boolean modificando;
	private boolean clonar = false;
	private int campaniaId;
	private CampaniaTO campaniaClon;
	
	private List<MenuTO> menuPadre;
	private FormatoCuponTO formato;
	private FormularioTO nuevoFormularioTO;
	
	
	
	private String condicion;

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		campanias = campanias;
	}
		
	public CampaniaTO getCampania() {
		return campania;
	}

	public void setCampania(CampaniaTO campania) {
		this.campania = campania;
	}
	
//	public List<String> getTipoCampanias() {
//		return tipoCampanias;
//	}
//
//	public void setTipoCampanias(List<String> tipoCampanias) {
//		this.tipoCampanias = tipoCampanias;
//	}
	
	public String getDesde() {
		return desde;
	}

	public List<TipoCampaniaTO> getTipoCampanias() {
		return tipoCampanias;
	}

	public void setTipoCampanias(List<TipoCampaniaTO> tipoCampanias) {
		this.tipoCampanias = tipoCampanias;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}
	
	public boolean isModificando() {
		return modificando;
	}

	public void setModificando(boolean modificando) {
		this.modificando = modificando;
	}
	
	public boolean isClonar() {
		return clonar;
	}

	public void setClonar(boolean clonar) {
		this.clonar = clonar;
	}
	
	

	public List<MenuTO> getMenuPadre() {
		return menuPadre;
	}

	public void setMenuPadre(List<MenuTO> menuPadre) {
		this.menuPadre = menuPadre;
	}



	public FormatoCuponTO getFormato() {
		return formato;
	}

	public void setFormato(FormatoCuponTO formato) {
		this.formato = formato;
	}

	public int getCampaniaId() {
		return campaniaId;
	}

	public void setCampaniaId(int campaniaId) {
		this.campaniaId = campaniaId;
	}
	
	public String getCondicion() {
		return condicion;
	}

	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}

	
	public CampaniaTO getCampaniaClon() {
		return campaniaClon;
	}

	public void setCampaniaClon(CampaniaTO campaniaClon) {
		this.campaniaClon = campaniaClon;
	}

	public FormularioTO getNuevoFormularioTO() {
		return nuevoFormularioTO;
	}

	public void setNuevoFormularioTO(FormularioTO nuevoFormularioTO) {
		this.nuevoFormularioTO = nuevoFormularioTO;
	}

	public String init()
	{
		isSecure();
		this.modificando = false;
		Controller c = Controller.getInstance();
		this.campania = new CampaniaTO();
		this.campania.setTipoCampania(new TipoCampaniaTO());
		this.campania.setTipoCampania(new TipoCampaniaTO());
		this.tipoCampanias = c.findAllTipoCampaniasByEmpresa(this.usuario.getIdEmpresa());
		this.campanias = c.findAllCampaniasByEmpresa(this.usuario.getIdEmpresa());
		this.clonar = false;
		//tipoCampanias = new LinkedList<String>();
		//tipoCampanias.add("FIDELIZACION");
		return "";
	}
	
	public String ingresarCampania()
	{
		logger.info("Ingresando Campania:" + this.campania);
		Controller c = Controller.getInstance();
		this.campania.setEstado("ESPERA");
		this.campania.setEmpresaId(this.usuario.getIdEmpresa());
		this.deleteAllMensajes();
		
		if(clonar && campaniaId != 0)
			{
				logger.info("Clonando Campania");
				
				Hashtable ht =  c.clonarCampania(this.campania);
				MensajeTO mensajeCreacion = (MensajeTO)ht.get("MENSAJE");
				Integer idCampania = (Integer)ht.get("DATA");
				if(idCampania != 0){
					
					MensajeTO msg = (MensajeTO)ht.get("MENSAJE");
					logger.info(msg);
					String msgText = msg.getMensaje();
					logger.info("ID de la campania: " + idCampania);
					
					if(idCampania != 0) {
					logger.info("Clonando Campania" + this.campania);
		//			menuPadre = c.findAllMenuByCampania(campaniaId);
					msgText += "<br /> Se copi� lo suigiente de la campa�a seleccionada:";
					menuPadre = campaniaClon.getMenu();
					logger.info("Clonando menu: " + menuPadre);
					if(menuPadre != null && menuPadre.size()>0)
					{
						Iterator<MenuTO> i = menuPadre.iterator();
						while(i.hasNext()){
							MenuTO to = i.next();
							clonarMenu(to, idCampania);
							c.addMenu(to);
			//				this.campania.getMenu().add(to);
						}
						msgText +="<br /> - Menu";
					}
					
					
					formato = c.findFormatoCuponByCampania(campaniaId);
		//			formato = campaniaClon.getFormulario();
					if(formato != null)
					{
						clonarFormatoCupon(formato,idCampania);
						logger.info("Clonando formato: " + formato);
						c.addFormatoCupon(formato);
						msgText +="<br /> - Formato mail del Cupon";
					}
					
					
					nuevoFormularioTO = campaniaClon.getFormulario();
					if(nuevoFormularioTO != null)
					{
						clonarFormulario(nuevoFormularioTO,idCampania);
						nuevoFormularioTO.setIdCampania(idCampania);
						nuevoFormularioTO.setIdEmpresa(usuario.getIdEmpresa());
						logger.info("Clonando formulario: " + nuevoFormularioTO);
						c.guardarFormulario(nuevoFormularioTO);
						msgText +="<br /> - Formato formulario de registro";
					}
					
					
					
					List<FormatoMailTO> formatoMails = campaniaClon.getFormato();
					if(formatoMails != null && formatoMails.size() > 0)
					{
						Iterator<FormatoMailTO> i2 = formatoMails.iterator();
						while(i2.hasNext())
						{
							FormatoMailTO mail = i2.next();
							clonarImagenMail(mail, idCampania);
							c.guardarFormatoMail(mail);
						}
						msgText +="<br /> - Formato mail de la campania";
					}
					msg.setMensaje(msgText);
					
					mensajes.add(msg);
				}else {
					logger.info("mensajes" + (MensajeTO) ht.get("MENSAJE"));
					mensajes.add((MensajeTO) ht.get("MENSAJE"));
					
				}
				
				
				clonar = false;
				this.campaniaClon = new CampaniaTO();
				this.campaniaId = 0;
				
				
			}else {
				clonar = false;
				this.campaniaClon = new CampaniaTO();
				this.campaniaId = 0;
				logger.info("mensajes" + (MensajeTO) ht.get("MENSAJE"));
				mensajes.add((MensajeTO) ht.get("MENSAJE"));
			}
		}else {
			mensajes.add(c.addCampania(this.campania));
			clonar = false;
			this.campaniaClon = new CampaniaTO();
			this.campaniaId = 0;
		}
		
		this.campanias = c.findAllCampaniasByEmpresa(this.usuario.getIdEmpresa());
		
		return "";
	}
	
	public void eliminar() {
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("id"));
		logger.info("Eliminar Campania :" + id);
		Controller c = Controller.getInstance();
		this.deleteAllMensajes();
		addMensaje(c.eliminarCampania(id));
	}
	
	public void ChangeSelect()
	{
		deleteAllMensajes();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Map params = facesContext.getExternalContext().getRequestParameterMap();
		Integer id = new Integer((String) params.get("mod"));
		logger.info("Modificar campania :" + id);
		Controller c = Controller.getInstance();
		this.campania = c.findCampaniasById(id, this.usuario.getIdEmpresa());
		modificando = true;
	}
	
	public void modificarCampania() {
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		addMensaje(c.actualizarCampania(this.campania));
	}
	
	public void FinalizarModoModificacion() {
		this.campania = new CampaniaTO();
		modificando = false;
	}
	
	public void selectCampania() {
		for(TipoCampaniaTO to : this.tipoCampanias)
		{
			if(this.campania.getTipoCampania().getId() == to.getId())
			{
				this.campania.setTipoCampania(to);
			}
		}
		logger.info(this.campania.getTipoCampania());
	}
	
	public void copiarCampaniaSeleccion()
	{
		logger.info(campaniaId);
		if(campaniaId != 0)
		{
//			Iterator<CampaniaTO> i = campanias.iterator();
//			while(i.hasNext())
//			{
//				CampaniaTO c = i.next();
//				if(c.getId() == this.campaniaId)
//				{
					Controller controller = new Controller();

					this.campaniaClon = controller.findCampaniasById(campaniaId, this.usuario.getIdEmpresa());
					logger.info(campaniaClon);
					logger.info(campaniaClon.getFormato());
					logger.info(campaniaClon.getFormulario());
					logger.info(campaniaClon.getMenu());
					
					this.campania.setNombre(campaniaClon.getNombre());
					this.campania.setAsunto(campaniaClon.getAsunto());
					this.campania.setTipoCampania(campaniaClon.getTipoCampania());
//				}
//			}
		}
		
	}
	
	public void clonarMenu(MenuTO to, int campania)
	{
		
		String filePath = System.getProperty("CARPETA_ARCHIVOS")+"menu/";
		
		String extension = to.getImagen().substring(to.getImagen().length()-4,to.getImagen().length());
			String fileNameCopia = "opcion" +(new Date()).getTime()+extension;
			String fileNameOriginal = to.getImagen();
			to.setImagen(fileNameCopia);
			to.setId(0);
			CampaniaTO t = new CampaniaTO();
			t.setId(campania);
			to.setCampania(t);
			
			
			File fileOriginal = new File(filePath+fileNameOriginal);
			if(fileOriginal.exists())
			{
				File fileCopia = new File(filePath+fileNameCopia);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(fileCopia);
					try {

						FileUtils.copyFile(fileOriginal, fos);
//						IOUtils.copy(inputstream, fos);
					} catch (IOException e) {
						e.printStackTrace();
					}
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();

				}
			}
			
	}
	
	public void clonarFormatoCupon(FormatoCuponTO to, int campania)
	{
		
		logger.info(to);
		String filePath = System.getProperty("CARPETA_ARCHIVOS")+"cupon/";
		
		String extension = to.getUrlFoto().substring(to.getUrlFoto().length()-4,to.getUrlFoto().length());
			String fileNameCopia = "banner" +(new Date()).getTime()+extension;
			String fileNameOriginal = to.getUrlFoto();
			to.setUrlFoto(fileNameCopia);
			to.setId(0);
			to.setCampania_id(campania);			
			
			File fileOriginal = new File(filePath+fileNameOriginal);
			if(fileOriginal.exists())
			{
				File fileCopia = new File(filePath+fileNameCopia);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(fileCopia);
					try {

						FileUtils.copyFile(fileOriginal, fos);
//						IOUtils.copy(inputstream, fos);
					} catch (IOException e) {
						e.printStackTrace();
					}
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();

				}
			}
	}
	
	public void clonarFormulario(FormularioTO to, int campania)
	{
		to.setId(0);
		to.setIdCampania(campania);
	}
	
	public void clonarImagenMail(FormatoMailTO to, int campania){
		
		logger.info(to);
		
		String filePath = System.getProperty("FILE_PUBLICO");
		
		String extension = to.getUrlFoto().substring(to.getUrlFoto().length()-4,to.getUrlFoto().length());
			String fileNameCopia = "camp_" +(new Date()).getTime()+extension;
			String fileNameOriginal = to.getUrlFoto();
			to.setUrlFoto(fileNameCopia);
			to.setId(0);
			to.setIdCampania(campania);
			
			
			File fileOriginal = new File(filePath+fileNameOriginal);
			if(fileOriginal.exists())
			{
				File fileCopia = new File(filePath+fileNameCopia);
				FileOutputStream fos = null;
				try {
					fos = new FileOutputStream(fileCopia);
					try {

						FileUtils.copyFile(fileOriginal, fos);
//						IOUtils.copy(inputstream, fos);
					} catch (IOException e) {
						e.printStackTrace();
					}
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();

				}
			}
		
			

	}

}
