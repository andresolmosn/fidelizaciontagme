package com.puma.fidelizacion.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.glassfish.api.container.RequestDispatcher;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.FormularioTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ValidacionTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name="administracionFormularioBean")
@ViewScoped
public class AdministracionFormularioBean extends BaseBean{

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(AdministracionFormularioBean.class);
	

	private String [] rut = new String[2];
	private String fechaNacimiento;
	private List<CategoriaTO> listaCategoria;
	private String preferencias;
//	private ClienteTO cliente;
	private boolean condiciones=false;
	public boolean[] dato;
	public int campania;
	public int empresa;
	private boolean validado = false;
	private List<MenuTO> menu;
	private boolean existeMenu;
	private int idCampania;
	private List<CampaniaTO> campanias;
	private CampaniaTO campaniaSelecionada;
	private List<CamposAdicionalesTO> campos;
	private List<CamposAdicionalesCampaniaTO> camposCampania;
	private FormularioTO nuevo;
	private int idCampo;
	
	public String actualizarCampos() {
		logger.info("actualizarCampos");
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		MensajeTO msg = c.guardarFormulario(nuevo);
		logger.info("resultado de la actualizacion: " + msg);
		this.addMensaje(msg);
		campaniaSelecionada = c.findCampaniasFidelizacionById(idCampania,usuario.getIdEmpresa());
		menu=campaniaSelecionada.getMenu();
		setListaCategoria(c.findAllCategoriasByEmpresa(usuario.getIdEmpresa(), 1));
		logger.info("Mensajes: " + getMensajes());
		return "";
	}
	
	public  void eliminarCampo (int index) {
		logger.info("::eliminarCampo "+index);
		Controller con = Controller.getInstance();
		con.deleteCampoAdicionalCampaniaById(camposCampania.get(index).getId());
		camposCampania=con.findCamposAdicionalesCampania(idCampania);
	}
	public int getIdCampo() {
		return idCampo;
	}


	public void setIdCampo(int idCampo) {
		this.idCampo = idCampo;
	}


	public List<CamposAdicionalesTO> getCampos() {
		return campos;
	}

	public void setCampos(List<CamposAdicionalesTO> campos) {
		this.campos = campos;
	}

	public CampaniaTO getCampaniaSelecionada() {
		return campaniaSelecionada;
	}

	public void setCampaniaSelecionada(CampaniaTO campaniaSelecionada) {
		this.campaniaSelecionada = campaniaSelecionada;
	}

	public int getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}

	public FormularioTO getNuevo() {
		return nuevo;
	}

	public void setNuevo(FormularioTO nuevo) {
		this.nuevo = nuevo;
	}

	public boolean isExisteMenu() {
		existeMenu = (menu!=null && menu.size()>0)?true:false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}

	
	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	public int getCampania() {
		return campania;
	}

	public void setCampania(int campania) {
		this.campania = campania;
	}

	public boolean[] getDato() {
		return dato;
	}

	public void setDato(boolean[] dato) {
		this.dato = dato;
	}

	public List<CategoriaTO> getListaCategoria() {
		return listaCategoria;
	}
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getPreferencias() {
		return preferencias;
	}

	public void setPreferencias(String preferencias) {
		this.preferencias = preferencias;
	}

	public void setListaCategoria(List<CategoriaTO> listaCategoria) {
		this.listaCategoria = listaCategoria;
	}
	
//	public ClienteTO getCliente() {
//		return cliente;
//	}
//
//	public void setCliente(ClienteTO cliente) {
//		this.cliente = cliente;
//	}

	public String[] getRut() {
		return rut;
	}

	public void setRut(String[] rut) {
		this.rut = rut;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	public boolean isCondiciones() {
		return condiciones;
	}

	public void setCondiciones(boolean condiciones) {
		this.condiciones = condiciones;
	}
	
	public boolean isValidado() {
		return validado;
	}

	public void setValidado(boolean validado) {
		this.validado = validado;
	}
	
	public void agregaCampo() {
		deleteAllMensajes();
		Controller con = Controller.getInstance();
		logger.debug(idCampo);
		if(idCampania!=0) {
			boolean cargar =true;
			if(camposCampania!=null ) {
				Iterator rev=camposCampania.iterator();
				while(rev.hasNext()) {
					CamposAdicionalesCampaniaTO cc = (CamposAdicionalesCampaniaTO) rev.next();
					if(cc.getCampo().getId()==idCampo) {
						cargar = false;
						break;
					}
				}
			}
			if(cargar) {
				Iterator c = campos.iterator();
				while(c.hasNext()) {
					CamposAdicionalesTO ca = (CamposAdicionalesTO) c.next();
					if(ca.getId()==idCampo) {
						con.guardarCampoAdicionalCampania(ca,idCampania);
						addMensaje(new MensajeTO("Aviso!","Campo agregado al formulario con exito",MensajeTO.SUCCESS));
					}
					
				}
			}else {
				addMensaje(new MensajeTO("Alerta!","Campo ya existe en el formulario",MensajeTO.WARNING));
			}
		}
		camposCampania=con.findCamposAdicionalesCampania(idCampania);
	}
	
	
	public List<CamposAdicionalesCampaniaTO> getCamposCampania() {
		return camposCampania;
	}

	public void setCamposCampania(List<CamposAdicionalesCampaniaTO> camposCampania) {
		this.camposCampania = camposCampania;
	}


	public void changeCampania(AjaxBehaviorEvent event) {
		logger.info("changeCampania "+idCampania);
		Controller c = Controller.getInstance();
		if(idCampania != 0)
		{
			campaniaSelecionada = c.findCampaniasFidelizacionById(idCampania,usuario.getIdEmpresa());
			menu=campaniaSelecionada.getMenu();
//			setListaCategoria(c.findAllCategoriasByEmpresa(cliente.getEmpresa_id(), 1));
			setListaCategoria(c.findAllCategoriasByEmpresa(usuario.getIdEmpresa(), 1));
			camposCampania=c.findCamposAdicionalesCampania(idCampania);
			if(campaniaSelecionada.getFormulario()!=null) {
				nuevo.setSeccion1(campaniaSelecionada.getFormulario().getSeccion1());
				nuevo.setSeccion2(campaniaSelecionada.getFormulario().getSeccion2());
				nuevo.setSeccion3(campaniaSelecionada.getFormulario().getSeccion3());
				nuevo.setActivoS2(campaniaSelecionada.getFormulario().getActivoS2());
				nuevo.setActivoS3(campaniaSelecionada.getFormulario().getActivoS3());
				nuevo.setBoton(campaniaSelecionada.getFormulario().getBoton());
				nuevo.setCondiciones(campaniaSelecionada.getFormulario().getCondiciones());
				nuevo.setId(campaniaSelecionada.getFormulario().getId());
				
				logger.info(camposCampania!=null ? camposCampania.size(): "NULO");
				
			}else {
			
				nuevo.setSeccion1("");
				nuevo.setSeccion2("");
				nuevo.setSeccion3("");
				nuevo.setBoton("");
				nuevo.setCondiciones("");
			}
			nuevo.setIdCampania(idCampania);
			nuevo.setIdEmpresa(usuario.getIdEmpresa());
		}else {
			this.nuevo = new FormularioTO();
			camposCampania=new LinkedList();
		}
		
		logger.info("Mensajes: " + getMensajes() );
		
	}
	
	    
	    
	public String init() throws IOException{
		this.isSecure();
		logger.info("Usuario :" + this.usuario );
		Controller c = Controller.getInstance();
		EmpresaTO emp= new EmpresaTO();
		emp.setId(usuario.getIdEmpresa());
		campanias = c.findAllCampaniasVigentesByEmpresa(usuario.getIdEmpresa());
//		cliente = new ClienteTO();
		nuevo=new FormularioTO();
		setMensajes(new LinkedList<MensajeTO>());
		campos = c.getCamposAdicionales();
		return "";
	}
	

	public void validarFormulario(ClienteTO cliente)	{
		this.validado = true;
		mensajes = new LinkedList<MensajeTO>();
		Controller c = Controller.getInstance();
		List<ValidacionTO> validaciones = c.validaCliente(cliente);
		Iterator<ValidacionTO> i = validaciones.iterator();
		while(i.hasNext())
		{
			ValidacionTO validacion = i.next();
			if(!validacion.isValido() && this.validado == true)
			{
				this.validado = false;
			}
			logger.info(validacion);
			this.mensajes.add(validacion.getMensaje());
		}
	}
	
	
	
}
