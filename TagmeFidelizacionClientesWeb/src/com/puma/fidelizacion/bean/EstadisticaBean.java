package com.puma.fidelizacion.bean;

import java.security.cert.X509Certificate;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.net.ssl.TrustManager;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.EstadisticaTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.util.Utilidad;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

@ManagedBean(name="estadisticaBean")
@ViewScoped
public class EstadisticaBean extends BaseBean{
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(EstadisticaBean.class);
	
	private String desde;
private List<EstadisticaTO>datos;
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	
	public String init() {
		setDia(-7);
		buscar();
		return "";
	}

	
	
	public List<EstadisticaTO> getDatos() {
		return datos;
	}
	public void setDatos(List<EstadisticaTO> datos) {
		this.datos = datos;
	}
	public String getDesde() {
		return desde;
	}
	public void setDesde(String desde) {
		this.desde = desde;
	}
	
	public String getDatosJson() {
		String info="";
		String color ="";
		String enviados="\"label\":\"Enviado\",\"data\":[";
		String fail="\"label\":\"Rechazado\",\"data\":[";
		String abierto="\"label\":\"Abierto\",\"data\":[";
		String clickeado="\"label\":\"Click\",\"data\":[";
		String label ="{"+"\"labels\":[";
		//"labels":["January","February","March","April","May","June","July"]
		if(datos!=null && datos.size()>0) {
			Iterator i=datos.iterator();
			while (i.hasNext()) {
				EstadisticaTO e = (EstadisticaTO)i.next();
				label+="\""+e.getFechaFormateada()+"\",";
				enviados+=e.getEnviado()+",";
				fail+=e.getRechazado()+",";
				clickeado+=e.getClickeado()+",";
				abierto+=e.getAbierto()+",";
			}
		}
		label=label.substring(0,label.length()-1);
		label+="],";
		
		enviados=enviados.substring(0,enviados.length()-1);
		enviados+="],  \"backgroundColor\": \"rgba(99, 179, 157, .5)\"  ";
		fail=fail.substring(0,fail.length()-1);
		fail+="],  \"backgroundColor\": \"rgba(212,68,254, .5)\" , ";
		
		abierto=abierto.substring(0,abierto.length()-1);
		abierto+="],  \"backgroundColor\": \"rgba(190,247,68, .5)\" , ";
		clickeado=clickeado.substring(0,clickeado.length()-1);
		clickeado+="] , \"backgroundColor\": \"rgba(68,247,125, .5)\",  ";
		
//		color =" \"backgroundColor\": [\"#3e95cd\", \"#8e5ea2\",\"#3cba9f\",\"#e8c3b9\"],";
//		{"labels":["January","February","March","April","May","June","July"],
//			   "datasets":[{"label":"My First Dataset","data":[65,59,80,81,56,55,40]}]}
		
		info=label+ "\"datasets\":[{"+clickeado+"},{"+abierto+"},{"+enviados+"}]}";
		System.out.println(info);
		return info;
	}
	public void setDia(int dia) {
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.get(Calendar.DAY_OF_WEEK);
		logger.info((Calendar.DAY_OF_WEEK));
		logger.info((cal.getFirstDayOfWeek()));
		cal.add(Calendar.DAY_OF_MONTH, dia);
		Date da= cal.getTime();
		desde = Utilidad.getFechaDateStr(da);
	}
	
	public void buscar() {
		
		 
		datos=new LinkedList();
		 
		String YOUR_DOMAIN_NAME="tagmeservices.com";
	    String API_KEY = "bf8ee12b5fb70d4f08e386b723ac1b7b-a5d1a068-54c592e9";
	    
	    
	    Date fecha = Utilidad.getFechaDate(desde);
	   
	    
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 1);
		Date da= cal.getTime();
		
		try {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}
		}};
			
		SSLContext sslcontext = SSLContext.getInstance("SSL");
		sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
		CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		Unirest.setHttpClient(httpclient);
		
		 HttpResponse<JsonNode> request;
		
			request = Unirest.get("https://api.mailgun.net/v3/" + YOUR_DOMAIN_NAME + "/stats/total")
			            .basicAuth("api", API_KEY)
			            .queryString("event", "delivered")
			            .queryString("event", "failed")
			            .queryString("event", "clicked")
			            .queryString("event", "opened")
			            .queryString("start",Utilidad.getFechaDateMailGunStr(da)+" UTC")
			            .asJson();
		
		 System.out.println(request.getBody());
		  		JSONObject obj = request.getBody().getObject();
		  		System.out.println(request.getBody().getObject());
		  		JSONArray arr = obj.getJSONArray("stats");
		  		for (int i = 0; i < arr.length(); i++)
		  		{
		  			JSONObject o = arr.getJSONObject(i);
		  			String v_time= (String)o.get("time");
		  			JSONObject	delivered = (JSONObject) o.get("delivered");
		  			JSONObject	open =(JSONObject) o.get("opened");
		  			JSONObject	fail =(JSONObject) o.get("failed");
		  			JSONObject	click =(JSONObject) o.get("clicked");
		  			Integer v_del= (Integer)delivered.get("total");
		  			Integer v_open = (Integer)open.get("total");
		  			Integer v_click = (Integer)click.get("total");
 		  		JSONObject	fail_t = (JSONObject)fail.get("temporary");
 		  		JSONObject	fail_p = (JSONObject)fail.get("permanent");
 		  		Integer v_fail_t = (Integer)fail_t.get("total");
 		  		Integer v_fail_p = (Integer)fail_p.get("total");
 		  		EstadisticaTO to = new EstadisticaTO( v_time,v_del,v_fail_p,v_fail_t,v_open,v_click);
 		  		System.out.println( v_time+" " +v_del+ " "+v_fail_p+ " "+v_fail_t+" "+v_open+" "+v_click);
		  		datos.add(to);
		  		}
		  		request.getBody();
		  		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	
	
	
}
