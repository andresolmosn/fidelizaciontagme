package com.puma.fidelizacion.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.SendMail;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.util.Utilidad;

@ManagedBean(name = "mailBean")
@SessionScoped
public class MailBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(MailBean.class);

	private List<MenuTO> menu;
	private String to;
	private List<FormatoMailTO> fotos;
	private String codigoCliente;
	private String urlPropia;
	private boolean existeMenu;
	private String logo;
	private String texto1;
	private String texto2;
	private String contacto;
	private String mailCliente;
	

	public String getMailCliente() {
		return mailCliente;
	}

	public void setMailCliente(String mailCliente) {
		this.mailCliente = mailCliente;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTexto1() {
		return texto1;
	}

	public void setTexto1(String texto1) {
		this.texto1 = texto1;
	}

	public String getTexto2() {
		return texto2;
	}

	public void setTexto2(String texto2) {
		this.texto2 = texto2;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public boolean isExisteMenu() {
		existeMenu = (menu != null && menu.size() > 0) ? true : false;
		return existeMenu;
	}

	public void setExisteMenu(boolean existeMenu) {
		this.existeMenu = existeMenu;
	}

	public String getUrlPropia() {
		return urlPropia;
	}

	public void setUrlPropia(String urlPropia) {
		this.urlPropia = urlPropia;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}

	public List<FormatoMailTO> getFotos() {
		return fotos;
	}

	public void setFotos(List<FormatoMailTO> fotos) {
		this.fotos = fotos;
	}

	public void agregarFoto(MimeMultipart multipart, String foto, String id) {
		try {
			MimeBodyPart messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(foto);
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", id);

			multipart.addBodyPart(messageBodyPart);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public String init() {
		logger.debug("init");
		String parametro = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
		System.out.println("parametro: " + parametro);
		Controller c = Controller.getInstance();
		// int idempresa=2;
		// setMenu(c.findAllMenuPadreByEmpresa(idempresa));
		// CampaniaTO to = c.findCampaniasFidelizacionByEmpresa(idempresa);
		// fotos = to.getFormato();
		codigoCliente = parametro;
		urlPropia = "https://" + getDireccionServer() + "/TagmeFidelizacion/plantillaMail.xhtml?id=" + parametro;
		// logger.info(to.getFormato());

		ClienteTO cliente = c.findClienteByHash(parametro);
		if (cliente != null) {
			int campania = c.getCampaniaCliente(cliente.getFecha_registro(), cliente.getEmpresa_id(),
					cliente.getTipoCampania().getId());
			int empresa = cliente.getEmpresa_id();
			CampaniaTO to = c.findCampaniasFidelizacionByEmpresaActiva(empresa,campania);
			fotos = to.getFormato();
			setMenu(c.findAllMenuPadreByEmpresa(empresa, campania));

			ParametroTO p1 = c.getSingleParaemtroByTypeAndEmpresa(empresa, "footer.text");
			ParametroTO p2 = c.getSingleParaemtroByTypeAndEmpresa(empresa, "footer.text2");
			ParametroTO p3 = c.getSingleParaemtroByTypeAndEmpresa(empresa, "footer.imagen");
			ParametroTO p4 = c.getSingleParaemtroByTypeAndEmpresa(empresa, "footer.contacto");

			texto1 = p1.getValor();
			logo = p3.getValor();
			contacto = p4.getValor();
			texto2 = p2.getValor();
			
			
			texto1=Utilidad.traducir(texto1);
			texto2=Utilidad.traducir(texto2);
			
			
			mailCliente=cliente.getMail();
		}
		return "";
	}

	public void enviaStartTLS() {
		logger.debug("enviaStartTLS");
		logger.info("TARE ENVIO ----");
		// Controller c = Controller.getInstance();
		// c.getMailPendientes(2);
		SendMail s = new SendMail(2);
		try {
			logger.info("ENVIAR CORREO ----");
			URL url = new URL("http://" + getDireccionServer() + "/TagmeFidelizacion/plantillaMail.xhtml");
			s.sendMail(this.to, "VIIIIIIIIIVEEEEEEEEE!!!!!!", "fidelizacion@tagmeservices.com", null, url);
			// s.sendSimpleMessage("tagme@tagmeservices.com", "delaparra1985@gmail.com",
			// "Registrate", "tagmeservices.com",
			// "b8939e86dd740ab2b85aec5a108c99ae-770f03c4-d310f748");

		} catch (Exception e) {
			logger.info("Error ENVIO ----");
			e.printStackTrace();
		}

		// SendMail s = new SendMail();
		// s.enviar(to);
		// setMensaje("Email Enviado con Exito");
		logger.info("FIN ----");
		logger.debug("FIN enviaStartTLS");

	}

}