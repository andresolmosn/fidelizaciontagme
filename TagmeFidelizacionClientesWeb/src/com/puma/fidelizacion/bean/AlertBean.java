package com.puma.fidelizacion.bean;

import java.util.Iterator;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.MensajeTO;

@ManagedBean
@ViewScoped
public class AlertBean extends BaseBean {

	protected List<MensajeTO> mensajes;
	protected int errorCount;
	protected int warningCount;
	
	public String init()
	{
		isSecure();
		this.mensajes = null;
		Controller c = Controller.getInstance();
		deleteAllMensajes();
		errorCount = 0;
		warningCount = 0;
		setMensajes(c.check(this.usuario.getIdEmpresa()));
		count();
		return "";
	}
	

	public List<MensajeTO> getMensajes() {
		return mensajes;
	}

	public void setMensajes(List<MensajeTO> mensajesAlerta) {
		this.mensajes = mensajesAlerta;
	}


	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public int getWarningCount() {
		return warningCount;
	}

	public void setWarningCount(int warningCount) {
		this.warningCount = warningCount;
	}
	
	public void count() {
		if(this.mensajes != null && this.mensajes.size() > 0) {
			Iterator<MensajeTO> i = this.mensajes.iterator();
			while(i.hasNext())
			{
				MensajeTO m = i.next();
				if(m.getTipo().equals(MensajeTO.WARNING)) {
					this.warningCount ++;
				}
				if(m.getTipo().equals(MensajeTO.ERROR)) {
					this.errorCount ++;
				}
				
			}
		}
	}
}
