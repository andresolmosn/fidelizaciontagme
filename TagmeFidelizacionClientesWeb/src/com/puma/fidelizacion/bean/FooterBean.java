package com.puma.fidelizacion.bean;

import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.ParametroTO;


@ManagedBean(name = "footerBean")
@ViewScoped
public class FooterBean extends BaseBean {

	private static final long serialVersionUID = 1L;
	private final static Logger logger = Logger.getLogger(FooterBean.class);

	private String logo;
	private String texto1;
	private String texto2; 
	private String contacto ; 
	private String facebook;
	private String twitter;
	private String instagram;
	private String youtube;
	


	public String init() {
		logger.debug("init");
		//isSecure();
		ClienteTO cliente = null;
		Controller c = Controller.getInstance();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		Map params = externalContext.getRequestParameterMap();
		String clienteHash = null;
		
		if(params.containsKey("hash")) {
			clienteHash = (String) params.get("hash");
			
		}else if(params.containsKey("id")){
			clienteHash = (String) params.get("id");
		}
		
		if (clienteHash != null && clienteHash.length() > 0) {
			cliente = c.findClienteByHash(clienteHash);
		}
		
		if (cliente != null) {

			int empresaId = cliente.getEmpresa_id();

			ParametroTO p1 = c.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.text");
			ParametroTO p2 = c.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.text2");
			ParametroTO p3 = c.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.imagen");
			ParametroTO p4 = c.getSingleParaemtroByTypeAndEmpresa(empresaId, "footer.contacto");
	
	 		setFacebook(c.getSingleParaemtroByTypeAndEmpresa(empresaId, "app.facebook").getValor());
			setTwitter(c.getSingleParaemtroByTypeAndEmpresa(empresaId, "app.twitter").getValor()) ;
			setInstagram(c.getSingleParaemtroByTypeAndEmpresa(empresaId, "app.instagram").getValor());
			setYoutube(c.getSingleParaemtroByTypeAndEmpresa(empresaId, "app.youtube").getValor());

			texto1= p1.getValor();
			logo= p3.getValor();
			contacto = p4.getValor();
			texto2= p2.getValor();

		}else {
			logger.info("No se encontro cliente");
			setFacebook("");
			setTwitter("") ;
			setInstagram("");
			setYoutube("");

			texto1= "";
			logo= "";
			contacto = "";
			texto2= "";
		}
		
		
		return "";
	}



	public String getLogo() {
		return logo;
	}



	public void setLogo(String logo) {
		this.logo = logo;
	}



	public String getTexto1() {
		return texto1;
	}



	public void setTexto1(String texto1) {
		this.texto1 = texto1;
	}



	public String getTexto2() {
		return texto2;
	}



	public void setTexto2(String texto2) {
		this.texto2 = texto2;
	}



	public String getContacto() {
		return contacto;
	}



	public void setContacto(String contacto) {
		this.contacto = contacto;
	}



	public String getFacebook() {
		return facebook;
	}



	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}



	public String getTwitter() {
		return twitter;
	}



	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}



	public String getInstagram() {
		return instagram;
	}



	public void setInstagram(String instagram) {
		this.instagram = instagram;
	}



	public String getYoutube() {
		return youtube;
	}



	public void setYoutube(String youtube) {
		this.youtube = youtube;
	}
	
	
	
}