package com.puma.fidelizacion.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.MenuTO;

@ManagedBean(name="mClienteBean")
@ViewScoped
public class MenuClienteBean implements Serializable{
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(MenuClienteBean.class);
	private List<MenuTO> menu;

	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}
	
	public String init()
	{
		FacesContext facesContext = FacesContext. getCurrentInstance();
		ExternalContext externalContext = facesContext.getExternalContext();
		Map params = externalContext.getRequestParameterMap();
		String campania =(String) params.get("id");
		String empresa =(String) params.get("emp");
		logger.debug(campania);
		logger.debug(empresa);
		if(campania !=null && empresa !=null && campania.length()>0 && empresa.length()>0) {
		Controller c = Controller.getInstance();
//		setListaCategoria(c.findAllCategoriasByEmpresa(2, 1));
//		if(clienteHash != null && clienteHash.length()>0)
//		{
//		this.cliente = c.findClienteByHash(clienteHash);
//		Controller c = Controller.getInstance();
		setMenu(c.findAllMenuPadreByEmpresa(Integer.parseInt(empresa),Integer.parseInt(campania)));
		}
		
		return "";
	}
}
