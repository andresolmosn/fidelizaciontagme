
package com.puma.fidelizacion.bean;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.krysalis.barcode4j.HumanReadablePlacement;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.impl.upcean.EAN13Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.SendMail;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.UsuarioTO;

 
@ManagedBean(name = "cuponBarCodeBean")
@ViewScoped
public class CuponBarCodeBean extends BaseBean {
	final static Logger logger = Logger.getLogger(CuponBarCodeBean.class);


	public void algo() {
		logger.info("algo");
		
			String parametro= FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
			logger.debug("parametro: "+parametro);
			Controller c = Controller.getInstance();
			CuponTO cupon = c.getCupon(parametro);
		    String strCode = cupon.getCodigo();
		    if(strCode!=null && strCode.length()>0) {
		    try {
		    	EAN13Bean bean = new EAN13Bean();
		             
		             final int dpi = 150;
		      
		             bean.setMsgPosition(HumanReadablePlacement.HRP_BOTTOM);
		             bean.setModuleWidth(0.5);
		             bean.setBarHeight(20.0);
		             bean.setFontSize(5.0);
		             bean.setQuietZone(5.0);
		             bean.doQuietZone(true);
		             String fileName=System.getProperty("FILE_PUBLICO")+"bc_"+(new Date()).getTime()+".png";
		             File outputFile = new File(fileName);
		             OutputStream out = new FileOutputStream(outputFile);
		      
		             try {
		      
		                 //Set up the canvas provider for monochrome PNG output
		                 BitmapCanvasProvider canvas = new BitmapCanvasProvider(
		                     out, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
		      
		                 //Generate the barcode
		                 bean.generateBarcode(canvas, strCode);
		      
		                 //Signal end of generation
		                 canvas.finish();
		             } finally {
		                 out.close();
		             }
		             
			logger.info("abrir");
			File initialFile = new File(fileName);
		    InputStream targetStream = new FileInputStream(initialFile);
			BufferedInputStream in = new BufferedInputStream(targetStream);
			byte[] bytes = new byte[in.available()];
			in.read(bytes);
			in.close();
			 FacesContext facesContext = FacesContext.getCurrentInstance(); //Get the context ONCE
		     HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();
		    ServletOutputStream servletOutputStream = response.getOutputStream();
		    response.setContentType("image/png");
		    facesContext.responseComplete();
		    
		    servletOutputStream.write(bytes);
		    servletOutputStream.flush();
		    servletOutputStream.close();
		    logger.info("enviado");
		    initialFile.delete();
		    
		    
		} catch (Exception ex) {
			ex.printStackTrace();
		   }
		}
	}

	// public static void main(String[] args) {
	// Barcode barcode = null;
	// String strCode = "123581321";
	// try {
	// barcode = BarcodeFactory.createCode39(strCode, true);//Reemplazar esto por el
	// valor que deseen
	// } catch (BarcodeException e) {
	// }
	// barcode.setDrawingText(true);//determina si se agrega o no el número
	// codificado debajo del código de barras
	// barcode.setBarWidth(2);
	// barcode.setBarHeight(60);
	// try {
	// FileOutputStream fos = new
	// FileOutputStream(System.getProperty("FILE_PUBLICO")+"foto1.png");
	// BarcodeImageHandler.writePNG(barcode, fos);
	//
	// } catch (Exception ex) {
	// System.out.println("Error: "+ ex.getMessage());
	// }
	// }

}