package com.puma.fidelizacion.bean;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.to.HeaderMenuItemTO;
import com.puma.fidelizacion.to.MenuIntranetTO;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.UsuarioTO;

@ManagedBean
@ViewScoped
public class headerBean extends BaseBean{
	
	final static Logger logger = Logger.getLogger(headerBean.class);

	private PerfilTO perfil;
	private List<HeaderMenuItemTO> padre;
	
	public PerfilTO getPerfil() {
		return perfil;
	}

	public void setPerfil(PerfilTO perfil) {
		this.perfil = perfil;
	}

	public List<HeaderMenuItemTO> getPadre() {
		return padre;
	}

	public void setPadre(List<HeaderMenuItemTO> padre) {
		this.padre = padre;
	}

	public String init() {
		isSecure();
		perfil = usuario.getPerfil();
		List<MenuIntranetTO> menu = perfil.getMenuIntranet();
		generarMenu(menu);
		if(!validarPermisoURL(menu)) {
			logger.info("<<<< Redirigiendo a la url_def '" + perfil.getDef_url() +"'>>>>");
			
		    try {
		    	HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				String[] url = req.getRequestURI().toString().split("/");
		    	FacesContext facesContext = FacesContext.getCurrentInstance();
			    ExternalContext externalContext = facesContext.getExternalContext();
			    logger.info(url[0] + "/" + perfil.getDef_url());
				externalContext.redirect(perfil.getDef_url());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return perfil.getDef_url() +"?faces-redirect=true";
		}
		logger.info("perfil del usuario: " + padre);
		return "";
	}
	
	public boolean validarPermisoURL(List<MenuIntranetTO> menu) {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		String[] url = req.getRequestURI().toString().split("/");
		logger.info("URL del sitio ===> " + url);
		logger.info(url[url.length-1] );
		Iterator<MenuIntranetTO> i = menu.iterator();
		while(i.hasNext()) {
			MenuIntranetTO to = i.next();
			logger.info("URL menuTO revisado =====> " + to.getUrl());
			if(url[url.length-1].equals(to.getUrl()))
			{
				logger.info("Usuario con permiso par ala URL");
				return true;
			}
		}
		return false;
	}
	
	public void generarMenu(List<MenuIntranetTO> menu) {
		
		this.padre = new LinkedList<HeaderMenuItemTO>();
		
		if(menu != null && menu.size()>0)
		{
			Iterator<MenuIntranetTO> i = menu.iterator();
			while(i.hasNext())
			{
				MenuIntranetTO to = i.next();
				if(to.getIdPadre() == 0) {
					HeaderMenuItemTO nuevoPadre = new HeaderMenuItemTO();
					nuevoPadre.setPadre(to);
					nuevoPadre.setHijos(new LinkedList<MenuIntranetTO>());
					this.padre.add(nuevoPadre);
				}
			}
			
			Iterator<MenuIntranetTO> i2 = menu.iterator();
			while(i2.hasNext())
			{
				MenuIntranetTO to = i2.next();
				if(to.getIdPadre() != 0) {
					addHijoToPadre(to);
				}
			}
		}
		
		

	}
	
	public boolean addHijoToPadre(MenuIntranetTO hijo)
	{
		if(this.padre != null && this.padre.size()>0) {
			Iterator<HeaderMenuItemTO> i = this.padre.iterator();
			while(i.hasNext())
			{
				HeaderMenuItemTO menuItem = i.next();
				MenuIntranetTO to = menuItem.getPadre();
				if(to.getId() == hijo.getIdPadre())
				{
					menuItem.getHijos().add(hijo);
					return true;
				}
			}
		}
		return false;
		
	}
	
	
}
