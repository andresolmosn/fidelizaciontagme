package com.puma.fidelizacion.bean;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.GenericInfoTO;
import com.puma.fidelizacion.to.ResumenExperienciaTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.util.Utilidad;
@ManagedBean(name="informacionBean")
@ViewScoped
public class InformacionBean extends BaseBean{
	protected static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(InformacionBean.class);
	
	private List<ClienteTO> clientes;
	private boolean nombre;
	private boolean apellido;
	private boolean sexo;
	private boolean email;
	private boolean experiencia;
	private boolean fecha_registro;
	private boolean fecha_envio;
	private List<CamposAdicionalesCampaniaTO> camposAdicionales;
	private List<CategoriaTO> allCategorias;
	private String desde;
	private String hasta;
	private int tienda;
	private int campania;
	private List<CampaniaTO> campanias;
	private List<TiendaTO> tiendas;
	private List<ResumenExperienciaTO> dataExperiencia;

private String target;
	List<GenericInfoTO> experiencias;
	List<GenericInfoTO> registros;
	List<GenericInfoTO> infoCategorias;
	List<GenericInfoTO> infoVendedores;

	private String experienciaInfo;
	private String registroInfo;
	private String categoriaInfo;
	private String vendedorInfo;
	
	private String categoriaInfoHombre;
	private String categoriaInfoMujer;
	private String categoriaInfoTotal;
	private String categoriaColores;
	
	private List<String> colores; 
	
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public List<GenericInfoTO> getRegistros() {
		return registros;
	}

	public void setRegistros(List<GenericInfoTO> registros) {
		this.registros = registros;
	}

	public List<GenericInfoTO> getInfoCategorias() {
		return infoCategorias;
	}

	public void setInfoCategorias(List<GenericInfoTO> infoCategorias) {
		this.infoCategorias = infoCategorias;
	}

	public List<GenericInfoTO> getInfoVendedores() {
		return infoVendedores;
	}

	public void setInfoVendedores(List<GenericInfoTO> infoVendedores) {
		this.infoVendedores = infoVendedores;
	}

	public List<GenericInfoTO> getExperiencias() {
		return experiencias;
	}

	public void setExperiencias(List<GenericInfoTO> experiencias) {
		this.experiencias = experiencias;
	}

	public void setExperienciaInfo(String experienciaInfo) {
		this.experienciaInfo = experienciaInfo;
	}

	public List<ResumenExperienciaTO> getDataExperiencia() {
		return dataExperiencia;
	}

	public void setDataExperiencia(List<ResumenExperienciaTO> dataExperiencia) {
		this.dataExperiencia = dataExperiencia;
	}

	public List<TiendaTO> getTiendas() {
		return tiendas;
	}

	public void setTiendas(List<TiendaTO> tiendas) {
		this.tiendas = tiendas;
	}

	public List<CampaniaTO> getCampanias() {
		return campanias;
	}

	public void setCampanias(List<CampaniaTO> campanias) {
		this.campanias = campanias;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public String getDesde() {
		return desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

	public int getTienda() {
		return tienda;
	}

	public void setTienda(int tienda) {
		this.tienda = tienda;
	}

	public int getCampania() {
		return campania;
	}

	public void setCampania(int campania) {
		this.campania = campania;
	}

	public List<CategoriaTO> getAllCategorias() {
		return allCategorias;
	}

	public void setAllCategorias(List<CategoriaTO> allCategorias) {
		this.allCategorias = allCategorias;
	}

	public List<CamposAdicionalesCampaniaTO> getCamposAdicionales() {
		return camposAdicionales;
	}

	public void setCamposAdicionales(List<CamposAdicionalesCampaniaTO> camposAdicionales) {
		this.camposAdicionales = camposAdicionales;
	}

	public List<ClienteTO> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClienteTO> clientes) {
		this.clientes = clientes;
	}
	
	public boolean isNombre() {
		return nombre;
	}

	public void setNombre(boolean nombre) {
		this.nombre = nombre;
	}
	
	public boolean isApellido() {
		return apellido;
	}

	public void setApellido(boolean apellido) {
		this.apellido = apellido;
	}

	public boolean isSexo() {
		return sexo;
	}

	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isExperiencia() {
		return experiencia;
	}

	public void setExperiencia(boolean experiencia) {
		this.experiencia = experiencia;
	}
	

	public boolean isFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(boolean fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public boolean isFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(boolean fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	
	public String getCategoriaInfoMujer() {
		return categoriaInfoMujer;
	}

	public void setCategoriaInfoMujer(String categoriaInfoMujer) {
		this.categoriaInfoMujer = categoriaInfoMujer;
	}

	public String getCategoriaInfoTotal() {
		return categoriaInfoTotal;
	}

	public void setCategoriaInfoTotal(String categoriaInfoTotal) {
		this.categoriaInfoTotal = categoriaInfoTotal;
	}

	public String getCategoriaInfoHombre() {
		return categoriaInfoHombre;
	}

	public void setCategoriaInfoHombre(String categoriaInfoHombre) {
		this.categoriaInfoHombre = categoriaInfoHombre;
	}

	public String getCategoriaColores() {
		return categoriaColores;
	}

	public void setCategoriaColores(String categoriaColores) {
		this.categoriaColores = categoriaColores;
	}

	public String getExperienciaInfo() {
		int cantColores = 0;
		String data = "data: [";
		String labels = "[";
		if (experiencias != null) {
			Iterator it = experiencias.iterator();
			int i = 0;
			
			while (it.hasNext()) {
				GenericInfoTO s = (GenericInfoTO) it.next();
				labels += "\"" + s.getColValue()[1] + "\"";
				data += s.getColValue()[0];
				cantColores++;
				if (i < (experiencias.size() - 1)) {
					labels += ",";
					data += ", ";
					i++;
				}
			}
			data += "]";
			labels += "]";
		} else {
			data += "]";
			labels += "]";
		}
		
		String background=this.getBackground(cantColores,0,0);
		String border=this.getBorder(cantColores,0,0);
		
		String result = " { labels: "+labels+", datasets: [{ label: \"Promedio\", "+ data +", "+ background +", " + border +"  }]}";
		logger.info(result);
		return result;
	}
	
//	registroInfo;
//	private String categoriaInfo;
	public String getRegistroInfo() {
		int cantColores = 0;
		String data1 = "data: [";
		String data2 = "data: [";
		String labels = "[";
//		registroInfo="";
		if (registros != null) {
			Iterator it = registros.iterator();
			registroInfo = "[";
			int i = 0;
			while (it.hasNext()) {
				GenericInfoTO s = (GenericInfoTO) it.next();
				labels += "\"" + s.getColValue()[2] + "\"";
				data1 += s.getColValue()[0];
				data2 += s.getColValue()[1];
				cantColores++;
				if (i < (registros.size() - 1)) {
					labels += ",";
					data1 += ", ";
					data2 += ", ";
					i++;
				}

			}
//			registroInfo += "]";
			data1 += "]";
			data2 += "]";
			labels += "]";
		} else {
//			registroInfo = "[{\"y\":\"\"},{\"a\":0}]";
			data1 += "]";
			data2 += "]";
			labels += "]";
		}
		
		String background1=this.getBackground(cantColores,0,cantColores);
		String background2=this.getBackground(cantColores,5,cantColores);
		
		String result = " { labels: "+labels+", datasets: [{ label: \"Inscritos\", stack: 'Stack 0', "+ data1 +", "+ background1 +", }, { label: \"No inscritos\", stack: 'Stack 0', "+ data2 +", "+ background2 +", }]}";
		logger.info(result);
		return result;
	}
//	
	public String getCategoriaInfo() {
		categoriaInfo="";
		 if(infoCategorias!=null){
	 		    Iterator it = infoCategorias.iterator();
	 		   categoriaInfo="[";
	 		   int i=0;
	 		   while (it.hasNext()){
	 			  GenericInfoTO s =(GenericInfoTO)it.next();
	 			// categoriaInfo += "{\"y\":\"" + s.getColValue()[1]+  "\",\"a\":" + s.getColValue()[0] +" }";
	 			categoriaInfo += "{\"y\":\"" + s.getColValue()[2]+  "\",\"a\":" + s.getColValue()[0] + ",\"b\":" + s.getColValue()[1] +" }";
//		 			
	 			   if(i<(infoCategorias.size()-1))
	 				  categoriaInfo+=",";
	 			   i++;
	 		   }
	 		  categoriaInfo +="]";
	 	    }else{
	 	    	categoriaInfo = "[{\"y\":\"\"},{\"a\":0}]";
	 	    }
		 logger.info(categoriaInfo);
		return categoriaInfo;
	}
	
	public String getCategoriaInfoJSON() {
		categoriaInfo="";
		 if(infoCategorias!=null){
	 		    Iterator it = infoCategorias.iterator();
	 		   getColores();
	 		   categoriaInfo="{";
	 		   String label = "labels: [";
	 		   
	 		   String dataHombre="data: [";
	 		   String dataMujer="data: [";
	 		   String dataTotal="data: [";
	 		   String colores="backgroundColor: [";
	 		   int i=0;
	 		   int color=0;
	 		   while (it.hasNext()){
	 			  GenericInfoTO s =(GenericInfoTO)it.next();
	 			// categoriaInfo += "{\"y\":\"" + s.getColValue()[1]+  "\",\"a\":" + s.getColValue()[0] +" }";
	 			 categoriaInfo += "{\"y\":\"" + s.getColValue()[2]+  "\",\"a\":" + s.getColValue()[0] + ",\"b\":" + s.getColValue()[1] +" }";
	 			 BigDecimal hombre = (BigDecimal)s.getColValue()[1];
	 			 BigDecimal mujer= (BigDecimal)s.getColValue()[0];
	 			 BigDecimal total = hombre.add(mujer);
	 			 if(color >= this.colores.size()) {
	 				 color = 0;
	 			 }
	 			 colores += "\"" + this.colores.get(color++) +  "\",";
	 			 label += "\"" + s.getColValue()[2]+  "\",";
	 			 dataHombre += hombre + ",";
	 			 dataMujer += mujer + ",";
	 			 dataTotal += total + ",";

	 			   if(i<(infoCategorias.size()-1))
	 				  categoriaInfo+=",";
	 			   i++;
	 		   }
	 		  colores += "],";
	 		  label += "],";
	 		  dataHombre +="],";
	 		  dataMujer +="],";
	 		  dataTotal +="],";
	 		 
	 		 
	 		 String nodoHombre= "{"+label+"datasets: [{ label: '# de preferencias'," + dataHombre + colores +"}],}";
	 		 String nodoMujer= "{"+label+"datasets: [{ label: '# de preferencias'," + dataMujer + colores +"}],}";
	 		 String nodoTotal="{"+label+"datasets: [{ label: '# de preferencias'," + dataTotal + colores +"}],}";

	 		 this.categoriaInfoHombre = nodoHombre;
	 		 this.categoriaInfoMujer = nodoMujer;
	 		 this.categoriaInfoTotal = nodoTotal;
	 	    }else{
	 	    	
	 	    }
		 logger.info(categoriaInfo);
		return categoriaInfo;
	}
	
	public String getInfoVendedorJSON()
	{
		String[] labels = {"inscritos","noInscritos","tienda","vendedor","5"};
		return valueToJSON(this.infoVendedores, labels);
	}
	
	public String valueToJSON(List<GenericInfoTO> info, String [] labels)
	{
		
		if(info != null && info.size()>0)
		{
			String data = "[";
			Iterator<GenericInfoTO> i = info.iterator();
			while(i.hasNext())
			{
				GenericInfoTO to = i.next();
				data += "{";
				int it = 0;
				for(Object o: to.getColValue())
				{
					data += "\"" + labels[it] + "\":\"" + o.toString() + "\"" ;
					if((to.getColValue().length-1) == it)
					{
						data += "}";
						it=0;
					}else {
						data += ",";
					}
					it++;
				}
				
				if(i.hasNext())
				{
					data += ",";
				}else {
					data += "]";
				}
			}
			return data;
		}
		return "[]";
	}
	
	public void changeTienda(AjaxBehaviorEvent event) {
		logger.info("changeTienda: "+tienda);
		Controller c = Controller.getInstance();
		infoVendedores=c.getCantRegistrosTiendasVendedor(desde, hasta,tienda, usuario.getId());
		infoCategorias = c.getCantCategoriaPorGenero(desde,hasta,tienda, usuario.getId());	
		getCategoriaInfoJSON();
		target="#formulario\\:portienda";
	}
	
	public String getVendedorInfo() {
		int cantColores = 0;
		String data1 = "data: [";
		String data2 = "data: [";
		String labels = "[";
		 if(infoVendedores!=null){
	 		    Iterator it = infoVendedores.iterator();
	 		   vendedorInfo="[";
	 		   int i=0;
	 		   while (it.hasNext()){
	 			 	 			   
	 			  GenericInfoTO s = (GenericInfoTO) it.next();
					labels += "\"" + s.getColValue()[3] + "\"";
					data1 += s.getColValue()[0];
					data2 += s.getColValue()[1];
					cantColores++;
					if (i < (infoVendedores.size() - 1)) {
						labels += ",";
						data1 += ", ";
						data2 += ", ";
						i++;
					}
	 		   }
	 		  data1 += "]";
				data2 += "]";
				labels += "]";
	 	    }else{
	 	    	data1 += "]";
				data2 += "]";
				labels += "]";
	 	    }
		 String background1=this.getBackground(cantColores,4,cantColores);
			String background2=this.getBackground(cantColores,5,cantColores);
			
			String result = " { labels: "+labels+", datasets: [{ label: \"Inscritos\", stack: 'Stack 0', "+ data1 +", "+ background1 +", }, { label: \"No inscritos\", stack: 'Stack 0', "+ data2 +", "+ background2 +", }]}";
			logger.info(result);
			return result;
		
		
		
	}
	
	
	public void init()
	{
		isSecure();
		Controller c = Controller.getInstance();
		campanias = c.findAllCampaniasVigentesByEmpresa(usuario.getIdEmpresa());
//		
		
		if(usuario.getPerfil().getNombre().equals("Administrador")) {
			tiendas = c.getTiendasActivaByEmpresa(usuario.getIdEmpresa());
		}else if(usuario.getPerfil().getNombre().equals("AdministradorEmpresa")){
			tiendas = c.getTiendaActivaToAdministradorEmpresa(usuario.getIdEmpresa());
		}else{
			tiendas = usuario.getTiendas();
		}
		
		hasta = Utilidad.getHoy();
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.get(Calendar.DAY_OF_WEEK);
		logger.info((Calendar.DAY_OF_WEEK));
		logger.info((cal.getFirstDayOfWeek()));
		cal.add(Calendar.DAY_OF_MONTH, -7);
		
		Date da= cal.getTime();
		hasta = Utilidad.getHoy();
		desde = Utilidad.getFechaDateStr(da);
		experiencias = c.getExperienciaResumen(desde,hasta,usuario.getId());
		registros=c.getCantRegistrosTiendas(desde, hasta, usuario.getId());
		infoVendedores=c.getCantRegistrosTiendasVendedor(desde, hasta,tienda, usuario.getId());
		infoCategorias = c.getCantCategoriaPorGenero(desde,hasta,tienda, usuario.getId());
		getCategoriaInfoJSON();
		logger.info("fin init");
	}
	
	public void buscar() {
		Controller c = Controller.getInstance();
		logger.info("busqueda desde:" + desde + " , hasta: " +hasta);
		experiencias = c.getExperienciaResumen(desde,hasta,usuario.getId());
		infoVendedores=c.getCantRegistrosTiendasVendedor(desde, hasta,tienda, usuario.getId());
		infoCategorias = c.getCantCategoriaPorGenero(desde,hasta,tienda, usuario.getId());
		registros=c.getCantRegistrosTiendas(desde, hasta, usuario.getId());
		target="#formMenu";
		getCategoriaInfoJSON();
		logger.info("FIN BUSCAR");
//		clientes = c.findClienteReporte(usuario.getIdEmpresa(),tienda,campania,desde,hasta);
//		camposAdicionales = c.findCamposAdicionalesCampania(0);
//		allCategorias = c.findAllCategoriasByEmpresa(usuario.getIdEmpresa(), 3);
//		Iterator it = clientes.iterator();
//		while (it.hasNext()) {
//			ClienteTO cl=(ClienteTO)it.next();
//			cl.setCategoriasReporte(allCategorias);	
//			cl.setAdicionalesReporte(camposAdicionales);
//		}
	}
	
	public void getColores(){
		colores = new LinkedList<String>();
		this.colores.add("rgba(255, 99, 132, .7)");
		this.colores.add("rgba(54, 162, 235, .7)");
		this.colores.add("rgba(255, 205, 86, .7)");
		this.colores.add("rgba(130, 224, 170, .7)");
		this.colores.add("rgba(84, 153, 199, .7 )");
		this.colores.add("rgba(69, 179, 157, .7)");
		this.colores.add("rgba(195, 155, 211, .7)");
		this.colores.add("rgba(218, 247, 166, .7 )");
		

	}
	
	public String getBackground(int cantidadColeres, int start, int repeat){
		System.out.println("-------------Cantidad de colores: " + cantidadColeres);
		List<String> colores = new LinkedList<String>();
		colores.add("rgba(255, 99, 132, .7)");
		colores.add("rgba(54, 162, 235, .7)");
		colores.add("rgba(255, 205, 86, .7)");
		colores.add("rgba(130, 224, 170, .7)");
		colores.add("rgba(84, 153, 199, .7 )");
		colores.add("rgba(69, 179, 157, .7)");
		colores.add("rgba(195, 155, 211, .7)");
		colores.add("rgba(218, 247, 166, .7 )");
		int repCount = 0;
		String background = "backgroundColor: [";
		int color = start;
		for(int i=0; i< cantidadColeres; i++) {
			
			if(color >= colores.size()) {
				color = 0;
			}
			background += "\"" + colores.get(color) + "\"";
			
			if(repeat <= repCount) {
				repCount = 0;
				color ++;
			}else {
				repCount++;
			}
			
			
			if(i < cantidadColeres) {
				background += ", ";
			}

		}
		background += "]";
		return background;

	}
	
	public String getBorder(int cantidadColeres, int start, int repeat){
		System.out.println("-------------Cantidad de colores: " + cantidadColeres);
		List<String> colores = new LinkedList<String>();
		colores.add("rgba(255, 99, 132, 1)");
		colores.add("rgba(54, 162, 235, 1)");
		colores.add("rgba(255, 205, 86, 1)");
		colores.add("rgba(130, 224, 170, 1)");
		colores.add("rgba(84, 153, 199, 1 )");
		colores.add("rgba(69, 179, 157, 1)");
		colores.add("rgba(195, 155, 211, 1)");
		colores.add("rgba(218, 247, 166, 1 )");
		int repCount = 0;
		String background = "borderColor: [";
		int color = start;
		for(int i=0; i< cantidadColeres; i++) {
			
			if(color >= colores.size()) {
				color = 0;
			}
			background += "\"" + colores.get(color) + "\"";
			
			if(repeat <= repCount) {
				repCount = 0;
				color ++;
			}else {
				repCount++;
			}
			
			
			if(i < cantidadColeres) {
				background += ", ";
			}

		}
		background += "]";
		return background;

	}
}
