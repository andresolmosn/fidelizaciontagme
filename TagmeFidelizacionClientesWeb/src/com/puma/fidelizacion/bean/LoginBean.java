
package com.puma.fidelizacion.bean;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.UsuarioTO;

@ManagedBean (name="loginBean")
@SessionScoped
public class LoginBean implements Serializable {
	
	 	private static final long serialVersionUID = 1L;
		final static Logger logger = Logger.getLogger(LoginBean.class);
		private String nick;
		private String pass;
		private String compania;
		
	
		public String getCompania() {
			return compania;
		}

		public void setCompania(String compania) {
			this.compania = compania;
		}

		public String init() throws IOException, SQLException{
			this.compania = "";
			return "";
		}
		 
		public String cerrar(){
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			return "login.xhtml?faces-redirect=true";
		}
		public String autenticar(){
			
			logger.debug(nick+" "+pass);
			Controller c = Controller.getInstance();
			UsuarioTO usuario = c.autenticar(nick, pass, compania);
			logger.debug("usuario: "+usuario);
			if(usuario!=null ){
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				Map<String, Object> sessionMap = externalContext.getSessionMap();
				sessionMap.put("usuario",usuario);
				PerfilTO perfil = usuario.getPerfil();
				logger.debug(usuario.getNombre());
				logger.info("Perfil del usuario  ===> " + perfil);
				logger.info("Redireccionando al la def_url ===> " + perfil.getDef_url());
				return perfil.getDef_url() + "?faces-redirect=true";
			}
			else return "login";
		}
		public String getNick() {
			return nick;
		}

		public void setNick(String nick) {
			this.nick = nick;
		}

		public String getPass() {
			return pass;
		}

		public void setPass(String pass) {
			this.pass = pass;
		}
		
}
