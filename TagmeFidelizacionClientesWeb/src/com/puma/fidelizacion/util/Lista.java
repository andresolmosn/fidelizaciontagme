package com.puma.fidelizacion.util;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import sun.nio.cs.ext.ISO_8859_11;

public class Lista {

	public static <T> List<T> filter(Class<T> clazz, List<?> items) {
	    return items.stream()
	        .filter(clazz::isInstance)
	        .map(clazz::cast)
	        .collect(Collectors.toList());
	}
	private static final Charset UTF_8 = Charset.forName("UTF-8");
	private static final Charset ISO = Charset.forName("ISO-8859-1");
	public static void main (String []s) {
		String sd ="hol�";
		String textwithaccent="Th�s �s a text with accent";
		String textwithletter="�and�";

		String text1 = new String(textwithaccent.getBytes(ISO), UTF_8);
		String text2 = new String(textwithletter.getBytes(ISO),UTF_8);
		
		Charset charset = Charset.forName("UTF-8");

		CharBuffer m_buffer =  CharBuffer.wrap(sd.toCharArray()); 

		ByteBuffer s1 = charset.encode(m_buffer);
		String v = new String(s1.array());
		System.out.println(v);
//		System.out.println(text);
		
		String encodedWithISO88591 = "Hol��";
		System.out.println(encodedWithISO88591);
		
		try {
			String decodedToUTF8 = new String(encodedWithISO88591.getBytes("ISO-8859-1"), "UTF-8");
			System.out.println(decodedToUTF8);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	
		
		
	}
}
