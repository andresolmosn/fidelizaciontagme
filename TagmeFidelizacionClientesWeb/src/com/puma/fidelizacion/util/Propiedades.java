package com.puma.fidelizacion.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.omg.CORBA.portable.InputStream;

public class Propiedades {

	private Properties props = null;
	private String nombrePROP = "var/parametro.properties";
	private boolean desarrollo = true;
	private String nombrePROP_desarrollo = "parametro.properties";
	
	public String getValor(String prop){
		if(props==null){
			props = new Properties();
			try {
				if(desarrollo){
					props.load(getClass().getResourceAsStream(nombrePROP_desarrollo));
				}else{
					FileInputStream f=new FileInputStream(new File(nombrePROP));
					props.load(f);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return props.getProperty(prop);
	}

}
