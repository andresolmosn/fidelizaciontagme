package com.puma.fidelizacion.util;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtils;

import java.beans.PropertyDescriptor;


import com.sun.el.parser.ParseException;

public class UtilArray {


	private static String formatoFecha = "dd/MM/yyyy";
	private static String formatoFechaSP = "yyyyMMdd";
	private static String formatoFechaHora = "dd/MM/yyyy HH:mm";

	public static Date getFechaDate(String fecha) {
		return getFechaByFormat(fecha, formatoFecha);
	}

	public static Date getFechaDatetime(String fecha) {
		return getFechaByFormat(fecha, formatoFechaHora);
	}

	private static Date getFechaByFormat(String fecha, String formato) {
		if (fecha != null && fecha.length() > 0) {
			try {
				return new SimpleDateFormat(formato).parse(fecha);
			} catch (java.text.ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static String getFechaStrByFormat(Date fecha, String formato) {
		if (fecha != null)
			return new SimpleDateFormat(formato).format(fecha);
		else
			return "";
	}

	public static String getFechaDateStr(Date fecha) {
		return getFechaStrByFormat(fecha, formatoFecha);
	}

	public static String getFechaDateSPStr(Date fecha) {
		return getFechaStrByFormat(fecha, formatoFechaSP);
	}

	public static String getFechaStrDatetime(Date fecha) {
		return getFechaStrByFormat(fecha, formatoFechaHora);
	}



	public static Object transformarObject(Object from, Object to) {
		PropertyDescriptor[] objDescriptors = PropertyUtils.getPropertyDescriptors(from);
		PropertyDescriptor[] obj2Descriptors = PropertyUtils.getPropertyDescriptors(to);
		try {
			for (PropertyDescriptor objDescriptor : objDescriptors) {
				String propertyName = objDescriptor.getName();
				for (PropertyDescriptor objDescriptor2 : obj2Descriptors) {
					String propertyName2 = objDescriptor2.getName();
					Object propValue = PropertyUtils.getProperty(from, propertyName);
					if (propertyName.equals(propertyName2)) {
						Class<?> clazz = to.getClass();
						try {
							Field field = clazz.getDeclaredField(propertyName2);
							field.setAccessible(true);
							field.set(to, propValue);
						} catch (NoSuchFieldException e) {
							clazz = clazz.getSuperclass();
						} catch (Exception e) {
							throw new IllegalStateException(e);
						}
					}
				}
			}
			return to;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static List transformarListas(List  list, List linkedList, Class claseDestino) {
		try {
			for (int i = 0; i < list.size(); i++) {
				Object obj = list.get(i);
				Object resp = transformarObject(obj, claseDestino.newInstance());
				linkedList.add(resp);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return linkedList;
	}
}
