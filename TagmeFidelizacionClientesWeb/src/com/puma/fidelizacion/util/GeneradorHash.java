package com.puma.fidelizacion.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.zip.CRC32;
import java.util.zip.Checksum;


public class GeneradorHash {
	   // Miembros de clase
	   private String texto = "";
	//   private Algoritmo algoritmo = null;
	 
	   // Constantes
	   public static final String VERSION = "1.0";
	 
	  
	   public GeneradorHash() {
	      
	   }
	 
	  
	   
	   public enum Algoritmo {
	      MD2("MD2"), MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256"), SHA512(
	            "SHA-512"), CRC32("CRC32");
	 
	      private String nombre;
	 
	      private Algoritmo(String nombre) {
	         this.nombre = nombre;
	      }
	 
	      private String getNombre() {
	         return nombre;
	      }
	   }
	 
	   public String getTexto() {
	      return texto;
	   }
	   public String generateHash(String texto, Algoritmo algoritmo ) {
	      this.texto = texto;
	      String hash = "";
	      byte[] digest = null;
	      byte[] buffer = texto.getBytes();
	 
	      switch (algoritmo) {
	      case MD2:
	      case MD5:
	      case SHA1:
	      case SHA256:
	      case SHA512:
	         try {
	            MessageDigest md = MessageDigest.getInstance(algoritmo
	                  .getNombre());
	            md.reset();
	            md.update(buffer);
	            digest = md.digest();
	            hash = toHexadecimal(digest);
	         } catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	         }
	         break;
	 
	      case CRC32:
	         hash = getCRC32();
	         break;
	      }
	      return hash;
	   }
	 
	  
	   private String toHexadecimal(byte[] digest) {
	      String hash = "";
	      for (byte aux : digest) {
	         int b = aux & 0xff; // Hace un cast del byte a hexadecimal
	         if (Integer.toHexString(b).length() == 1)
	            hash += "0";
	         hash += Integer.toHexString(b);
	      }
	      return hash;
	   }
	 
	   private String getCRC32() {
	      String hash = null;
	      byte[] bytes = texto.getBytes();
	      Checksum crc32 = new CRC32();
	      crc32.reset();
	      crc32.update(bytes, 0, bytes.length);
	      hash = Long.toHexString(crc32.getValue()).toUpperCase();
	      return hash;
	   }
	    
	     
	  
	   public static void main(String[] args) {
		  GeneradorHash ejemplo = new GeneradorHash();
//	      ejemplo.setAlgoritmo(Algoritmo.SHA256);
	      int i=0;
	      while(i<20) {
			String texto="delaparra@gmail.com"+(new Date()).getTime()+ ((Math.random() * 9) + 1);
			System.out.println( ejemplo.generateHash(texto, Algoritmo.SHA256));
			i++;
	      }
	   }
}
