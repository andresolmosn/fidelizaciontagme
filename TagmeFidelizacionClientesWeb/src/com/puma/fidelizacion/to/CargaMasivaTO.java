package com.puma.fidelizacion.to;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class CargaMasivaTO {
	private int id;
	private String nombre;
	private String fecha;
	private Long cupones;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public Long getCupones() {
		return cupones;
	}
	public void setCupones(Long cupones) {
		this.cupones = cupones;
	}
	@Override
	public String toString() {
		return "CargaMasivaTO [id=" + id + ", nombre=" + nombre + ", fecha=" + fecha + ", cupones=" + cupones + "]";
	}
	


	

}
