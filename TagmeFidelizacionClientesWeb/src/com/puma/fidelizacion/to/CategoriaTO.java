package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Empresa;

public class CategoriaTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int prioridad;
	private String texto;
	private int vigente;
	private int empresa;
	private boolean seleccionado;

	
	public CategoriaTO() {}

	public CategoriaTO(Categoria categoria) {
		super();
		this.id = categoria.getId();
		this.prioridad = categoria.getPrioridad();
		this.texto = categoria.getTexto();
		this.vigente = categoria.isVigente();
		this.empresa = categoria.getEmpresa().getId();
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getPrioridad() {
		return prioridad;
	}


	public void setPrioridad(int prioridad) {
		this.prioridad = prioridad;
	}


	public String getTexto() {
		return texto;
	}


	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public int isVigente() {
		return vigente;
	}

	public void setVigente(int vigente) {
		this.vigente = vigente;
	}

	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	
	
	public boolean isSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}

	public int getVigente() {
		return vigente;
	}

	@Override
	public String toString() {
		return "CategoriaTO [id=" + id + ", prioridad=" + prioridad + ", texto=" + texto + ", vigente=" + vigente
				+ ", empresa=" + empresa + ", seleccionado=" + seleccionado + "]";
	}

	


	
}
