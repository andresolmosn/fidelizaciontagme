package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.puma.fidelizacion.db.model.MenuIntranet;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.util.Utilidad;

public class EstadisticaTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int enviado;
	private int rechazado;
	private int abierto;
	private int clickeado;
	private String fecha;
	private String fechaFormateada;
	
	private double enviadoClick;
		private double enviadoAbierto;
	private double abiertoClick;
	
	public EstadisticaTO(String v_time, Integer v_del, Integer v_fail_p, Integer v_fail_t, Integer v_open,
			Integer v_click) {
		fecha=v_time;
		enviado= v_del;
		rechazado=v_fail_p;
		abierto=v_open;
		clickeado=v_click;
		
	}
	
	
	public double getEnviadoClick() {
		if(enviado !=0)
			enviadoClick = Math.round(new Double(clickeado)/new Double(enviado) * 100.0 );
		
		return enviadoClick;
	}


	public void setEnviadoClick(double enviadoClick) {
		this.enviadoClick = enviadoClick;
	}


	public double getEnviadoAbierto() {
		if(enviado !=0)
			enviadoAbierto = Math.round(new Double(abierto)/ new Double(enviado) * 100.0 );
		return enviadoAbierto;
	}


	public void setEnviadoAbierto(double enviadoAbierto) {
		this.enviadoAbierto = enviadoAbierto;
	}


	public double getAbiertoClick() {
		if(abierto!=0)
			abiertoClick = Math.round(new Double(clickeado)/ new Double(abierto) * 100.0);
		return abiertoClick;
	}


	public void setAbiertoClick(double abiertoClick) {
		this.abiertoClick = abiertoClick;
	}


	public String getFechaFormateada() {
		fechaFormateada = Utilidad.getFechaDateStr( Utilidad.getFechaDateMailGun(fecha.replaceAll(" UTC", "")));
		return fechaFormateada;
	}


	public void setFechaFormateada(String fechaFormateada) {
		this.fechaFormateada = fechaFormateada;
	}


	public int getEnviado() {
		return enviado;
	}
	public void setEnviado(int enviado) {
		this.enviado = enviado;
	}
	public int getRechazado() {
		return rechazado;
	}
	public void setRechazado(int rechazado) {
		this.rechazado = rechazado;
	}
	public int getAbierto() {
		return abierto;
	}
	public void setAbierto(int abierto) {
		this.abierto = abierto;
	}
	public int getClickeado() {
		return clickeado;
	}
	public void setClickeado(int clickeado) {
		this.clickeado = clickeado;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	

}
