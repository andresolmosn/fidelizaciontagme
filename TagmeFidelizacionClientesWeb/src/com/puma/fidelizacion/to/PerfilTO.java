package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

import com.puma.fidelizacion.db.model.Perfil;

public class PerfilTO implements Serializable{
	private static final Long serialVersionUID = 1L;
	
	private int id;
	private String nombre;
	private String def_url;
	private List<MenuIntranetTO> menuIntranet;
	
	
	public PerfilTO() {
		super();
	}
	
	public PerfilTO(Perfil p, List<MenuIntranetTO> menuIntranet) {
		super();
		this.id = p.getId();
		this.nombre = p.getNombre();
		this.menuIntranet = menuIntranet;
		this.def_url = p.getDef_url();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<MenuIntranetTO> getMenuIntranet() {
		return menuIntranet;
	}
	public void setMenuIntranet(List<MenuIntranetTO> menuIntranet) {
		this.menuIntranet = menuIntranet;
	}
	
	public String getDef_url() {
		return def_url;
	}

	public void setDef_url(String def_url) {
		this.def_url = def_url;
	}

	@Override
	public String toString() {
		return "PerfilTO \n[id=" + id + ", nombre=" + nombre + ", def_url=" + def_url + ", menuIntranet="
				+ menuIntranet + "]";
	}

 


	
}
