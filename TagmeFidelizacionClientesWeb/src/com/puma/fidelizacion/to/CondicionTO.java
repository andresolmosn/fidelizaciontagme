package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;

public class CondicionTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String texto;
	private int empresaId;
	
	public CondicionTO(Condicion c) {
		super();
		this.id = c.getId();
		this.texto = c.getTexto().replaceAll("<br />", "\n" );
		this.empresaId = c.getEmpresa();
	}
	
	public CondicionTO() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	public int getEmpresaId() {
		return empresaId;
	}
	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}
	@Override
	public String toString() {
		return "CondicionTO [id=" + id + ", texto=" + texto + ", empresaId=" + empresaId + "]";
	}
	


	

}
