package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.util.Utilidad;


public class CampaniaTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nombre;
	private String desde;
	private String estado;
	private String hasta;
	private int empresaId;

	private List<FormatoMailTO> formato;
	private FormularioTO formulario;
	private List<MenuTO>  menu;
	private String asunto;
	private TipoCampaniaTO tipoCampania;
	
	public CampaniaTO() {}
	
	
	public CampaniaTO(Campania c) {
		super();
		if(c!=null){
		this.id = c.getId();
		this.nombre = c.getNombre();
		this.desde = Utilidad.getFechaDateStr(c.getDesde());
		this.estado = c.getEstado();
		this.hasta =  Utilidad.getFechaDateStr(c.getHasta());
		this.empresaId = c.getEmpresa().getId();

		this.asunto = c.getAsunto();
		this.tipoCampania = new TipoCampaniaTO(c.getTipoCampania());
//		this.formato = c.getFormatoMails();
//		this.formulario = new FormularioTO(c.getFormulario());
//		this.menu = c.getMenu();
		}
	}
	
	public FormularioTO getFormulario() {
		return formulario;
	}

	public void setFormulario(FormularioTO formulario) {
		this.formulario = formulario;
	}

	public List<MenuTO> getMenu() {
		return menu;
	}

	public void setMenu(List<MenuTO> menu) {
		this.menu = menu;
	}

	public List<FormatoMailTO> getFormato() {
		return formato;
	}

	public void setFormato(List<FormatoMailTO> formato) {
		this.formato = formato;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDesde() {
		return desde;
	}

	public void setDesde(String desde) {
		this.desde = desde;
	}

	public String getHasta() {
		return hasta;
	}

	public void setHasta(String hasta) {
		this.hasta = hasta;
	}

	public int getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	
	public TipoCampaniaTO getTipoCampania() {
		return tipoCampania;
	}


	public void setTipoCampania(TipoCampaniaTO tipoCampania) {
		this.tipoCampania = tipoCampania;
	}


	public String getAsunto() {
		return asunto;
	}


	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}


	@Override
	public String toString() {
		return "CampaniaTO [id=" + id + ", nombre=" + nombre + ", desde=" + desde + ", estado=" + estado + ", hasta="
				+ hasta + ", empresaId=" + empresaId + ", formato=" + formato + ", formulario=" + formulario + ", menu="
				+ menu + ", asunto=" + asunto + ", tipoCampania=" + tipoCampania + "]";
	}



}
