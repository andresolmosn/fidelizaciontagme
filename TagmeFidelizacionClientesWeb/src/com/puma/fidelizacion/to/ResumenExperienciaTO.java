package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

public class ResumenExperienciaTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int experiencia; 
	private String estadoRegistro;
	private String  vendedor;
	private String tienda ;
	private Date f_registroTienda;
	private Date f_envio;
	private Date f_actualizacion;
	private int vendedor_id;
	private int tienda_id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getExperiencia() {
		return experiencia;
	}
	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}
	public String getEstadoRegistro() {
		return estadoRegistro;
	}
	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}
	public String getVendedor() {
		return vendedor;
	}
	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}
	public String getTienda() {
		return tienda;
	}
	public void setTienda(String tienda) {
		this.tienda = tienda;
	}
	
	public int getVendedor_id() {
		return vendedor_id;
	}
	public void setVendedor_id(int vendedor_id) {
		this.vendedor_id = vendedor_id;
	}
	public int getTienda_id() {
		return tienda_id;
	}
	public void setTienda_id(int tienda_id) {
		this.tienda_id = tienda_id;
	}
	public Date getF_registroTienda() {
		return f_registroTienda;
	}
	public void setF_registroTienda(Date f_registroTienda) {
		this.f_registroTienda = f_registroTienda;
	}
	public Date getF_envio() {
		return f_envio;
	}
	public void setF_envio(Date f_envio) {
		this.f_envio = f_envio;
	}
	public Date getF_actualizacion() {
		return f_actualizacion;
	}
	public void setF_actualizacion(Date f_actualizacion) {
		this.f_actualizacion = f_actualizacion;
	}
	
	
}
