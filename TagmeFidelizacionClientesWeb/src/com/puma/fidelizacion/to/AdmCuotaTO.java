package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Date;

import com.puma.fidelizacion.db.model.AdmCuota;

public class AdmCuotaTO implements Serializable{

	private static final Long serialVersionUID = 1L;
	
	private int id;
	private int cantidad;
	private Date fecha;
	private int vigencia;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getVigencia() {
		return vigencia;
	}
	public void setVigencia(int vigencia) {
		this.vigencia = vigencia;
	}
	public AdmCuotaTO(int id, int cantidad, Date fecha, int vigencia) {
		super();
		this.id = id;
		this.cantidad = cantidad;
		this.fecha = fecha;
		this.vigencia = vigencia;
	}
	public AdmCuotaTO() {
		super();
	}
	public AdmCuotaTO(AdmCuota admCuota) {
		super();
		this.id = admCuota.getId();
		this.cantidad = admCuota.getCantidad();
		this.fecha = admCuota.getFecha();
		this.vigencia = admCuota.getVigencia();
	}
	
	
	
	
}
