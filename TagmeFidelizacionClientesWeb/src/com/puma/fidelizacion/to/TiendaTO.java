package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.Tienda;

public class TiendaTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int id;
	
	private String nombre;

	private String tipo;

	private String ubicacion;
	
	private int estado;
	
	private int empresaId;
	
	public TiendaTO() {}
	public TiendaTO(Tienda t) {
		id= t.getId();
		nombre=t.getNombre();
		tipo=t.getTipo();
		ubicacion=t.getUbicacion();
		estado=t.getEstado();
		empresaId = t.getEmpresa().getId();
	}
	
	public TiendaTO(int idTienda) {
		id= idTienda;
	}

	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	

	public int getEmpresaId() {
		return empresaId;
	}
	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}
	@Override
	public String toString() {
		return "TiendaTO [id=" + id + ", nombre=" + nombre + ", tipo=" + tipo + ", ubicacion=" + ubicacion + "]";
	}
	
	

}
