package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.puma.fidelizacion.db.CamposAdicionalesDAO;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.Utilidad;

public class ClienteTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	Logger logger = Logger.getLogger(ClienteTO.class);
	private int id;
	private String apellidoM;
	private String apellidoP;
	private Date fechaNacimiento;
	private String fono;
	private String hash;
	private String mail;
	private String movil;
	private String nombre;
	private String rut;
	private String sexo;
	private int experiencia;
	private int empresa_id;
	private int registro;
	private int tienda_id;
	private int usuario_id;
	private Date fecha_actualizacion;
	private Date fecha_envio;
	private Date fecha_registro;
	private List<CategoriaTO> categorias;
	private TiendaTO tienda;
	private UsuarioTO vendedor;
	private List<CamposAdicionalesClienteTO> adicionales;
	private int cantAdicionales;
	private int cantCategorias;
	private List<CategoriaTO> categoriasReporte;
	private List<CamposAdicionalesClienteTO> adicionalesReporte;
	private TipoCampaniaTO tipoCampania;
	private int unsubscribe;
	private Date fechaUnsubscribe;
	private String fechaNacFormateada;
	private Date enviado;
	private Date aceptado;
	private Date fallado;
	private Date falso;
	private Date abierto;
	private Date clickeado;
	private Date reclamo;
	
	private String enviadoStr;
	private String aceptadoStr;
	private String falladoStr;
	private String falsoStr;
	private String abiertoStr;
	private String clickeadoStr;
	private String reclamoStr;
	
	public String getAceptadoStr() {
		return Utilidad.getFechaStrDatetime(aceptado);
	}

	public void setAceptadoStr(String aceptadoStr) {
		this.aceptadoStr = aceptadoStr;
	}

	public String getFalladoStr() {
		return Utilidad.getFechaStrDatetime(fallado);
	}

	public void setFalladoStr(String falladoStr) {
		this.falladoStr = falladoStr;
	}

	public String getFalsoStr() {
		return Utilidad.getFechaStrDatetime(falso);
	}

	public void setFalsoStr(String falsoStr) {
		this.falsoStr = falsoStr;
	}

	public String getAbiertoStr() {
		return Utilidad.getFechaStrDatetime(abierto);
	}

	public void setAbiertoStr(String abiertoStr) {
		this.abiertoStr = abiertoStr;
	}

	public String getClickeadoStr() {
		return Utilidad.getFechaStrDatetime(clickeado);
	}

	public void setClickeadoStr(String clickeadoStr) {
		this.clickeadoStr = clickeadoStr;
	}

	public String getReclamoStr() {
		return Utilidad.getFechaStrDatetime(reclamo);
	}

	public void setReclamoStr(String reclamoStr) {
		this.reclamoStr = reclamoStr;
	}

	public String getEnviadoStr() {
		
		return Utilidad.getFechaStrDatetime(enviado);
	}

	public void setEnviadoStr(String enviadoStr) {
		this.enviadoStr = enviadoStr;
	}

	public String getFechaNacFormateada() {
		if(fechaNacimiento!=null)
			fechaNacFormateada= Utilidad.getFechaDateStr(fechaNacimiento);
		return fechaNacFormateada;
	}

	public void setFechaNacFormateada(String fechaNacFormateada) {
		this.fechaNacFormateada = fechaNacFormateada;
	}

	public int getUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(int unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public String getFechaUnsubscribe() {
		return Utilidad.getFechaDateStr(fechaUnsubscribe);
	}

	public Date getFechaUnsubscribeDate() {
		return fechaUnsubscribe;
	}

	public void setFechaUnsubscribe(Date fechaUnsubscribe) {
		this.fechaUnsubscribe = fechaUnsubscribe;
	}

	public List<CategoriaTO> getCategoriasReporte() {
		return categoriasReporte;
	}

	public int getCantCategorias() {
		cantCategorias=0;
		if(categorias!=null)cantCategorias=categorias.size();
		return cantCategorias;
	}

	public void setCantCategorias(int cantCategorias) {
		this.cantCategorias = cantCategorias;
	}

	public int getCantAdicionales() {
		cantAdicionales=0;
		if(adicionales!=null)cantAdicionales = adicionales.size();
		return cantAdicionales;
	}

	public void setCantAdicionales(int cantAdicionales) {
		this.cantAdicionales = cantAdicionales;
	}

	public void setTipoCampania(TipoCampaniaTO tipoCampania) {
		this.tipoCampania = tipoCampania;
	}

	public ClienteTO() {
		
	}
	
	public ClienteTO(Cliente c) {
		super();
		this.id = c.getId();
		this.apellidoM = c.getApellidoM();
		this.apellidoP = c.getApellidoP();
		this.fechaNacimiento = c.getFechaNacimiento();
		this.fono = c.getFono();
		this.hash = c.getHash();
		this.mail = c.getMail();
		this.movil = c.getMovil();
		this.nombre = c.getNombre();
		this.rut = c.getRut();
		this.sexo = c.getSexo();
		this.unsubscribe = c.getUnsuscribe();
		this.fechaUnsubscribe = c.getFecha_unsuscribe();
		this.empresa_id = c.getEmpresa().getId();
		this.categorias = new ArrayList<CategoriaTO>();
		Iterator<Categoria> i = c.getCategorias().iterator();
		while(i.hasNext())
		{
			CategoriaTO to = new CategoriaTO(i.next());
			this.categorias.add(to);
		}
		this.registro = c.getRegistro();
		this.experiencia = c.getExperiencia();
		this.fecha_actualizacion = c.getFecha_actualizacion();
		this.fecha_envio = c.getFecha_envio();
		this.fecha_registro=c.getFecha_registro();
		if(c.getTipoCampania()!=null)
			this.tipoCampania = new TipoCampaniaTO(c.getTipoCampania());
		
		
		enviado = c.getEnviado();
		aceptado = c.getAceptado();
		fallado = c.getFallado();

		falso = c.getFalso();
		abierto = c.getAbierto();
		clickeado = c.getClickeado();
		reclamo = c.getReclamo();
		
	}



	public List<CamposAdicionalesClienteTO> getAdicionales() {
		return adicionales;
	}

	public void setAdicionales(List<CamposAdicionalesClienteTO> adicionales) {
		this.adicionales = adicionales;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getApellidoM() {
		return apellidoM;
	}

	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}

	public String getApellidoP() {
		return apellidoP;
	}

	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFono() {
		return fono;
	}

	public void setFono(String fono) {
		this.fono = fono;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	
	public List<CategoriaTO> getCategorias() {
		return categorias;
	}
	
	public int getTienda_id() {
		return tienda_id;
	}

	public void setTienda_id(int tienda_id) {
		this.tienda_id = tienda_id;
	}

	public int getUsuario_id() {
		return usuario_id;
	}
	
	public void setUsuario_id(int usuario_id) {
		this.usuario_id = usuario_id;
	}

	public void setCategorias(List<CategoriaTO> categorias) {
		this.categorias = categorias;
	}

	public int getExperiencia() {
		return experiencia;
	}

	public void setExperiencia(int experiencia) {
		this.experiencia = experiencia;
	}
	
	public int getEmpresa_id() {
		return empresa_id;
	}

	public void setEmpresa_id(int empresa_id) {
		this.empresa_id = empresa_id;
	}

	public int getRegistro() {
		return registro;
	}

	public void setRegistro(int registro) {
		this.registro = registro;
	}
	
	public String getFecha_actualizacion() {
		return Utilidad.getFechaStrDatetime(fecha_actualizacion);
	}

	public void setFecha_actualizacion(Date fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}

	public String getFecha_envio() {
		return Utilidad.getFechaStrDatetime(fecha_envio);
	}

	public void setFecha_envio(Date fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	public String getFecha_registro() {
		return Utilidad.getFechaStrDatetime(fecha_registro);
	}

	public String getFechaUnsubscribeStr() {
		if(fechaUnsubscribe!=null)
		return Utilidad.getFechaStrDatetime(fechaUnsubscribe);
		else
			return "";
	}
	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public TipoCampaniaTO getTipoCampania() {
		return tipoCampania;
	}


	@Override
	public String toString() {
		return "ClienteTO [id=" + id + ", apellidoM=" + apellidoM + ", apellidoP=" + apellidoP + ", fechaNacimiento="
				+ fechaNacimiento + ", fono=" + fono + ", hash=" + hash + ", mail=" + mail + ", movil=" + movil
				+ ", nombre=" + nombre + ", rut=" + rut + ", sexo=" + sexo + ", experiencia=" + experiencia
				+ ", empresa_id=" + empresa_id + ", registro=" + registro + ", tienda_id=" + tienda_id + ", usuario_id="
				+ usuario_id + ", fecha_actualizacion=" + fecha_actualizacion + ", fecha_envio=" + fecha_envio
				+ ", fecha_registro=" + fecha_registro + ", categorias=" + categorias + "]";
	}

	public TiendaTO getTienda() {
		return tienda;
	}

	public void setTienda(TiendaTO tienda) {
		this.tienda = tienda;
	}

	public UsuarioTO getVendedor() {
		return vendedor;
	}

	public void setVendedor(UsuarioTO vendedor) {
		this.vendedor = vendedor;
	}
	
	public String getEnviado() {
		return Utilidad.getFechaDateStr(enviado);
	}
	
	public Date getEnviadoDate() {
		return enviado;
	}

	public void setEnviado(Date enviado) {
		this.enviado = enviado;
	}

	public String getAceptado() {
		return Utilidad.getFechaDateStr(aceptado);
	}
	
	public Date getAceptadoDate() {
		return aceptado;
	}

	public void setAceptado(Date aceptado) {
		this.aceptado = aceptado;
	}

	public String getFallado() {
		return Utilidad.getFechaDateStr(fallado);
	}
	public Date getFalladoDate() {
		return fallado;
	}

	public void setFallado(Date fallado) {
		this.fallado = fallado;
	}

	public String getFalso() {
		return Utilidad.getFechaDateStr(falso);
	}
	
	public Date getFalsoDate() {
		return falso;
	}

	public void setFalso(Date falso) {
		this.falso = falso;
	}

	public String getAbierto() {
		return Utilidad.getFechaDateStr(abierto);
	}
	
	public Date getAbiertoDate() {
		return abierto;
	}

	public void setAbierto(Date abierto) {
		this.abierto = abierto;
	}

	public String getClickeado() {
		return Utilidad.getFechaDateStr(clickeado);
	}
	
	public Date getClickeadoDate() {
		return clickeado;
	}

	public void setClickeado(Date clickeado) {
		this.clickeado = clickeado;
	}

	public String getReclamo() {
		return Utilidad.getFechaDateStr(reclamo);
	}

	public Date getReclamoDate() {
		return reclamo;
	}
	
	public void setReclamo(Date reclamo) {
		this.reclamo = reclamo;
	}
	
	

	public void setCategoriasReporte(List<CategoriaTO> allCategorias) {
		categoriasReporte = new LinkedList();
		Iterator i = allCategorias.iterator();
		while(i.hasNext()) {
			CategoriaTO tmp =(CategoriaTO)i.next();
			
			CategoriaTO nuevo= new CategoriaTO ();
			Iterator mi = categorias.iterator();
			while(mi.hasNext()) {
				CategoriaTO to= (CategoriaTO)mi.next();
				
				if(to.getId()==tmp.getId()) {
					nuevo.setSeleccionado(true);
					break;
				}else
					nuevo.setSeleccionado(false);
				
			}
			categoriasReporte.add(nuevo);
		}
	}

	public List<CamposAdicionalesClienteTO> getAdicionalesReporte() {
		return adicionalesReporte;
	}

	public void setAdicionalesReporte(List<CamposAdicionalesCampaniaTO> allAdicionales) {
		adicionalesReporte = new LinkedList();
		Iterator i = allAdicionales.iterator();
		while(i.hasNext()) {
			CamposAdicionalesCampaniaTO tmp =(CamposAdicionalesCampaniaTO)i.next();
			
			CamposAdicionalesClienteTO nuevo= new CamposAdicionalesClienteTO ();
			Iterator mi = adicionales.iterator();
			while(mi.hasNext()) {
				CamposAdicionalesClienteTO to= (CamposAdicionalesClienteTO)mi.next();
				
				if(to.getIdCampoCampania()==tmp.getCampo().getId()) {
					nuevo.setValorIngresado(to.getValor());
					break;
				}else
					nuevo.setValorIngresado("");
				
			}
			adicionalesReporte.add(nuevo);
		}
		
		this.adicionalesReporte = adicionalesReporte;
	}
	
	
	
}
