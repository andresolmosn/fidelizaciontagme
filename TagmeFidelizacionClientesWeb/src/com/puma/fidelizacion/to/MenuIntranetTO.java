package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.MenuIntranet;

public class MenuIntranetTO implements Serializable{
	private static final Long serialVersionUID =1L;
	
	private int id;
	private String nombre;
	private String url;
	private int idPadre;
	private int orden;
	
	public MenuIntranetTO() {
		super();	
	}
	
	public MenuIntranetTO(MenuIntranet menu) {
		super();
		this.id = menu.getId();
		this.nombre = menu.getNombre();
		this.url = menu.getUrl();
		this.idPadre = menu.getIdPadre();
		this.orden = menu.getOrden();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getIdPadre() {
		return idPadre;
	}
	public void setIdPadre(int idPadre) {
		this.idPadre = idPadre;
	}
	public int getOrden() {
		return orden;
	}
	public void setOrden(int orden) {
		this.orden = orden;
	}
	@Override
	public String toString() {
		return "MenuIntranetTO \n[id=" + id + ", nombre=" + nombre + ", url=" + url + ", idPadre=" + idPadre
				+ ", orden=" + orden + "]";
	}
	
	
	
	
}
