
package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.puma.fidelizacion.db.model.MenuIntranet;
import com.puma.fidelizacion.db.model.Usuario;

public class UsuarioTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int id;
	private String nick;
	private String nombre;
//	private TiendaTO tienda;
	private int idEmpresa;
	private List<TiendaTO> tiendas; 
	private String image;
	private int estado; 
	private String apellido;
	private PerfilTO perfil;
	private String pass;
	
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public UsuarioTO() {}
	
	public UsuarioTO(Usuario u ) {
		id=u.getId();
		nick = u.getNick();
		nombre=u.getNombre();
		apellido=u.getApellido();
		estado=u.getEstado();
		
		List<MenuIntranetTO> listaTO = new LinkedList<MenuIntranetTO>();
		List<MenuIntranet> lista = u.getPerfil().getMenuIntranet();
		if(lista != null && lista.size() > 0)
		{
			Iterator<MenuIntranet> i = lista.iterator();
			while(i.hasNext()) {
				MenuIntranet menu = i.next();
				MenuIntranetTO to = new MenuIntranetTO(menu);
				listaTO.add(to);
			}
		}
		
		perfil=new PerfilTO(u.getPerfil(), listaTO);
		
		
	}
	public String getImage() {
		return image;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public List<TiendaTO> getTiendas() {
		return tiendas;
	}
	public void setTiendas(List<TiendaTO> tiendas) {
		this.tiendas = tiendas;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

//	public TiendaTO getTienda() {
//		return tienda;
//	}
//
//	public void setTienda(TiendaTO tienda) {
//		this.tienda = tienda;
//	}
	public PerfilTO getPerfil() {
		return perfil;
	}
	public void setPerfil(PerfilTO perfil) {
		this.perfil = perfil;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	

}
