package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.CountCuota;

public class CountCuotaTO implements Serializable{
	private static final Long serialVersionUID = 1L;
	
	private int id;
	private int empresaId;
	private int cuota;
	private String estado;
	private int countCuota;
	public int getEmpresaId() {
	
	return empresaId;
	}
	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}
	public int getCuota() {
		return cuota;
	}
	public void setCuota(int cuota) {
		this.cuota = cuota;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getCountCuota() {
		return countCuota;
	}
	public void setCountCuota(int countCuota) {
		this.countCuota = countCuota;
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public CountCuotaTO(int empresaId, int cuota, String estado, int countCuota) {
		super();
		
		this.empresaId = empresaId;
		this.cuota = cuota;
		this.estado = estado;
		this.countCuota = countCuota;
	}
	
	public CountCuotaTO(CountCuota countCuota) {
		super();
		this.id = countCuota.getId();
		this.empresaId = countCuota.getEmpresaId();
		this.cuota = countCuota.getCuota();
		this.estado = countCuota.getEstado();
		this.countCuota = countCuota.getCountCuota();
	}
	public CountCuotaTO() {
		super();
	}
	@Override
	public String toString() {
		return "CountCuotaTO [\\nempresaId=" + empresaId + ", cuota=" + cuota + ", estado=" + estado + ", countCuota="
				+ countCuota + "]";
	}
	
	
	
	
}
