package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.Empresa;

public class EmpresaTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nombre;
	private String razonSocial;
	private String rut;
	private String hash;
	private String estado;
	
	public EmpresaTO()  {}
	
	public EmpresaTO(Empresa empresa) {
		super();
		this.id = empresa.getId();
		this.nombre = empresa.getNombre();
		this.razonSocial = empresa.getRazonSocial();
		this.rut = empresa.getRut();
		this.hash = empresa.getHash();
		this.estado = empresa.getEstado();
	}

	public EmpresaTO(int id, String nombre, String razonSocial, String rut, String estado) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.razonSocial = razonSocial;
		this.rut = rut;
		this.estado = estado;
	}
	
	

	public EmpresaTO(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}
	
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "EmpresaTO [id=" + id + ", nombre=" + nombre + ", razonSocial=" + razonSocial + ", rut=" + rut
				+ ", hash=" + hash + "]";
	}

	
	
	

}
