package com.puma.fidelizacion.to;

import java.io.Serializable;

public class MensajeTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public static final String WARNING = "WARNING";
	public static final String ERROR = "ERROR";
	public static final String INFO = "INFO";
	public static final String SUCCESS = "SUCCESS";
	public static final String ALERT = "ALERT";
	
	private String titulo;
	private String mensaje;
	private String tipo;
	
	public MensajeTO() {
		super();
	}
	public MensajeTO(String mensaje, String tipo) {
		super();
		this.mensaje = mensaje;
		this.tipo = tipo;
	}
	
	public MensajeTO(String titulo, String mensaje, String tipo) {
		super();
		this.mensaje = mensaje;
		this.tipo = tipo;
		this.titulo= titulo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	@Override
	public String toString() {
		return "MensajeTO [titulo=" + titulo + ", mensaje=" + mensaje + ", tipo=" + tipo + "]";
	}
	
	
	
}
