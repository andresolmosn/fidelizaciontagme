package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

import com.puma.fidelizacion.db.model.CamposAdicionales;
import com.puma.fidelizacion.db.model.CamposAdicionalesCliente;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Empresa;

public class CamposAdicionalesClienteTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String valor;
	private String valorIngresado;
	private int idCampoCampania;
	
	public int getIdCampoCampania() {
		return idCampoCampania;
	}

	public void setIdCampoCampania(int idCampoCampania) {
		this.idCampoCampania = idCampoCampania;
	}

	public CamposAdicionalesClienteTO() {}

	public CamposAdicionalesClienteTO(CamposAdicionalesCliente dao) {
		super();
		id=dao.getId();
		valor=dao.getValor();
		idCampoCampania  = dao.getCampoCampania().getId();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValorIngresado() {
		return valorIngresado;
	}

	public void setValorIngresado(String valorIngresado) {
		this.valorIngresado = valorIngresado;
	}
	
}

