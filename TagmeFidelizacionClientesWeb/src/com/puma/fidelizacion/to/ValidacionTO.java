package com.puma.fidelizacion.to;

import java.io.Serializable;

public class ValidacionTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String campo;
	private boolean valido;
	private MensajeTO mensaje;

	
	public ValidacionTO(String campo, MensajeTO mensaje) {
		super();
		this.campo = campo;
		System.out.println(mensaje);
		switch(mensaje.getTipo()){
			case MensajeTO.ERROR: {
				this.valido = false;
				break;
			}
			case MensajeTO.WARNING: {
				this.valido = true;
				break;
			}
			case MensajeTO.INFO:{
				this.valido = true;
				break;
			}default:{
				this.valido = true;
			}
		}
		this.mensaje = mensaje;
	}
	
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public boolean isValido() {
		return valido;
	}
	public void setValido(boolean valido) {
		this.valido = valido;
	}
	public MensajeTO getMensaje() {
		return mensaje;
	}
	public void setMensaje(MensajeTO mensaje) {
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "ValidacionTO [campo=" + campo + ", valido=" + valido + ", mensaje=" + mensaje + "]";
	}
	
	

}
