package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.TipoCampania;

public class TipoCampaniaTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String nombre;
	private String url;
	private EmpresaTO empresa;
	
	
	
	public TipoCampaniaTO() {
		super();
	}

	public TipoCampaniaTO(TipoCampania Tipocampania) {
		super();
		this.id = Tipocampania.getId();
		this.nombre = Tipocampania.getNombre();
		this.url = Tipocampania.getUrl();
		this.empresa = new EmpresaTO(Tipocampania.getEmpresa());
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public EmpresaTO getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaTO empresa) {
		this.empresa = empresa;
	}
	@Override
	public String toString() {
		return "tipoCampania [id=" + id + ", nombre=" + nombre + ", url=" + url + ", empresa=" + empresa + "]";
	}
	
	
	

}
