package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

import com.puma.fidelizacion.db.model.CamposAdicionales;
import com.puma.fidelizacion.db.model.CamposAdicionalesCampania;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;

public class CamposAdicionalesCampaniaTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String valorCliente;
	private CamposAdicionalesTO campo;

	public CamposAdicionalesCampaniaTO() {}

	public CamposAdicionalesCampaniaTO(CamposAdicionalesCampania dao) {
		super();
		id=dao.getId();
		 
		campo = new CamposAdicionalesTO(dao.getDefCampo());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getValorCliente() {
		return valorCliente;
	}

	public void setValorCliente(String valorCliente) {
		this.valorCliente = valorCliente;
	}

	public CamposAdicionalesTO getCampo() {
		return campo;
	}

	public void setCampo(CamposAdicionalesTO campo) {
		this.campo = campo;
	}

	
}
