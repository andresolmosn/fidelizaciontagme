package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

public class HeaderMenuItemTO implements Serializable{
	
	private static final Long serialVersionUID = 1L;
	
	private MenuIntranetTO padre;
	private List<MenuIntranetTO> hijos;
	
	public MenuIntranetTO getPadre() {
		return padre;
	}
	public void setPadre(MenuIntranetTO padre) {
		this.padre = padre;
	}
	public List<MenuIntranetTO> getHijos() {
		return hijos;
	}
	public void setHijos(List<MenuIntranetTO> hijos) {
		this.hijos = hijos;
	}
	
	@Override
	public String toString() {
		return "HeaderMenuItemTO \n[padre=" + padre + ", hijos=" + hijos + "]";
	}
	
	

}
