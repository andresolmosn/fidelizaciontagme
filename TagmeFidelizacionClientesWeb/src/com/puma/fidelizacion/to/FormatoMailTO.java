package com.puma.fidelizacion.to;

import java.io.Serializable;

public class FormatoMailTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int id;
	
	private String texto;
	private String textoBoton;
	private String url;
	private String urlFoto;
	private int idCampania;
	
	public int getIdCampania() {
		return idCampania;
	}

	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}

	public FormatoMailTO() {}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getTextoBoton() {
		return textoBoton;
	}

	public void setTextoBoton(String textoBoton) {
		this.textoBoton = textoBoton;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrlFoto() {
		return urlFoto;
	}

	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}

	@Override
	public String toString() {
		return "ForamtoMailTO [id=" + id + ", texto=" + texto + ", textoBoton=" + textoBoton + ", url=" + url
				+ ", urlFoto=" + urlFoto + "]";
	}
}
