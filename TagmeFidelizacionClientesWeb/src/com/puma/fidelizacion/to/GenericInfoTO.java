package com.puma.fidelizacion.to;

import java.util.Arrays;

/**
 * @author Tagme Dev1
 *
 */
public class GenericInfoTO {

	
	private String[] colName;
	private Object[] colValue;
	
	public String[] getColName() {
		return colName;
	}
	public void setColName(String[] colName) {
		this.colName = colName;
	}
	public Object[] getColValue() {
		return colValue;
	}
	public void setColValue(Object[] colValue) {
		this.colValue = colValue;
	}
	@Override
	public String toString() {
		return "GenericInfoTO [colName=" + Arrays.toString(colName) + ", colValue=" + Arrays.toString(colValue)
				+ "]";
	}
	
	
	
}
