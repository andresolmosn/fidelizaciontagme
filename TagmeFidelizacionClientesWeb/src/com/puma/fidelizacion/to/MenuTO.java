package com.puma.fidelizacion.to;

import java.io.Serializable;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.Menu;

public class MenuTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int id;
	private int idMenuPadre;
	private int posicion;
	private String texto;
	private String url;
	private int empresa;
	private int ancho;
	private String imagen;
	private int anchoImagen;
	private int altoImagen;
	private CampaniaTO campania;
	public MenuTO() {}
	
	public MenuTO(Menu menu) {
		super();
		this.id = menu.getId();
		this.idMenuPadre = menu.getIdMenuPadre();
		this.posicion = menu.getPosicion();
		this.texto = menu.getTexto();
		this.url = menu.getUrl();
		this.anchoImagen=menu.getAncho();
		this.altoImagen=menu.getAlto();
		campania=new CampaniaTO(menu.getCampania());
		imagen=menu.getImagen();
	}
	
	public CampaniaTO getCampania() {
		return campania;
	}

	public void setCampania(CampaniaTO campania) {
		this.campania = campania;
	}

	public int getAnchoImagen() {
		return anchoImagen;
	}

	public void setAnchoImagen(int anchoImagen) {
		this.anchoImagen = anchoImagen;
	}

	public int getAltoImagen() {
		return altoImagen;
	}

	public void setAltoImagen(int altoImagen) {
		this.altoImagen = altoImagen;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getAncho() {
		return ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdMenuPadre() {
		return idMenuPadre;
	}

	public void setIdMenuPadre(int idMenuPadre) {
		this.idMenuPadre = idMenuPadre;
	}

	public int getPosicion() {
		return posicion;
	}

	public void setPosicion(int posicion) {
		this.posicion = posicion;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public int getEmpresa() {
		return empresa;
	}

	public void setEmpresa(int empresa) {
		this.empresa = empresa;
	}

	@Override
	public String toString() {
		return "MenuTO [id=" + id + ", idMenuPadre=" + idMenuPadre + ", posicion=" + posicion + ", texto=" + texto
				+ ", url=" + url + ", empresa=" + empresa + "]";
	}

	
	

}
