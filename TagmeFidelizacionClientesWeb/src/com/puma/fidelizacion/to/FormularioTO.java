package com.puma.fidelizacion.to;

import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.puma.fidelizacion.db.model.Campania;

public class FormularioTO {
	private static final long serialVersionUID = 1L;
	private int id;
	private String boton;
	private String seccion1;
	private String seccion2;
	private String seccion3;
 private boolean flags2;
 private boolean flags3;
	private int activoS2;
	private int activoS3;
	private String condiciones;
	private int idCampania;
	private int idEmpresa;
	
	
	public boolean isFlags2() {
		return flags2;
	}
	public void setFlags2(boolean flags2) {
		this.flags2 = flags2;
	}
	public boolean isFlags3() {
		return flags3;
	}
	public void setFlags3(boolean flags3) {
		this.flags3 = flags3;
	}
	public int getActivoS2() {
		return activoS2;
	}
	public void setActivoS2(int activoS2) {
		this.activoS2 = activoS2;
		setFlags2(activoS2==1?true:false);
	}
	public int getActivoS3() {
		return activoS3;
	}
	public void setActivoS3(int activoS3) {
		this.activoS3 = activoS3;
		setFlags3(activoS3==1?true:false);
	}
	
	public int getIdEmpresa() {
		return idEmpresa;
	}
	public void setIdEmpresa(int idEmpresa) {
		this.idEmpresa = idEmpresa;
	}
	public int getIdCampania() {
		return idCampania;
	}
	public void setIdCampania(int idCampania) {
		this.idCampania = idCampania;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBoton() {
		return boton;
	}
	public void setBoton(String boton) {
		this.boton = boton;
	}
	public String getSeccion1() {
		return seccion1;
	}
	public void setSeccion1(String seccion1) {
		this.seccion1 = seccion1;
	}
	public String getSeccion2() {
		return seccion2;
	}
	public void setSeccion2(String seccion2) {
		this.seccion2 = seccion2;
	}
	public String getSeccion3() {
		return seccion3;
	}
	public void setSeccion3(String seccion3) {
		this.seccion3 = seccion3;
	}
	public String getCondiciones() {
		return condiciones;
	}
	public void setCondiciones(String condiciones) {
		this.condiciones = condiciones;
	}
	@Override
	public String toString() {
		return "FormularioTO [id=" + id + ", boton=" + boton + ", seccion1=" + seccion1 + ", seccion2=" + seccion2
				+ ", seccion3=" + seccion3 + ", condiciones=" + condiciones + ", idCampania=" + idCampania
				+ ", idEmpresa=" + idEmpresa + "]";
	}
	
	
	
}
