package com.puma.fidelizacion.to;

import java.io.Serializable;
import javax.persistence.*;

import com.puma.fidelizacion.to.EmpresaTO;

import java.util.Date;
import java.util.List;



public class MailEstadoTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	
	private String mail;
	private String estado;
	private int campania;
	private Date fecha;
	private int procesado;
	public MailEstadoTO (String m, String e, int c ,Date f, int p) {
		mail=m;
		estado=e;
		campania = c;
		fecha=f;
		procesado=p;
	}
	
	
	public Date getFecha() {
		return fecha;
	}


	public int getProcesado() {
		return procesado;
	}


	public void setProcesado(int procesado) {
		this.procesado = procesado;
	}


	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public int getCampania() {
		return campania;
	}
	public void setCampania(int campania) {
		this.campania = campania;
	}
	 
}