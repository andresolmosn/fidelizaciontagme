package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.util.Utilidad;

public class CuponTO implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int id;
	private int asignado;
	private int campaniaId;
	private String codigo;
	private Date desde;
	private Date hasta;
	private int porcentaje;
	private Condicion condicion;
	private int cargaMasiva;
	private int clienteId;
	private int condicionId;
	private int empresaId;
	private Date fecha_utilizacion;
	
	
	public CuponTO(Cupon cu) {
		id= cu.getId();
		asignado=cu.getAsignado();
		campaniaId = cu.getCampaniaId();
		codigo=cu.getCodigo();
		desde=cu.getDesde();
		hasta=cu.getHasta();
		porcentaje=cu.getPorcentaje();
//		cargaMasiva=cu.getCargaMasiva();
		clienteId=getClienteId();
		condicionId=cu.getCondicionId();
		empresaId=cu.getEmpresaId();
		campaniaId=cu.getCampaniaId();
		fecha_utilizacion = cu.getFecha_utilizacion();
	}
	
	
	public int getEmpresaId() {
		return empresaId;
	}


	public void setEmpresaId(int empresaId) {
		this.empresaId = empresaId;
	}


	public CuponTO() {	}

	public int getId() {
		return id;
	}

	public int getCondicionId() {
		return condicionId;
	}
	public void setCondicionId(int condicionId) {
		this.condicionId = condicionId;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getAsignado() {
		return asignado;
	}

	public void setAsignado(int asignado) {
		this.asignado = asignado;
	}

	public Date getDesde() {
		return desde;
	}

	public void setDesde(Date desde) {
		this.desde = desde;
	}

	public Date getHasta() {
		return hasta;
	}

	public void setHasta(Date hasta) {
		this.hasta = hasta;
	}

	public int getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(int porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getCampaniaId() {
		return campaniaId;
	}

	public void setCampaniaId(int campaniaId) {
		this.campaniaId = campaniaId;
	}

	public Condicion getCondicion() {
		return condicion;
	}

	public void setCondicion(Condicion condicion) {
		this.condicion = condicion;
	}

	public int getCargaMasiva() {
		return cargaMasiva;
	}

	public void setCargaMasiva(int cargaMasiva) {
		this.cargaMasiva = cargaMasiva;
	}
	
	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}
	
	public Date getFecha_utilizacion() {
		return fecha_utilizacion;
	}
	
	public String getFecha_utilizacionFormateda() {
		return Utilidad.getFechaDateStr(fecha_utilizacion);
	}

	public void setFecha_utilizacion(Date fecha_utilizacion) {
		this.fecha_utilizacion = fecha_utilizacion;
	}

	@Override
	public String toString() {
		return "CuponTO [id=" + id + ", asignado=" + asignado + ", codigo=" + codigo + ", desde=" + desde + ", hasta="
				+ hasta + ", porcentaje=" + porcentaje + "]";
	}

	

}
