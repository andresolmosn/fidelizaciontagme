package com.puma.fidelizacion.to;

import java.io.Serializable;
import java.util.List;

import com.puma.fidelizacion.db.model.CamposAdicionales;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Empresa;

public class CamposAdicionalesTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private int id;

	private String label;
	private String tipo;

	public CamposAdicionalesTO() {}

	public CamposAdicionalesTO(CamposAdicionales dao) {
		super();
		id=dao.getId();
	
		label=dao.getLabel();
		tipo=dao.getTipo();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
}
