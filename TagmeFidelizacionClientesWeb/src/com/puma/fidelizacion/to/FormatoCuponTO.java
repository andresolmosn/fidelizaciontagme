package com.puma.fidelizacion.to;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.FormatoCupon;

public class FormatoCuponTO {
	private static final long serialVersionUID = 1L;
	private int id;
	private String urlFoto;
	private String asunto;
	private String texto;
	private int campania_id;
	private int cantFoto;
	
	
	public int getCantFoto() {
		cantFoto=urlFoto!=null && urlFoto.length()>0? urlFoto.length():0;
		return cantFoto;
	}
	public void setCantFoto(int cantFoto) {
		this.cantFoto = cantFoto;
	}
	public FormatoCuponTO( ) {
		asunto="";
		urlFoto="";
		texto="";
		
	}
	public FormatoCuponTO(FormatoCupon cu) {
		id = cu.getId();
		urlFoto = cu.getUrlFoto();
		asunto=cu.getAsunto();
		texto=cu.getTexto();
		if(cu.getCampania()!=null)
			campania_id=cu.getCampania().getId();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrlFoto() {
		return urlFoto;
	}
	public void setUrlFoto(String urlFoto) {
		this.urlFoto = urlFoto;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public int getCampania_id() {
		return campania_id;
	}
	public void setCampania_id(int campania_id) {
		this.campania_id = campania_id;
	}
	@Override
	public String toString() {
		return "FormatoCuponTO [id=" + id + ", urlFoto=" + urlFoto + ", asunto=" + asunto + ", texto=" + texto
				+ ", campania_id=" + campania_id + "]";
	}

	
	
	
	
}
