package com.puma.fidelizacion.alarma;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;

import com.puma.fidelizacion.business.Controller;

@Stateless
public class RevisionEstadoMailGun {

	public RevisionEstadoMailGun() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("unused")
	@Schedule(hour = "*/12",persistent=false )
    private void scheduledTimeout(final Timer t) {
        System.out.println("RevisionEstadoMailGun >>>> @Schedule called at: " + new java.util.Date());
        Controller c =  Controller.getInstance();
        c.buscarEstadosMailGun();
        System.out.println("FIN RevisionEstadoMailGun");
    }

}
