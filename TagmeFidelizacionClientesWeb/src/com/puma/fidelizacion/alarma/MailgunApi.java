package com.puma.fidelizacion.alarma;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.*;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.puma.fidelizacion.to.EstadisticaTO;
import com.puma.fidelizacion.to.MailEstadoTO;
import com.puma.fidelizacion.util.Utilidad;

public class MailgunApi {

	public static void main(String[] a) {
		//1.53521058177461E9
//		1535166540662168E9
	 
//		
		MailgunApi t = new MailgunApi();
		try {
//			List<MailEstadoTO> estados1 = t.callBounces("https://api.mailgun.net/v3/" + YOUR_DOMAIN_NAME + "/bounces",
//					new LinkedList<MailEstadoTO>());

			List<MailEstadoTO> estados = t.callEvents("https://api.mailgun.net/v3/" + YOUR_DOMAIN_NAME + "/events",
					new LinkedList<MailEstadoTO>());

			System.out.println("fin!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}

	public static String YOUR_DOMAIN_NAME = "alert.tagmeservices.com";
	public static String API_KEY = "bf8ee12b5fb70d4f08e386b723ac1b7b-a5d1a068-54c592e9";

	public List<MailEstadoTO> callEvents(String url, List<MailEstadoTO> estados) throws UnirestException, NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}

		} };
		SSLContext sslcontext = SSLContext.getInstance("SSL");
		sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
		CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.get(Calendar.DAY_OF_WEEK);
		cal.add(Calendar.HOUR_OF_DAY, -6);
		Date da= cal.getTime();
		
		Unirest.setHttpClient(httpclient);
System.out.println("DESDE:::: "+Utilidad.getFechaDateMailGunStr(da));
		HttpResponse<JsonNode> request = Unirest.get(url).basicAuth("api", API_KEY)
				.queryString("begin", Utilidad.getFechaDateMailGunStr(da)+" GMT")
				.queryString("end", Utilidad.getFechaDateMailGunStr(new Date())+" GMT")
				.asJson();

		JSONObject obj = request.getBody().getObject();

		JSONArray arr = obj.getJSONArray("items");
		if (arr != null && arr.length() == 0) {
			System.out.println("SALIR");
			return estados;
		}
		
		for (int i = 0; i < arr.length(); i++) {
			JSONObject o = arr.getJSONObject(i);
			String event = (String) o.get("event");
			Object d = (Object)o.get("timestamp");
			String fecha = d.toString();
			fecha=fecha.replace(".", "");
			boolean ignorar=false;
			JSONArray tags =  (JSONArray)o.getJSONArray("tags");
			String tagValue="15";
			if(tags!=null && tags.length()>0 && tags.get(0)!=null) {
				 tagValue = (String)tags.get(0);
				 if(tagValue.contains("cup:"))
					 ignorar=true;
				 else
					 tagValue=tagValue.replaceAll("cam:","").replaceAll("cup:", "");
			}

			String digito = fecha.substring(fecha.length()-1, fecha.length());
			fecha = fecha.substring(0, 10);
//		 	System.out.println(fecha);
			Date algo = new Date();
			String recipient = (String) o.get("recipient");
//			System.out.println(Long.parseLong(fecha.replace(".", "")));
//			System.out.println((new Date()).getTime());
			  Timestamp ts=new Timestamp((long)(Long.parseLong(fecha)*1000));  
              Date date=new Date(ts.getTime());  
               
//			String myDateStr = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(date.getTime());
			String type = "";
//  			System.out.println(recipient+" "+event );
//			System.out.println("java : "+date);
			if(!ignorar) {
				MailEstadoTO to = new MailEstadoTO(recipient, event, Integer.parseInt(tagValue),date ,0);
				estados.add(to);
			}
		}
		JSONObject paginas = obj.getJSONObject("paging");
		String next = (String) paginas.get("next");
//		String first = (String)paginas.get("first");
		String previous = (String) paginas.get("previous");
//		String last = (String)paginas.get("last");

		if (!next.equals(previous))
			callEvents(next, estados);
		return estados;
	}

	public List<MailEstadoTO> callBounces(String url, List<MailEstadoTO> estados) throws UnirestException,NoSuchAlgorithmException, KeyManagementException {
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {
			}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {
			}

		} };
		SSLContext sslcontext = SSLContext.getInstance("SSL");
		sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
		CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		Unirest.setHttpClient(httpclient);
		
		Calendar cal = Calendar.getInstance();
		cal.setFirstDayOfWeek(Calendar.MONDAY);
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.clear(Calendar.MILLISECOND);
		cal.get(Calendar.DAY_OF_WEEK);
		cal.add(Calendar.HOUR_OF_DAY, -6);
		Date da= cal.getTime();
		System.out.println("DESDE bounded:::: "+Utilidad.getFechaDateMailGunStr(da));
		HttpResponse<JsonNode> request = Unirest.get(url).basicAuth("api", API_KEY)
				.queryString("begin", Utilidad.getFechaDateMailGunStr(da)+" GMT")
				.queryString("end", Utilidad.getFechaDateMailGunStr(new Date())+" GMT")
				.asJson();

		JSONObject obj = request.getBody().getObject();
		 
		JSONArray arr = obj.getJSONArray("items");
		if (arr != null && arr.length() == 0) {
			System.out.println("SALIR");
			return estados;
		}
		for (int i = 0; i < arr.length(); i++) {
			JSONObject o = arr.getJSONObject(i);
			String address = (String) o.get("address");
			String code = (String) o.get("code");
			
			 
			
			java.lang.String fecha = (java.lang.String)o.get("created_at");
			Date fechaForm=Utilidad.getFechaDateMailGun(fecha.substring(0,fecha.length()-3));
			MailEstadoTO to = new MailEstadoTO(address, "bounced", 0,fechaForm,0);
			estados.add(to);

		}
		JSONObject paginas = obj.getJSONObject("paging");
		String next = (String) paginas.get("next");
		String first = (String) paginas.get("first");
		String previous = (String) paginas.get("previous");
		String last = (String) paginas.get("last");

		if (!next.equals(previous))
			callBounces(next, estados);
		return estados;
	}

	public JsonNode sendSimpleMessage() throws UnirestException {
//	   

		try {

			Calendar cal = Calendar.getInstance();
			cal.setFirstDayOfWeek(Calendar.MONDAY);
			cal.clear(Calendar.MINUTE);
			cal.clear(Calendar.SECOND);
			cal.clear(Calendar.MILLISECOND);
			cal.get(Calendar.DAY_OF_WEEK);
			cal.add(Calendar.DAY_OF_MONTH, -20);

			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 1);
			Date da = cal.getTime();

			

			HttpResponse<JsonNode> request = Unirest
					.get("https://api.mailgun.net/v3/" + "tagmeservices.com" + "/stats/total")
					.basicAuth("api", "bf8ee12b5fb70d4f08e386b723ac1b7b-a5d1a068-54c592e9")
					.queryString("event", "delivered").queryString("event", "failed").queryString("event", "clicked")
					.queryString("event", "opened").queryString("start", Utilidad.getFechaDateMailGunStr(da) + " UTC")
//	    		            .queryString("end","Tue, 14 Aug 2018 00:00:00 UTC")
//	    		            .queryString("duration","5d")

					.asJson();

			JSONObject obj = request.getBody().getObject();
			System.out.println(request.getBody().getObject());
			JSONArray arr = obj.getJSONArray("stats");
			for (int i = 0; i < arr.length(); i++) {
				JSONObject o = arr.getJSONObject(i);
				String v_time = (String) o.get("time");
				JSONObject delivered = (JSONObject) o.get("delivered");
				JSONObject open = (JSONObject) o.get("opened");
				JSONObject fail = (JSONObject) o.get("failed");
				JSONObject click = (JSONObject) o.get("clicked");
				Integer v_del = (Integer) delivered.get("total");
				Integer v_open = (Integer) open.get("total");
				Integer v_click = (Integer) click.get("total");
				JSONObject fail_t = (JSONObject) fail.get("temporary");
				JSONObject fail_p = (JSONObject) fail.get("permanent");
				Integer v_fail_t = (Integer) fail_t.get("total");
				Integer v_fail_p = (Integer) fail_p.get("total");
				EstadisticaTO to = new EstadisticaTO(v_time, v_del, v_fail_p, v_fail_t, v_open, v_click);
				System.out
						.println(v_time + " " + v_del + " " + v_fail_p + " " + v_fail_t + " " + v_open + " " + v_click);
			}
			request.getBody();

//	    		        HttpResponse<JsonNode> request = Unirest.get("https://api.mailgun.net/v3/" + YOUR_DOMAIN_NAME + "/events")
//	    		                .basicAuth("api", API_KEY)
//	    		                .queryString("begin", "Thurs, 15 May 2018 00:00:01 -0000")
//	    		                .queryString("ascending", "yes")
//	    		                .queryString("event", "failed")
////	    		                .queryString("limit", 1)
//	    		                .asJson();
//
//	    		             request.getBody().getArray();

//	    		    return request.getBody();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}
}
