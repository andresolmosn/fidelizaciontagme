package com.puma.fidelizacion.alarma;

import java.util.Iterator;
import java.util.List;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.to.EmpresaTO;

@Stateless
public class DaemonEstadoCampanias {

	public DaemonEstadoCampanias() {
	}
	
	@SuppressWarnings("unused")
	@Schedule(minute="*", hour = "*",persistent=false )
    private void scheduledTimeout(final Timer t) {
        System.out.println("@Schedule called at: " + new java.util.Date());
        Controller c =  Controller.getInstance();
        List<EmpresaTO> empresas = c.getAllEmpresaActiva();
        Iterator<EmpresaTO> i = empresas.iterator();
        while(i.hasNext())
        {
        	c.cambioCampaniaFidelizacion(i.next().getId());
        }
        
    }

}
