package com.puma.fidelizacion.alarma;

import java.util.Iterator;
import java.util.List;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;

import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.SendMail;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.ParametroTO;

@Stateless
public class RevisionMails {

	/**
	* Default constructor.
	*/
	public RevisionMails() {
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("unused")
	@Schedule(minute="*/1",hour = "*",persistent=false )	
	private void scheduledTimeout(final Timer t) {
		System.out.println("@Schedule called at: " + new java.util.Date());
		
		Controller c = Controller.getInstance();
		List<EmpresaTO> empresas = c.getAllEmpresaActiva();
		if(empresas != null && empresas.size() > 0) {
			Iterator<EmpresaTO> i = empresas.iterator();
			while(i.hasNext()) {
				try {
					EmpresaTO empresa = i.next();
					SendMail mail = new SendMail(empresa.getId());
					List<ParametroTO> parametros = c.getParaemtroByGrupoAndEmpresa(empresa.getId(), ParametroTO.APP);
					ParametroTO maxClientes = ParametroTO.getParametro(parametros, "app.enviosPorCiclo");
					ParametroTO enviando = ParametroTO.getParametro(parametros, "app.sending");
					ParametroTO remitente = ParametroTO.getParametro(parametros, "app.fromSendMail");
					System.out.println("Estado "+ empresa.getNombre() +" SendMailTask: '"+enviando.getValor()+"'");
					if(enviando.getValor().equals("Running")) {
						c.getMailPendientes(empresa.getId(), Integer.parseInt(maxClientes.getValor()), remitente);
						c.getClientesSinCuponAsignado(empresa.getId(), Integer.parseInt(maxClientes.getValor()));
						c.getCuponPendienteEnvio(empresa.getId(), Integer.parseInt(maxClientes.getValor()), remitente);
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
