package com.puma.fidelizacion.alarma;

import java.util.Iterator;
import java.util.List;
import java.net.URL;

import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;


import com.puma.fidelizacion.business.Controller;
import com.puma.fidelizacion.business.SendMail;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.ParametroTO;

@Stateless
public class DaemonProblemCheck {

	public DaemonProblemCheck() {
		// TODO Auto-generated constructor stub
	}
	
	
	@SuppressWarnings("unused")
	@Schedule( hour = "*/12",persistent=false )	
    private void scheduledTimeout(final Timer t) {
		System.out.println("@Schedule called at: " + new java.util.Date());
		Controller c = Controller.getInstance();
		List<EmpresaTO> empresas = c.getAllEmpresaActiva();
		if(empresas != null && empresas.size() > 0) {
			Iterator<EmpresaTO> i = empresas.iterator();
			while(i.hasNext())
			{
				EmpresaTO empresa = i.next();
				SendMail mail = SendMail.getInstance(empresa.getId());
				if(c.problemas(empresa.getId()))
				{
					try {
						List<ParametroTO> parametros = c.getParametroAlerta(empresa.getId());
						ParametroTO emails = ParametroTO.getParametro(parametros, "alert.mail");
						ParametroTO from = ParametroTO.getParametro(parametros, "alert.from");
						ParametroTO asunto = ParametroTO.getParametro(parametros, "alert.asunto");
						String[] email = emails.getValor().split(";");
						URL url = new URL("http://tagmeservices.com/PumaFidelizacion/FormatoMailAlerta.xhtml?id=" + empresa.getId());
						for(int it = 0; it <= email.length; it++) {
							mail.sendMail(
									email[it],
									asunto.getValor(),
									from.getValor(),
									null,
									url);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}else {
					 System.out.println("No se enviara correo. No se encontraron problemas");
				}
			}
		}
		
    }
	
}
