package com.puma.fidelizacion.business;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.puma.fidelizacion.alarma.MailgunApi;
import com.puma.fidelizacion.db.AdmCuotaDAO;
import com.puma.fidelizacion.db.CampaniaDAO;
import com.puma.fidelizacion.db.CamposAdicionalesDAO;
import com.puma.fidelizacion.db.CargaMasivaDAO;
import com.puma.fidelizacion.db.CategoriaDAO;
import com.puma.fidelizacion.db.ClienteDAO;
import com.puma.fidelizacion.db.CondicionDAO;
import com.puma.fidelizacion.db.CountCuotaDAO;
import com.puma.fidelizacion.db.CuponDAO;
import com.puma.fidelizacion.db.EmpresaDAO;
import com.puma.fidelizacion.db.FormatoCuponDAO;
import com.puma.fidelizacion.db.MailEstadoDAO;
import com.puma.fidelizacion.db.UsuarioDAO;
import com.puma.fidelizacion.db.MenuDAO;
import com.puma.fidelizacion.db.ParametroDAO;
import com.puma.fidelizacion.db.ReportesDAO;
import com.puma.fidelizacion.db.TiendaDAO;
import com.puma.fidelizacion.db.TipoCampaniaDAO;
import com.puma.fidelizacion.db.model.AdmCuota;
import com.puma.fidelizacion.db.model.Campania;
import com.puma.fidelizacion.db.model.CamposAdicionales;
import com.puma.fidelizacion.db.model.CamposAdicionalesCampania;
import com.puma.fidelizacion.db.model.CamposAdicionalesCliente;
import com.puma.fidelizacion.db.model.CargaMasiva;
import com.puma.fidelizacion.db.model.Categoria;
import com.puma.fidelizacion.db.model.Cliente;
import com.puma.fidelizacion.db.model.Condicion;
import com.puma.fidelizacion.db.model.CountCuota;
import com.puma.fidelizacion.db.model.Cupon;
import com.puma.fidelizacion.db.model.Cupones;
import com.puma.fidelizacion.db.model.Empresa;
import com.puma.fidelizacion.db.model.FormatoCupon;
import com.puma.fidelizacion.db.model.FormatoMail;
import com.puma.fidelizacion.db.model.Formulario;
import com.puma.fidelizacion.db.model.Usuario;
import com.puma.fidelizacion.db.model.Menu;
import com.puma.fidelizacion.db.model.MenuIntranet;
import com.puma.fidelizacion.db.model.Parametro;
import com.puma.fidelizacion.db.model.Tienda;
import com.puma.fidelizacion.db.model.TipoCampania;
import com.puma.fidelizacion.to.CampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesCampaniaTO;
import com.puma.fidelizacion.to.CamposAdicionalesTO;
import com.puma.fidelizacion.to.CargaMasivaTO;
import com.puma.fidelizacion.to.CategoriaTO;
import com.puma.fidelizacion.to.ClienteTO;
import com.puma.fidelizacion.to.CondicionTO;
import com.puma.fidelizacion.to.CountCuotaTO;
import com.puma.fidelizacion.to.CuponTO;
import com.puma.fidelizacion.to.EmpresaTO;
import com.puma.fidelizacion.to.FormatoCuponTO;
import com.puma.fidelizacion.to.FormatoMailTO;
import com.puma.fidelizacion.to.FormularioTO;
import com.puma.fidelizacion.to.GenericInfoTO;
import com.puma.fidelizacion.to.MailEstadoTO;
import com.puma.fidelizacion.to.MensajeTO;
import com.puma.fidelizacion.to.MenuIntranetTO;
import com.puma.fidelizacion.to.UsuarioTO;
import com.puma.fidelizacion.to.ValidacionTO;
import com.puma.fidelizacion.util.Lista;
import com.puma.fidelizacion.util.UtilArray;
import com.puma.fidelizacion.util.Utilidad;
import com.puma.fidelizacion.to.MenuTO;
import com.puma.fidelizacion.to.ParametroTO;
import com.puma.fidelizacion.to.PerfilTO;
import com.puma.fidelizacion.to.ResumenExperienciaTO;
import com.puma.fidelizacion.to.TiendaTO;
import com.puma.fidelizacion.to.TipoCampaniaTO;

public class Controller implements Serializable {

	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(Controller.class);
	private static Controller control;

	public static Controller getInstance() {
		if (control == null)
			control = new Controller();
		return control;
	}
	/*
	 * 
	 * Categoria
	 * 
	 */

	public List<CategoriaTO> findAllCategoriasByEmpresa(int empresa, int isVisible) {
		CategoriaDAO dao = new CategoriaDAO();
		List<Categoria> categorias = dao.findAllCategoriasByEmpresa(empresa, isVisible);
		List<CategoriaTO> categoriasTO = new LinkedList<CategoriaTO>();
		if (categorias != null && categorias.size() > 0) {
			Iterator<Categoria> i = categorias.iterator();
			while (i.hasNext()) {
				CategoriaTO to = new CategoriaTO(i.next());
				categoriasTO.add(to);
			}
		}
		return categoriasTO;
	}

	public ParametroTO getParametroByNombre(String nombre)
	{
		ParametroDAO dao = new ParametroDAO();
		Parametro parametro = dao.getParametroByNombre(nombre);
		if(parametro != null)
		{
			return new ParametroTO(parametro);
		}
		return null;
	}
	
	public MensajeTO addCategoria(CategoriaTO categoriaTO) {
		CategoriaDAO dao = new CategoriaDAO();
		Categoria categoria = new Categoria(categoriaTO);
		Categoria cat = dao.findCategoriaByTexto(categoria.getTexto(), categoriaTO.getEmpresa());
		if (cat == null) {
			return dao.addCategoria(categoria);
		} else {
			return new MensajeTO("Registro categoria", "La categor�a '" + categoria.getTexto() + "' ya existe.",
					MensajeTO.WARNING);
		}
	}

	public void updatePrioridadCategoria(CategoriaTO categoriaTO) {
		Categoria c = new Categoria(categoriaTO);
		CategoriaDAO dao = new CategoriaDAO();
		dao.updatePrioridadCategoria(c);
	}

	public MensajeTO deleteCategoria(int id) {
		CategoriaDAO dao = new CategoriaDAO();
		return dao.deleteCategoria(id);
	}

	public void addCliente(CategoriaTO categoria, ClienteTO cliente) {
		Categoria c = new Categoria(categoria);
		Cliente cl = new Cliente(cliente);
		CategoriaDAO dao = new CategoriaDAO();
		dao.agregasPreferencia(c, cl);
	}

	public MensajeTO nuevoCliente(ClienteTO to)
	{
		ClienteDAO dao = new ClienteDAO();
		Cliente cliente = new Cliente(to);
		MensajeTO verificar = dao.existeCorreo(cliente.getMail(), cliente.getEmpresa().getId());
		if(verificar.getTipo().equals(MensajeTO.INFO))
		{
			return dao.nuevoCliente(cliente);
		}else {
			return verificar;
		}
		
	}
	
	public MensajeTO setFechaUtilizacion(int idCupon) {
		CuponDAO dao = new CuponDAO();
		return dao.setFechaUtilizacion(idCupon);
	}
	
	public Hashtable getCupon(int empresaId, String codigo) {
		CuponDAO dao = new CuponDAO();
		Hashtable ht = dao.getCupon(empresaId, codigo);
		return ht;
	}
	
	/*
	 * 
	 * Menu
	 * 
	 */
	public List<MenuTO> transformarMenu(List<Menu> desde) {
		List<MenuTO> menuTO = new LinkedList<MenuTO>();

		if (desde != null && desde.size() > 0) {
			Iterator<Menu> i = desde.iterator();
			while (i.hasNext()) {
				MenuTO to = new MenuTO(i.next());
				menuTO.add(to);
			}
		}
		return menuTO;
	}

	public List<MenuTO> findAllMenuByCampania(int idCamp) {

		MenuDAO dao = new MenuDAO();
		List<Menu> menu = dao.findAllMenuByCampania(idCamp);
		List<MenuTO> menuTO = transformarMenu(menu);
		return menuTO;
	}

	public List<MenuTO> findAllMenuPadreByEmpresa(int empresaId, int campania) {
		MenuDAO dao = new MenuDAO();
		List<Menu> menu = dao.findAllMenuPadreByEmpresa(empresaId, campania);
		logger.info("Lista Menu: " + menu);
		List<MenuTO> menuTO = new LinkedList<MenuTO>();

		if (menu != null && menu.size() > 0) {
			Iterator<Menu> i = menu.iterator();
			while (i.hasNext()) {
				MenuTO to = new MenuTO(i.next());
				int ancho = 610 / menu.size();
				to.setAncho(ancho);
				menuTO.add(to);
			}
		}
		logger.debug(menuTO != null ? menuTO.size() : "VACIO");
		return menuTO;
	}

	public MensajeTO addMenu(MenuTO menuTO) {
		MenuDAO dao = new MenuDAO();
		Menu menu = new Menu(menuTO);

		return dao.addMenu(menu);

	}

	public MensajeTO updatePosicionMenu(MenuTO menuTO) {
		Menu c = new Menu(menuTO);
		MenuDAO dao = new MenuDAO();
		return dao.updatePosicionMenu(c);
	}

	public MensajeTO deleteMenu(int id) {
		MenuDAO dao = new MenuDAO();
		return dao.deleteMenu(id);
	}

	/*
	 * 
	 * Usuario
	 * 
	 */

	public UsuarioTO autenticar(String nick, String pass, String compania) {
		UsuarioDAO bd = new UsuarioDAO();

		Usuario u = bd.autenticar(nick, pass, compania);
		if (u != null) {
			UsuarioTO uto = new UsuarioTO();
			uto.setNombre(u.getNombre());
			uto.setId(u.getId());
			if (u.getTiendas() != null) {
				uto.setIdEmpresa(u.getTiendas().get(0).getEmpresa().getId());
				
				List<TiendaTO> tiendasTO = new LinkedList<TiendaTO>();
				List<Tienda> tiendas = u.getTiendas();
				Iterator<Tienda> i = tiendas.iterator();
				while(i.hasNext()) {
					TiendaTO to = new TiendaTO(i.next());
					tiendasTO.add(to);
				}
				uto.setTiendas(tiendasTO);
				
				List<MenuIntranetTO> menuTO = new LinkedList<MenuIntranetTO>();
				List<MenuIntranet> intranet = u.getPerfil().getMenuIntranet();
				
				if(intranet != null && intranet.size() > 0) {
					Iterator<MenuIntranet> i2= intranet.iterator();
					while(i2.hasNext()) {
						MenuIntranetTO to = new MenuIntranetTO(i2.next());
						menuTO.add(to);
					}
				}
				PerfilTO perfil = new PerfilTO(u.getPerfil(), menuTO);
				uto.setPerfil(perfil);
			}
			// uto.setTienda(u.getTienda());
			return uto;
		}
		return null;
	}

	/*
	 * 
	 * Cliente
	 * 
	 */

	public ClienteTO findClienteByHash(String hash) {
		logger.info("Buscando Cliente por hash");
		ClienteDAO dao = new ClienteDAO();
		logger.info(dao.findClienteByHash(hash));
		return dao.findClienteByHash(hash);
	}

	public void updateCliente(ClienteTO cliente) {
		logger.info(cliente);
		ClienteDAO dao = new ClienteDAO();
		CuponDAO cuponDAO = new CuponDAO();
		Cliente c = new Cliente(cliente);
		dao.beginTransaction();
		// cuponDAO.asignarCupon(cliente.getEmpresa_id(), c);
		dao.updateCliente(c);
		dao.commitTransaction();
		dao.cerrarConexion();
	}

	public void clienteAddCategoria(int clienteId, int categoriaId) {
		ClienteDAO dao = new ClienteDAO();
		dao.addCategoria(clienteId, categoriaId);
	}

	public List<ClienteTO> findAllClientes() {
		ClienteDAO dao = new ClienteDAO();
		List<ClienteTO> resp = dao.findAllCliente();
		List<ClienteTO> nuevo = new LinkedList();
		Iterator t = resp.iterator();
		while (t.hasNext()) {
			ClienteTO to = (ClienteTO) t.next();
			if (to.getTienda_id() != 0)
				to.setTienda(getTiendaById(to.getTienda_id()));
			if (to.getUsuario_id() != 0)
				to.setVendedor(getUsuarioById(to.getUsuario_id()));
			
			
			nuevo.add(to);
		}
		return nuevo;
	}
	
	public List<ClienteTO> findClienteReporte(int idUsuario,int idTienda,int idCampania,String desde,String hasta){
		ClienteDAO cdao = new ClienteDAO();
		 List<ClienteTO> nuevo = new LinkedList();
		 List<Cliente> resp = cdao.findClienteReporte(idUsuario,idTienda,idCampania,desde,hasta);
		 if(resp!=null && resp.size()>0) {
			
			 Iterator t = resp.iterator();
			 while (t.hasNext()){
				 Cliente dao = (Cliente)t.next();
				 ClienteTO cnuevo= new ClienteTO(dao);
				 
				 if(dao.getTienda_id()!=0)
					 cnuevo.setTienda(getTiendaById(dao.getTienda_id()));
				 if(dao.getUsuario_id()!=0)
					 cnuevo.setVendedor(getUsuarioById(dao.getUsuario_id()));
				 
				
	
				 //TRAER TODAS 
				 List l = dao.getCategorias();
				 List<CategoriaTO> misCat = new LinkedList();
				 Iterator<Categoria> it = l.iterator();
				 while(it.hasNext()) {
					 Categoria cat = it.next();
					 misCat.add(new CategoriaTO(cat));
				 }
				 cnuevo.setCategorias(misCat);
				//TRAER campos adicionales
				 CamposAdicionalesDAO adDAO = new CamposAdicionalesDAO();
				 cnuevo.setAdicionales(adDAO.findCamposAdicionalesCliente(dao.getId()));
				 
				 nuevo.add(cnuevo);
			 }
		 }
		 return nuevo;
	}

	public List<ClienteTO> findClienteReporteV2(int idEmpresa,int idTienda,int idCampania,String desde,String hasta,List<TiendaTO> tiendasT){
		ClienteDAO cdao = new ClienteDAO();
		 CamposAdicionalesDAO adDAO = new CamposAdicionalesDAO();
		 List<ClienteTO> nuevo = new LinkedList();
		 List<Cliente> resp = cdao.findClienteReporteV2(idEmpresa,idTienda,idCampania,desde,hasta,tiendasT);
		 if(resp!=null && resp.size()>0) {
			
			 Iterator t = resp.iterator();
			 while (t.hasNext()){
				 Cliente dao = (Cliente)t.next();
				 ClienteTO cnuevo= new ClienteTO(dao);
				 
				 if(dao.getTienda_id()!=0)
					 cnuevo.setTienda(getTiendaById(dao.getTienda_id()));
				 if(dao.getUsuario_id()!=0)
					 cnuevo.setVendedor(getUsuarioById(dao.getUsuario_id()));
	
				 //TRAER TODAS 
			 	if(dao.getRegistro()==1) {
					 List l = dao.getCategorias();
					 List<CategoriaTO> misCat = new LinkedList();
					 if(l!=null && l.size()>0) {
						 Iterator<Categoria> it = l.iterator();
						 while(it.hasNext()) {
							 Categoria cat = it.next();
							 misCat.add(new CategoriaTO(cat));
						 }
					 }
					 cnuevo.setCategorias(misCat);
				 
				//TRAER campos adicionales
				
				 cnuevo.setAdicionales(adDAO.findCamposAdicionalesCliente(dao.getId()));
				 
			 	}
				 nuevo.add(cnuevo);
			 }
		 }
		 return nuevo;
	}
	
	public List<ValidacionTO> validaCliente(ClienteTO cliente) {
		ClienteDAO dao = new ClienteDAO();
		List<ValidacionTO> validaciones = new LinkedList<ValidacionTO>();

		validaciones.add(new ValidacionTO("rut", dao.existeRUT(cliente.getRut(), cliente.getEmpresa_id())));
		// validaciones.add(new ValidacionTO("rut",dao.existeCorreo(cliente.getMail(),
		// cliente.getEmpresa_id())));
		logger.info(validaciones);
		return validaciones;

	}

	public MensajeTO addCarga_MasivaWithCupones(List<String> cupones, int dcto, int campaniaId, int condicionId,
			int empresaId, CargaMasivaTO cargaMasivaTO) {
		CargaMasivaDAO dao = new CargaMasivaDAO();
		return dao.addCarga_MasivaWithCupones(cargaMasivaTO, cupones, dcto, campaniaId, condicionId, empresaId);
	}
	
	public MensajeTO addCarga_MasivaCupon(List<String> cupones, int dcto, int campaniaId, int condicionId,
			int empresaId, CargaMasivaTO cargaMasivaTO) {
		CargaMasivaDAO dao = new CargaMasivaDAO();
		return dao.addCarga_MasivaCupon(cargaMasivaTO, cupones, dcto, campaniaId, condicionId, empresaId);
	}

	public List<CargaMasivaTO> findAllCargaMasivaByEmpresa(int empresaId) {
		CargaMasivaDAO dao = new CargaMasivaDAO();
		return dao.findAllCargaMasivaByEmpresa(empresaId);
	}
	public List<CargaMasivaTO> findAllCargaMasivaByCampania(int idCampania) {
		CargaMasivaDAO dao = new CargaMasivaDAO();
		return dao.findAllCargaMasivaByCampania(idCampania);
	}
	public String getAsuntoByCampania(int campaniaId) {
		FormatoCuponDAO dao = new FormatoCuponDAO();
		return dao.getAsuntoByCampania(campaniaId);
	}

	/*
	 * 
	 * Condicion
	 * 
	 */

	public int addCondicion(String condicion, int empresaId) {
		CondicionDAO dao = new CondicionDAO();
		return dao.addCondicion(condicion, empresaId);
	}

	public List<CondicionTO> findAllCondicionByEmpresa(int empresaId) {
		CondicionDAO dao = new CondicionDAO();
		List<Condicion> condiciones = dao.findAllCondicionByEmpresa(empresaId);
		List<CondicionTO> condicionesTO = new LinkedList<CondicionTO>();

		if (condiciones != null && condiciones.size() > 0) {
			Iterator<Condicion> i = condiciones.iterator();
			while (i.hasNext()) {
				CondicionTO to = new CondicionTO(i.next());
				condicionesTO.add(to);
			}
		}
		return condicionesTO;
	}

	/*
	 * 
	 * Campania
	 * 
	 */
	public List<CampaniaTO> findAllCampaniasVigentesByEmpresa(int empresaId) {
		CampaniaDAO dao = new CampaniaDAO();

		List<Campania> campanias = dao.findAllCampaniasVigentesByEmpresa(empresaId);
		List<CampaniaTO> campaniasTO = new LinkedList<CampaniaTO>();
		if (campanias != null && campanias.size() > 0) {
			Iterator<Campania> i = campanias.iterator();
			while (i.hasNext()) {
				Campania c = i.next();
				CampaniaTO to = new CampaniaTO(c);
				campaniasTO.add(to);
			}
		}

		return campaniasTO;

	}

	public List<CampaniaTO> findAllCampaniasByEmpresa(int empresaId) {
		CampaniaDAO dao = new CampaniaDAO();

		List<Campania> campanias = dao.findAllCampaniasVigentesByEmpresa(empresaId);
		List<CampaniaTO> campaniasTO = new LinkedList<CampaniaTO>();
		if (campanias != null && campanias.size() > 0) {
			Iterator<Campania> i = campanias.iterator();
			while (i.hasNext()) {
				Campania c = i.next();
				CampaniaTO to = new CampaniaTO(c);
				campaniasTO.add(to);
			}
		}

		return campaniasTO;

	}

	public CampaniaTO findCampaniasFidelizacionByEmpresaActiva(int empresaId,int idCampania) {
		CampaniaDAO dao = new CampaniaDAO();
		Campania campania = dao.findCampaniasFidelizacionByEmpresaActiva(empresaId,idCampania);
		CampaniaTO to = new CampaniaTO(campania);
		List resp = UtilArray.transformarListas(campania.getFormatoMails(), new LinkedList(), FormatoMailTO.class);
		to.setFormato(resp);
		return to;
	}

	public CampaniaTO findCampaniasFidelizacionByEmpresa(int empresaId,int idCamp) {
		CampaniaDAO dao = new CampaniaDAO();
		Campania campania = dao.findCampaniasFidelizacionByEmpresa(empresaId,idCamp);
		CampaniaTO to = new CampaniaTO(campania);
		List resp = UtilArray.transformarListas(campania.getFormatoMails(), new LinkedList(), FormatoMailTO.class);
		to.setFormato(resp);
		return to;
	}
	
	public List<CampaniaTO> findCampaniaActiva(String tipo, int empresaId) {
		CampaniaDAO dao = new CampaniaDAO();
		List<Campania> campanias = dao.findCampaniaActiva(tipo, empresaId);
		List<CampaniaTO> campaniasTO = new LinkedList<CampaniaTO>();
		if(campanias != null && campanias.size() > 0) {
			Iterator<Campania> i = campanias.iterator();
			while(i.hasNext())
			{
				CampaniaTO to = new CampaniaTO(i.next());
				campaniasTO.add(to);
			}
		}
		return campaniasTO;
	}

	public CampaniaTO findCampaniasFidelizacionById(int idCamp, int idEmp) {
		CampaniaDAO dao = new CampaniaDAO();
		logger.info("id campania:" + idCamp);
		Campania campania = dao.findCampaniasFidelizacionById(idCamp, idEmp);
		logger.info("Campania ===> " + campania);
		CampaniaTO to = new CampaniaTO(campania);
		if(campania != null){
			if (campania.getFormatoMails() != null) {
				List resp = UtilArray.transformarListas(campania.getFormatoMails(), new LinkedList(), FormatoMailTO.class);
				to.setFormato(resp);
			}
			if (campania.getMenu() != null)
				to.setMenu(transformarMenu(campania.getMenu()));
			if (campania.getFormulario() != null) {
				FormularioTO form = (FormularioTO) UtilArray.transformarObject(campania.getFormulario(),
						new FormularioTO());
				form.setFlags2(form.getActivoS2()==1?true:false);
				form.setFlags3(form.getActivoS3()==1?true:false);
				to.setFormulario(form);
			}
		}
		logger.info(to);
		return to;
	}
	
	public CampaniaTO findCampaniasById (int idCamp, int idEmp) {
		CampaniaDAO dao = new CampaniaDAO();
		Campania campania = dao.findCampaniasById(idCamp, idEmp);
		CampaniaTO to = new CampaniaTO(campania);
		if (campania.getFormatoMails() != null) {
			List resp = UtilArray.transformarListas(campania.getFormatoMails(), new LinkedList(), FormatoMailTO.class);
			to.setFormato(resp);
		}
		if (campania.getMenu() != null)
			to.setMenu(transformarMenu(campania.getMenu()));
		if (campania.getFormulario() != null) {
			FormularioTO form = (FormularioTO) UtilArray.transformarObject(campania.getFormulario(),
					new FormularioTO());
			to.setFormulario(form);
		}
		return to;
	}

	public List<FormatoMailTO> findAllFormatoMailByCampania(int campaniaID) {
		CampaniaDAO dao = new CampaniaDAO();
		List<FormatoMail> campanias = dao.findAllFormatoMailByCampania(campaniaID);
		List resp = UtilArray.transformarListas(campanias, new LinkedList(), FormatoMailTO.class);
		return resp;
	}

	/*
	 * 
	 * Campania
	 * 
	 */
	public int addCarga_Masiva(CargaMasivaTO cargaMasivaTO) {
		CargaMasiva cargaMasiva = new CargaMasiva(cargaMasivaTO);
		CargaMasivaDAO dao = new CargaMasivaDAO();
		return dao.addCarga_Masiva(cargaMasiva);
	}

	public MensajeTO guardarFormatoMail(FormatoMailTO fm) {
		try {
			CampaniaDAO c = new CampaniaDAO();
			c.beginTransaction();
			Campania camp = c.findCampaniaById(fm.getIdCampania());
			FormatoMail f = new FormatoMail();
			f.setCampania(camp);
			f.setTexto(fm.getTexto());
			f.setUrlFoto(fm.getUrlFoto());
			f.setUrl(fm.getUrl());
			camp.getFormatoMails().add(f);
			c.commitTransaction();
			return new MensajeTO("Plantilla mail","Se inserto la nueva imagen en el template",MensajeTO.SUCCESS);
			
		} catch(Exception e){
			return new MensajeTO("Plantilla mail","Ocurrio un problema al insertar la imagen",MensajeTO.ERROR);
		}
		
	}

	public MensajeTO eliminaFormatoMail(int id) {
		try{
			CampaniaDAO dao = new CampaniaDAO();
			dao.eliminaFormatoMail(id);
			return new MensajeTO("Eliminar Image", "Se elimino la imagen correctamente.",MensajeTO.SUCCESS);
		}catch(Exception e) {
			return new MensajeTO("Eliminar Image", "Error al eliminar la imagen",MensajeTO.ERROR);
		}
		
	}

	public CuponTO getCupon(String id) {
		ClienteDAO dao = new ClienteDAO();
		int idClient = dao.getIdCliente(id);
		if (idClient != 0) {
			CuponDAO dao2 = new CuponDAO();
			CuponTO cup = dao2.getCuponByIdCliente(idClient);
			return cup;
		} else {
			return null;
		}
	}

	public CondicionTO getCondicionById(int id) {
		// ClienteDAO dao = new ClienteDAO();
		// int idClient = dao.getIdCliente(id);
		CondicionDAO dao2 = new CondicionDAO();
		CondicionTO cup = dao2.getCondicionById(id);
		return cup;
	}

	public MensajeTO eliminarCampania(int campaniaId) {
		CampaniaDAO dao = new CampaniaDAO();
		return dao.eliminarCampania(campaniaId);
	}

	public void getMailPendientes(int idEmpresa, int maxResults, ParametroTO remitente) {

					ClienteDAO dao = new ClienteDAO();
					List<Cliente> clientes = dao.findClientesRegistradoSinCupon(idEmpresa, maxResults);
					logger.info("envio pendiente:" + clientes);
					if (clientes != null && clientes.size() > 0) {
						logger.info("Cantidad clientes pendientes: " + clientes.size());
						SendMail send = new SendMail(idEmpresa);
						
						Iterator<Cliente> i = clientes.iterator();
						
						while(i.hasNext())
						{
							Cliente c = i.next();
							try {
								int campania = this.getCampaniaCliente(Utilidad.getFechaDateStr(c.getFecha_registro()), c.getEmpresa().getId(), c.getTipoCampania().getId());
								String asunto = this.getAsuntoMailByCampania(c.getFecha_registro(), c.getTipoCampania().getId());
								logger.info("asunto del mensaje: "+ asunto);
								// TODO URL MAIL PENDIENTE
								URL url = new URL(
										"https://tagmeservices.com/TagmeFidelizacion/plantillaMail.xhtml?id=" + c.getHash());
//								 URL url = new
//								 URL("https://localhost:8181/TagmeFidelizacion/plantillaMail.xhtml?id=" +
//								 c.getHash());
								if (send.sendMailTag(c.getMail(), asunto, remitente.getValor(), null,
										url, "cam:"+campania)) {
									// if(send.sendMail(c.getMail(),"Gracias por registrarte","Fidelizacion
									// <fidelizacion@email-latam.puma.com>", null, url)) {

									ClienteDAO transaccion = new ClienteDAO();
									transaccion.mailEnviado(c);
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						

					} else {
						logger.info("Sin clientes pendientes");
					}

	}

	public void getClientesSinCuponAsignado(int empresaId, int maxResults) {
		ClienteDAO dao = new ClienteDAO();
		List<Integer> sinCupon = dao.getClientesSinCuponAsignado(empresaId, maxResults);
		if (sinCupon != null && sinCupon.size() > 0) {
			CuponDAO cupondao = new CuponDAO();
			Iterator<Integer> i = sinCupon.iterator();
			while(i.hasNext())
			{
				Integer idCupon = i.next();
				logger.info(cupondao.asignarCupon(empresaId, idCupon));
			}
			
		}
	}

	public void getCuponPendienteEnvio(int idEmpresa, int maxResults, ParametroTO remitente) {
		CuponDAO dao = new CuponDAO();
		List<Cupon> cupones = dao.findCuponNoEnviado(idEmpresa,maxResults);
		if (cupones != null && cupones.size() > 0) {
			logger.info("Cantidad cupones pendientes: " + cupones.size());
			SendMail send = new SendMail(idEmpresa);
			
			Iterator<Cupon> i = cupones.iterator();
			while(i.hasNext())
			{
				Cupon c = i.next();
				try {
					int idCampania = c.getCampaniaId();
					String asunto = this.getAsuntoByCampania(idCampania);
					logger.info("Email cuponCliente: " + c.getCliente().getMail());
					// TODO URL CUPON PENDIENTE
					URL url = new URL(
							"https://tagmeservices.com/TagmeFidelizacion/cupon.xhtml?id=" + c.getCliente().getHash());
//					 URL url = new URL("https://localhost:8181/TagmeFidelizacion/cupon.xhtml?id=" +
//					 c.getCliente().getHash());
					if (send.sendMailTag(c.getCliente().getMail(), asunto, remitente.getValor(),
							null, url, "cup:" + idCampania)) {
						// if(send.sendMail(c.getCliente().getMail(),"Gracias por
						// registrarte","Fidelizacion <fidelizacion@email-latam.puma.com>", null, url))
						// {
						CuponDAO cupon = new CuponDAO();
						cupon.cuponEnviado(c);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			logger.info("Sin Cupones pendientes");
		}
	}

//	TODO no utilizada
//	public List<Campania> findCampaniaActiva(int empresaId) {
//		CampaniaDAO dao = new CampaniaDAO();
//		return dao.findCapaniaActiva(empresaId);
//	}

	public void findCuponCondicion(int clienteId) {
		CuponDAO dao = new CuponDAO();
		// dao.findCuponCondicion(clienteId);
	}

	public int getAltoMenu(int idEmpresa) {

		MenuDAO dao = new MenuDAO();
		// List<Menu> menu = dao.getAltoMenu(idEmpresa);
		return 0;
	}

	public int getCampaniaCliente(String fecha_registro, int empresa_id, int tipo_campania_id) {
		CampaniaDAO dao = new CampaniaDAO();
		return dao.getCampaniaCliente(fecha_registro, empresa_id, tipo_campania_id);
	}

	public List<MensajeTO> check(int empresaId) {
		List<MensajeTO> mensajes = new LinkedList<MensajeTO>();
		this.checkCuponesDisponiblesCampania(empresaId, mensajes);
		this.checkCampaniaFidelizacionActiva(empresaId, mensajes);
		this.checkVencimientoCampania(empresaId, mensajes);
		// List<MensajeTO> checkCuponCampania =
		// this.checkCuponesDisponiblesCampania(empresaId);
		// if(checkCuponCampania.size()>0)
		// {
		// for(MensajeTO to: checkCuponCampania)
		// {
		// mensajes.add(to);
		// }
		// }
		return mensajes;
	}

	public void checkCuponesDisponiblesCampania(int empresaId, List<MensajeTO> mensajes) {
		CuponDAO dao = new CuponDAO();
		List<Cupones> cupones = dao.checkCuponesDisponiblesCampania();
		Iterator<Cupones> i = cupones.iterator();
		while (i.hasNext()) {
			Cupones consultaCupon = i.next();
			if (consultaCupon.getEmpresa() == empresaId && consultaCupon.getAsignado() == 0) {

				MensajeTO nuevoMensaje = new MensajeTO();

				if (consultaCupon.getCantidad() == 0) {
					nuevoMensaje.setMensaje("La campa�a <b>'" + consultaCupon.getNombre()
							+ "'</b> no tiene cupones disponibles.");
					nuevoMensaje.setTitulo("Critico!");
					nuevoMensaje.setTipo(MensajeTO.ERROR);
					mensajes.add(nuevoMensaje);
				} else if (consultaCupon.getCantidad() < 20) {
					nuevoMensaje.setMensaje("La campa�a <b>'" + consultaCupon.getNombre() + "'</b> tiene solo <b>"
							+ consultaCupon.getCantidad() + "</b> cupones disponibles.");
					nuevoMensaje.setTitulo("Alerta!");
					nuevoMensaje.setTipo(MensajeTO.WARNING);
					mensajes.add(nuevoMensaje);
				}
			}
		}
	}

	public MensajeTO addVendedor(UsuarioTO to, int idTienda, int empresaId) {
		UsuarioDAO dao = new UsuarioDAO();
		Usuario u = new Usuario(to,to.getPerfil());
		return dao.addVendedor(u, idTienda, empresaId);
	}

	public void checkCampaniaFidelizacionActiva(int empresaId, List<MensajeTO> mensajes) {
		CampaniaDAO dao = new CampaniaDAO();
		MensajeTO mensaje = dao.campaniaFidelizacionActiva(empresaId);
		logger.info(mensaje);
		if (mensaje.getTipo() == MensajeTO.ERROR || mensaje.getTipo() == MensajeTO.WARNING) {
			mensajes.add(mensaje);
		}
	}

	public void checkVencimientoCampania(int empresaId, List<MensajeTO> mensajes) {
		CampaniaDAO dao = new CampaniaDAO();
		List<MensajeTO> msgs = dao.vencimientoCampania(empresaId);
		if (msgs != null && msgs.size() > 0) {

			Iterator<MensajeTO> i = msgs.iterator();
			while (i.hasNext()) {
				MensajeTO mensaje = i.next();
				logger.info(mensaje);
				mensajes.add(mensaje);
			}
		}
	}

	public MensajeTO addCampania(CampaniaTO campaniaTO) {
		MensajeTO mensaje = this.validarFechaCampania(campaniaTO);
		if (mensaje.getTipo().equals(MensajeTO.SUCCESS)) {
			CampaniaDAO dao = new CampaniaDAO();
			return dao.addCampania(campaniaTO);
		} else {
			return mensaje;
		}

	}
	
	
	public Hashtable clonarCampania(CampaniaTO campaniaTO) {
		MensajeTO mensaje = this.validarFechaCampania(campaniaTO);
		Hashtable ht = null;
		logger.info(mensaje);
		if (mensaje.getTipo().equals(MensajeTO.SUCCESS)) {
			CampaniaDAO dao = new CampaniaDAO();
			return dao.clonarCampania(campaniaTO);
		} else {
			ht = new Hashtable();
			ht.put("DATA", 0);
			ht.put("MENSAJE", mensaje);
			return ht;
		}

	}

	public MensajeTO actualizarCampania(CampaniaTO campania) {
		MensajeTO mensaje = this.validarFechaCampania(campania);
		if (mensaje.getTipo().equals(MensajeTO.SUCCESS)) {
			CampaniaDAO dao = new CampaniaDAO();
			return dao.actualizarCampania(campania);
		} else {
			return mensaje;
		}

	}

	public MensajeTO validarFechaCampania(CampaniaTO campaniaTO) {
		CampaniaDAO dao = new CampaniaDAO();
		List<CampaniaTO> desde = dao.findCampaniaByFecha(campaniaTO);

		logger.info("------------------------desde-----------------------");
		if (desde != null && desde.size() > 0) {
			return new MensajeTO("Nueva campa�a", "No puedes tener fechas del mismo tipo de campa�a("
					+ campaniaTO.getTipoCampania().getNombre() + ") traslapadas al generar una campa�a", MensajeTO.WARNING);
		} else {

			// CampaniaDAO dao2 = new CampaniaDAO();
			// List<CampaniaTO> hasta = dao2.findCampaniaByFecha(campaniaTO);
			// logger.info("------------------------hasta-----------------------");
			// if(hasta != null && hasta.size() > 0)
			// {
			// return new MensajeTO("Nueva campa�a","No puedes tener fechas traslapadas al
			// generar una camapa�a", MensajeTO.WARNING);
			// }
		}

		return new MensajeTO("Nueva campa�a", "no existen fechas traslapadas", MensajeTO.SUCCESS);

	}

	public String getAsuntoMailByCampania(Date fechaRegistroCliente, int tipoCampania) {
		CampaniaDAO dao = new CampaniaDAO();
		return dao.getAsuntoByCampania(fechaRegistroCliente, tipoCampania);
	}

	public MensajeTO eliminarCargaMasiva(int cargaId, int idEmpresa) {
		CuponDAO cupondao = new CuponDAO();
		List<Cupon> cupones = cupondao.cuponesUtilizadosByCarga(idEmpresa, cargaId);
		if (cupones != null && cupones.size() > 0) {
			return new MensajeTO("Eliminar carga de cupones", "La Carga de cupones no se puede eliminar.",
					MensajeTO.WARNING);
		} else {
			CargaMasivaDAO dao = new CargaMasivaDAO();
			return dao.eliminarCargaMasiva(cargaId);
		}

	}

	public MensajeTO guardarFormulario(FormularioTO nuevo) {
		CampaniaDAO dao = new CampaniaDAO();
		Campania c = dao.findCampaniasFidelizacionById(nuevo.getIdCampania(), nuevo.getIdEmpresa());
		try {
			dao.beginTransaction();
			if (c.getFormulario() != null) {
				Formulario f = c.getFormulario();
				f.setBoton(nuevo.getBoton());
				f.setSeccion1(nuevo.getSeccion1());
				f.setSeccion2(nuevo.getSeccion2());
				f.setSeccion3(nuevo.getSeccion3());
				f.setActivoS2(nuevo.isFlags2()?1:0);
				f.setActivoS3(nuevo.isFlags3()?1:0);
				f.setCondiciones(nuevo.getCondiciones());
			} else {
				Formulario f = new Formulario(nuevo);
				f.setCampania(c);
				dao.guardaFormulario(f);
			}
			dao.commitTransaction();
			return new MensajeTO("Formulario Registro",
					"Se actualizo el formulario de registro para la campa�a '" + c.getNombre() + "'",
					MensajeTO.SUCCESS);
		} catch (Exception e) {
			return new MensajeTO("Formulario Registro",
					"Error al actualizar lel formulario de registro para la campa�a '" + c.getNombre() + "'",
					MensajeTO.ERROR);
		}

	}

	public List<TiendaTO> getTiendasActivaByEmpresa(int idEmpresa) {
		TiendaDAO t = new TiendaDAO();
		List<Tienda> a = t.getTiendaActivaByEmpresa(idEmpresa);
		List<TiendaTO> resp = new LinkedList<TiendaTO>();
		Iterator it = a.iterator();
		while (it.hasNext()) {
			Tienda tienda = (Tienda) it.next();
			resp.add(new TiendaTO(tienda));
		}
		return resp;
	}
	
	public List<TiendaTO> getTiendaActivaToAdministradorEmpresa(int idEmpresa) {
		TiendaDAO t = new TiendaDAO();
		List<Tienda> a = t.getTiendaActivaToAdministradorEmpresa(idEmpresa);
		List<TiendaTO> resp = new LinkedList<TiendaTO>();
		Iterator it = a.iterator();
		while (it.hasNext()) {
			Tienda tienda = (Tienda) it.next();
			resp.add(new TiendaTO(tienda));
		}
		return resp;
	}
	
	public List<TiendaTO> getTiendaUsuarioActivaByEmpresa(int idEmpresa, int idUsuario)	{
		TiendaDAO t = new TiendaDAO();
		List<Tienda> a = t.getTiendaUsuarioActivaByEmpresa(idEmpresa, idUsuario);
		List<TiendaTO> resp = new LinkedList<TiendaTO>();
		Iterator it = a.iterator();
		while (it.hasNext()) {
			Tienda tienda = (Tienda) it.next();
			resp.add(new TiendaTO(tienda));
		}
		return resp;
	}

	public TiendaTO getTiendaById(int id) {
		TiendaDAO t = new TiendaDAO();
		Tienda a = t.getTiendaById(id);
		if(a != null) {
			return new TiendaTO(a);
		}else {
			return null;
		}
		
	}

	public MensajeTO addTienda(TiendaTO tienda) {
		TiendaDAO dao = new TiendaDAO();
		Tienda t = new Tienda(tienda);
		Hashtable ht = dao.addTienda(t);
		MensajeTO mensaje = (MensajeTO)ht.get("MENSAJE");
		return mensaje;
//		EmpresaDAO dao = new EmpresaDAO();
//		dao.addTienda(new Tienda(tienda), tienda.getEmpresaId());
//		MensajeTO mensaje = new MensajeTO("Ingresar tienda", "Tienda ingresada correctamente",MensajeTO.SUCCESS);
//		return mensaje;
	}
	
	public Hashtable addTiendaHashtable(TiendaTO tienda, EmpresaTO empresa) {
		TiendaDAO dao = new TiendaDAO();
		Tienda t = new Tienda(tienda);
		Empresa e = new Empresa(empresa);
		t.setEmpresa(e);
		return dao.addTiendaEmpresa(t);
	}

	public MensajeTO updateTienda(TiendaTO tienda) {
		TiendaDAO dao = new TiendaDAO();
		return dao.updateTienda(tienda);
	}

	public MensajeTO desactivaTienda(int tiendaId) {
		TiendaDAO dao = new TiendaDAO();
		return dao.desactivaTienda(tiendaId);
	}

	public MensajeTO desactivaVendedor(int vendedorId) {
		UsuarioDAO dao = new UsuarioDAO();
		return dao.desactivaVendedor(vendedorId);
	}

	public List<UsuarioTO> getUsuariosActivosByTienda(int tiendaId, int idAdministrador) {
		UsuarioDAO dao = new UsuarioDAO();
		List<Usuario> usuario = dao.findAllVendedoresByTienda(tiendaId, idAdministrador);
		if (usuario != null && usuario.size() > 0) {
			List<UsuarioTO> to = new LinkedList<UsuarioTO>();
			UtilArray.transformarListas(usuario, to, UsuarioTO.class);
			return to;
		}
		return null;

	}

	public UsuarioTO getUsuarioById(int id) {
		UsuarioDAO t = new UsuarioDAO();
		Usuario a = t.getUsuarioById(id);
		if(a != null) {
			return new UsuarioTO(a);
		}else {
			return null;
		}
		

	}

	public MensajeTO updateUsuario(UsuarioTO usuario) {
		UsuarioDAO dao = new UsuarioDAO();
		return dao.updateUsuario(usuario);
	}

	public List<UsuarioTO> findAllVendedoresByTienda(int idTienda, int idAdministrador) {
		UsuarioDAO t = new UsuarioDAO();
		List<Usuario> a = t.findAllVendedoresByTienda(idTienda, idAdministrador);
		List<UsuarioTO> resp = new LinkedList<UsuarioTO>();
		Iterator it = a.iterator();
		while (it.hasNext()) {
			Usuario tienda = (Usuario) it.next();
			resp.add(new UsuarioTO(tienda));
		}
		return resp;
	}

	public void cambioCampaniaFidelizacion(int empresaId) {
		CampaniaDAO dao = new CampaniaDAO();
		dao.cambioCampaniaActiva(empresaId);
	}

	public FormatoCuponTO findFormatoCuponByCampania(int campaniaId) {
		CampaniaDAO dao = new CampaniaDAO();
		return dao.findFormatoCuponByCampania(campaniaId);
	}

	public MensajeTO addFormatoCupon(FormatoCuponTO to) {
		FormatoCuponDAO dao = new FormatoCuponDAO();
		if (to.getId() == 0) {
			logger.info("insertando formato: " + to);
			FormatoCupon formato = new FormatoCupon(to);
			logger.info("insertando modelo formato: " + formato);
			return dao.addFormatoCupon(formato);
		} else {
			logger.info("actualizando formato: " + to);
			return dao.updateFormatoCupon(to);
		}

	}

	public List<CamposAdicionalesTO> getCamposAdicionales() {
		CamposAdicionalesDAO dao = new CamposAdicionalesDAO();
		List<CamposAdicionales> campos = dao.getAllCamposAdicionales();
		List<CamposAdicionalesTO> resp = new LinkedList<CamposAdicionalesTO>();
		if (campos != null && campos.size() > 0) {
			Iterator it = campos.iterator();
			while (it.hasNext()) {
				CamposAdicionales obj = (CamposAdicionales) it.next();
				resp.add(new CamposAdicionalesTO(obj));
			}
		}
		return resp;
	}

	public List<CamposAdicionalesCampaniaTO> findCamposAdicionalesCampania(int idCampania) {
		CamposAdicionalesDAO dao = new CamposAdicionalesDAO();
		List<CamposAdicionalesCampania> campos = dao.findCamposAdicionalesCampania(idCampania);
		List<CamposAdicionalesCampaniaTO> resp = new LinkedList<CamposAdicionalesCampaniaTO>();
		if (campos != null && campos.size() > 0) {
			Iterator it = campos.iterator();
			String lab_ant = "";
			while (it.hasNext()) {
				CamposAdicionalesCampania obj = (CamposAdicionalesCampania) it.next();
				if(!lab_ant.equals(obj.getDefCampo().getLabel()))
					resp.add(new CamposAdicionalesCampaniaTO(obj));
				lab_ant=obj.getDefCampo().getLabel();
			}
		}
		return resp;
	}

	public void guardarCampoAdicionalCampania(CamposAdicionalesTO ca, int idCampania) {
		CamposAdicionalesDAO dao = new CamposAdicionalesDAO();
		CampaniaDAO camp = new CampaniaDAO();
		dao.beginTransaction();
		CamposAdicionales campo = dao.getCamposAdicionalesById(ca.getId());
		CamposAdicionalesCampania nuevo = new CamposAdicionalesCampania();
		nuevo.setDefCampo(campo);
		nuevo.setCampania(camp.findCampaniaById(idCampania));
		dao.save(nuevo);
		dao.commitTransaction();
		dao.cerrarConexion();
	}

	public void deleteCampoAdicionalCampaniaById(int id) {
		CamposAdicionalesDAO dao = new CamposAdicionalesDAO();
		dao.beginTransaction();
		CamposAdicionalesCampania c = dao.getCamposAdicionalesCampaniaById(id);
		dao.remove(c);
		dao.commitTransaction();
		dao.cerrarConexion();
	}

	public void guardaCampoAdicionalesCliente(List<CamposAdicionalesCampaniaTO> camposCampania, int idCamp, int idCliente) {
		CamposAdicionalesDAO dao = new CamposAdicionalesDAO();
		ClienteDAO cDAO = new ClienteDAO();
		Iterator it = camposCampania.iterator();
		dao.beginTransaction();

		while (it.hasNext()) {
			CamposAdicionalesCampaniaTO tmp = (CamposAdicionalesCampaniaTO) it.next();
			CamposAdicionalesCliente nuevo = new CamposAdicionalesCliente();
			CamposAdicionalesCampania camp = dao.getCamposAdicionalesCampaniaById(tmp.getId());
			nuevo.setValor(tmp.getValorCliente());
			nuevo.setCampoCampania(camp);
			nuevo.setCliente(cDAO.findClienteById(idCliente));
			dao.save(nuevo);
		}
		dao.commitTransaction();
		dao.cerrarConexion();

	}

	public List<TipoCampaniaTO> findAllTipoCampaniasByEmpresa(int empresaId) {
		TipoCampaniaDAO dao = new TipoCampaniaDAO();
		List<TipoCampania> tipoCampania = dao.findAllTipoCampaniasByEmpresa(empresaId);
		logger.info("..........................." + tipoCampania);
		List<TipoCampaniaTO> tipoCampaniaTO = new LinkedList<TipoCampaniaTO>();
		if (tipoCampania != null && tipoCampania.size() > 0) {
			Iterator<TipoCampania> i = tipoCampania.iterator();
			while (i.hasNext()) {
				TipoCampania t1 = i.next();
				TipoCampaniaTO t2 = new TipoCampaniaTO();
				t2.setId(t1.getId());
				t2.setNombre(t1.getNombre());
				t2.setUrl(t1.getUrl());
				t2.setEmpresa(new EmpresaTO(t1.getEmpresa()));
				tipoCampaniaTO.add(t2);
			}
		}
		return tipoCampaniaTO;

	}

	public List<ParametroTO> getParametroMail(int empresaId) {
		ParametroDAO dao = new ParametroDAO();
		List<Parametro> parametros = dao.getParaemtroByTypeAndEmpresa(empresaId, "smtp.%");
		// List<Parametro> parametros = dao.getAllParaemtroByEmpresa(empresaId);
		List<ParametroTO> parametrosTO = new LinkedList<ParametroTO>();
		Empresa e = null;
		if (parametros != null && parametros.size() > 0) {
			Iterator<Parametro> i = parametros.iterator();
			while (i.hasNext()) {
				Parametro p = i.next();
				if (e == null) {
					e = p.getEmpresa();
				}
				ParametroTO pTO = new ParametroTO(p, e);
				parametrosTO.add(pTO);
			}
		}
		return parametrosTO;
	}
	public ParametroTO getSingleParaemtroByTypeAndEmpresa(int idEmpresa, String param) {
		ParametroDAO dao = new ParametroDAO();
		Parametro p = dao.getSingleParaemtroByTypeAndEmpresa(idEmpresa, param);
		return new ParametroTO(p);
	}
	public List<ParametroTO> getParametroAlerta(int empresaId) {
		ParametroDAO dao = new ParametroDAO();
		List<Parametro> parametros = dao.getParaemtroByTypeAndEmpresa(empresaId, "alert.%");
		// List<Parametro> parametros = dao.getAllParaemtroByEmpresa(empresaId);
		List<ParametroTO> parametrosTO = new LinkedList<ParametroTO>();
		Empresa e = null;
		if (parametros != null && parametros.size() > 0) {
			Iterator<Parametro> i = parametros.iterator();
			while (i.hasNext()) {
				Parametro p = i.next();
				if (e == null) {
					e = p.getEmpresa();
				}
				ParametroTO pTO = new ParametroTO(p, e);
				parametrosTO.add(pTO);
			}
		}
		return parametrosTO;
	}
	
	public List<ParametroTO> getParaemtroByGrupoAndEmpresa(int empresaId, String parametro)
	{
		
		ParametroDAO dao = new ParametroDAO();
		List<Parametro> parametros = dao.getParaemtroByGrupoAndEmpresa(empresaId, parametro);
		// List<Parametro> parametros = dao.getAllParaemtroByEmpresa(empresaId);
		List<ParametroTO> parametrosTO = new LinkedList<ParametroTO>();
		Empresa e = null;
		if (parametros != null && parametros.size() > 0) {
			Iterator<Parametro> i = parametros.iterator();
			while (i.hasNext()) {
				Parametro p = i.next();
				if (e == null) {
					e = p.getEmpresa();
				}
				ParametroTO pTO = new ParametroTO(p, e);
				parametrosTO.add(pTO);
			}
		}
		return parametrosTO;
	}

	
	public void validacionCampaniaActivaByTipo(int empresaId, List<MensajeTO> mensajes) {
		CampaniaDAO dao = new CampaniaDAO();
		List<Object[]> objects = dao.validacionCampaniaActivaByTipo(empresaId);
		MensajeTO mensaje = null;
		if(objects != null && objects.size() > 0)
		{
			Iterator<Object[]> i = objects.iterator();
			while(i.hasNext())
			{
				Object[] obj = i.next();
				Long cantCampanias = (Long)obj[3];
				if(cantCampanias < 1)
				{
					mensaje = new MensajeTO("Campa�as Activas","No existe campa�a activa del tipo '"+ obj[0] +"'", MensajeTO.ERROR);
					mensajes.add(mensaje);
				}
			}
		}
	}

	public List<MensajeTO> validacion(int empresaId)
	{
		List<MensajeTO> mensajes = new LinkedList<MensajeTO>();
		CampaniaDAO dao = new CampaniaDAO();
		this.validacionCampaniaActivaByTipo(empresaId, mensajes);
		dao.vencimientoCampaniaByTipo(empresaId, mensajes);
		this.checkCuponesDisponiblesCampania(empresaId, mensajes);
		return mensajes;
		
	}
	
	public boolean problemas(int empresaId)
	{
		List<MensajeTO> mensajes = new LinkedList<MensajeTO>();
		CampaniaDAO dao = new CampaniaDAO();
		this.validacionCampaniaActivaByTipo(empresaId, mensajes);
		dao.vencimientoCampaniaByTipo(empresaId, mensajes);
		this.checkCuponesDisponiblesCampania(empresaId, mensajes);
		return (mensajes.size() > 0) ? true:false;
	}
	
	public List<EmpresaTO> getAllEmpresaActiva()
	{
		EmpresaDAO dao = new EmpresaDAO();
		List<Empresa> empresas = dao.getAllEmpresaActiva();
		List<EmpresaTO> empresasTO = new LinkedList<EmpresaTO>();
		if(empresas != null && empresas.size() > 0)
		{
			Iterator<Empresa>i = empresas.iterator();
			while(i.hasNext())
			{
				empresasTO.add(new EmpresaTO(i.next()));
			}
		}
		return empresasTO;
	}
	
	public MensajeTO updateParametro(ParametroTO to)
	{
		ParametroDAO dao = new ParametroDAO();
		return dao.updateParametro(to);
	}
	
	public MensajeTO SendMailStart(int empresaId)
	{
		logger.info("starting SendMail");
		List<ParametroTO> parametros = getParaemtroByGrupoAndEmpresa(empresaId, "app");
		ParametroTO enviando = ParametroTO.getParametro(parametros, "app.sending");
		enviando.setValor("Running");
		return this.updateParametro(enviando);
	}
	
	public MensajeTO SendMailStop(int empresaId)
	{
		logger.info("stoping SendMail");
		List<ParametroTO> parametros = getParaemtroByGrupoAndEmpresa(empresaId, "app");
		ParametroTO enviando = ParametroTO.getParametro(parametros, "app.sending");
		enviando.setValor("Stop");
		return this.updateParametro(enviando);
	}

	public List<ResumenExperienciaTO> getInfoResumenExperiencia(int idTienda, String desde, String hasta) {
		ReportesDAO dao=new ReportesDAO();
		return dao.getInfoResumenExperiencia(idTienda, desde, hasta);
	}

	public  List<GenericInfoTO>  getExperienciaResumen(String desde, String hasta, int id_usuario) {
		ReportesDAO dao=new ReportesDAO();
		return dao.getInfoResumen(desde, hasta, id_usuario, "getExperienciaResumen");
		
	}
	public  List<GenericInfoTO>  getCantRegistrosTiendas(String desde, String hasta, int id_usuario) {
		ReportesDAO dao=new ReportesDAO();
		return dao.getInfoResumen(desde, hasta, id_usuario, "getCantRegistrosTiendas");
		
	}
	public  List<GenericInfoTO>  getCantRegistrosTiendasVendedor(String desde, String hasta, int tienda, int id_usuario) {
		ReportesDAO dao=new ReportesDAO();
		return dao.getInfoResumen(desde, hasta,tienda,id_usuario, "getCantRegistrosTiendasVendedor");
		
	}
	public  List<GenericInfoTO>  getCantCategoriaPorGenero(String desde, String hasta,int tienda, int id_usuario) {
		ReportesDAO dao=new ReportesDAO();
		return dao.getInfoResumen(desde, hasta,tienda,id_usuario,"getCantCategoriaPorGenero");
		
	}

	public int getCantCuponesLibres(int empresaId, int idCamp) {
		CuponDAO c=new CuponDAO();
		return c.getCuponesDisponible(empresaId, idCamp);
	}

	public boolean unsuscribe(int id) {
		try {
		ClienteDAO c = new ClienteDAO();
		c.beginTransaction();
		Cliente cli = c.findClienteById(id);
		cli.setUnsuscribe(1);
		cli.setFecha_unsuscribe(new Date());
		c.commitTransaction();
		return true;
		}catch(Exception e ) {
			return false;
		}
		
	}

	public void buscarEstadosMailGun() {
		logger.info("======= > buscarEstadosMailGun");
		MailgunApi t = new MailgunApi();
		MailEstadoDAO dao = new MailEstadoDAO();
		try {
			List<MailEstadoTO> estados = t.callEvents("https://api.mailgun.net/v3/" + MailgunApi.YOUR_DOMAIN_NAME + "/events",new LinkedList<MailEstadoTO>());
			dao.guardarEstados(estados);
			if(estados!=null)
				logger.info("Cantidad 2 : "+estados.size());
			
			List<MailEstadoTO> estados1 = t.callBounces("https://api.mailgun.net/v3/" + MailgunApi.YOUR_DOMAIN_NAME + "/bounces" , new LinkedList<MailEstadoTO>());
			dao.guardarEstados(estados1);
			if(estados1!=null)
				logger.info("Cantidad: "+estados1.size());
			
			dao.actualizarEstados();
			System.out.println("fin!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<EmpresaTO> findAllEmpresas(){
		EmpresaDAO dao = new EmpresaDAO();
		List<Empresa> empresas = dao.findAllEmpresas();
		List<EmpresaTO> empresasTO = new LinkedList<EmpresaTO>();
		if(empresas != null && empresas.size() > 0)
		{
			Iterator<Empresa> i = empresas.iterator();
			while(i.hasNext())
			{
				Empresa e = i.next();
				EmpresaTO eTO = new EmpresaTO(e);
				empresasTO.add(eTO);
			}
		}
		return empresasTO;
	}
	
	public EmpresaTO addEmpresa(EmpresaTO nuevaEmpresa)
	{
		EmpresaDAO dao = new EmpresaDAO();
		Empresa e = dao.addEmpresa(new Empresa(nuevaEmpresa));
		nuevaEmpresa.setId(e.getId());
		return nuevaEmpresa;
	}
	
	public void addParametros(List<ParametroTO> parametros, int e) {
		Parametro p ;
//		ParametroDAO dao = new ParametroDAO();
//		dao.beginTransaction();
//		Iterator<ParametroTO> i = parametros.iterator();
//		while(i.hasNext())
//		{
//			p = new Parametro(i.next());
//			dao.addParametro(p);
//		}
//		
//		dao.commitTransaction();
//		dao.cerrarConexion();
		EmpresaDAO dao = new EmpresaDAO();
		Empresa emp = dao.findEmpresaById(e);
		List<Parametro> parametross = new LinkedList<Parametro>();
		emp.setParametros(parametross);
		dao.beginTransaction();
		Iterator<ParametroTO> i = parametros.iterator();
		while(i.hasNext())
		{
			p = new Parametro(i.next());
			dao.addParametros(p, emp);
		}
		
		dao.commitTransaction();
		dao.cerrarConexion();
		
	}
	
	public void addTipoCampania(String nombre, int empresaId){
		TipoCampaniaDAO dao = new TipoCampaniaDAO();
		dao.addTipoCampaniaByEmpresa(nombre, empresaId);
	}
	
	public CountCuotaTO getCountCuotaByEmpresa(int empresaId)
	{
		try {
			CountCuotaDAO dao = new CountCuotaDAO();
			CountCuota cc = dao.getCountCuotaByEmpresa(empresaId);
			if(cc != null) {
				CountCuotaTO to = new CountCuotaTO(cc);
				return to;
			}else {
				return null;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public MensajeTO addBaseCountCuota(int empresaId)
	{
		CountCuota cc = new CountCuota();
		cc.setCountCuota(0);
		cc.setCuota(0);
		cc.setEmpresaId(empresaId);
		cc.setEstado("INACTIVO");
		CountCuotaDAO dao = new CountCuotaDAO();
		return dao.addCountCuota(cc);
	}
	
	public MensajeTO addBaseAdmCuota(int empresaId, int cantidad)
	{
		AdmCuota ac = new AdmCuota();
		ac.setIdEmpresa(empresaId);
		ac.setCantidad(cantidad);
		ac.setFecha(new Date());
		int vigencia = Integer.parseInt(this.getParametroByNombre("tagme.app.vencimientoCuota").getValor());
		logger.info("Dias de vigencia =======> " + vigencia);
		ac.setVigencia(vigencia);
		AdmCuotaDAO dao = new AdmCuotaDAO();
		return dao.addAdmCuota(ac);
	}
}
