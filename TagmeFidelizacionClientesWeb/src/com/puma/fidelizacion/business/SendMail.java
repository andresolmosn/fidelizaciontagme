package com.puma.fidelizacion.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.net.ssl.SSLContext;

import java.io.File;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


import com.puma.fidelizacion.bean.LoginBean;
import com.puma.fidelizacion.to.ParametroTO;
import com.sun.mail.smtp.SMTPTransport;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class SendMail implements Serializable {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(SendMail.class);
	
	private String host;
	private String port;
	private String userMail;
	private String passwd;
	private String from;
	
	public SendMail(int empresaId)
	{
		Controller c = new Controller();
		List<ParametroTO> parametros = c.getParametroMail(empresaId);
		this.host = ParametroTO.getParametro(parametros, "smtp.host").getValor();
		this.port = ParametroTO.getParametro(parametros, "smtp.port").getValor();
		this.userMail = ParametroTO.getParametro(parametros, "smtp.user").getValor();
		this.passwd = ParametroTO.getParametro(parametros, "smtp.passwd").getValor();
		this.from = ParametroTO.getParametro(parametros, "smtp.from").getValor();
		
		logger.info(host + port + userMail + passwd);
	}
	
	private static SendMail sendMail ;

	public static SendMail getInstance(int empresaId) {
		if(sendMail==null) {
			sendMail= new SendMail(empresaId);
			logger.info(sendMail);
		}
		return sendMail;	
	}
	
//	public void enviar(String to) {
//		
//		// TODO Auto-generated method stub
//				URL url = null;
//				try {
////					url = new URL("http://tagmeservices.com/TagmeFidelizacion/plantillaMail.xhtml");
//					url = new URL("http://localhost:8080/PumaFidelizacion/plantillaMail.xhtml");
////					url = new URL("http://view.corp.mundohomy.com/?qs=1a5e39314c5580bbf34017520187bd35448d495742805c40341c620a2b52916f849479c55ae083e5f9394cde0e63e72cc4f507a948140aec1875f6f9be4c8c2992436c6ddcb28acfa6b3c2f0b48982b0");
//
//					java.net.URLConnection conn;
//
//					conn = url.openConnection();
//
//					InputStream is = conn.getInputStream();
//					BufferedReader in = new BufferedReader(new InputStreamReader(is));
//					String resultado = "";
//					String val = "";
//					while ((val = in.readLine()) != null) {
////						if(val.contains("/PumaFidelizacion/javax.faces.resource/dynamiccontent")) {
//////							val = val.replace("/PumaFidelizacion/javax.faces.resource/dynamiccontent", "localhost:8080/PumaFidelizacion/javax.faces.resource/dynamiccontent");
////							logger.info("URL CAMBIADA");
////							logger.info(val);
////							
////						}
//						resultado += val.trim() + " ";
//					}
//					in.close();
//					Properties props = new Properties();
//					props.put("mail.smtp.auth", "true");
//					props.put("mail.smtp.starttls.enable", "true");
//					props.put("mail.smtp.host", "smtp.gmail.com");
//					props.put("mail.smtp.port", "587");
//					Session session = Session.getInstance(props, new javax.mail.Authenticator() {
//						protected PasswordAuthentication getPasswordAuthentication() {
//							return new PasswordAuthentication("tagmecl@gmail.com", "Nunoa2014");
//						}
//					});
//
//					Message message = new MimeMessage(session);
//					message.setFrom(new InternetAddress("tagmecl@gmail.com"));
//					message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
//					message.setSubject("Inscripcion Dev");
//					MimeMultipart multipart = new MimeMultipart("alternative");
//
//					BodyPart messageBodyPart = new MimeBodyPart();
//					messageBodyPart.setContent(resultado, "text/html");
//					multipart.addBodyPart(messageBodyPart);
//					
//
//					message.setContent(multipart,"text/html");
//
//					Transport.send(message);
//
//					System.out.println("Enviado");
//
//				} catch (IOException e) {
//					e.printStackTrace();
//				} catch (AddressException e) {
//					e.printStackTrace();
//				} catch (MessagingException e) {
//					e.printStackTrace();
//				}
//				
//	}
	
	public boolean sendMail(String to, String asunto,String from,String hash, URL url ) {
//		String resultado = "";
//		logger.info("envioByMailGun:: "+hash);
//		URL url = null;
//		try {
//			url = new URL("http://www.tagmeservices.com/PumaFidelizacion/plantillaMail.xhtml");
////			url = new URL("http://localhost:8080/PumaFidelizacion/cupon.xhtml");
//			java.net.URLConnection conn;
//			conn = url.openConnection();
//			InputStream is = conn.getInputStream();
//			BufferedReader in = new BufferedReader(new InputStreamReader(is));
//			String val = "";
//			while ((val = in.readLine()) != null) {
//				resultado += val + "\n";
//			}
//			in.close();
//		}catch(Exception e ) {
//			
//		}
//		Properties props = System.getProperties();
//        props.put("mail.smtps.host", "smtp.mailgun.org");
//        props.put("mail.smtps.auth", "true");
//
//        Session session = Session.getInstance(props, null);
//        Message msg = new MimeMessage(session);
//        try {
//			msg.setFrom(new InternetAddress(from));
//		
//
//        InternetAddress[] addrs = InternetAddress.parse(to, false);
//        msg.setRecipients(Message.RecipientType.TO, addrs);
//
//        
//       
//        msg.setSubject(asunto);
//		MimeMultipart multipart = new MimeMultipart("alternative");
//		resultado="<b>hola</b>";
//		BodyPart messageBodyPart = new MimeBodyPart();
//		messageBodyPart.setContent(resultado, "text/html");
//		multipart.addBodyPart(messageBodyPart);
//		msg.setContent(multipart,"text/html");
//        msg.setSentDate(new Date());
//
//        SMTPTransport t =
//            (SMTPTransport) session.getTransport("smtps");
//        t.connect("smtp.mailgun.com", "postmaster@tagmeservices.com", "56aa9c0111198bcf2aca03c06e0871e5-770f03c4-2a214e69");
//        t.sendMessage(msg, msg.getAllRecipients());
//
//        System.out.println("Response: " + t.getLastServerResponse());
//
//        t.close();
//        } catch (AddressException e) {
//			e.printStackTrace();
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		}
		
		String html = htmlToString(url);
		
		try {

	     Properties props = System.getProperties();
	        props.put("mail.smtps.host", host);
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtps.port", "2525");
	        Session session = Session.getInstance(props, null);
	        Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(from));

	        InternetAddress[] addrs = InternetAddress.parse(to, false);
	        msg.setRecipients(Message.RecipientType.TO, addrs);

	        BodyPart mensageBP=new MimeBodyPart();
	        mensageBP.setContent(html, "text/html");
	        Multipart mPart= new MimeMultipart();
	        mPart.addBodyPart(mensageBP);
	        
	        msg.setSubject(asunto);
	        msg.setContent(mPart);
	        msg.setSentDate(new Date());
	     
	        SMTPTransport t =
	            (SMTPTransport) session.getTransport("smtp");
	        t.connect(host, Integer.parseInt(this.port), this.userMail, this.passwd);
	        t.sendMessage(msg, msg.getAllRecipients());

	        System.out.println("Response: " + t.getLastServerResponse());

	        t.close();
	        return true;
		 } catch (AddressException e) {
				e.printStackTrace();
				return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public String htmlToString(URL url)
	{
    	String resultado = "";
    	try {
			java.net.URLConnection conn;

			conn = url.openConnection();

			InputStream is = conn.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(is));
			String val = "";
			while ((val = in.readLine()) != null) {

				resultado += val + "\n";
			}
			in.close();
		}catch(Exception e) {
			return null;
		}
		return resultado;
	}
	
	public boolean sendMailTag(String to, String asunto,String from,String hash, URL url, String tag ) {
//		String resultado = "";
//		logger.info("envioByMailGun:: "+hash);
//		URL url = null;
//		try {
//			url = new URL("http://www.tagmeservices.com/PumaFidelizacion/plantillaMail.xhtml");
////			url = new URL("http://localhost:8080/PumaFidelizacion/cupon.xhtml");
//			java.net.URLConnection conn;
//			conn = url.openConnection();
//			InputStream is = conn.getInputStream();
//			BufferedReader in = new BufferedReader(new InputStreamReader(is));
//			String val = "";
//			while ((val = in.readLine()) != null) {
//				resultado += val + "\n";
//			}
//			in.close();
//		}catch(Exception e ) {
//			
//		}
//		Properties props = System.getProperties();
//        props.put("mail.smtps.host", "smtp.mailgun.org");
//        props.put("mail.smtps.auth", "true");
//
//        Session session = Session.getInstance(props, null);
//        Message msg = new MimeMessage(session);
//        try {
//			msg.setFrom(new InternetAddress(from));
//		
//
//        InternetAddress[] addrs = InternetAddress.parse(to, false);
//        msg.setRecipients(Message.RecipientType.TO, addrs);
//
//        
//       
//        msg.setSubject(asunto);
//		MimeMultipart multipart = new MimeMultipart("alternative");
//		resultado="<b>hola</b>";
//		BodyPart messageBodyPart = new MimeBodyPart();
//		messageBodyPart.setContent(resultado, "text/html");
//		multipart.addBodyPart(messageBodyPart);
//		msg.setContent(multipart,"text/html");
//        msg.setSentDate(new Date());
//
//        SMTPTransport t =
//            (SMTPTransport) session.getTransport("smtps");
//        t.connect("smtp.mailgun.com", "postmaster@tagmeservices.com", "56aa9c0111198bcf2aca03c06e0871e5-770f03c4-2a214e69");
//        t.sendMessage(msg, msg.getAllRecipients());
//
//        System.out.println("Response: " + t.getLastServerResponse());
//
//        t.close();
//        } catch (AddressException e) {
//			e.printStackTrace();
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		}
		
		String html = htmlToString(url);
		
		try {
		logger.info("SETEANDO PROPS");
	     Properties props = System.getProperties();
	     props.put("mail.smtps.host", host);
	        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        props.put("mail.smtps.auth", "true");
	        props.put("mail.smtps.port", "2525");

	        Session session = Session.getInstance(props, null);
	        Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(from));

	        InternetAddress[] addrs = InternetAddress.parse(to, false);
	        msg.setRecipients(Message.RecipientType.TO, addrs);

	        BodyPart mensageBP=new MimeBodyPart();
	        mensageBP.setContent(html, "text/html");
	        Multipart mPart= new MimeMultipart();
	        mPart.addBodyPart(mensageBP);
	    
	       
	        msg.setSubject(asunto);
	        msg.setContent(mPart);
	        msg.setSentDate(new Date());
	        msg.addHeader("X-Mailgun-Tag", tag);
	        
	        Flags flags = new Flags();
	        msg.setFlags(flags, true);
	 
	        SMTPTransport t =
	            (SMTPTransport) session.getTransport("smtp");
	        
	        t.connect(host, Integer.parseInt(this.port), this.userMail, this.passwd);
	                t.sendMessage(msg, msg.getAllRecipients());

	        System.out.println("Response: " + t.getLastServerResponse());

	        t.close();
	        return true;
		 } catch (AddressException e) {
				e.printStackTrace();
				return false;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	

//	public JsonNode sendSimpleMessage(String from, String to, String subject, String dominio, String apiKey) throws UnirestException {
//	    
//	    	URL url = null;
//	    	String resultado = "";
//	    	try {
////				url = new URL("http://tagmeservices.com/TagmeFidelizacion/plantillaMail.xhtml");
////				url = new URL("http://localhost:8080/PumaFidelizacion/cupon.xhtml");
////				url = new URL("http://view.corp.mundohomy.com/?qs=1a5e39314c5580bbf34017520187bd35448d495742805c40341c620a2b52916f849479c55ae083e5f9394cde0e63e72cc4f507a948140aec1875f6f9be4c8c2992436c6ddcb28acfa6b3c2f0b48982b0");
//				url = new URL("http://localhost:8080/PumaFidelizacion/testSuccess.html");
//				java.net.URLConnection conn;
//
//				conn = url.openConnection();
//
//				InputStream is = conn.getInputStream();
//				BufferedReader in = new BufferedReader(new InputStreamReader(is));
//				String val = "";
//				while ((val = in.readLine()) != null) {
//
//					resultado += val + "\n";
//				}
//				in.close();
//			}catch(Exception e) {
//				
//			}
//	    	try {
//	    		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
//	    			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//	    				return null;
//	    			}
//	    			public void checkClientTrusted(X509Certificate[] certs, String authType) {
//	    			}
//	    			public void checkServerTrusted(X509Certificate[] certs, String authType) {
//	    			}
//
//	    		} };
//	    		logger.info("=================");
//				logger.info("\n\n  "+resultado.length()+"  \n\\n");
//				logger.info("=================");
//	    		SSLContext sslcontext = SSLContext.getInstance("SSL");
//	    		sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());
//	    		HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
//	    		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext);
//	    		CloseableHttpClient httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
//	    		Unirest.setHttpClient(httpclient);
//	    		
//	    		HttpResponse<JsonNode> request = Unirest.post("https://api.mailgun.net/v3/" + dominio + "/messages")
//                        .basicAuth("api", apiKey)
//                    .queryString("from", from)
//                    .queryString("to", to)
//                    .queryString("subject", subject)      
//                    .queryString("html", resultado.substring(0, resultado.length()/2))
//                    .queryString("html", resultado.substring( resultado.length()/2,resultado.length()))
//                    .field("o:tag", "test-tag")
//                    .asJson();
//	    		Unirest.shutdown();
//	    		return request.getBody();
//
//	    	} catch (Exception e) {
//	    		e.printStackTrace();
//	    	}
//			
//	    	
//	        return null;
//	    		
//	           
//	    }
	    
	}
